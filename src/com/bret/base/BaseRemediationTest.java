package com.bret.base;

import org.junit.Assert;

import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;

public class BaseRemediationTest extends BaseTest {

	/* Constant for row not found in the table */
	protected static final int ROW_NOT_FOUND = 0;

	/* Data Source Keys */
	protected static final String BID_ID = "bidId";
	protected static final String RELEASED_BY = "releasedBy";
	protected static final String RELEASED_DATE = "releasedDate";
	protected static final String BID_DATE = "bidDate";
	protected static final String OUTCOME = "outcome";
	protected static final String OUTCOME_COMMENT = "outcomeComment";
	protected static final String REVIEWER_DECISION = "reviewerDecision";
	protected static final String REVIEWER_NAME = "reviewerName";

	protected void adminCheckAndReleaseBid(String bidId, String tcNumber) {

		/*
		 * BRETLoginPage object contains the userLogin method for logging in to the Bret
		 * Web application.
		 */
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		MyTaskPage myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		printToConsole("Verify that the 'Release' button is present for 'Admin' user");

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isReleaseVisible = myTaskPage.checkReleaseButtonVisibility(driver);
		logExpectedActualText("Release Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isReleaseVisible));
		Assert.assertTrue(isReleaseVisible);

		/* Now lets search for the newly loaded bid */
		int myTaskRowIndex = myTaskPage.searchRowIndex(driver, bidId);

		/* Checking if the newly loaded bid is found */
		Assert.assertTrue(myTaskRowIndex > ROW_NOT_FOUND);

		/* Now lets search for the newly loaded bid */
		myTaskPage.clickCheckbox(myTaskRowIndex);

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());

		/* Click the release button */
		if(isReleaseVisible) {
			myTaskPage.clickReleaseButton(driver);
			printToConsole("Release Button is clicked");
		} else {
			printToConsole("Release Cannot be clicked. Button is not visible.");
		}

		logOutSummaryPage(driver);
	}

	/**
	 * @param tcNumber
	 * @param bidId
	 * @param expectedReleasedBy
	 * @param expectedReleasedOnDate
	 */
	protected void adminCheckArchivedBidDisplay(String tcNumber, String bidId, String expectedReleasedBy,
			String expectedReleasedOnDate) {

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		MyTaskPage myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		ArchivePage archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		/* Now lets search for the newly archived bid */
		int archiveBidRowIndex = archivePage.searchRowIndex(driver, bidId);
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		/* Checking if the newly loaded bid is found */
		Assert.assertTrue(releasedBidIsFound);

		archivePage.clickCheckbox(archiveBidRowIndex);

		String actualReleasedBy = archivePage.getValueAtIndex(driver, archiveBidRowIndex,
				ArchivePage.BID_RELEASED_BY_COL);
		String actualReleasedOnDate = archivePage.getValueAtIndex(driver, archiveBidRowIndex,
				ArchivePage.BID_RELEASED_ON_COL);

		logExpectedActualText("Released By: ", expectedReleasedBy, actualReleasedBy);
		logExpectedActualText("Released On Date: ", expectedReleasedOnDate, actualReleasedOnDate);

		Assert.assertEquals(expectedReleasedBy, actualReleasedBy);
		Assert.assertEquals(expectedReleasedOnDate, actualReleasedOnDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel(), this.getClass());
		
		logOutSummaryPage(driver);
		pausePage();
	}
	
	/**
	 * Login as a reviewer and navigate to focus tab.
	 */
	protected void reviewerLoginNavigateFocusTab() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);

		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());
		logNavigation(false, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		System.out.println();

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
	}
	
	/**
	 * Checks if the bid exists based on the row and logs the process.
	 * 
	 * @param rowIndex
	 * @param logText
	 */
	protected void checkAndLogBidIfExist(int rowIndex, String logText) {
		boolean bidIsFound = rowIndex > ROW_NOT_FOUND;
		logExpectedActualText(logText, Boolean.TRUE.toString(), Boolean.toString(bidIsFound));
		/* Checking if the bid is found */
		Assert.assertTrue(bidIsFound);
	}
	
	/**
	 * Logs detail navigation.
	 */
	protected void logNavigateToDetail() {
		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		System.out.println();
	}
	
	/**
	 * Gets the Remediation table column values
	 * 
	 * @param rowIndex
	 * @return
	 */
	protected String getRemediationSubTabValue(RemediationLogPage remediationPage, int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, rowIndex);
	}
	
	/**
	 * 
	 * @param bidId
	 * @param bidDate
	 * @param reviewerDecision
	 * @param reviewerName
	 * @param tcNumber
	 */
	protected void checkFocusBidDateAndReviewerFields(String bidId, String bidDate, String reviewerDecision,
			String reviewerName, String tcNumber) {
		
		pausePage();
		reviewerLoginNavigateFocusTab();

		FocusPage focusPage = new FocusPage();
		pausePage();
		pausePage();
		pausePage();
		pausePage();
		pausePage();

		int returnedBidIndex = focusPage.searchRowIndex(driver, bidId);

		printToConsole("Checking if the Bid exists: " + bidId);
		checkAndLogBidIfExist(returnedBidIndex, "Bid is found");

		focusPage.clickCheckbox(returnedBidIndex);

		String actualBidDate = focusPage.getValueAtIndex(driver, returnedBidIndex, FocusPage.BID_DATE_COL);
		String actualReviewerDecision = focusPage.getValueAtIndex(driver, returnedBidIndex,
				FocusPage.REVIEWER_DECISION_COL);
		String actualReviewerName = focusPage.getValueAtIndex(driver, returnedBidIndex, FocusPage.REVIEWER_COL);

		String expectedBidDate = bidDate;
		String expectedReviewerDecision = reviewerDecision;
		String expectedReviewerName = reviewerName;

		logExpectedActualText("Bid Date: ", expectedBidDate, actualBidDate);
		logExpectedActualText("Reviewer Decision: ", expectedReviewerDecision, actualReviewerDecision);
		logExpectedActualText("Reviewer Name: ", expectedReviewerName, actualReviewerName);

		Assert.assertEquals(expectedBidDate, actualBidDate);
		Assert.assertEquals(expectedReviewerDecision, actualReviewerDecision);
		Assert.assertEquals(expectedReviewerName, actualReviewerName);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());

		logOutSummaryPage(driver);
	}
	
	/**
		 * Method for searching assigned bid in the focus tab and then this will
	 * remediate the selected assigned bid.
	 * 
	 * @param assignedBidId
	 *            - bid to be searched in the focus page.
	 * @param tcNumber
	 *            - test case number.
	 * @param remediationOutcome
	 *            - remediation outcome based on the test case.
	 * @param remediationOutcomeComments
	 *            - remediation outcome comments that should be fetched from the
	 *            datasource.
	 * @param preshipmentVerification - additional parameter for Preshipment Verification setting
	 */
	protected void searchFocusAssignedBidAndRemediate(String assignedBidId, String tcNumber, String remediationOutcome,
			String remediationOutcomeComments, String preshipmentVerification) {
		assignedBidRemediation(assignedBidId, tcNumber, remediationOutcome, remediationOutcomeComments, preshipmentVerification);
	}
	
	/**
	 * Method for searching assigned bid in the focus tab and then this will
	 * remediate the selected assigned bid.
	 * 
	 * @param assignedBidId
	 *            - bid to be searched in the focus page.
	 * @param tcNumber
	 *            - test case number.
	 * @param remediationOutcome
	 *            - remediation outcome based on the test case.
	 * @param remediationOutcomeComments
	 *            - remediation outcome comments that should be fetched from the
	 *            datasource.
	 */
	protected void searchFocusAssignedBidAndRemediate(String assignedBidId, String tcNumber, String remediationOutcome,
			String remediationOutcomeComments) {
		assignedBidRemediation(assignedBidId, tcNumber, remediationOutcome, remediationOutcomeComments, null);

	}
	
	private void assignedBidRemediation(String assignedBidId, String tcNumber, String remediationOutcome,
			String remediationOutcomeComments, String preshipmentVerification) {
		reviewerLoginNavigateFocusTab();

		FocusPage focusPage = new FocusPage();
		pausePage();
		pausePage();
		pausePage();
		
		int assignedBidIndex = focusPage.searchRowIndex(driver, assignedBidId);
		
		printToConsole("Checking if the Assigned Bid exists: " + assignedBidId);
		checkAndLogBidIfExist(assignedBidIndex, "Assigned Bid is found");

		focusPage.clickCheckbox(assignedBidIndex);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());

		logNavigateToDetail();
		pausePage();
		focusPage.clickDetailLink(driver, assignedBidIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();
		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		System.out.println();
		logNavigation(true, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		pausePage();
		RemediationLogPage remediationPage = new RemediationLogPage(driver);
		pausePage();
		remediationPage.clickRemediationEdit();

		pausePage();

		remediationPage.selectOutcome(remediationOutcome);
		remediationPage.inputOutcomeComment(remediationOutcomeComments);
		
		if(null != preshipmentVerification) {
			remediationPage.selectPreshipVerification(preshipmentVerification);
		}

		pausePage();
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());
		remediationPage.clickSaveButton();
		pausePage();
		setupNewURL(driver.getCurrentUrl());

		logOutDetailsPage(driver);
		
		pausePage();
	}
	
	/**
	 * Method for taking on a new focus bid.
	 * 
	 * @param tcNumber
	 * @param newlyLoadedBidId
	 * @param expectedOutcome
	 */
	protected void takeOnNewFocusBid(String tcNumber, String newlyLoadedBidId, String expectedOutcome) {

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer is able to TAKE ON a NEW bid with Outcome = Assigned (Under Remediation Log).");

		reviewerLoginNavigateFocusTab();

		FocusPage focusPage = new FocusPage(driver);
		printToConsole("Checking if the Newly loaded bid exists: " + newlyLoadedBidId);
		int newFocusBidIndex = focusPage.searchRowIndex(driver, newlyLoadedBidId);
		checkAndLogBidIfExist(newFocusBidIndex, "Newly loaded bid  is found");
		focusPage.clickCheckbox(newFocusBidIndex);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());
		pausePage();

		logNavigateToDetail();
		focusPage.clickDetailLink(driver, newFocusBidIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage();
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		flaggingBidPage.clickTakeThisBid();

		// Refresh page
		driver.navigate().refresh();

		System.out.println();
		logNavigation(true, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		RemediationLogPage remediationPage = new RemediationLogPage(driver);

		String actualOutcome = getRemediationSubTabValue(remediationPage, RemediationLogPage.ROW_REM_OUTCOME);

		logExpectedActualText("Outcome: ", expectedOutcome, actualOutcome);

		Assert.assertEquals(expectedOutcome, actualOutcome);

		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());

		logOutDetailsPage(driver);
	}


}
