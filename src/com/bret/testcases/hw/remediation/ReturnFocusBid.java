package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class ReturnFocusBid extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 4, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 4, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 4, 1);   
	 String outcomeStat = Excel.getCellValue (xlpath, sheet2, 4, 2);   
	 String outcomeCom = Excel.getCellValue (xlpath, sheet2, 4, 3);   
	 
	 String brand = "HW"; //[updated-Belle-9/14/2017]  
	
   @Test
	public void tc_RHW_007 () throws Exception {
		 //------------------------------------------------------- 
		 // Test case that will perform bid Remediation =  Retun
	     // Checked the remediated bid in Bid Details page
		 //--------------------------------------------------------  
		
		 String className = ReturnFocusBid.class.getSimpleName();
		 String methodName = ("TC_RHW-007_" + className);
	 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // Return a bid 
		 // Check on Bid Details after refresh
		 BRETFocusPage returnBid = new BRETFocusPage (driver);
		 returnBid.bidRemediation(bidNumber, Uname, outcomeStat, outcomeCom); 
		 returnBid.bidRemediatonChckbidDetails(bidNumber, outcomeStat, outcomeCom, brand, methodName);
		 
		 //Screen Capture
//		 ScreenCapture screenShots = new ScreenCapture(driver);  
//		 screenShots.screenCaptures(methodName);
		
	}
	
   @Test
	public void tc_RHW_008 () throws Exception {
		 //-----------------------------------------------------   
		 // This test case will checked the remedediation bid on
	     // on the FOCUS tab.
	  	 //-----------------------------------------------------   -  
		
		 String className = ReturnFocusBid.class.getSimpleName();
		 String methodName = ("TC_RHW-008_" + className);
	 	 
		 // check returned bid in Focus tab
		 BRETFocusPage returnBidChck = new BRETFocusPage (driver);
		 returnBidChck.bidRemediationCheckFocusTab (bidNumber, Uname, outcomeStat); 
		 
		 //Screen Capture
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 screenShots.screenCaptures(methodName);
		
		
	}

}
