package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class RejectFocusBid extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 4, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 4, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 5, 1);   
	 String outcomeStat = Excel.getCellValue (xlpath, sheet2, 5, 2);   
	 String outcomeCom = Excel.getCellValue (xlpath, sheet2, 5, 3);   
	 
	 String brand = "HW"; //[updated-Belle-9/14/2017]  
	
  @Test
	public void tc_RHW_009 () throws Exception {
		 //------------------------------------------------------ 
		 // Test case that will perform bid Remediation =  Reject
	     // Checked the remediated bid in Bid Details page
		 //------------------------------------------------------  
		
		 String className = RejectFocusBid.class.getSimpleName();
		 String methodName = ("TC_RHW-009_" + className);
	 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // Reject a bid 
		 // Check on Bid Details after refresh
		 BRETFocusPage rejectBid = new BRETFocusPage (driver);
		 rejectBid.bidRemediation(bidNumber, Uname, outcomeStat, outcomeCom); 
		 rejectBid.bidRemediatonChckbidDetails(bidNumber, outcomeStat, outcomeCom, brand, methodName);
		 
		 //Screen Capture
//		 ScreenCapture screenShots = new ScreenCapture(driver);  
//		 screenShots.screenCaptures(methodName);
		 
		 
}
	
  @Test
	public void tc_RHW_010 () throws Exception {
		 //-----------------------------------------------------   
		 // This test case will checked the remedediation bid on
	     // on the FOCUS tab.
	  	 //-----------------------------------------------------   
		
		 String className = RejectFocusBid.class.getSimpleName();
		 String methodName = ("TC_RHW-010_" + className);
	 	 
		 // check returned bid in Focus tab
		 BRETFocusPage rejectBidChck = new BRETFocusPage (driver);
		 rejectBidChck.bidRemediationCheckFocusTab (bidNumber, Uname, outcomeStat); 
		 
		 //Screen Capture
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 screenShots.screenCaptures(methodName);
		
	}

}
