package com.bret.testcases.hw.remediation;

import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseRemediationTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

/**
 * 
 * Test Case execution for Hardware Focus Remediation.
 * 
 * @author CheyenneFreyLazo
 *
 */
public class HardwareFocusRemediationTest extends BaseRemediationTest {

	/**
	 * 
	 * Test case: RHW-004
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Reviewer
	 * 
	 * 2. Go to Focus tab
	 * 
	 * 3. Click the Bid Detail of the newly loaded Focus bid
	 * 
	 * 4. Click Take This Bid button
	 * 
	 * 5. Refresh the page
	 * 
	 * 6. Go to Remediation Log
	 */
	@Test(priority=4)
	public void testFocusTakeOnNew() {

		String tcNumber = "TC_RHW-004";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String newlyLoadedBidId = testData.get(BID_ID);
		takeOnNewFocusBid(tcNumber, newlyLoadedBidId, testData.get(OUTCOME));
	}

	/**
	 * Test case: RHW-005
	 * 
	 * Steps:
	 * 
	 * 1. Go to Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 */
	@Test(priority=5)
	public void testFocusAssignedBidDisplay() {
		String tcNumber = "TC_RHW-005";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole("Verify that Assigned bid have the correct Bid Date, Reviewer Decision and Reviewer Name.");

		checkFocusBidDateAndReviewerFields(assignedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		
		pausePage();
	}

	/**
	 * 
	 * Test case: RHW-006
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Return Comments =
	 * use default/add/edit comments
	 * 
	 * 4. Save changes
	 */
	@Test(priority=6)
	public void testFocusAssignedToReturn() {
		String tcNumber = "TC_RHW-006";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer be able to set the remediation details to 'Return' 'Outcome' and 'Outcome Comment' fields");
		pausePage();
		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RETURN,
				testData.get(OUTCOME_COMMENT));
		pausePage();
	}

	/**
	 * Test case: RHW-007
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Returned HW focus bid
	 * 
	 */
	@Test(priority=7)
	public void testFocusReturnedBidDisplay() {
		String tcNumber = "TC_RHW-007";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String returnedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Returned' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");
		pausePage();
		checkFocusBidDateAndReviewerFields(returnedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		pausePage();
	}

	/**
	 * 
	 * Test case: RHW-008
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome =
	 * 
	 * Reject Comments = use default/add/edit comments
	 * 
	 * 4. Save changes
	 * 
	 */
	@Test(priority=8)
	public void testFocusAssignedToReject() {
		String tcNumber = "TC_RHW-008";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer be able to set the remediation details to 'Reject' 'Outcome' and 'Outcome Comment' fields");
		
		pausePage();
		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_REJECT,
				testData.get(OUTCOME_COMMENT));
		pausePage();
	}

	/**
	 * 
	 * Test case: RHW-009
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Rejected HW focus bid
	 */
	@Test(priority=9)
	public void testFocusRejectBidDisplay() {
		String tcNumber = "TC_RHW-009";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String rejectedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Reject' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");

		pausePage();
		checkFocusBidDateAndReviewerFields(rejectedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		pausePage();
	}

	/**
	 * 
	 * Test case: RHW-010
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Release Comments
	 * = use default/add/edit comments
	 * 
	 * 4. Save changes
	 */
	@Test(priority=10)
	public void testFocusAssignedToRelease() {
		String tcNumber = "TC_RHW-010";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer is able to set the remediation details to 'Release' 'Outcome' and 'Outcome Comment' fields");

		pausePage();
		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RELEASE,
				testData.get(OUTCOME_COMMENT));
		pausePage();
	}

	/**
	 * 
	 * Test case: RHW-011
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Released HW focus bid
	 */
	@Test(priority=11)
	public void testFocusReleaseBidDisplay() {
		String tcNumber = "TC_RHW-011";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String releasedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Release' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");
		pausePage(); 

		checkFocusBidDateAndReviewerFields(releasedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		
		pausePage();
	}

	/**
	 * Test case: RHW-012
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Admin
	 * 
	 * 2. Go to My Task Tab.
	 * 
	 * 3. Select for the remediated Focus bid/s.
	 * 
	 * 4. Click Release Button
	 * 
	 */
	@Test(priority=12)
	public void testFocusReleaseToArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RHW-012";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		/* Gets the value of the bidId value from the excel file test data source */
		String remediatedBidId = testData.get(BID_ID);

		adminCheckAndReleaseBid(remediatedBidId, tcNumber);
		pausePage();

	}

	/**
	 * Test Case: RHW-013
	 * 
	 * Steps:
	 * 
	 * 1. Go to Archive Tab.
	 * 
	 * 2. Locate the remediated Focus bid/s.
	 */
	@Test(priority=13)
	public void testFocusReleasedArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RHW-013";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String remediatedArchivedBid = testData.get(BID_ID);
		adminCheckArchivedBidDisplay(tcNumber, remediatedArchivedBid, testData.get(RELEASED_BY),
				testData.get(RELEASED_DATE));
		pausePage();
	}

	/**
	 * 
	 * Test case: RHW-014
	 * 
	 * Steps:
	 * 
	 * 1. Click on Bid detail of the bid
	 *
	 * 2. Click ROLLBACK button 
	 * 
	 * 3. Go to Focus Tab and Search for the Rollback Bid
	 * 
	 */
	@Test(priority=14)
	public void testNonFocusRollBackArchivedBid() {
		/* Get the test data from excel */
		String tcNumber = "TC_RHW-014";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		MyTaskPage myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		ArchivePage archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		/* Now lets search for the newly archived bid */
		int archiveBidRowIndex = archivePage.searchRowIndex(driver, testData.get(BID_ID));
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		archivePage.clickCheckbox(archiveBidRowIndex);

		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Doing logging here */
		displayLogHeader(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		archivePage.clickDetailLink(driver, archiveBidRowIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isRollbackVisible = flaggingBidPage.checkRollbackButtonVisibility(driver);
		logExpectedActualText("Rollback Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isRollbackVisible));
		Assert.assertTrue(isRollbackVisible);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());

		printToConsole("Rollback Button is clicked");
		flaggingBidPage.clickRollbackButton(driver);

		pausePage();
		setupNewURL(driver.getCurrentUrl());

		pausePage();
		printToConsole("After clicking Rollback Button, the user is automatically navigated back to My Task Page");
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		pausePage();

		myTaskPage = new MyTaskPage();

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());
		logNavigation(false, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		FocusPage focusPage = new FocusPage(driver);

		int focusBidRowIndex = focusPage.searchRowIndex(driver, testData.get(BID_ID));
		boolean focusBidIsFound = focusBidRowIndex > ROW_NOT_FOUND;
		Assert.assertTrue(focusBidIsFound);
		focusPage.clickCheckbox(focusBidRowIndex);

		logExpectedActualText("Bid Id is found in Focus tab: ", Boolean.TRUE.toString(),
				Boolean.toString(focusBidIsFound));

		String actualDate = focusPage.getValueAtIndex(driver, focusBidRowIndex, NonFocusPage.BID_DATE_COL);
		String expectedDate = testData.get(BID_DATE);
		logExpectedActualText("Bid Date: ", expectedDate, actualDate);

		Assert.assertEquals(expectedDate, actualDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());
	}
}
