package com.bret.testcases.hw.remediation;

import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.DetailPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

/**
 * Hardware Remediation test.
 * 
 * @author CheyenneFreyLazo
 *
 */
public class HardwareRemediationTest extends BaseTest {

	/* Constant for row not found in the table */
	private static final int ROW_NOT_FOUND = 0;

	/* Data Source Keys */
	private static final String BID_ID = "bidId";
	private static final String RELEASED_BY = "releasedBy";
	private static final String RELEASED_DATE = "releasedDate";
	private static final String BID_DATE = "bidDate";
	private static final String OUTCOME = "outcome";
	private static final String OUTCOME_COMMENT = "outcomeComment";
	private static final String REVIEWER_DECISION = "reviewerDecision";
	private static final String REVIEWER_NAME = "reviewerName";

	/* Page objects */
	private MyTaskPage myTaskPage;
	private ArchivePage archivePage;
	private NonFocusPage nonFocusPage;
	private FocusPage focusPage;
	private FlaggingBidDataPage flaggingBidPage;
	private RemediationLogPage remediationPage;

	private int archiveBidRowIndex;

	/**
	 * Test case: RHW-001
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Admin -- This step is done.
	 * 
	 * 2. Go to My Task Tab.
	 * 
	 * 3. Select for the newly loaded Non-Focus did.
	 * 
	 * 4. Click Release Button
	 * 
	 */
	@Test(priority = 1)
	public void testNonFocusReleaseToArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RHW-001";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		/* Gets the value of the bidId value from the excel file test data source */
		String newlyLoadedBidId = testData.get(BID_ID);

		adminCheckAndReleaseBid(newlyLoadedBidId, tcNumber);
	}

	/**
	 * Test Case: RHW-002
	 * 
	 * Steps:
	 * 
	 * 1. Go to Archive Tab.
	 * 
	 * 2. Locate the released HW Non-focus bid
	 */
	@Test(priority = 2)
	public void testNonFocusReleasedArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RHW-002";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String newlyArchivedBid = testData.get(BID_ID);
		adminCheckArchivedBidDisplay(tcNumber, newlyArchivedBid, testData.get(RELEASED_BY),
				testData.get(RELEASED_DATE));
	}

	/**
	 * 
	 * Test case: RHW-003
	 * 
	 * Steps:
	 * 
	 * 1. Click on Bid detail of the bid
	 * 
	 * 2. Click ROLLBACK button
	 * 
	 * 3. Go to Non-Focus Tab and Search for the Rollback Bid
	 * 
	 */
	@Test(priority = 3)
	public void testNonFocusRollBackArchivedBid() {
		/* Get the test data from excel */
		String tcNumber = "TC_RHW-003";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		/* Now lets search for the newly archived bid */
		int archiveBidRowIndex = archivePage.searchRowIndex(driver, testData.get(BID_ID));
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		archivePage.clickCheckbox(archiveBidRowIndex);
		
		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Doing logging here */
		displayLogHeader(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		archivePage.clickDetailLink(driver, archiveBidRowIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isRollbackVisible = flaggingBidPage.checkRollbackButtonVisibility(driver);
		logExpectedActualText("Rollback Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isRollbackVisible));
		Assert.assertTrue(isRollbackVisible);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());

		flaggingBidPage.clickRollbackButton(driver);
		printToConsole("Rollback Button is clicked");

		logNavigation(true, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		logNavigation(false, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());
		pausePage();

		flaggingBidPage.navigateBackToSummary(driver, DetailPage.NAVIGATE_BACK_NON_FOCUS);

		pausePage();
		setupNewURL(driver.getCurrentUrl());
		pausePage();
		
		nonFocusPage = new NonFocusPage(driver);

		int nonFocusBidRowIndex = nonFocusPage.searchRowIndexNavFromDetail(driver, testData.get(BID_ID));
		boolean nonFocusBidIsFound = nonFocusBidRowIndex > ROW_NOT_FOUND;
		Assert.assertTrue(nonFocusBidIsFound);
		nonFocusPage.clickCheckbox(nonFocusBidRowIndex);

		logExpectedActualText("Bid Id is found in Non-Focus tab: ", Boolean.TRUE.toString(),
				Boolean.toString(nonFocusBidIsFound));

		String actualDate = nonFocusPage.getValueAtIndex(driver, nonFocusBidRowIndex, NonFocusPage.BID_DATE_COL);
		String expectedDate = testData.get(BID_DATE);
		logExpectedActualText("Bid Date: ", expectedDate, actualDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel(), this.getClass());

		printToConsole("Logging Out");
		logOutSummaryPage(driver);
	}

	/**
	 * 
	 * Test case: RHW-004
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Reviewer
	 * 
	 * 2. Go to Focus tab
	 * 
	 * 3. Click the Bid Detail of the newly loaded Focus bid
	 * 
	 * 4. Click Take This Bid button
	 * 
	 * 5. Refresh the page
	 * 
	 * 6. Go to Remediation Log
	 */
	@Test(priority = 4)
	public void testFocusTakeOnNew() {

		String tcNumber = "TC_RHW-004";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String newlyLoadedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer is able to TAKE ON a NEW bid with Outcome = Assigned (Under Remediation Log).");

		reviewerLoginNavigateFocusTab();

		focusPage = new FocusPage();
		printToConsole("Checking if the Newly loaded bid exists: " + newlyLoadedBidId);
		int newFocusBidIndex = focusPage.searchRowIndex(driver, newlyLoadedBidId);
		checkAndLogBidIfExist(newFocusBidIndex, "Newly loaded bid  is found");
		focusPage.clickCheckbox(newFocusBidIndex);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());
		pausePage();

		logNavigateToDetail();
		focusPage.clickDetailLink(driver, newFocusBidIndex);
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());

		flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage();

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());

		flaggingBidPage.clickTakeThisBid();

		// Refresh page
		driver.navigate().refresh();

		System.out.println();
		logNavigation(true, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		remediationPage = new RemediationLogPage(driver);

		String actualOutcome = getRemediationSubTabValue(RemediationLogPage.ROW_REM_OUTCOME);
		String expectedOutcome = testData.get(OUTCOME);

		logExpectedActualText("Outcome: ", expectedOutcome, actualOutcome);

		Assert.assertEquals(expectedOutcome, actualOutcome);

		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());

		logOutDetailsPage(driver);
	}

	/**
	 * Test case: RHW-005
	 * 
	 * Steps:
	 * 
	 * 1. Go to Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 */
	@Test(priority = 5)
	public void testFocusAssignedBidDisplay() {
		String tcNumber = "TC_RHW-005";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole("Verify that Assigned bid have the correct Bid Date, Reviewer Decision and Reviewer Name.");

		checkFocusBidDateAndReviewerFields(assignedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
	}

	/**
	 * 
	 * Test case: RHW-006
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Return Comments =
	 * use default/add/edit comments
	 * 
	 * 4. Save changes
	 */
	@Test(priority = 6)
	public void testFocusAssignedToReturn() {
		String tcNumber = "TC_RHW-006";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer be able to set the remediation details to 'Return' 'Outcome' and 'Outcome Comment' fields");

		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RETURN,
				testData.get(OUTCOME_COMMENT));
	}

	/**
	 * Test case: RHW-007
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Returned HW focus bid
	 * 
	 */
	@Test(priority = 7)
	public void testFocusReturnedBidDisplay() {
		String tcNumber = "TC_RHW-007";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String returnedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Returned' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");

		checkFocusBidDateAndReviewerFields(returnedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
	}

	/**
	 * 
	 * Test case: RHW-008
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome =
	 * 
	 * Reject Comments = use default/add/edit comments
	 * 
	 * 4. Save changes
	 * 
	 */
	@Test(priority = 8)
	public void testFocusAssignedToReject() {
		String tcNumber = "TC_RHW-008";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer be able to set the remediation details to 'Reject' 'Outcome' and 'Outcome Comment' fields");

		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_REJECT,
				testData.get(OUTCOME_COMMENT));
	}

	/**
	 * 
	 * Test case: RHW-009
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Rejected HW focus bid
	 */
	@Test(priority = 9)
	public void testFocusRejectBidDisplay() {
		String tcNumber = "TC_RHW-009";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String rejectedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Reject' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");

		checkFocusBidDateAndReviewerFields(rejectedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
	}

	/**
	 * 
	 * Test case: RHW-010
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned HW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Release Comments
	 * = use default/add/edit comments
	 * 
	 * 4. Save changes
	 */
	@Test(priority = 10)
	public void testFocusAssignedToRelease() {
		String tcNumber = "TC_RHW-010";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Release be able to set the remediation details to 'Release' 'Outcome' and 'Outcome Comment' fields");

		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RELEASE,
				testData.get(OUTCOME_COMMENT));
	}

	/**
	 * 
	 * Test case: RHW-011
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Released HW focus bid
	 */
	@Test(priority = 11)
	public void testFocusReleaseBidDisplay() {
		String tcNumber = "TC_RHW-011";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String releasedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Release' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");

		checkFocusBidDateAndReviewerFields(releasedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
	}

	/**
	 * Test case: RHW-012
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Admin
	 * 
	 * 2. Go to My Task Tab.
	 * 
	 * 3. Select for the remediated Focus bid/s.
	 * 
	 * 4. Click Release Button
	 * 
	 */
	@Test(priority = 12)
	public void testFocusReleaseToArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RHW-012";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		/* Gets the value of the bidId value from the excel file test data source */
		String remediatedBidId = testData.get(BID_ID);

		adminCheckAndReleaseBid(remediatedBidId, tcNumber);

	}

	/**
	 * Test Case: RHW-013
	 * 
	 * Steps:
	 * 
	 * 1. Go to Archive Tab. 
	 * 
	 * 2. Locate the remediated Focus bid/s.
	 */
	@Test(priority = 13)
	public void testFocusReleasedArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RHW-013";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String remediatedArchivedBid = testData.get(BID_ID);
		adminCheckArchivedBidDisplay(tcNumber, remediatedArchivedBid, testData.get(RELEASED_BY),
				testData.get(RELEASED_DATE));
	}
	
	/**
	 * 
	 * Test case: RHW-014
	 * 
	 * Steps:
	 * 
	 * 1. Click on Bid detail of the bid
	 * 
	 * 2. Click ROLLBACK button
	 * 
	 * 3. Go to Non-Focus Tab and Search for the Rollback Bid
	 * 
	 */
	@Test(priority = 3)
	public void testocusRollBackArchivedBid() {
		/* Get the test data from excel */
		String tcNumber = "TC_RHW-003";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		/* Now lets search for the newly archived bid */
		int archiveBidRowIndex = archivePage.searchRowIndex(driver, testData.get(BID_ID));
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		archivePage.clickCheckbox(archiveBidRowIndex);
		
		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Doing logging here */
		displayLogHeader(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		archivePage.clickDetailLink(driver, archiveBidRowIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isRollbackVisible = flaggingBidPage.checkRollbackButtonVisibility(driver);
		logExpectedActualText("Rollback Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isRollbackVisible));
		Assert.assertTrue(isRollbackVisible);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());

		flaggingBidPage.clickRollbackButton(driver);
		printToConsole("Rollback Button is clicked");

		logNavigation(true, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		logNavigation(false, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());
		pausePage();

		flaggingBidPage.navigateBackToSummary(driver, DetailPage.NAVIGATE_BACK_NON_FOCUS);

		pausePage();
		setupNewURL(driver.getCurrentUrl());
		pausePage();
		
		nonFocusPage = new NonFocusPage(driver);

		int nonFocusBidRowIndex = nonFocusPage.searchRowIndexNavFromDetail(driver, testData.get(BID_ID));
		boolean nonFocusBidIsFound = nonFocusBidRowIndex > ROW_NOT_FOUND;
		Assert.assertTrue(nonFocusBidIsFound);
		nonFocusPage.clickCheckbox(nonFocusBidRowIndex);

		logExpectedActualText("Bid Id is found in Non-Focus tab: ", Boolean.TRUE.toString(),
				Boolean.toString(nonFocusBidIsFound));

		String actualDate = nonFocusPage.getValueAtIndex(driver, nonFocusBidRowIndex, NonFocusPage.BID_DATE_COL);
		String expectedDate = testData.get(BID_DATE);
		logExpectedActualText("Bid Date: ", expectedDate, actualDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel(), this.getClass());

		printToConsole("Logging Out");
		logOutSummaryPage(driver);
	}


	/**
	 * Method for searching assigned bid in the focus tab and then this will
	 * remediate the selected assigned bid.
	 * 
	 * @param assignedBidId
	 *            - bid to be searched in the focus page.
	 * @param tcNumber
	 *            - test case number.
	 * @param remediationOutcome
	 *            - remediation outcome based on the test case.
	 * @param remediationOutcomeComments
	 *            - remediation outcome comments that should be fetched from the
	 *            datasource.
	 */
	private void searchFocusAssignedBidAndRemediate(String assignedBidId, String tcNumber, String remediationOutcome,
			String remediationOutcomeComments) {
		reviewerLoginNavigateFocusTab();

		focusPage = new FocusPage();
		int assignedBidIndex = focusPage.searchRowIndex(driver, assignedBidId);

		printToConsole("Checking if the Assigned Bid exists: " + assignedBidId);
		checkAndLogBidIfExist(assignedBidIndex, "Assigned Bid is found");

		focusPage.clickCheckbox(assignedBidIndex);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());

		logNavigateToDetail();
		focusPage.clickDetailLink(driver, assignedBidIndex);
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());

		flaggingBidPage = new FlaggingBidDataPage(driver);

		System.out.println();
		logNavigation(true, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		remediationPage = new RemediationLogPage(driver);
		remediationPage.clickRemediationEdit();

		pausePage();

		remediationPage.selectOutcome(remediationOutcome);
		remediationPage.inputOutcomeComment(remediationOutcomeComments);

		remediationPage.clickSaveButton();

		pausePage();
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());

		logOutDetailsPage(driver);

	}

	/**
	 * 
	 * @param bidId
	 * @param bidDate
	 * @param reviewerDecision
	 * @param reviewerName
	 * @param tcNumber
	 */
	private void checkFocusBidDateAndReviewerFields(String bidId, String bidDate, String reviewerDecision,
			String reviewerName, String tcNumber) {
		reviewerLoginNavigateFocusTab();

		focusPage = new FocusPage();
		int returnedBidIndex = focusPage.searchRowIndex(driver, bidId);

		printToConsole("Checking if the Bid exists: " + bidId);
		checkAndLogBidIfExist(returnedBidIndex, "Bid is found");

		focusPage.clickCheckbox(returnedBidIndex);

		String actualBidDate = focusPage.getValueAtIndex(driver, returnedBidIndex, FocusPage.BID_DATE_COL);
		String actualReviewerDecision = focusPage.getValueAtIndex(driver, returnedBidIndex,
				FocusPage.REVIEWER_DECISION_COL);
		String actualReviewerName = focusPage.getValueAtIndex(driver, returnedBidIndex, FocusPage.REVIEWER_COL);

		String expectedBidDate = bidDate;
		String expectedReviewerDecision = reviewerDecision;
		String expectedReviewerName = reviewerName;

		logExpectedActualText("Bid Date: ", expectedBidDate, actualBidDate);
		logExpectedActualText("Reviewer Decision: ", expectedReviewerDecision, actualReviewerDecision);
		logExpectedActualText("Reviewer Name: ", expectedReviewerName, actualReviewerName);

		Assert.assertEquals(expectedBidDate, actualBidDate);
		Assert.assertEquals(expectedReviewerDecision, actualReviewerDecision);
		Assert.assertEquals(expectedReviewerName, actualReviewerName);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());

		logOutSummaryPage(driver);
	}

	/**
	 * Login as a reviewer and navigate to focus tab.
	 */
	private void reviewerLoginNavigateFocusTab() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);

		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());
		logNavigation(false, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		System.out.println();

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
	}

	/**
	 * @param tcNumber
	 * @param bidId
	 * @param expectedReleasedBy
	 * @param expectedReleasedOnDate
	 */
	private void adminCheckArchivedBidDisplay(String tcNumber, String bidId, String expectedReleasedBy,
			String expectedReleasedOnDate) {


		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		/* Now lets search for the newly archived bid */
		archiveBidRowIndex = archivePage.searchRowIndex(driver, bidId);
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		/* Checking if the newly loaded bid is found */
		Assert.assertTrue(releasedBidIsFound);

		archivePage.clickCheckbox(archiveBidRowIndex);

		String actualReleasedBy = archivePage.getValueAtIndex(driver, archiveBidRowIndex,
				ArchivePage.BID_RELEASED_BY_COL);
		String actualReleasedOnDate = archivePage.getValueAtIndex(driver, archiveBidRowIndex,
				ArchivePage.BID_RELEASED_ON_COL);

		logExpectedActualText("Released By: ", expectedReleasedBy, actualReleasedBy);
		logExpectedActualText("Released On Date: ", expectedReleasedOnDate, actualReleasedOnDate);

		Assert.assertEquals(expectedReleasedBy, actualReleasedBy);
		Assert.assertEquals(expectedReleasedOnDate, actualReleasedOnDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel(), this.getClass());
		
		logOutSummaryPage(driver);
	}

	private void adminCheckAndReleaseBid(String bidId, String tcNumber) {

		/*
		 * BRETLoginPage object contains the userLogin method for logging in to the Bret
		 * Web application.
		 */
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		printToConsole("Verify that the 'Release' button is present for 'Admin' user");

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isReleaseVisible = myTaskPage.checkReleaseButtonVisibility(driver);
		logExpectedActualText("Release Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isReleaseVisible));
		Assert.assertTrue(isReleaseVisible);

		/* Now lets search for the newly loaded bid */
		int myTaskRowIndex = myTaskPage.searchRowIndex(driver, bidId);

		/* Checking if the newly loaded bid is found */
		Assert.assertTrue(myTaskRowIndex > ROW_NOT_FOUND);

		/* Now lets search for the newly loaded bid */
		myTaskPage.clickCheckbox(myTaskRowIndex);

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());

		/* Click the release button */
		myTaskPage.clickReleaseButton(driver);
		printToConsole("Release Button is clicked");

		logOutSummaryPage(driver);
	}

	private void logNavigateToDetail() {
		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		System.out.println();
	}

	/**
	 * Checks if the bid exists based on the row and logs the process.
	 * 
	 * @param rowIndex
	 * @param logText
	 */
	private void checkAndLogBidIfExist(int rowIndex, String logText) {
		boolean bidIsFound = rowIndex > ROW_NOT_FOUND;
		logExpectedActualText(logText, Boolean.TRUE.toString(), Boolean.toString(bidIsFound));
		/* Checking if the bid is found */
		Assert.assertTrue(bidIsFound);
	}

	/**
	 * Gets the Remediation table column values
	 * 
	 * @param rowIndex
	 * @return
	 */
	private String getRemediationSubTabValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, rowIndex);
	}
	
}
