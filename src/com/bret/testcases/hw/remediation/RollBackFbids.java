package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class RollBackFbids extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 5, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 5, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 8, 1);   
	
	 @Test
	 public void tc_RHW_015 () throws Exception {
		 //-------------------------------------------------------------  
		 // This test case will Roll back a Focus bid from Archive Tab
		 //------------------------------------------------------------- 
		 
		 String className = RollBackFbids.class.getSimpleName();
		 String methodName; //methodName2;
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 	 
		 // search and rollback the bid
		 // capture page prior to rollback
		 BRETArchivePage rollBackBid = new BRETArchivePage (driver);
		 methodName = ("TC_RHW-015_" + className);                
		 rollBackBid.bidrollBack_F(bidNumber, methodName);
				 
	 }
	 
	 @Test
	 public void tc_RHW_016 () throws Exception {
		 //---------------------------------------------------------------- 
		 // This test case will checked the rolled back bid in Focus Tab
		 //---------------------------------------------------------------- 
	 
		 BRETFocusPage rolledBackbidChck = new BRETFocusPage (driver);
		 rolledBackbidChck.rolledBackbidChck(bidNumber);
		 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = RollBackFbids.class.getSimpleName();
		 String methodName = ("TC_RHW-016_" + className);
		 screenShots.screenCaptures(methodName);
				 
	 }
}
