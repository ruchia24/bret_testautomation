package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETMyTaskPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class ArchiveFbids extends BRETmainTestNGbase{
	
	// Initializations
	// reading from data source
	
	 String xlpath = "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 7, 1);   

	 @Test
	 public void tc_RHW_013 () throws Exception {
		 //------------------------------------------------------------------------------------
		 // This test case will verify the RELEASE button being present on the My Task page 
		 // for the administrator
		 //------------------------------------------------------------------------------------
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // Verify release button
		 BRETMyTaskPage releaseBtn = new BRETMyTaskPage (driver);
		 releaseBtn.releaseBtnCheck(); 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = ArchiveFbids.class.getSimpleName();
		 String methodName = ("TC_RHW-013_" + className);
		 screenShots.screenCaptures(methodName);
				 
	 }
	 
	 @Test
	 public void tc_RHW_014 () throws Exception {
		 //-------------------------------------------------------------- 
		 // This test case will verify release the bid to Archive tab.
		 // Bid details are checked on Archive Tab
		 //-------------------------------------------------------------- 
		 
		 String className = ArchiveFbids.class.getSimpleName();
		 String methodName1 = ("TC_RHW-014a_" + className  );
		 
		 // release bid to archive
		 BRETMyTaskPage releaseFbid = new BRETMyTaskPage (driver);
		 releaseFbid.releaseFbid2Archive(bidNumber, methodName1); 
		 
		 // check bid in Archive
		 BRETArchivePage releaseFbidChck =  new BRETArchivePage(driver);
		 releaseFbidChck.releasedtoArchiveChck_F(bidNumber, Uname);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName = ("TC_RHW-014b_" + className );
		 screenShots.screenCaptures(methodName);
				 
	 }
	 

}
