package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETMyTaskPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class ArchiveNFbids extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
		
	 String xlpath = "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 1, 1);  
	
	 

	 @Test
	 public void tc_RHW_001 () throws Exception {
		 //------------------------------------------------------------------------------------
		 // This test case will verify the RELEASE button being present on the My Task page 
		 // for the administrator
		 //------------------------------------------------------------------------------------
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // Verify release button
		 BRETMyTaskPage releaseBtn = new BRETMyTaskPage (driver);
		 releaseBtn.releaseBtnCheck(); 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = ArchiveNFbids.class.getSimpleName();
		 String methodName = ("TC_RHW-001_" + className );
		 screenShots.screenCaptures(methodName);
				 
	 }
	 
	 @Test
	 public void tc_RHW_002 () throws Exception  {
		 //------------------------------------------------------------------------------------
		 // This test case will verify that non focus bid can be released to archived
		 //------------------------------------------------------------------------------------
		 	 
		 // search a particular bid in My Task tab
		 BRETMyTaskPage findBidMyTask = new BRETMyTaskPage (driver);
		 findBidMyTask.bidFindonMyTasktab_NF(bidNumber); 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = ArchiveNFbids.class.getSimpleName();
		 String methodName = ("TC_RHW-002a_" + className);
		 screenShots.screenCaptures(methodName);
		 
		 findBidMyTask.bidReleasetoArchive();
		 
		 // Perform search on archive tab
		 BRETArchivePage findBidArchive = new BRETArchivePage(driver);
		 findBidArchive.releasedtoArchiveChck_NF(bidNumber, Uname);
			
		 //Screen capture 
		 ScreenCapture screenShots2 = new ScreenCapture(driver);  
		 //String className2 = ArchiveNFbids.class.getSimpleName();
		 String methodName2 = ("TC_RHW-002b_" + className);
		 screenShots2.screenCaptures(methodName2);
		 
	}
}
