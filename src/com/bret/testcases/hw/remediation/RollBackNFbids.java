package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETNonFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class RollBackNFbids extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	
	 String xlpath = "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 2, 1);   
	
	 @Test
	 public void tc_RHW_003 () throws Exception {
		 //------------------------------------------------------------------ 
		 // This test case will Roll back a Non Focus bid from Archive Tab
		 //------------------------------------------------------------------
		 
		 String className = RollBackNFbids.class.getSimpleName();
		 String methodName; //methodName2;
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 	 
		 // search and rollback the bid
		 // capture page prior to rollback
		 BRETArchivePage rollBackBid = new BRETArchivePage (driver);
		 methodName = ("TC_RHW-003_" + className);                
		 rollBackBid.bidrollBack(bidNumber, methodName);
				 
	 }
	 
	 @Test
	 public void tc_RHW_004 () throws Exception {
		 //------------------------------------------------------------------- 
		 // This test case will checked the rolled back bid in Non Focus Tab
		 //-------------------------------------------------------------------- 
	 
		 BRETNonFocusPage rolledBackbidChck = new BRETNonFocusPage (driver);
		 rolledBackbidChck.rolledbackBidChck(bidNumber);
		 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = RollBackNFbids.class.getSimpleName();
		 String methodName = ("TC_RHW-004_" + className);
		 screenShots.screenCaptures(methodName);
				 
	 }

}
