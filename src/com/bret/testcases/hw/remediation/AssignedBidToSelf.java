package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;


public class AssignedBidToSelf extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	
	 String xlpath = "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 4, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 4, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 3, 1);   
	 
	 @Test
	 public void tc_RHW_005 () throws Exception {
		 //---------------------------------------------- 
		 // This test case will assign Focus bid to Self
		 //---------------------------------------------- 
		
		 String className = AssignedBidToSelf.class.getSimpleName();
		 String methodName = ("TC_RHW-005_" + className);
	 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // Assigned bid to self
		 // Screen capture after the bid is assigned
		 // Bid Details window will close
		 BRETFocusPage assignBid2Self = new BRETFocusPage (driver);
		 assignBid2Self.bidAssignToSefl(bidNumber, methodName);
		 
		 
	 }
	 
	 @Test
	 public void tc_RHW_006 () throws Exception {
		 //--------------------------------------------------------------------------------
		 // This test case will checked on the assigned bid in Summary and Bid Details page
		 //-------------------------------------------------------------------------------- 
		 
		 BRETFocusPage chckAssignBid = new BRETFocusPage (driver);
		 
		//check in Focus tab
		chckAssignBid.assignedBidChckFocusTab(bidNumber, Uname); 
		 
		//Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = AssignedBidToSelf.class.getSimpleName();
		String methodName = ("TC_RHW-006a_" + className);
		screenShots.screenCaptures(methodName);
		 
		//check in Bid Details page
		chckAssignBid.assignedBidChckbidDetails(bidNumber, Uname);
		 
		//Screen capture 
		String methodName1 = ("TC_RHW-006b_" + className);
		screenShots.screenCaptures(methodName1);
		 

	 }
	 

}
