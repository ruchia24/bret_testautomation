package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class AutoArchiveNFbids extends BRETmainTestNGbase {
	
    // reading from data source
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 1, 1);  
	 String className = AutoArchiveNFbids.class.getSimpleName();
	 
	 @Test
	 public void tc_RHW_002_autoArch () throws Exception {
		 
		 //------------------------------------------------------------------------------------
		 // This test case will verify the non Focus bid released to Archive after CRON job
		 //------------------------------------------------------------------------------------
		 		
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // check bid in Archive
		 BRETArchivePage releaseNFbidChck =  new BRETArchivePage(driver);
		 releaseNFbidChck.autoreleasedtoArchiveChck_NF(bidNumber, Uname);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName = ("TC_RHW-002_" + className );
		 screenShots.screenCaptures(methodName);

   }
}
