package com.bret.testcases.hw.scoring;

import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SupportDataPage;
import com.bret.util.BretTestUtils;

/**
 * TODO: Frey - your current implementation for print/output to console is ugly
 * -- needs improvement.
 * 
 * 
 * @author CheyenneFreyLazo
 * 
 */
public class HardwareNonFocusTest extends BaseTest {
	private static final String FLAG_NON_FOCUS = "N";
	private static final String BID_ID = "bidId";
	private static final String SUPP_FACT_SCORE = "suppFactScore";
	private static final String DISC_MARG_FILE = "discMargFilePath";

	private static final int ROW_NOT_FOUND = 0;

	private MyTaskPage myTaskPage;
	private NonFocusPage nonFocusPage;
	private String detailLinkPart;

	@Test(priority = 1)
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}

	/**
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin user.
	 * 
	 * 3. Go to My Task Tab.
	 * 
	 * 4. Search for the newly loaded non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Non-Focus Bids Tab
	 */
	@Test(priority = 2)
	public void testNonFocusFlagging() {

		/* Get the test data from excel */
		String tcNumber = "TC_BSHW-001";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String newlyLoadedBidId = testData.get(BID_ID);

		/*
		 * The landing page upon login is the My Task Tab page - so no need to
		 * navigate MyTaskPage represents the My Task tab page object
		 */
		myTaskPage = new MyTaskPage();
	
		myTaskPage.closeNPSSurvey(driver);

		/* Now lets search for the newly loaded bid */
		int myTaskRowIndex = myTaskPage
				.searchRowIndex(driver, newlyLoadedBidId);
		
		myTaskPage.clickCheckbox(myTaskRowIndex);
		
		String myTaskFlag = myTaskPage.getValueAtIndex(driver, myTaskRowIndex,
				MyTaskPage.FLAG_COL);

		String actualMyTaskBidId = myTaskPage.getValueAtIndex(driver,
				myTaskRowIndex, MyTaskPage.BID_ID_COL);

		/* Check if it is found - meaning the returned row is not 0 */
		Assert.assertTrue(myTaskRowIndex > ROW_NOT_FOUND);
		Assert.assertEquals(myTaskFlag, FLAG_NON_FOCUS);

		/* Doing some logging and screen capture */
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== " + SummaryPageTabsE.MY_TASK.getLabel()
				+ " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in My Task Tab");
		
		System.out.println("Expected Bid Id: " + newlyLoadedBidId);
		System.out.println("Bid Id (Display): " + actualMyTaskBidId);
		
		System.out.println();
		System.out.println("Expected Flag: " + FLAG_NON_FOCUS);
		System.out.println("Flag (Display): " + myTaskFlag);

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		System.out.println();
		printToConsole("Navigating FROM: My Task tab");

		/* Next lets navigate to Non-focus tab page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);

		/* This NonFocusTabPage object represents the Non-focus tab page */
		nonFocusPage = new NonFocusPage();

		/*
		 * In the Non - focus tab page lets search the table for our newly
		 * loaded bid row
		 */

		int nonFocusRowIndex = nonFocusPage.searchRowIndex(driver,
				newlyLoadedBidId);
		
		nonFocusPage.clickCheckbox(nonFocusRowIndex);
		/*
		 * Store the URL for detail access for later -- navigate to the detail
		 * page First we will get the specific url (contains - bidId,
		 * sourceSysCd, batch id)
		 */
		detailLinkPart = nonFocusPage.getDetailSubUrl(driver,
				SummaryPageTabsE.NON_FOCUS_BIDS, nonFocusRowIndex);

		/* Checking if the newly loaded bid is found */
		Assert.assertTrue(nonFocusRowIndex > ROW_NOT_FOUND);
		String actualNonFocusBidId = nonFocusPage.getValueAtIndex(driver,
				nonFocusRowIndex, NonFocusPage.BID_ID_COL);
		
		System.out.println();
		System.out.println("========== "
				+ SummaryPageTabsE.NON_FOCUS_BIDS.getLabel()
				+ " tab ==========");
		
		System.out.println("Verify that the newly loaded bid is present in Non-Focus Tab");
		System.out.println("Expected Bid Id: " + newlyLoadedBidId);
		System.out.println("Bid Id (Display): " + actualNonFocusBidId);

		/* Alright if everything is done lets take a picture :D */
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());
	}

	/**
	 * Steps
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin user.
	 * 
	 * 3. Go to My Task Tab.
	 * 
	 * 4. Search for the newly loaded non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Non-Focus Bids Tab
	 * 
	 * 6. Go to bid details.
	 * 
	 * 7. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 */
	@Test(priority = 3)
	public void testNonFocusSupportingFactors() {
		/*
		 * TC number for picture taking :) We can find the Score in:
		 * BRET.BRETWEB_DETAIL.MAX_SF_SCORE
		 */
		String tcNumber = "TC_BSHW-002";

		/* We will get the test data */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String expectedScore = testData.get(SUPP_FACT_SCORE);

		/*
		 * Then we will have to update our driver to accommodate the newly
		 * opened window
		 */
		setupNewURL(getFullDetailURL(detailLinkPart));
		
		System.out.println();
		System.out.println("Clicking the detail link");
		printToConsole("Opening Detail page", "Current detail tab is: "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/*
		 * FlaggingBidDataPage represents the Flagging Bid Data tab page of the
		 * Detail page
		 */
		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		/*
		 * Next we will check the Supporting Factors row - score column value
		 * the compare it to our expected value
		 */
		String suppFactTabScore = flaggingBidPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS,
				FlaggingBidDataPage.COL_SCORE);
		
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel()
				+ " tab ==========");
		System.out.println("Verify that the score under the supporting factors tab is correctly displayed");
		System.out.println("Expected Supporting Factors Score: "
				+ expectedScore);
		System.out.println("Supporting Factors Score (Display): "
				+ suppFactTabScore);

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		System.out.println();
		printToConsole("Navigating FROM: "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);
		System.out.println();
		SupportDataPage suppDataPage = new SupportDataPage(driver);
		
		/* Take a picture */
		suppDataPage.scrollingScreenshot(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.SUPPORT_DATA.getLabel(),
						this.getClass()));
		
		int computedSuppDataScore = suppDataPage.computeTotalScore(driver);

		System.out.println();
		System.out.println("========== "
				+ DetailPageTabsE.SUPPORT_DATA.getLabel() + " tab ==========");
		System.out.println("Verify that the computed supporting factors is correct.");
		System.out.println("Expected Supporting Factors Score: "
				+ expectedScore);
		System.out.println("Supporting Factors Score (Computed): "
				+ computedSuppDataScore);

		/* Lets check if our expected result is correct */
		Assert.assertTrue(expectedScore.equalsIgnoreCase(suppFactTabScore));
		Assert.assertTrue(suppFactTabScore.equalsIgnoreCase(String
				.valueOf(computedSuppDataScore)));
	}

	/**
	 * 
	 Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin user.
	 * 
	 * 3. Go to My Task Tab.
	 * 
	 * 4. Search for the newly loaded non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Non-Focus Bids Tab
	 * 
	 * 6. Go to bid details.
	 * 
	 * 7. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 8. Verify that the scores form the focus factors are being added to the
	 * total bid score and being displayed correctly on the summary page of bid
	 * detail.
	 * 
	 * -- Flagging and Bid Data tab
	 * 
	 * -- Bid Q tab
	 */
	@Test(priority = 4)
	public void testNonFocusScores() {
		/*
		 * TC number for picture taking
		 */
		String tcNumber = "TC_BSHW-003";

		SupportDataPage suppDataPage = new SupportDataPage(driver);
		System.out.println();
		printToConsole("Navigating FROM: "
				+ DetailPageTabsE.SUPPORT_DATA.getLabel());
		suppDataPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		int totalBidLevelScoreComputed = flaggingBidPage
				.getTotalBidLevelScore();

		String totalBidValueScoreDisplay = flaggingBidPage.getValueAtIndex(
				driver, FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE,
				FlaggingBidDataPage.COL_SCORE);

		String displayBPCopsScore = flaggingBidPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertEquals(String.valueOf(totalBidLevelScoreComputed),
				totalBidValueScoreDisplay);
		
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("Verify Total Bid Level Score and Questions & BPCOPS is correctly displayed and computed");

		System.out.println("========== "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel()
				+ " tab ==========");
		System.out.println("Total Bid Level Score (Display): "
				+ totalBidValueScoreDisplay);
		System.out.println("Total Bid Level Score (Computed): "
				+ totalBidLevelScoreComputed);
		
		System.out.println();
		printToConsole("Navigating FROM: "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		String tier1Ceid = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);
		
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		BidQPage bidQPage = new BidQPage(driver);
		int computedBPCOPS = bidQPage.getTotalForBPCops(driver, tier1Ceid.equalsIgnoreCase(tier2Ceid));
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());
		
		System.out.println();
		System.out.println("========== " + DetailPageTabsE.BID_Q.getLabel()
				+ " tab ==========");
		System.out.println("Questions & BPCOPS (Display): "
				+ displayBPCopsScore);
		System.out.println("Questions & BPCOPS (Computed): " + computedBPCOPS);

		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		Assert.assertEquals(String.valueOf(computedBPCOPS), displayBPCopsScore);

		/* Alright if everything is done lets take a picture :D */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

	}

	/**
	 * To verify that correctLeading product (Power / Storage for HW) , bid
	 * date, region, country and line items are being displayed correctly on the
	 * bid detail page.
	 * 
	 * 
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin user.
	 * 
	 * 3. Go to My Task Tab.
	 * 
	 * 4. Search for the newly loaded non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Non-Focus Bids Tab
	 * 
	 * 6. Go to bid details.
	 * 
	 * 7. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 8. Verify that the score of the bid is being added to the total bid score
	 * and being displayed correctly on the summary page of bid detail.
	 * 
	 * 9. Verify thatcorrectLeading product (Power / Storage for HW) , bid
	 * date, region, country and line items are being displayed correctly on the
	 * bid detail page.
	 */
	@Test(priority = 5)
	public void testNonFocusDisplay() {

		/*
		 * TC number for picture taking
		 */
		String tcNumber = "TC_BSHW-004";

		/* We will get the test data */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		String primaryBrand = flaggingBidPage.getValueAtIndexBidDataGrid(
				driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND);
		String bidDate = flaggingBidPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_BID_DATE);
		String region = flaggingBidPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_REGION);
		String country = flaggingBidPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_COUNTRY_NAME);

		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel()
				+ " tab ==========");
		System.out.println("Primary Brand (Expected): " + expectedPrimaryBrand);
		System.out.println("Primary Brand (Display): " + primaryBrand);
		System.out.println("Bid Date (Expected): " + expectedBidDate);
		System.out.println("Bid Date (Display): " + bidDate);
		System.out.println("Region (Expected): " + expectedRegion);
		System.out.println("Region (Display): " + region);
		System.out.println("Country (Expected): " + expectedCountry);
		System.out.println("Country (Display): " + country);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);
		
		System.out.println();
		System.out.println("Verify that the displayed Discount and Margin Line items are correct.");
		boolean isDiscMargValid = checkDiscountMarginValidity(testData.get(DISC_MARG_FILE), tcNumber);
		Assert.assertTrue(isDiscMargValid);
		System.out.println();
		
		System.out.println("Discount + Margin display is valid: " + isDiscMargValid);
		
		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());
	}
	
	/**
	 * Returns if the Discount + Margin tab display is valid by comparing the
	 * BRET Web display to the expected CSV data.
	 * 
	 * @param filePath of the expected CSV data - exported from the DB.
	 * @return true if the CSV data extract matches the BRET Web Display. 
	 */
	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) {
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);
		
		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(), BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}

	private String getFullDetailURL(String urlPartFromLink) {
		String detailURL = getBretWebURL() + "?"
				+ urlPartFromLink.split("\\?")[1].split("\"")[0];

		return detailURL;
	}

	/**
	 * Method to take screenshot.
	 * 
	 * @param tcNumber
	 * @param tabLabel
	 */
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,
						this.getClass()));
	}
	
}
