package com.bret.testcases.hw.scoring;

//import static org.testng.Assert.assertEquals;

import java.util.Map;

//import javax.swing.border.EmptyBorder;

//import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.testng.Assert;
//import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BRETSearchPage;
//import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
//import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
//import com.bret.pages.SupportDataPage;
import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.util.BretTestUtils;

public class HardwareOPRAwithValue extends BaseTest {
	private static final String BID_ID = "bidId";
	private static final String OPPTY = "opptyNum";
	private static final String OO = "ooValue";
	private static final String BM = "bmValue";
	private static final String T3NAME = "t3NameValue";
	private static final String T3ID = "t3IdValue";
	private static final String T4NAME = "t4NameValue";
	private static final String T4ID = "t4IdValue";
	
	private MyTaskPage myTaskPage;
//	private NonFocusPage nonFocusPage;
	private ArchivePage archivePage; // go back to using non focus once executing RT
	private BRETSearchPage searchPage;
	private FlaggingBidDataPage flagAndbidPage;
	private RemediationLogPage remediationPage;
	private DetailPage detailPage;

	@BeforeTest
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}
	
	/**
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any user role.
	 * 
	 * 3. Go to Search Tab
	 * 
	 * 4. Search for the newly loaded OPRA AP bid.
	 * 
	 * 5. Go to Bid Details
	 * 
	 * 6. Locate Opportunity Number field
	 * 
	 * 7. Click Remediation Log Tab
	 * 
	 * 8. Locate OPPTY# field
	 */
	@Test(priority = 1, description = "[BSHW-051] BRET Web Details > OPPTY# (OPRA bids): OPPTY# displayed on Flagging and Bid data tab")
	public void testWithOPPTY() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
//		nonFocusPage = new NonFocusPage();
		archivePage = new ArchivePage(); // go back to using non focus once executing R
		detailPage = new DetailPage();
		
		flagAndbidPage = new FlaggingBidDataPage(driver);
		remediationPage = new RemediationLogPage(driver);

		/* Get the test data from excel */
		String tcNumber = "TC_BSHW-051";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String newlyLoadedBidId = testData.get(BID_ID);

		System.out.println(
				"================= START [BSHW-051] BRET Web Details > OPPTY# (OPRA bids): OPPTY# displayed on Flagging and Bid data tab ==================\n");

		System.out.println("You are currently at My Task Tab");

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.enterBid(newlyLoadedBidId);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		searchPage.clickHere();
		Thread.sleep(5000);

		int chainBidDetails = archivePage.searchRowIndex(driver, testData.get(BID_ID));

		driver.findElement(By.xpath(
				".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div[" + chainBidDetails + "]/table/tbody/tr/td[4]/span"))
				.click();
		System.out.println("Navigating to Bid Details Page");

		/* GET the control of the current window */
//		String winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(5000);

		String expectedFlagAndBidOPPTYnum = testData.get(OPPTY);
		String actualFlagAndBidOPPTYnum = flagAndbidPage.getValueAtIndexBidDataGrid(driver,	FlaggingBidDataPage.ROW_OPPTY_NUMBER);

		System.out.println("Verify that Opportunity Number of Flagging and Bid Data Tab and Remediation Log Tab should have a value.\n");
		System.out.println("Expected Opportunity Number: " + expectedFlagAndBidOPPTYnum);
		System.out.println("Actual Opportunity Number  : " + actualFlagAndBidOPPTYnum);
		System.out.println();
		
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		Assert.assertEquals(actualFlagAndBidOPPTYnum, expectedFlagAndBidOPPTYnum);
		
		System.err.println();
		/* NAVIGATE to Remediation Log */
		detailPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		Thread.sleep(5000);

		String expectedRemedLogOPPTYnum = testData.get(OPPTY);
		String actualRemedLogOPPTYnum = remediationPage.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_OPPTY);

		System.out.println("\nExpected OPPTY #: " + expectedRemedLogOPPTYnum);
		System.out.println("Actual OPPTY #  : " + actualRemedLogOPPTYnum);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
		Assert.assertEquals(actualRemedLogOPPTYnum, expectedRemedLogOPPTYnum);

	}
	
	/**
	 * Step/s:
	 *
	 * 1. Locate OO and BID MGR fields
	 *
	 */
	@Test(priority = 2, description = "[BSHW-052] BRET Web Details > Bid Manager and Opportunity Owner (OPRA bids): Loaded bid with value on Bid Manager and Opportunity Owner")
	public void testWithBMandOO() throws InterruptedException {
		remediationPage = new RemediationLogPage(driver);
		
		String tcNumber = "TC_BSHW-052";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		
		System.out.println();
		System.out.println(
				"================= START [BSHW-052] BRET Web Details > Bid Manager and Opportunity Owner (OPRA bids): Loaded bid with value on Bid Manager and Opportunity Owner ==================");
		
		/* NAVIGATE to Remediation Log */
		detailPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		Thread.sleep(5000);
		
		String expectedBM = testData.get(BM).replace("\"", "");
		String expectedOO = testData.get(OO);
		
		String actualBM = getContactsSubTableValue(RemediationLogPage.ROW_CONTACTS_BID_MGR);
		String actualOO = getContactsSubTableValue(RemediationLogPage.ROW_CONTACTS_OO);
		 
		System.out.println("Verify that values on the OO and BID MGR fields should have a value.\n");
		System.out.println("Expected Opportunity Owner: " + expectedOO);
		System.out.println("Actual  Opportunity Owner : " + actualOO);
		System.out.println("--------------------------------------------------------");
		System.out.println("Expected Bid Manager: " + expectedBM);
		System.out.println("Actual Bid Manager  : " + actualBM);
		
		System.out.println();
		
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
		Assert.assertEquals(actualBM, expectedBM);
		Assert.assertEquals(actualOO, expectedOO);
	
	}
	/**
	* Step/s:
	*
	* 1. Locate the T3 ID/NAME and T4 ID/NAME fields
	*
	* 2. Click the Flagging and Bid Data Tab
	*
	* 3. Locate the Additional BP (CEID) field
	*/
	@Test(priority = 3, description = "[BSHW-053] BRET Web Details > CEID T3/4 (OPRA bids): CEID T3/4 displayed on Flagging and Bid data tab")
	public void testWithCEID3and4() throws InterruptedException {
		remediationPage = new RemediationLogPage(driver);
	
		String tcNumber = "TC_BSHW-053";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());	
		
		System.out.println(
					"================= START [BSHW-053] BRET Web Details > CEID T3/4 (OPRA bids): CEID T3/4 displayed on Flagging and Bid data tab");
	
		String expectedT3Name = testData.get(T3NAME).replace("\"", "");
		String expectedT3Id = testData.get(T3ID);
		String expectedT4Name = testData.get(T4NAME).replace("\"", "");
		String expectedT4Id = testData.get(T4ID);
		
		String actualT3Name = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T3_NAME);
		String actualT3Id = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T3_ID);
		String actualT4Name = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T4_NAME);
		String actualT4Id = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T4_ID);
	
		System.out.println("Verify that values on the CEID T3/T4 ID/NAME and Additional BP (CEID) fields should have a value.");
		System.out.println();
		System.out.println("Expected T3 Name: " + expectedT3Name);
		System.out.println("Actual T3 Name  : " + actualT3Name);
		System.out.println();
		System.out.println("Expected T3 ID: " + expectedT3Id);
		System.out.println("Actual T3 ID  : " + actualT3Id);
		System.out.println("--------------------------------------------------------");
		System.out.println("Expected T4 Name: " + expectedT4Name);
		System.out.println("Actual T4 Name) : " + actualT4Name);
		System.out.println();
		System.out.println("Expected T4 Id: " + expectedT4Id);
		System.out.println("Actual T4 ID  : " + actualT4Id);
		System.out.println();
	
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
		Assert.assertEquals(actualT3Name, expectedT3Name);
		Assert.assertEquals(actualT3Id, expectedT3Id);
		Assert.assertEquals(actualT4Name, expectedT4Name);
		Assert.assertEquals(actualT4Id, expectedT4Id);
	
	
		/* NAVIGATE to Flagging and Bid Data Page */
		detailPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
	
		String expectedT3T4 = expectedT3Name + " (" + expectedT3Id + ")\n" + expectedT4Name + " (" + expectedT4Id + ")" ;
		String actualNoT3T4 = flagAndbidPage.getValueAtIndexBidDataGrid(driver,
		FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);
	
		System.out.println();
		System.out.println("Expected Additional BP (CEID): " + expectedT3T4);
		System.out.println("Actual Additional BP (CEID)  : " + actualNoT3T4);
		System.out.println();
	
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		Assert.assertEquals(actualNoT3T4, expectedT3T4);

	}
	 
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver,	getScreenshotPath(),BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,	this.getClass()));
	}
	
	private String getContactsSubTableValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_CONTACTS, rowIndex);
	}

	private String getRTMSubTableValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_RTM, rowIndex);
	}
}
