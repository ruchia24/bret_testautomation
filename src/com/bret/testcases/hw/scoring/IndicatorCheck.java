package com.bret.testcases.hw.scoring;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.util.BretTestUtils;

public class IndicatorCheck extends BaseTest {

	String winHandleBefore;
	DetailPage pageSelect = new DetailPage();

	/**
	 * Currently on Bid Details Page now
	 * 
	 * Verify that the Indicators are present on Factors and are backed up by
	 * 
	 * Detail Page detail_page - use to navigate to tab: 1. Flagging and Bid
	 * Data 2. Discount + Margin 3. Bid Q's 4. Support Data 5. Below Clip Bids
	 * 6. Remediation Log
	 * 
	 */

	public boolean aChecker(WebDriver driver, String bid, boolean A) {
		
		this.driver = driver;
		/*
		 * Get the Value at Factors Get value at index: - Bid Margin% and - Bid
		 * Discount%
		 */

		String margin_factor_expected = "MARGIN%";
		String discount_factor_expected = "DISCOUNT%";

		String margin_outlier, discount_outlier, factor;
		boolean aDefault, aActual;

		aActual = A; // Passed Value
		aDefault = true; // Comparator

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/* get value of BID_MARGIN_PERCENT */
		margin_outlier = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_OUTLIER_STAT);

		/* get value of BID_DISCOUNT_PERCENT */
		discount_outlier = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_OUTLIER_STAT);

		/* get value of FACTOR */
		factor = driver.findElement(
				By.xpath(".//*[@id='idx_grid_PropertyFormatter_14']"))
				.getText();

		System.out.println("============ INDICATOR A ============\n");

		if (aActual == aDefault
				&& (factor.contains(discount_factor_expected) && discount_outlier
						.contains("Y"))
				|| (factor.contains(margin_factor_expected) && margin_outlier
						.contains("Y"))) {
			System.out.println("Factors: " + factor);
			System.out.println("Margin Percent flag:   " + margin_outlier);
			System.out.println("Discount Percent flag: " + discount_outlier);
			System.out.println();

		} else if (aDefault != aActual) {
			System.out.println("Indicator A is not Present");
		}
		return true;
	}

	public boolean bChecker(WebDriver driver, String bid, boolean B,
			String tc_num) throws InterruptedException {
		this.driver = driver;
		/*
		 * Get the Value at Factors Get value at index on BID Q tab
		 */
		boolean bDefault, bActual;
		bActual = B; // Passed Value
		bDefault = true; // Comparator

		String factor = driver.findElement(
				By.xpath(".//*[@id='idx_grid_PropertyFormatter_14']"))
				.getText();

		System.out.println("Navigating from Flagging and Bid Data Tab to Bid Q");
		/* Navigate to Bid Q */
		pageSelect.navigateToTab(driver, DetailPageTabsE.BID_Q);
		Thread.sleep(10000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tc_num, DetailPageTabsE.BID_Q.getLabel());

		Thread.sleep(5000);
		
		BidQPage q_page = new BidQPage(driver);

		/*
		 * Get the values on each Review Factor Confidentiality Route to Market
		 * Non Competitive Bid Offshore Payment Terms Bundled Solutions (Always
		 * N/A) Contingency Fee Payments
		 */

		String confidentiality_outlier, routeToMarket_outlier, nonCompetitive_outlier, offshore_outlier, bundledSolutions_outlier, contingency_outlier;

		confidentiality_outlier = q_page.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_CONFIDENTIALITY, BidQPage.COL_OUTLIER_STATUS_VAL);

		routeToMarket_outlier = q_page.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_ROUTE_TO_MARKET, BidQPage.COL_OUTLIER_STATUS_VAL);

		nonCompetitive_outlier = q_page.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_NON_COMPETITIVE_BID,
				BidQPage.COL_OUTLIER_STATUS_VAL);

		offshore_outlier = q_page.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_OFFSHORE_PAYMENT_TERMS,
				BidQPage.COL_OUTLIER_STATUS_VAL);

		bundledSolutions_outlier = q_page
				.getValueAtIndexRelQuestions(driver,
						BidQPage.ROW_BUNDLED_SOLUTIONS,
						BidQPage.COL_OUTLIER_STATUS_VAL);

		contingency_outlier = q_page.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_CONTINGENCY_FEE_PAYMENTS,
				BidQPage.COL_OUTLIER_STATUS_VAL);

		System.out.println("============ INDICATOR B ============\n");

		if (bDefault == bActual
				&& (factor.contains("CONFIDENTIALITY") && confidentiality_outlier
						.contains("Y"))
				|| (factor.contains("ROUTE TO MARKET") && routeToMarket_outlier
						.contains("Y"))
				|| (factor.contains("NON COMPETITIVE BID") && nonCompetitive_outlier
						.contains("Y"))
				|| (factor.contains("OFFSHORE PAYMENT TERMS") && offshore_outlier
						.contains("Y"))
				|| (factor.contains("BUNDLED SOLUTIONS") && bundledSolutions_outlier
						.contains("Y"))
				|| (factor.contains("CONTIGENCY FEE") && contingency_outlier
						.contains("Y"))) {

			System.out.println("BID Q FLAG:");
			System.out.println();
			System.out.println("Factors: " + factor);
			System.out.println("CONFIDENTIALITY flag:          "
					+ confidentiality_outlier);
			System.out.println("ROUTE TO MARKET flag:          "
					+ routeToMarket_outlier);
			System.out.println("NON COMPETITIVE BID flag:      "
					+ nonCompetitive_outlier);
			System.out.println("OFFSHORE PAYMENT TERMS flag:   "
					+ offshore_outlier);
			System.out.println("BUNDLED SOLUTIONS flag:        "
					+ bundledSolutions_outlier);
			System.out.println("CONTINGENCY FEE PAYMENTS flag: "
					+ contingency_outlier);
			System.out.println();

		} else if (bDefault != bActual) {
			System.out.println("Indicator B is not Present");
		}
		Thread.sleep(4000);
		return true;
	}

	public boolean cChecker(WebDriver driver, String bid, boolean C) {
		this.driver = driver;
		/*
		 * Get the Value and Factors Get value at index on BID Q tab
		 */
		boolean cDefault, cActual;
		cActual = C; // Passed Value
		cDefault = true; // Comparator

		BidQPage q_page = new BidQPage(driver);

		/* Get values of OUTLIER Status */
		String confidentiality_distributor_outlier, confidentiality_customer_outlier, investigation_distributor_outlier, investigation_customer_outlier;

		confidentiality_distributor_outlier = q_page.getValueAtIndexHistory(
				driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_OUTLIER_STATUS_1);
		confidentiality_customer_outlier = q_page.getValueAtIndexHistory(
				driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_OUTLIER_STATUS_1);
		investigation_distributor_outlier = q_page.getValueAtIndexHistory(
				driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_OUTLIER_STATUS_2);
		investigation_customer_outlier = q_page.getValueAtIndexHistory(driver,
				BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_OUTLIER_STATUS_2);

		/* Navigate to Flagging and Bid Data Tab */
		System.out.println("Navigating to Flagging and Bid Data Tab");
		pageSelect.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		String factor = driver.findElement(
				By.xpath(".//*[@id='idx_grid_PropertyFormatter_14']"))
				.getText();

		System.out.println("============ INDICATOR C ============\n");
		if (cDefault == cActual
				&& (factor.contains("Tier1 Investigation Findings") && investigation_distributor_outlier
						.contains("Y"))
				|| (factor.contains("Tier2 Investigation Findings") && investigation_customer_outlier
						.contains("Y"))
				|| (factor.contains("Tier1 Confidentiality") && confidentiality_distributor_outlier
						.contains("Y"))
				|| (factor.contains("Tier2 Confidentiality") && confidentiality_customer_outlier
						.contains("Y"))) {
			System.out.println("Factors: " + factor);
			System.out.println("Tier1 Investigation Findings flag:   "
					+ investigation_distributor_outlier);
			System.out.println("Tier2 Investigation Findings flag:   "
					+ investigation_customer_outlier);
			System.out.println("Tier1 Confidentiality flag:          "
					+ confidentiality_distributor_outlier);
			System.out.println("Tier2 Confidentiality flag:          "
					+ confidentiality_customer_outlier);
		} else if (cDefault != cActual) {
			System.out.println("Indicator C is not Present\n");
		}
		return true;
	}

	public boolean dChecker(WebDriver driver, String bid, boolean D) {
		this.driver = driver;
		String factor_expected = "TOTAL BID LEVEL SCORE";
		boolean dDefault, dActual;
		dActual = D; // Passed Value
		dDefault = true; // Comparator

		String total_bidscore_outlier;

		/* */
		String factor = driver.findElement(
				By.xpath(".//*[@id='idx_grid_PropertyFormatter_14']"))
				.getText();

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/* Get status value of Total Bid Score */
		total_bidscore_outlier = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE,
				FlaggingBidDataPage.COL_OUTLIER_STAT);

		System.out.println("============ INDICATOR D ============\n");
		if (dActual == dDefault
				&& (factor.contains(factor_expected) && total_bidscore_outlier
						.contains("Y"))) {
			System.out.println("Factors: " + factor);
			System.out.println("TOTAL BID LEVEL SCORE Outlier Status:   "
					+ total_bidscore_outlier);
		} else if (dDefault != dActual) {
			System.out.println("Indicator D is not Present");
		}
		return true;
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,
						this.getClass()));
	}
}
