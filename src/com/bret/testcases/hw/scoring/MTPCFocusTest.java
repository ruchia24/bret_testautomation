package com.bret.testcases.hw.scoring;

import java.util.List;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.pages.SupportDataPage;
import com.bret.util.BretTestUtils;

/**
 * 
 * 
 * @author CalvinNadua
 * copied by LhoydCastillo - 03/01/2018
 */
public class MTPCFocusTest extends BaseTest {

	private FocusPage focusPage;
	private DetailPage detailPage;
	private SupportDataPage supportdataPage;
	private FlaggingBidDataPage flagAndbid_page;
	private static final String DISC_MARG_FILE = "discMargFilePath";

	private boolean actualFactorA2, actualFactorB2;
	private String actualFactorA3, actualFactorB3, actualFactorC3, actualFactorD3;

	// private FocusPage myFocusPage;
	private static final String BID_ID = "bidId";

	@BeforeTest
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}

	/**
	 * [BSHW-035] MTPC Bids (Above Clip Level): Flagging To verify that the newly
	 * loaded MTPC bid is present in Focus Bid Tab.
	 * 
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web. 2. Login using any user role. 3. Go to Focus Bids Tab 4.
	 * Search for the newly loaded MTPC bid 5. Verify that the newly loaded MTPC bid
	 * is present in Focus Bid Tab.
	 *
	 */

	/**
	 * @throws InterruptedException
	 */

	@Test(priority = 1, description = "[BSHW-035] MTPC Bids (Above Clip Level): Flagging - To verify that the newly loaded MTPC bid is present in Focus Bid Tab.")
	public void mtpcBidsFlagging() throws InterruptedException {

		/* NAVIGATE to FOCUS BIDS TAB */
		focusPage = new FocusPage();
		focusPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
		Thread.sleep(5000);

		/* Get BID_ID from Excel File */
		String tcNumber = "TC_BSHW-035";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("\n\n====== START [BSHW-035] MTPC Bids (Above Clip Level): Flagging ======\n");
		System.out.println("DESCRIPTION: To verify that the newly loaded MTPC bid is present in Focus Bid Tab.\n");
		System.out.println("You are currently at Focus Bids Tab");
		System.out.println("Looking for MTPC Bid in Focus Bids Tab");

		/* Check if MTPC bid is present */
		int rowAtFocusTab = focusPage.searchRowIndex(driver, testData.get(BID_ID));

		/* CLICK on the flag */
		focusPage.clickCheckbox(rowAtFocusTab);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		System.out.println(
				"\n====== END [BSHW-035] MTPC Bids (Above Clip Level): Flagging - To verify that the newly loaded MTPC bid is present in Focus Bid Tab. ======\n");
		Thread.sleep(2000);
		/* YOU ARE AT FOCUS BIDS TAB */

	}

	/**
	 * [BSHW-036] MTPC Bids (Above Clip Level): ABCD indicators on Summary Page To
	 * verify that the ABCD indicators on the summary page is displayed correctly.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web. 2. Login using any user role. 3. Go to Focus Bids Tab 4.
	 * Search for the newly loaded MTPC bid 5. Check the ABCD indicators. 6. Click
	 * Bid Detail of newly loaded bid. 7. Check the factors and compare to the ABCD
	 * indicator of the newly loaded bid
	 *
	 */

	/**
	 * @throws InterruptedException
	 */

	@Test(priority = 2, description = "[BSHW-036] MTPC Bids (Above Clip Level): ABCD indicators on Summary Page - To verify that the ABCD indicators on the summary page is displayed correctly.")
	public void mtpcBidsABCDindicators() throws InterruptedException {

		String tcNumber = "TC_BSHW-036";
		focusPage = new FocusPage();

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println(
				"\n\n====== START [BSHW-036] MTPC Bids (Above Clip Level): ABCD indicators on Summary Page ======\n");
		System.out.println(
				"DESCRIPTION: To verify that the ABCD indicators on the summary page is displayed correctly.\n");
		System.out.println("You are currently at Focus Bids Tab\n");
		System.out.println("Checking the ABCD indicator");

		int rowAtFocusTab = focusPage.searchRowIndex(driver, testData.get(BID_ID));

		/* CHECK if Indicators are present. True=Present, False=Not */
		boolean aActual = focusPage.aHasMarker(rowAtFocusTab);
		boolean bActual = focusPage.bHasMarker(rowAtFocusTab);
		boolean cActual = focusPage.cHasMarker(rowAtFocusTab);
		boolean dActual = focusPage.dHasMarker(rowAtFocusTab);

		/* CHECK for the indicators in Properties File. True=Present, False=Not */
		boolean aExpected = Boolean.valueOf(testData.get("A"));
		boolean bExpected = Boolean.valueOf(testData.get("B"));
		boolean cExpected = Boolean.valueOf(testData.get("C"));
		boolean dExpected = Boolean.valueOf(testData.get("D"));

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		Thread.sleep(5000);

		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();

		// Perform the click operation that opens new window
		focusPage.bidDetails(rowAtFocusTab);

		// Close the first window (Focus Bid)
		driver.switchTo().window(winHandleBefore).close();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		System.out.println("\nYou are currently at Bid Details window\n");

		// Perform the actions on new window (Bids Details)

		String actualFactor = focusPage.bidFactors(driver);

		System.out.println("\nBID ID: " + testData.get(BID_ID));
		System.out.println("Actual Factor/s displayed in Bid Details: " + actualFactor + "\n");

		/* SCREENSHOT, testcase#, currentpage */
		Thread.sleep(5000);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		/* CHECK for the factors in Bid Details. True=Present, False=Not */
		String actualFactorA1[] = { "DISCOUNT", "MARGIN%" };
		String actualFactorB1[] = { "CONFIDENTIALITY", "ROUTE TO MARKET", "CONTINGENCY FEE", "NON COMPETITIVE BID",
				"OFFSHORE PAYMENT TERMS" };

		for (int i = 0; i < actualFactorA1.length; i++) {
			if (actualFactor.contains(actualFactorA1[i])) {
				actualFactorA2 = true;
			} else {
				actualFactorA2 = false;
			}
		}

		for (int i = 0; i < actualFactorB1.length; i++) {
			if (actualFactor.contains(actualFactorB1[i])) {
				actualFactorB2 = true;
			} else {
				actualFactorB2 = false;
			}
		}

		boolean actualFactorA = actualFactorA2;
		boolean actualFactorB = actualFactorB2;
		boolean actualFactorC = actualFactor.contains("FOCUS BP:");
		boolean actualFactorD = actualFactor.contains("TOTAL BID LEVEL SCORE");

		System.out.println("\nCOMPARING THE ABCD INDICATOR AND FACTORS");

		/* DATA INPUT (Expected) */
		System.out.println("\nINDICATOR\tDATA INPUT (Expected)");
		System.out.println("     A:\t\t" + aExpected);
		System.out.println("     B:\t\t" + bExpected);
		System.out.println("     C:\t\t" + cExpected);
		System.out.println("     D:\t\t" + dExpected);

		/* Focus TAB (Actual) */
		System.out.println("\nINDICATOR\tFocus TAB (Actual)");
		System.out.println("     A:\t\t" + aActual);
		System.out.println("     B:\t\t" + bActual);
		System.out.println("     C:\t\t" + cActual);
		System.out.println("     D:\t\t" + dActual);

		/* Factors in Bid Details (Actual) */
		System.out.println("\nINDICATOR\tFactors in Bid Details (Actual)");
		System.out.println("     A:\t\t" + actualFactorA);
		System.out.println("     B:\t\t" + actualFactorB);
		System.out.println("     C:\t\t" + actualFactorC);
		System.out.println("     D:\t\t" + actualFactorD + "\n");

		/* COMPARE the Indicators in Focus Tab and in Properties */

		Assert.assertEquals(aActual, aExpected);
		Assert.assertEquals(bActual, bExpected);
		Assert.assertEquals(cActual, cExpected);
		Assert.assertEquals(dActual, dExpected);

		/* COMPARE the Indicators in Focus Tab and in Bid Details */

		Assert.assertEquals(aActual, actualFactorA);
		Assert.assertEquals(bActual, actualFactorB);
		Assert.assertEquals(cActual, actualFactorC);
		Assert.assertEquals(dActual, actualFactorD);

		System.out.println(
				"\n====== END [BSHW-036] MTPC Bids (Above Clip Level): ABCD indicators on Summary Page ======\n");
		Thread.sleep(2000);
		/* YOU ARE AT BID DETAILS WINDOW */
	}

	/**
	 * [BSHW-037] MTPC Bids (Above Clip Level): Focus factors To verify that correct
	 * focus factors are being flagged and the word "MTPC" is indicated as one of
	 * the "Factors" under Flagging and Bid Data Tab.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web. 2. Login using any user role. 3. Go to Focus Bids Tab 4.
	 * Search for the newly loaded MTPC bid 5. Check the ABCD indicators. 6. Click
	 * Bid Detail of newly loaded bid. 7. Check the focus factor values on Factors
	 * of the "Flagging and Bid Data" Tab
	 *
	 */

	/**
	 * @throws InterruptedException
	 */

	@Test(priority = 3, description = "[BSHW-037] MTPC Bids (Above Clip Level): Focus factors - To verify that correct focus factors are being flagged and the word \"MTPC\" is indicated as one of the \"Factors\" under Flagging and Bid Data Tab.")
	public void mtpcBidsFocusFactor() throws InterruptedException {

		String tcNumber = "TC_BSHW-037";
		focusPage = new FocusPage();

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("\n\n====== START [BSHW-037] MTPC Bids (Above Clip Level): Focus factors ======\n");
		System.out.println(
				"DESCRIPTION: To verify that correct focus factors are being flagged and the word \"MTPC\" is indicated as one of the \"Factors\" under Flagging and Bid Data Tab.\n");
		System.out.println("You are currently at Bid Details Window\n");
		System.out.println("Checking the Factors\n");

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Get and display the list of factors
		String actualFactor = focusPage.bidFactors(driver);

		System.out.println("Actual Factor/s displayed in Bid Details: " + actualFactor + "\n");

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		Thread.sleep(5000);

		/* CHECK for the MTPC Indicator in Properties File. */
		String aMTPCExpected = testData.get("MTPCind");
		if (testData.get("MTPCind") == null) {
			aMTPCExpected = "-";
		}

		/* CHECK for the factors in Properties File. */

		String aFactorExpected = testData.get("A1");
		if (testData.get("A1") == null) {
			aFactorExpected = "-";
		}

		String bFactorExpected = testData.get("B1");
		if (testData.get("B1") == null) {
			bFactorExpected = "-";
		}

		String cFactorExpected = testData.get("C1");
		if (testData.get("C1") == null) {
			cFactorExpected = "-";
		}

		String dFactorExpected = testData.get("D1");
		if (testData.get("D1") == null) {
			dFactorExpected = "-";
		}

		aMTPCExpected = aMTPCExpected.trim();
		aFactorExpected = aFactorExpected.trim();
		bFactorExpected = bFactorExpected.trim();
		cFactorExpected = cFactorExpected.trim();
		dFactorExpected = dFactorExpected.trim();

		/* CHECK for the factors in Bid Details. */
		String actualFactorA1[] = { "DISCOUNT", "MARGIN%" };
		String actualFactorB1[] = { "CONFIDENTIALITY", "ROUTE TO MARKET", "CONTINGENCY FEE", "NON COMPETITIVE BID",
				"OFFSHORE PAYMENT TERMS" };
		String actualFactorC1[] = { "FOCUS BP:" };
		String actualFactorD1[] = { "TOTAL BID LEVEL SCORE" };
		String actualMTPC = "MTPC BID";

		actualFactorA3 = "";
		actualFactorB3 = "";
		actualFactorC3 = "";
		actualFactorD3 = "";

		for (int i = 0; i < actualFactorA1.length; i++) {
			if (actualFactor.contains(actualFactorA1[i])) {
				actualFactorA3 = actualFactorA1[i] + ", " + actualFactorA3;
			}
		}

		for (int i = 0; i < actualFactorB1.length; i++) {
			if (actualFactor.contains(actualFactorB1[i])) {
				actualFactorB3 = actualFactorB1[i] + ", " + actualFactorB3;
			}
		}

		for (int i = 0; i < actualFactorC1.length; i++) {
			if (actualFactor.contains(actualFactorC1[i])) {
				actualFactorC3 = actualFactorC1[i] + ", " + actualFactorC3;
			}
		}

		for (int i = 0; i < actualFactorD1.length; i++) {
			if (actualFactor.contains(actualFactorD1[i])) {
				actualFactorD3 = actualFactorD1[i] + ", " + actualFactorD3;
			}
		}

		String actualFactorA = actualFactorA3;
		if (actualFactorA != "") {
			actualFactorA = actualFactorA3.replace(actualFactorA3.substring(actualFactorA3.length() - 2), "");
		} else {
			actualFactorA = "-";
		}

		String actualFactorB = actualFactorB3;
		if (actualFactorB != "") {
			actualFactorB = actualFactorB3.replace(actualFactorB3.substring(actualFactorB3.length() - 2), "");
		} else {
			actualFactorB = "-";
		}

		String actualFactorC = actualFactorC3;
		if (actualFactorC != "") {
			actualFactorC = actualFactorC3.replace(actualFactorC3.substring(actualFactorC3.length() - 2), "");
		} else {
			actualFactorC = "-";
		}

		String actualFactorD = actualFactorD3;
		if (actualFactorD != "") {
			actualFactorD = actualFactorD3.replace(actualFactorD3.substring(actualFactorD3.length() - 2), "");
		} else {
			actualFactorD = "-";
		}

		System.out.println("\nCOMPARING THE ABCD INDICATOR AND FACTORS IN BID DETAILS");

		/* DATA INPUT (Expected) */
		System.out.println("\nINDICATOR\tDATA INPUT (Expected)");
		System.out.println("  MTPC:\t\t" + aMTPCExpected);
		System.out.println("     A:\t\t" + aFactorExpected);
		System.out.println("     B:\t\t" + bFactorExpected);
		System.out.println("     C:\t\t" + cFactorExpected);
		System.out.println("     D:\t\t" + dFactorExpected);

		/* Factors in Bid Details (Actual) */
		System.out.println("\nINDICATOR\tFactors in Bid Details (Actual)");
		System.out.println("  MTPC:\t\t" + actualMTPC);
		System.out.println("     A:\t\t" + actualFactorA);
		System.out.println("     B:\t\t" + actualFactorB);
		System.out.println("     C:\t\t" + actualFactorC);
		System.out.println("     D:\t\t" + actualFactorD + "\n");

		/* COMPARE the factors in Bid Details and in Properties */

		Assert.assertEquals(actualMTPC, aMTPCExpected);
		Assert.assertEquals(actualFactorA, aFactorExpected);
		Assert.assertEquals(actualFactorB, bFactorExpected);
		Assert.assertEquals(actualFactorC, cFactorExpected);
		Assert.assertEquals(actualFactorD, dFactorExpected);

		System.out.println("\n====== END [BSHW-037] MTPC Bids (Above Clip Level): Focus factors ======\n");
		Thread.sleep(2000);
		/* YOU ARE AT BID DETAILS WINDOW */

	}

	/**
	 * [BSHW-038] MTPC Bids (Above Clip Level): Supporting Factors To verify that
	 * score under the supporting factors tab is correctly displayed on the summary
	 * page of the Bid Detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web 2. Login using any user role 3. Go to Focus Bids Tab 4.
	 * Search for the newly loaded MTPC bid 5. Check the ABCD indicators 6. Click
	 * Bid Detail of newly loaded bid 7. Check the Score of Supporting Factors 8.
	 * Click Support Data Tab 9. Compare the Score from Bid Detail's Supporting
	 * Factors to score of Support Data Tab 10. Verify that score under the
	 * supporting factors tab is correctly displayed on the summary page of the Bid
	 * Detail.
	 *
	 */

	/**
	 * @throws InterruptedException
	 */

	@Test(priority = 4, description = "[BSHW-038] MTPC Bids (Above Clip Level): Supporting Factors - To verify that score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.")
	public void mtpcSupportingFactor() throws InterruptedException {

		String tcNumber = "TC_BSHW-038";

		System.out.println("\n\n====== START [BSHW-038] MTPC Bids (Above Clip Level): Supporting Factors ======\n");
		System.out.println(
				"DESCRIPTION: To verify that score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.\n");
		System.out.println("You are currently at Bid Details Window - Flagging and Bid Data Tab\n");
		System.out.println("Checking the Score of Supporting Factors\n");

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/*
		 * actual_score is the score in Flagging and Bid Data Tab (CURRENTLY AT FLAGGING
		 * AND BID DATA TAB)
		 */
		String actual_score = flagAndBid_page.getValueAtIndex(driver, FlaggingBidDataPage.ROW_SUPPORTING_FACTORS,
				FlaggingBidDataPage.COL_SCORE);

		Thread.sleep(3000);

		/* YOU ARE AT FLAGGING AND BID DATA TAB */

		/* NAVIGATE to Support Data Tab to get the scores */
		detailPage = new DetailPage();
		detailPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);

		Thread.sleep(5000);
		supportdataPage = new SupportDataPage(driver);

		/* SCREENSHOT, testcase#, currentpage */
		// takeScreenshot(tcNumber, DetailPageTabsE.SUPPORT_DATA.getLabel());
		supportdataPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
				.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.SUPPORT_DATA.getLabel(), this.getClass()));

		/* expected_score is the score in Support Data Tab */
		int expected_score = supportdataPage.computeTotalScore(driver);

		System.out.println("\nSupporting Factors Score in Flagging and Bid Data Tab (Actual) : " + actual_score);
		System.out.println("Supporting Factors Score in Support Data Tab (Expected)        : " + expected_score);

		Assert.assertTrue(actual_score.equalsIgnoreCase(String.valueOf(expected_score)));

		System.out.println("\n====== END [BSHW-038] MTPC Bids (Above Clip Level): Supporting Factors ======\n");
		Thread.sleep(2000);

		/* YOU ARE AT SUPPORT DATA PAGE */

	}

	/**
	 * [BSHW-039] MTPC Bids (Above Clip Level): Scores To verify that all score
	 * should be added to Total Bid Level Score.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web 2. Login using any user role 3. Go to Focus Bids Tab 4.
	 * Search for the newly loaded MTPC bid 5. Check the ABCD indicators 6. Click
	 * Bid Detail of newly loaded bid 7. Check the Score of Question & BPCOPS 8.
	 * Click Bid Qs Tab 9. Compare the Score from Flagging and Bid Data Tab's
	 * Question & BPCOPS to score of Bid Qs Tab 10. Compute All values under Score
	 * of Flagging and Bid Data Tab except for Total Bid Level Score 11. Verify that
	 * all score should be added to Total Bid Level Score.
	 */

	/**
	 * @throws InterruptedException
	 */

	@Test(priority = 5, description = "[BSHW-039] MTPC Bids (Above Clip Level): Scores - To verify that all score should be added to Total Bid Level Score.")
	public void mtpcScores() throws InterruptedException {

		String tcNumber = "TC_BSHW-039";

		System.out.println("\n\n====== START [BSHW-039] MTPC Bids (Above Clip Level): Scores ======\n");
		System.out.println("DESCRIPTION: To verify that all score should be added to Total Bid Level Score.\n");
		System.out.println("You are currently at Bid Details Window - Support Data Tab\n");

		SupportDataPage supportdata_page = new SupportDataPage(driver);

		/* NAVIGATE to Flagging Bid Data */
		supportdata_page.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		Thread.sleep(15000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/* actual_score is the score in Flagging and Bid Data Tab */
		String total_bidscore_actual = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE, FlaggingBidDataPage.COL_SCORE);

		/* expected_score is the summed up values in Flagging and Bid Data Tab */
		int total_bidscore_expected = flagAndBid_page.getTotalBidLevelScore();

		/* Checking for the Value of the Total Bid Level Score */
		System.out.println("\nTotal Bid Level Score (actual)     : " + total_bidscore_actual);
		System.out.println("Total Bid Level Score (calculated) : " + total_bidscore_expected);
		System.out.println();

		Assert.assertTrue(total_bidscore_actual.equalsIgnoreCase(String.valueOf(total_bidscore_expected)));

		/* get the score value at index of Questions and BPCOPS */
		String qbpcops_actual = flagAndBid_page.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);

		String tier1Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		Thread.sleep(5000);

		System.out.println("Navigating to BID Q Page to get the QBPCOPS Expected Score");
		/* NAVIGATE to Bid Q's to get the score */
		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.BID_Q);

		// ---- > having black page while taking screenshot
		Thread.sleep(10000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());
		Thread.sleep(5000);

		/* qbpcops_expected is the summed up values in BidQ Tab */
		BidQPage bidq_page = new BidQPage(driver);
		int qbpcops_expected = bidq_page.getTotalForBPCops(driver, tier1Ceid.equalsIgnoreCase(tier2Ceid));

		/* Checking for the QBCPCOPS Score */
		System.out.println("\nQuestions & BPCOPS (actual)   : " + qbpcops_actual);
		System.out.println("Questions & BPCOPS (expected) : " + qbpcops_expected);
		System.out.println();

		Assert.assertTrue(qbpcops_actual.equalsIgnoreCase(String.valueOf(qbpcops_expected)));

		detail_page.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		System.out.println("============ END [BSHW-039] MTPC Bids (Above Clip Level): Scores ============\n");

		/* YOU ARE NOW AT FLAGGING BID DATA PAGE */

	}

	/**
	 * [BSHW-040] MTPC Bids (Above Clip Level): Display To verify that correct
	 * details are being displayed in the Bid Detail page.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web 2. Login using any user role 3. Go to Focus Bids Tab 4.
	 * Search for the newly loaded MTPC bid 5. Check the ABCD indicators 6. Click
	 * Bid Detail of newly loaded bid 7. Check the Source System, Bid Date, Country
	 * Name, Region and Primary Brand of Flagging and Bid Data Tab 8. Click the
	 * Remediation Tab 9. Compare values checked from Flagging and Bid Data Tab to
	 * the values under General Bid Info 10. Check the line items on Discount +
	 * Margin Tab
	 */

	/**
	 * @throws InterruptedException
	 */

	@Test(priority = 6, description = "[BSHW-040] MTPC Bids (Above Clip Level): Display - To verify that correct details are being displayed in the Bid Detail page.")
	public void mtpcDisplay() throws InterruptedException {

		String tcNumber = "TC_BSHW-040";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("\n\n====== START [BSHW-040] MTPC Bids (Above Clip Level): Display ======\n");
		System.out.println("DESCRIPTION: To verify that correct details are being displayed in the Bid Detail page.\n");
		System.out.println("You are currently at Flagging Bid Data Page\n");

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Getting expected values from Excel File */
		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		flagAndbid_page = new FlaggingBidDataPage(driver);

		/* Getting expected values from Flagging and Bid DAta Tab */
		String primaryBrand = flagAndbid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND);

		String bidDate = flagAndbid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_BID_DATE);

		String region = flagAndbid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_REGION);

		String country = flagAndbid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_COUNTRY_NAME);

		/* NAVIGATE to Remediation Log */

		DetailPage detail_page2 = new DetailPage();
		detail_page2.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());

		RemediationLogPage remediationLog_page = new RemediationLogPage(driver);

		/* Getting expected values from Remediation Log Tab - General Bid Info */
		String primaryBrand2 = remediationLog_page.getGenBidInforValueAtIndex(driver,
				RemediationLogPage.ROW_PRIMARY_BRAND);

		String bidDate2 = remediationLog_page.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_BID_DATE);

		String region2 = remediationLog_page.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_REGION);

		String country2 = remediationLog_page.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_COUNTRY_NAME);

		System.out.println("\n  DETAILS\tDATA INPUT (Expected)");
		System.out.println("Primary Brand:\t\t" + expectedPrimaryBrand);
		System.out.println("Country Name :\t\t" + expectedCountry);
		System.out.println("Region       :\t\t" + expectedRegion);
		System.out.println("Bid Date     :\t\t" + expectedBidDate);
		System.out.println();

		System.out.println("\n  DETAILS\tFLAGGING AND BID DATA TAB (Actual)");
		System.out.println("Primary Brand:\t\t" + primaryBrand);
		System.out.println("Country Name :\t\t" + country);
		System.out.println("Region       :\t\t" + region);
		System.out.println("Bid Date     :\t\t" + bidDate);
		System.out.println();

		System.out.println("\n  DETAILS\tGENERAL BID INFO (Actual)");
		System.out.println("Primary Brand:\t\t" + primaryBrand2);
		System.out.println("Country Name :\t\t" + country2);
		System.out.println("Region       :\t\t" + region2);
		System.out.println("Bid Date     :\t\t" + bidDate2);
		System.out.println();

		/* Comparing Data Input (Expected) to Flagging and Bid Data Tab (Actual) */

		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		/* Comparing Flagging and Bid Data Tab to GENERAL BID INFO */

		Assert.assertEquals(primaryBrand, primaryBrand2);
		Assert.assertEquals(bidDate, bidDate2);
		Assert.assertEquals(region, region2);
		Assert.assertEquals(country, country2);

		/* NAVIGATE to Bid Margin And Discount */

		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);
		boolean isDiscMargValid = checkDiscountMarginValidity(testData.get(DISC_MARG_FILE), tcNumber);

		Assert.assertTrue(isDiscMargValid);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());

		System.out.println("============ END [BSHW-040] MTPC Bids (Above Clip Level): Display ============\n");

	}

	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) {
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);

		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(),
				BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}

	@AfterTest
	public void exit() throws InterruptedException {
		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);
		driver.quit();
	}

}
