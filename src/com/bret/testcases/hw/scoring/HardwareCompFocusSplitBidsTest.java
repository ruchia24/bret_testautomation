package com.bret.testcases.hw.scoring;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.SupportDataPage;
import com.bret.util.BretTestUtils;

/**
 * @author MarkLouisIgnacio
 * 
 */

public class HardwareCompFocusSplitBidsTest extends BaseTest {

	private static final String BID_ID = "bidId";
	private static final String REVIEWER_DECISION = "reviewerDecision";
	private static final String DISC_MARG_FILE = "discMargFilePath";
	private static final int ROW_NOT_FOUND = 0;

	private MyTaskPage myTaskPage;
	private FocusPage focusPage;
	private DetailPage detailPage;
	private SupportDataPage supportdataPage;
	private FlaggingBidDataPage flagAndbid_page;

	/**
	 * [BSHW-017] Focus Split Bids (Complete parts): Flagging - To Verify that container bid is the bid with 
	 * highest BP price among the set and that the newly loaded bid is flagged according to BRET's logic.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any user.
	 * 
	 * 3. Search for the newly loaded split bid.
	 * 
	 * 4. Verify that the combined Split bid  is flagged according to BRET's logic.
	 */
	
	@Test(priority=1, description = "[BSHW-017] Focus Split Bids (Complete parts): Flagging - To Verify that container bid is the bid with highest BP price among the set and that the newly loaded bid is flagged according to BRET's logic.\n"
			+ "[BSHW-018] Focus Split Bids (Complete parts): ABCD indicators on Summary Page - To verify that the ABCD indicators on the summary page is displayed correctly.\n"
			+ "[BSHW-019] Focus Split Bids (Complete parts): Focus factors -To verify that the correct focus factors are being flagged and being displayed on Factors under Flagging and Bid Data tab on the summary page of the bid detail.")
	public void focusSplitBidsFlagging() throws InterruptedException {
		
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());
		
		String tcNumber = "TC_BSHW-017_Focus";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		System.out
				.println("================= START [BSHW-017] Focus Split Bids (Complete parts): Flagging ==================\n");
		System.out
				.println("DESCRIPTION: To verify that container bid is the bid with highest BP price among the set and that the newly loaded bid is flagged according to BRET's logic.\n");

		myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		
		/* NAVIGATE to FOCUS BIDS TAB, check if bid is present */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
		Thread.sleep(5000);

		focusPage = new FocusPage(driver);
		int rowAtFocusTab = focusPage.searchRowIndex(driver,
				testData.get(BID_ID));

		/* CLICK on the flag */
		focusPage.clickCheckbox(rowAtFocusTab);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		/* get the the value: BID, REVIEWER DECISION - FOCUSTAB */
		String bidAtFocusTab = focusPage.getValueAtIndex(driver, rowAtFocusTab,
				FocusPage.BID_ID_COL);
		String reviewerDecisionAtFocusTab = focusPage.getValueAtIndex(driver,
				rowAtFocusTab, FocusPage.REVIEWER_DECISION_COL); 

		/* SHOW bid values from prop, focus tab */
		System.out.println("\nBID VALUE ON: ");
		System.out.println("Excel file  (Expected) - " + testData.get(BID_ID));
		System.out.println("Focus Tab   (Actual)   - " + bidAtFocusTab);

		/* SHOW Reviewer decision values from prop file, focus tab */
		System.out.println("\nREVIEWER DECISION VALUE ON: ");
		System.out.println("Excel file  (Expected) - "
				+ testData.get(REVIEWER_DECISION));
		System.out.println("Focus Tab   (Actual)   - "
				+ reviewerDecisionAtFocusTab); 

		Assert.assertTrue(rowAtFocusTab > ROW_NOT_FOUND);
		
		//END [BSHW-017] Focus Split Bids (Complete parts): Flagging
		
		
		/**
		 * [BSHW-018] Focus Split Bids (Complete parts): ABCD indicators on Summary Page To verify
		 * that the ABCD indicators on the summary page is displayed correctly.
		 * 
		 * Step/s:
		 * 
		 * 1. Check the ABCD indicators.
		 * 2. Click Bid Detail of newly loaded bid.
		 * 3. Check the factors and compare to the ABCD indicator of the newly loaded bid
		 * 
		 */
		
		
		String tcNumber2 = "TC_BSHW-018_Focus";
		
		Map<String, String> testData2 = BretTestUtils.getTestDataFromExcel(
				tcNumber2, getTestDataSource());
		
		System.out.println("\n\n====== START [BSHW-018] Focus Split Bids (Complete parts): ABCD indicators on Summary Page ======\n");
		System.out.println("DESCRIPTION: To verify that the ABCD indicators on the summary page is displayed correctly.\n");
		
		takeScreenshot(tcNumber2, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		
		/* CHECK if Indicators are present. True=Present, False=Not */
		boolean aActual = focusPage.aHasMarker(rowAtFocusTab);
		boolean bActual = focusPage.bHasMarker(rowAtFocusTab);
		boolean cActual = focusPage.cHasMarker(rowAtFocusTab);
		boolean dActual = focusPage.dHasMarker(rowAtFocusTab);

		/* CHECK for the indicators in Properties File. True=Present, False=Not */
		boolean aExpected = Boolean.valueOf(testData2.get("A"));
		boolean bExpected = Boolean.valueOf(testData2.get("B"));
		boolean cExpected = Boolean.valueOf(testData2.get("C"));
		boolean dExpected = Boolean.valueOf(testData2.get("D"));

		System.out.println("\nBID VALUE ON: ");
		System.out.println("Excel file  (Expected) - " + testData2.get(BID_ID));
		System.out.println("Focus Tab (Actual) - " + bidAtFocusTab);

		System.out.println("\nINDICATOR\tDATA INPUT (Expected)\tFOCUS TAB (Actual)");
		System.out.println("    A:\t\t\t" + aExpected + "\t\t\t" + aExpected);
		System.out.println("    B:\t\t\t" + bExpected + "\t\t\t" + bExpected);
		System.out.println("    C:\t\t\t" + cExpected + "\t\t\t" + cExpected);
		System.out.println("    D:\t\t\t" + dExpected + "\t\t\t" + dExpected);

		/* COMPARE the Indicators in Focus Tab and in Properties */

		Assert.assertEquals(aActual, aExpected);
		Assert.assertEquals(bActual, bExpected);
		Assert.assertEquals(cActual, cExpected);
		Assert.assertEquals(dActual, dExpected);

		// ====== END [BSHW-018] Focus Split Bids (Complete parts): ABCD indicators on Summary Page ======
		Thread.sleep(2000);
		/* YOU ARE AT FOCUS TAB */
		
		/**
		 * [BSHW-019] Focus Split Bids (Complete parts): Focus factors -To verify that the correct focus factors are being flagged 
		 * and being displayed on Factors under Flagging and Bid Data tab on the summary page of the bid detail.
		 * 
		 * Step/s:
		 * 
		 * 1. Check the focus factor values on Factors of the Flagging and Bid Data Tab
		 * 
		 */
		
		String tcNumber3 = "TC_BSHW-019_Focus";
		
		System.out.println("============== START [BSHW-019] Focus Split Bids (Complete parts): Focus factors ==============\n");
		System.out.println("DESCRIPTION: To verify that the correct focus factors are being flagged and being \ndisplayed on Factors under Flagging and Bid Data tab on the summary page of the bid detail.\n");

		
		focusPage.clickDetailLink(driver, rowAtFocusTab);
		Thread.sleep(10000);
		
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		
		takeScreenshot(tcNumber3, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		System.out.println("Navigating to Bid Details Page");
		
		IndicatorCheck focusFactors = new IndicatorCheck();

		System.out.println("\nNow Checking on Indicators");
		/* Evaluate Indicator A */
		boolean aIndicator = focusFactors.aChecker(driver, bidAtFocusTab,
				aActual);

		Thread.sleep(10000);
		/* Evaluate Indicator B */
		boolean bIndicator = focusFactors.bChecker(driver, bidAtFocusTab,
				bActual, tcNumber3);

		Thread.sleep(10000);
		/* Evaluate Indicator C */
		boolean cIndicator = focusFactors.cChecker(driver, bidAtFocusTab,
				cActual);

		Thread.sleep(10000);
		/* Evaluate Indicator D */
		boolean dIndicator = focusFactors.dChecker(driver, bidAtFocusTab,
				dActual);

		Assert.assertEquals(aIndicator, true);
		Assert.assertEquals(bIndicator, true);
		Assert.assertEquals(cIndicator, true);
		Assert.assertEquals(dIndicator, true);
		
		
		// ============ END [BSHW-019] Focus Split Bids (Complete parts): Focus factors ============
		System.out.println();
		/* YOU ARE AT FLAGGING AND BID DATA TAB */
		pausePage();
	}

	/**
	 * [BSHW-020] Focus Split Bids (Complete parts): Supporting Factors - To verify that the score under the supporting factors tab 
	 * is correctly displayed on the summary page of the Bid Detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Check the Score of Supporting Factors
	 * 2. Click Support Data Tab
	 * 3. Compare the Score from Bid Detail's Supporting Factors to score of Support Data Tab
	 * 
	 */

	@Test(priority=2, description = "[BSHW-020] Focus Split Bids (Complete parts): Supporting Factors - To verify that the score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.")
	public void focusSplitBidsSupportingFactors() throws InterruptedException {

		String tcNumber = "TC_BSHW-020_Focus";

		/* YOU ARE AT FLAGGING AND BID DATA TAB */

		System.out
				.println("============ START [BSHW-020] Focus Split Bids (Complete parts): Supporting Factors ============\n");
		System.out
				.println("DESCRIPTION: To verify that the score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.\n");
		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);
		Thread.sleep(3000);
		/*
		 * actual_score is the score in Flagging and Bid Data Tab (CURRENTLY AT
		 * FLAGGING AND BID DATA TAB)
		 */
		String actual_score = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS,
				FlaggingBidDataPage.COL_SCORE);

		Thread.sleep(3000);
		System.out.println("You are currently at Flagging and  Bid Data Tab");
		/* NAVIGATE to Support Data Tab to get the scores */
		//detailPage = new DetailPage();
		detailPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);

		Thread.sleep(5000);
		//supportdataPage = new SupportDataPage(driver);
		
		supportdataPage.scrollingScreenshot(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilenameNoFormat(tcNumber
						+ DetailPageTabsE.SUPPORT_DATA.getLabel(),
						this.getClass()));

		/* expected_score is the score in Support Data Tab */
		int expected_score = supportdataPage.computeTotalScore(driver);

		System.out
				.println("Supporting Factors Score in Flagging and Bid Data Tab (Actual) : "
						+ actual_score);
		System.out
				.println("Supporting Factors Score in Support Data Tab (Expected)      : "
						+ expected_score);

		Assert.assertTrue(actual_score.equalsIgnoreCase(String
				.valueOf(expected_score)));

		//============ END [BSHW-020] Focus Split Bids (Complete parts): Supporting Factors ============

		Thread.sleep(5000);
		/* YOU ARE AT SUPPORT DATA PAGE */
	}
	
	/**
	 * [BSHW-021] Focus Split Bids (Complete parts): Scores - To verify that the score of the bid is being added to the 
	 * total bid score and being displayed correctly on the summary page of bid detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Check the Score of Question & BPCOPS
	 * 2. Click Bid Qs Tab
	 * 3. Compare the Score from Flagging and Bid Data Tab's Question & BPCOPS to score of Bid Qs Tab
	 * 4. Compute All values under Score of Flagging and Bid Data Tab except for Total Bid Level Score
	 * 
	 * */

	@Test(priority=3, description = "[BSHW-021] Focus Split Bids (Complete parts): Scores - To verify that the score of the bid is being added to the total bid score and being displayed correctly on the summary page of bid detail.")
	public void focusBidsScores() throws InterruptedException {
		String tcNumber = "TC_BSHW-021_Focus";
		SupportDataPage supportdata_page = new SupportDataPage(driver);

		/* NAVIGATE to Flagging Bid Data */
		supportdata_page.navigateToTab(driver,
				DetailPageTabsE.FLAGGING_BID_DATA);

		System.out
				.println("============ START [BSHW-021] Focus Split Bids (Complete parts): Scores ============\n");
		System.out
				.println("DESCRIPTION: To verify that the score of the bid is being added to the total bid score and being displayed correctly on the summary page of bid detail.\n");

		Thread.sleep(15000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/* actual_score is the score in Flagging and Bid Data Tab */
		String total_bidscore_actual = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE,
				FlaggingBidDataPage.COL_SCORE);

		/* expected_score is the summed up values in Flagging and Bid Data Tab */
		int total_bidscore_expected = flagAndBid_page.getTotalBidLevelScore();

		/* Checking for the Value of the Total Bid Level Score */
		System.out.println("Total Bid Level Score (actual)     : "
				+ total_bidscore_actual);
		System.out.println("Total Bid Level Score (calculated) : "
				+ total_bidscore_expected);
		System.out.println();

		Assert.assertTrue(total_bidscore_actual.equalsIgnoreCase(String
				.valueOf(total_bidscore_expected)));

		/* get the score value at index of Questions and BPCOPS */
		String qbpcops_actual = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		
		String tier1Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);
		
		Thread.sleep(5000);

		System.out
				.println("Navigating to BID Q Page to get the QBPCOPS Expected Score");
		/* NAVIGATE to Bid Q's to get the score */
		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(10000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		/* qbpcops_expected is the summed up values in BidQ Tab */
		BidQPage bidq_page = new BidQPage(driver);
		int qbpcops_expected = bidq_page.getTotalForBPCops(driver, tier1Ceid.equalsIgnoreCase(tier2Ceid));

		/* Checking for the QBCPCOPS Score */
		System.out.println("\nQuestions & BPCOPS (actual)   : "
				+ qbpcops_actual);
		System.out.println("Questions & BPCOPS (expected) : "
				+ qbpcops_expected);
		System.out.println();

		Assert.assertTrue(qbpcops_actual.equalsIgnoreCase(String
				.valueOf(qbpcops_expected)));

		// ============ END [BSHW-021] Focus Split Bids (Complete parts): Scores ============
		detail_page.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		/* YOU ARE NOW AT FLAGGING BID DATA PAGE */
		pausePage();
	}

	/**
	 * [BSHW-022] Focus Split Bids (Complete parts): Display To verify that correct primary brand (Power / Storage for HW), 
	 * source system, bid date, region, country and line items are being displayed correctly 
	 * on the bid detail page.
	 * 
	 * Step/s:
	 * 
	 * 1. Check the Source System, Bid Date, Country Name, Region and Primary Brand of Flagging and Bid Data Tab
	 * 2. Click the Remediation Tab
	 * 3. Compare values checked from Flagging and Bid Data Tab to the values under General Bid Info
	 * 4. Check the line items on Discount + Margin Tab 
	 *
	 */

	@Test(priority=4, description = "[BSHW-022] Focus Split Bids (Complete parts): Display To verify that correct primary brand (Power / Storage for HW), source system, bid date, region, country and line items are being displayed correctly on the bid detail page.")
	public void focusBidsDisplay()  {

		String tcNumber = "TC_BSHW-022_Focus";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		System.out
				.println("============ START [BSHW-022] Focus Split Bids (Complete parts): Display ============\n");

		/* YOU ARE CURRENTLY AT FLAGGING BID DATA PAGE */

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		/* Getting expected values from Excel File */
		String expectedSourceSystem = testData.get("sourceSystem");
		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		flagAndbid_page = new FlaggingBidDataPage(driver);

		/* Getting expected values from Flagging and Bid DAta Tab */
		String sourceSystem = flagAndbid_page.getValueAtIndexBidDataGrid(
				driver, FlaggingBidDataPage.ROW_SOURCE_SYSTEM);
		
		String primaryBrand = flagAndbid_page.getValueAtIndexBidDataGrid(
				driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND);

		String bidDate = flagAndbid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_BID_DATE);

		String region = flagAndbid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_REGION);

		String country = flagAndbid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_COUNTRY_NAME);

		System.out
				.println("\n  DETAILS\tDATA INPUT (Expected)\tFLAGGING AND BID DATA TAB (Actual)");
		System.out.println("Source System:\t\t" + expectedSourceSystem + "\t\t"
				+ sourceSystem);
		System.out.println("Primary Brand:\t\t" + expectedPrimaryBrand + "\t\t"
				+ primaryBrand);
		System.out.println("Country Name :\t\t" + expectedCountry + "\t\t"
				+ country);
		System.out.println("Region       :\t\t" + expectedRegion + "\t\t"
				+ region);
		System.out.println("Bid Date     :\t\t" + expectedBidDate + "\t\t"
				+ bidDate);
		System.out.println();

		Assert.assertEquals(sourceSystem, expectedSourceSystem);
		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		/* NAVIGATE to Bid Margin And Discount */

		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);
		boolean isDiscMargValid = checkDiscountMarginValidity(testData
				.get(DISC_MARG_FILE), tcNumber);
		
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());
		pausePage();
		
		
		Assert.assertTrue(isDiscMargValid);

		
		// ============ END [BSHW-022] Focus Split Bids (Complete parts): Display ============
	}

	
	
	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) {
		// Added
		// 9/27/2017
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);

		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(), BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}


	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,
						this.getClass()));
	}

	@AfterTest
	public void exit() throws InterruptedException {
		/* Switch to Original Window */
		Thread.sleep(5000);
		driver.quit();
	}
	
}
	
	
	