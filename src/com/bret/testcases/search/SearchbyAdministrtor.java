package com.bret.testcases.search;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETSearchPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

//extends to class BRETmainTestNGbase for the FF launching methods

public class SearchbyAdministrtor extends BRETmainTestNGbase {

	// Initializations
	// reading from data source
	// ADMINISTRATOR account should be use for this test
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
//	 String sheet1 ="Credentials"; //Sheet name on excel
//	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
//	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	
	 // Supply bid details prior to running the code
	 // Flags: E = error, W = Waiting for Pricing
	 
	 String sheet2 ="Bids for Search"; //Sheet name on excel
	 String errorBidFromExcel = Excel.getCellValue (xlpath, sheet2, 4,1);   //error bid
	 String errorBidFlagFromExcel = Excel.getCellValue (xlpath, sheet2, 4,2); 	
	 String waitingForPricingBidFromExcel = Excel.getCellValue (xlpath, sheet2, 5,1);   //Waiting for Prcing bid
	 String waitingForPricingBidFlagFromExcel = Excel.getCellValue (xlpath, sheet2, 5,2); 
	 

	//--------------------------------------------------------------------------------- 
	// [Updated-Belle-9/11/17]
	// Change data inputs from Excel to Property files
	//----------------------------------------------------------------------------------
	String userRole = "Administrator";
	BWPropertiesFiles propRead = new BWPropertiesFiles ();
	String[] bretWebCredentials = propRead.propBW_credentials(userRole);
     
	String userName = bretWebCredentials[0];			
	String userPassword = bretWebCredentials[1];
	
	String errorBid = errorBidFromExcel.trim();
	String errorBidFlag = errorBidFlagFromExcel.trim();
	String waitingForPricingBid = waitingForPricingBidFromExcel.trim();
	String waitingForPricingBidFlag = waitingForPricingBidFlagFromExcel.trim();
	
	@Test
	private void tcErrorBidSearch () throws Exception{
		 //----------------------------- 
		 // SRCH_003: Performs ERROR bid search
	     //----------------------------- 
		
		 //Performs the Login event
		 // calls the method UserLogin from class BRETValidLogin to perform the site login		
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
	    // Perform Error bid search
		BRETSearchPage searchEbid = new BRETSearchPage(driver);
		searchEbid.bidSearchAdministrator(errorBid, errorBidFlag);
		
	    // Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = Search.class.getSimpleName();
		String methodName = ("TC_SRCH-003_" + className);
	    screenShots.screenCaptures(methodName);
	    	  
	}
	
	@Test
	private void tcWaitingForPricingBidSearch () throws Exception{
		 //----------------------------------------- 
		 // SRCH_005: Performs mWaiting for Pricing bid search
	     //----------------------------------------- 
			
	    // Perform Waiting bid search
		BRETSearchPage searchEbid = new BRETSearchPage(driver);
		searchEbid.bidSearchAdministrator(waitingForPricingBid, waitingForPricingBidFlag);
		
	    // Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = Search.class.getSimpleName();
		String methodName = ("TC_SRCH-005_" + className);
	    screenShots.screenCaptures(methodName);
	    	  
	}
}
