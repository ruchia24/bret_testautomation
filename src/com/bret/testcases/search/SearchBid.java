package com.bret.testcases.search;

import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BRETSearchPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.util.BretTestUtils;

/**
 * 
 * 
 * @author LhoydCastillo
 *
 */
public class SearchBid extends BaseTest {

	private static final String FOCUS_BID = "focusBid";
	private static final String FOCUS_BID_FLAG = "focusBidFlag";
	private static final String NONFOCUS_BID = "nonFocusBid";
	private static final String NONFOCUS_BID_FLAG = "nonFocusBidFlag";
	private static final String ERROR_BID = "errorBid";
	private static final String ERROR_BID_FLAG = "errorBidFlag";
	private static final String ARCHIVED_BID = "archivedBid";
	private static final String SQO_WAITING_FOR_PRICING_BID = "waitingForPricingBid";
	private static final String SQO_WAITING_FOR_PRICING_BID_DECISION = "waitingForPricingReviewerDecision";
	private static final String NOT_YET_LOADED_BID = "notYetLoadedBid";
	private static final String NOT_YET_LOADED_BID_FLAG = "notYetLoadedBidFlag";
	private static final String INVALID_BID = "invalidBid";
	private static final String INVALID_BID_FLAG = "invalidBidFlag";
	private static final String NO_INPUT_BID_ALERT = "noIputBidAlertMessage";

	private MyTaskPage myTaskPage;
	private BRETSearchPage searchPage;
	private FocusPage focusPage;
	private NonFocusPage nonFocusPage;
	private ArchivePage archivePage;

	@BeforeTest
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		// loginPage.userLogin(getReviewerUsername(), getReviewerPassword());
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}

	/**
	 * 1. Launch BRET web. 2. Login using any role. 3. Go to Search tab. 4.
	 * Enter valid focus bid id. 5. Click Search button. 6. Click "here"
	 * hyperlink
	 * 
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 1)
	public void searchFocus() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
		focusPage = new FocusPage();

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-001";
		String description = "FOCUS bids can be searched";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String focusBid = testData.get(FOCUS_BID);
		String focusBidFlag = testData.get(FOCUS_BID_FLAG);

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.enterBid(focusBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		searchPage.clickHere();
		Thread.sleep(5000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		Thread.sleep(2000);

		String resultBidID = focusPage.getSearchResultValue(driver,
				FocusPage.BID_ID_COL);
		String resultBidFlag = focusPage.getSearchResultValue(driver,
				FocusPage.FLAG_COL);

		System.out.println("Focus Bid from Source File   : " + focusBid);
		System.out.println("Focus Bid from Search REsult : " + resultBidID);

		System.out.println("Focus Bid flag from Source File   : "
				+ focusBidFlag);
		System.out.println("Focus Bid flag from Search REsult : "
				+ resultBidFlag);

		Assert.assertEquals(focusBid, resultBidID);
		Assert.assertEquals(resultBidFlag, focusBidFlag);
		Thread.sleep(5000);

	}

	/**
	 * 1. Go back to Search tab. 2. Enter valid non focus bid id. 3. Click
	 * Search button. 4. Click "here" hyperlink
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 2)
	public void searchNonFocus() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
		nonFocusPage = new NonFocusPage();
		focusPage = new FocusPage();

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-002";
		String description = "Non Focus bids can be search";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String nonFocusBid = testData.get(NONFOCUS_BID);
		String nonFocusBidFlag = testData.get(NONFOCUS_BID_FLAG);

		focusPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.clearSearch();
		Thread.sleep(3000);

		searchPage.enterBid(nonFocusBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		searchPage.clickHere();
		Thread.sleep(5000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());
		Thread.sleep(2000);

		String resultBidID = nonFocusPage.getSearchResultValue(driver,
				NonFocusPage.BID_ID_COL);
		String resultBidFlag = nonFocusPage.getSearchResultValue(driver,
				NonFocusPage.FLAG_COL);

		System.out.println("Non Focus Bid from Source File   : " + nonFocusBid);
		System.out.println("Non Focus Bid from Search Result : " + resultBidID);

		System.out.println("Non Focus Bid flag from Source File   : "
				+ nonFocusBidFlag);
		System.out.println("Non Focus Bid flag from Search Result : "
				+ resultBidFlag);

		Assert.assertEquals(nonFocusBid, resultBidID);
		Assert.assertEquals(nonFocusBidFlag, resultBidFlag);
		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Login using Administrator role. (Log Out to BRET Webif not
	 * Administrator Role and request to change ) 2. Go to Search tab. 3. Enter
	 * error bid id. 4. Click Search button. 5. Click "here" hyperlink
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3)
	public void searchError() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
		nonFocusPage = new NonFocusPage();

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-003";
		String description = "Error bids can be searched under my task tab by admin user.";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String errorBid = testData.get(ERROR_BID);
		String errorBidFlag = testData.get(ERROR_BID_FLAG);

		nonFocusPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.clearSearch();
		Thread.sleep(3000);

		searchPage.enterBid(errorBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		searchPage.clickHere();
		Thread.sleep(5000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.MY_TASK.getLabel());
		Thread.sleep(2000);

		String resultBidID = myTaskPage.getSearchResultValue(driver,
				MyTaskPage.BID_ID_COL);
		String resultBidFlag = myTaskPage.getSearchResultValue(driver,
				MyTaskPage.FLAG_COL);

		System.out.println("Error Bid from Source File   : " + errorBid);
		System.out.println("Error Bid from Search Result : " + resultBidID);

		System.out.println("Error Bid flag from Source File   : "
				+ errorBidFlag);
		System.out.println("Error Bid flag from Search Result : "
				+ resultBidFlag);

		Assert.assertEquals(errorBid, resultBidID);
		Assert.assertEquals(errorBidFlag, resultBidFlag);
		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Go back to Search tab. 2. Enter a valid bid id which is already
	 * released. 3. Click Search button. 4. Click "here" hyperlink
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 4)
	public void searchArchived() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
		archivePage = new ArchivePage();

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-004";
		String description = "Archive bids can be search";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");
		
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String archiveBid = testData.get(ARCHIVED_BID);
		Thread.sleep(3000);

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.clearSearch();
		Thread.sleep(3000);

		searchPage.enterBid(archiveBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		searchPage.clickHere();
		Thread.sleep(5000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.ARCHIVE.getLabel());
		Thread.sleep(2000);

		String resultBidID = archivePage.getSearchResultValue(driver,
				ArchivePage.BID_ID_COL);

		System.out.println("Archived Bid from Source File   : " + archiveBid);
		System.out.println("Archived Bid from Search Result : " + resultBidID);

		Assert.assertEquals(archiveBid, resultBidID);
		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Go back to Search tab. 2. Enter a SQO bid with waiting for pricing
	 * status. 3. Click Search button. 4. Click "here" hyperlink
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5)
	public void searchWaitingForPricing() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
		archivePage = new ArchivePage();

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-005";
		String description = "SQO waiting for pricing bids can be search";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String waitingForPricingBid = testData.get(SQO_WAITING_FOR_PRICING_BID);
		String bidReviewerDecision = testData
				.get(SQO_WAITING_FOR_PRICING_BID_DECISION);

		archivePage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.clearSearch();
		Thread.sleep(3000);

		searchPage.enterBid(waitingForPricingBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		searchPage.clickHere();
		Thread.sleep(5000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.MY_TASK.getLabel());
		Thread.sleep(2000);

		String resultBidID = myTaskPage.getSearchResultValue(driver,
				ArchivePage.BID_ID_COL);
		String resultBidReviewerDecision = myTaskPage.getSearchResultValue(
				driver, ArchivePage.REVIEWER_DECISION_COL);

		System.out.println("Waiting for Pricing Bid from Source File   : "
				+ waitingForPricingBid);
		System.out.println("Waiting for Pricing Bid from Search Result : "
				+ resultBidID);

		System.out.println("Reviewer Decision of Bid from Source File   : "
				+ bidReviewerDecision);
		System.out.println("Reviewer Decision of Bid from Search Result : "
				+ resultBidReviewerDecision);

		Assert.assertEquals(waitingForPricingBid, resultBidID);
		Assert.assertEquals(bidReviewerDecision, resultBidReviewerDecision);

		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Go back to Search tab. 2. Enter a valid bid id which is not yet loaded
	 * in BRET web. 3. Click Search button.
	 * 
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 6)
	public void searchValidBid() throws InterruptedException {
		searchPage = new BRETSearchPage(driver);
		myTaskPage = new MyTaskPage();

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-006";
		String description = "Valid bids but cannot be found in BRET web";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String notYetLoadedBid = testData.get(NOT_YET_LOADED_BID);
		String notYetLoadedBidFlag = testData.get(NOT_YET_LOADED_BID_FLAG);

		// archivePage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.clearSearch();
		Thread.sleep(3000);

		searchPage.enterBid(notYetLoadedBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		String resultFlag = driver.findElement(
				By.xpath(".//*[@id='locateBidDiv']/div")).getText();

		Assert.assertEquals(true, resultFlag.contains(notYetLoadedBidFlag));
		Thread.sleep(5000);

		System.out.println("Bid from Source File   : " + notYetLoadedBid);
		System.out.println("Bid's expected flag is : " + notYetLoadedBidFlag);
		System.out.println("Bid's actual flag is   : " + resultFlag);

		Assert.assertEquals(true, resultFlag.contains(notYetLoadedBidFlag));

		Thread.sleep(5000);
	}

	/**
	 * 
	 * 1. Go back to Search tab. 2. Enter an invalid bid id. 3. Click Search
	 * button.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 7)
	public void searchInvalidBid() throws InterruptedException {
		searchPage = new BRETSearchPage(driver);

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-007";
		String description = "Invalid bids that was entered for search";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String invalidBid = testData.get(INVALID_BID);
		String invalidBidFlag = testData.get(INVALID_BID_FLAG);

		archivePage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.clearSearch();
		Thread.sleep(3000);

		searchPage.enterBid(invalidBid);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		String resultFlag = driver.findElement(
				By.xpath(".//*[@id='locateBidDiv']/div")).getText();

		Assert.assertEquals(true, resultFlag.contains(invalidBidFlag));
		Thread.sleep(5000);

		System.out.println("Bid from Source File   : " + invalidBid);
		System.out.println("Bid's expected flag is : " + invalidBidFlag);
		System.out.println("Bid's actual flag is   : " + resultFlag);

		Assert.assertEquals(true, resultFlag.contains(invalidBidFlag));

		Thread.sleep(5000);
	}

	/**
	 * 
	 * 1. Clear the Search Box. 2. Click Search button. 3. Click OK button to
	 * Start another SEARCH task
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 8)
	public void noBidEntered() throws InterruptedException {
		searchPage = new BRETSearchPage(driver);

		/* Get the test data from excel */
		String tcNumber = "TC_SRCH-001-008";
		String testCase = "SRCH-008";
		String description = "No Bid ID entered to search";

		System.out.println("\n============ [" + testCase + "] "
				+ description + " ============\n");

		
		myTaskPage = new MyTaskPage();
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		String noIputBidAlertMessage = testData.get(NO_INPUT_BID_ALERT);

		searchPage.clearSearch();
		Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		Thread.sleep(2000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		// /* Take Screenshot */
		// takeScreenshot(testCase, SummaryPageTabsE.SEARCH.getLabel());
		// Thread.sleep(2000);

		// Switching to Alert
		Alert alert = driver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = driver.switchTo().alert().getText();

		// Displaying alert message
		System.out.println("Bid's expected alert message is : "
				+ noIputBidAlertMessage);
		System.out.println("Bid's actual alert message is   : " + alertMessage);

		/**
		 * TODO: Add screen capture for ALERT
		 * 
		 */

		Thread.sleep(5000);

		// Accepting alert
		alert.accept();
		Thread.sleep(5000);

		Assert.assertTrue(alertMessage.contains(noIputBidAlertMessage));
		
		System.out.println("\n====================================\n");
	}

	/**
	 * Method to take screenshot.
	 * 
	 * @param testCase
	 * @param tabLabel
	 */
	private void takeScreenshot(String testCase, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(testCase + "_" + tabLabel,
						this.getClass()));
	}

}
