package com.bret.testcases;

import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.DetailedReportPage;
import com.bret.pages.FocusPage;
import com.bret.pages.GoeDeterminationPage;
import com.bret.pages.ManualReviewPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SummaryPage;
import com.bret.pages.UserDetailPage;
import com.bret.pages.UserManagementPage;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;

public class testGetText extends BaseTest{
	private MyTaskPage myTaskPage;
	private SummaryPage summaryPage;
	private NonFocusPage nonFocusPage;
	private FocusPage focusPage;
	private ArchivePage archivePage;
	private AdvancedSearchPage advancedSearch;
	private ManualReviewPage manualReviewPage;
	private GoeDeterminationPage goeDeterminationPage;
	private UserManagementPage userManagementPage;
	private UserDetailPage userDetailPage;
	private DetailedReportPage detailedReportPage;

	@BeforeTest
	public void loginAsReaderDetail()  throws TimeoutException, InterruptedException, ExecutionException  {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
		
	
		try {
			WebDriverWait wait=new WebDriverWait(driver, 100);
			wait.until(ExpectedConditions.titleContains("Special Bid Risk Analysis - Bid Summary"));
	
		} catch (Exception e) {
			throw new TimeoutException("Invalid Username or Password");
		}
	}
	


@Test(priority = 1, enabled=false)
public void showAccessDetailsSourceSystem() throws InterruptedException {
	String tcNumber = "TC_BWRLR-001 ";
	displayLogHeader(tcNumber);
	Thread.sleep(7000);
	myTaskPage = new MyTaskPage();
	myTaskPage.clickUserName(driver);
	myTaskPage.clickShowMyPrivilege(driver);
	Thread.sleep(3000);
	
//	WebElement e1= driver.findElement(By.xpath(MyTaskPage.XPATH_SHOW_MY_PRIVILEGE));
//	
//	String s1=e1.getText();
//	System.out.println(e1.getText());
	boolean checkPrivilegeText=myTaskPage.getMyPrivilegeText(driver, "Role: Lead Reviewer");
	Thread.sleep(2000);
	
	try 
		{
			Assert.assertTrue(checkPrivilegeText);
			System.out.println("PASSED - Access administrator logged in successfully");
		
	} catch (Exception e) {
		System.out.println("Failed - logged in user is not access administrator");
		
	}
	WebElement closeButton = driver.findElement(By.xpath("//*[@id='dijit_form_Button_2_label']"));
	closeButton.click();
}
	@Test(priority = 4, enabled=false)
	public void archiveNonFocusBidAtMyTaskTab() throws InterruptedException {
	
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRLR-004 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);
		boolean clickOnArchive= myTaskPage.searchBidsInMyTaskAndClickArchive(driver, "Release");

		Thread.sleep(5000);
		//takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		try {
			Assert.assertTrue(clickOnArchive);
			System.out.println("PASSED - User is able to click on Archive button");
		} catch (AssertionError e) {
			System.out.println("FAILED - No Non-focus bids available to archive which is Not as Expected");
			throw e;
		}
	}
	@Test(priority = 7, enabled=false)
	public void changeErrorToFocusAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRLR-007 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);
		System.out.println(bidFieldText);
	    Thread.sleep(3000);
	    boolean flagStatus= myTaskPage.searchBidsByFlagInMyTaskandClickReadyToReview(driver, "E");
	    Thread.sleep(2000);
		//takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		try {
			//Assert.assertTrue(bidFieldText.contains("No items to display"));
			Assert.assertTrue(flagStatus);
			System.out.println("PASSED -  \"Error\" bids available to change into \"Focus\" as Expected");
		} catch (AssertionError e) {
			System.out.println("FAILED - There are no bid/s displayed which is Not as Expected");
			throw e;
		}
	}
	@Test(priority = 8)
	public void exportSummaryTableToCSVAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRLR-008 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		//takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		myTaskPage.searchBidsByFlagAndReviewedDecisionInMyTask(driver, "Y", "New");
	//	boolean exportButtonIsDisplayed = myTaskPage.exportButtonIsDisplayed(driver);
		boolean clickOnExportToCSVandSave= myTaskPage.ClickExportToExcelAndSaveTheFile(driver);
		Thread.sleep(2000);
		
		try {
			Assert.assertTrue(clickOnExportToCSVandSave);
			//Assert.assertFalse(exportButtonIsDisplayed);
			System.out.println("PASSED - Export To CSV Button is  Displayed and save as Expected");
		} catch (AssertionError e) {
			//System.out.println("FAILED - Export Button is displayed which is Not as Expected");
			System.out.println("FAILED - Export Button is not displayed which is Not as Expected");
			throw e;
		}
	}
}


