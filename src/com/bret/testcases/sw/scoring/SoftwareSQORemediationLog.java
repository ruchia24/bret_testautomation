package com.bret.testcases.sw.scoring;

import java.util.Map;

//import junit.framework.Assert;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

/**
 * @author CheyenneFreyLazo
 *
 */
public class SoftwareSQORemediationLog extends BaseTest {

	private static final String BLANK = "<Blank>";
	private MyTaskPage myTaskPage;
	private RemediationLogPage remediationPage;
	private FlaggingBidDataPage flaggingBidPage;

	private static final String BID_ID = "bidId";
	private static final String EMPTY = "";

	@Test(priority = 1)
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());
	}

	/**
	 * To verify that CEID T3/4 value will display as blank for SQO bids.
	 * 
	 * 
	 * 
	 * Step/s:
	 * 
	 * A. Load a new SQO bid.
	 * 
	 * 
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using a role that has access to Bid details.
	 * 
	 * 3. Go to Bid Details page ofthe bid in stepA.
	 * 
	 * 4. Go to Remediation log.
	 * 
	 * 5. Locate T3 NAME: , T3 ID:, T4NAME: and T4ID
	 */
	@Test(priority = 2)
	public void testRemediationCeidDisplay() {
		/* Test case number */
		
		String tcNumber = "TC_BSSW-020";
		navigateToRemediationLog(tcNumber);

		remediationPage = new RemediationLogPage(driver);

		String t3Name = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T3_NAME);
		String t3Id = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T3_ID);
		String t4Name = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T4_NAME);
		String t4Id = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T4_ID);

		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== "
				+ DetailPageTabsE.REMEDIATION_LOG.getLabel()
				+ " tab ==========");
		System.out
				.println("Verify that CEID T3/4 value will display as blank for SQO bids.");
		System.out.println();
		System.out.println("Expected T3 Name: " + addBlankIndicator(EMPTY));
		System.out.println("T3 Name (Display): " + addBlankIndicator(t3Name));
		System.out.println();
		System.out.println("Expected T3 Id: " + addBlankIndicator(EMPTY));
		System.out.println("T3 Id (Display): " + addBlankIndicator(t3Id));
		System.out.println();
		System.out.println("Expected T4 Name: " + addBlankIndicator(EMPTY));
		System.out.println("T4 Name (Display): " + addBlankIndicator(t3Name));
		System.out.println();
		System.out.println("Expected T4 Id: " + addBlankIndicator(EMPTY));
		System.out.println("T4 Id (Display): " + addBlankIndicator(t3Id));
		System.out.println();
		
		Assert.assertEquals(EMPTY, t3Name);
		Assert.assertEquals(EMPTY, t3Id);
		Assert.assertEquals(EMPTY, t4Name);
		Assert.assertEquals(EMPTY, t4Id);

		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());

	}

	/**
	 * To verify that Bid Manager and Opportunity Owner value will display as
	 * blank for SQO bids.
	 * 
	 * Step/s:
	 * 
	 * A. Load a new SQO bid.
	 * 
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using a role that has access to Bid details.
	 * 
	 * 3. Go to Bid Details page ofthe bid in stepA.
	 * 
	 * 4. Go to Remediation log.
	 * 
	 * 5. LocateOO and BID MGR fields
	 */
	@Test(priority = 3)
	public void testRemediationOOBidMgrDisplay() {
		/* Test case number */
		String tcNumber = "TC_BSSW-021";

		String opporOwner = getContactsSubTableValue(RemediationLogPage.ROW_CONTACTS_OO);
		String bidMgr = getContactsSubTableValue(RemediationLogPage.ROW_CONTACTS_BID_MGR);

		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== "
				+ DetailPageTabsE.REMEDIATION_LOG.getLabel()
				+ " tab ==========");
		System.out
				.println("Verify that Bid Manager and Opportunity Owner value will display as blank for SQO bids.");

		System.out.println();
		System.out.println("Expected Opportunity Owner: "
				+ addBlankIndicator(EMPTY));
		System.out.println("Opportunity Owner (Display): "
				+ addBlankIndicator(opporOwner));
		System.out.println();
		System.out.println("Expected Bid Manager: " + addBlankIndicator(EMPTY));
		System.out.println("Bid Manager (Display): "
				+ addBlankIndicator(bidMgr));
		System.out.println();

		Assert.assertEquals(EMPTY, opporOwner);
		Assert.assertEquals(EMPTY, bidMgr);

		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
	}

	/**
	 * To verify thatOPPTY# value will display as blank for SQO bids.
	 * 
	 * 
	 * Step/s:
	 * 
	 * A. Load a new SQO bid.
	 * 
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using a role that has access to Bid details.
	 * 
	 * 3. Go to Bid Details page ofthe bid in stepA.
	 * 
	 * 4. Go to Remediation log.
	 * 
	 * 5. LocateOPPTY# field,
	 */
	@Test(priority = 4)
	public void testRemediationOpptyNum() {
		
		/* Test case number */
		String tcNumber = "TC_BSSW-022";

		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== "
				+ DetailPageTabsE.REMEDIATION_LOG.getLabel()
				+ " tab ==========");
		System.out
				.println("Verify thatOPPTY# value will display as blank for SQO bids.");

		String opptyNum = remediationPage.getGenBidInforValueAtIndex(driver,
				RemediationLogPage.ROW_GEN_BID_OPPTY);

		System.out.println();
		System.out.println("Expected Opportunity Number: "
				+ addBlankIndicator(EMPTY));
		System.out.println("Opportunity Number (Display): "
				+ addBlankIndicator(opptyNum));
		System.out.println();
		
		Assert.assertEquals(EMPTY, opptyNum);
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
	}

	private String getContactsSubTableValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_CONTACTS, rowIndex);
	}

	private String getRTMSubTableValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_RTM, rowIndex);
	}

	private void navigateToRemediationLog(String tcNumber) {
		myTaskPage = new MyTaskPage();

		String sheetName = "TC_BSSW-020-022 (Remediation)";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				sheetName, getTestDataSource());

		String testDataBidId = testData.get(BID_ID);

		/* Now lets search for the newly loaded bid */
		int myTaskRowIndex = myTaskPage.searchRowIndex(driver, testDataBidId);
		

		System.out.println();
		System.out.println("Clicking the detail link");
		System.out.println("Opening Detail page" + "\nCurrent detail tab is: "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		myTaskPage.clickDetailLink(driver, myTaskRowIndex);

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		setupNewURL(driver.getCurrentUrl());

		flaggingBidPage = new FlaggingBidDataPage(driver);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
	}

	private String addBlankIndicator(String str) {
		if (str.isEmpty()) {
			return BLANK;
		}

		return str;
	}

	/**
	 * Method to take screenshot.
	 * 
	 * @param tcNumber
	 * @param tabLabel
	 */
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,
						this.getClass()));
	}

}
