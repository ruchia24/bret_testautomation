package com.bret.testcases.sw.scoring;

import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.SupportDataPage;
import com.bret.util.BretTestUtils;

public class SoftwareNonFocusTest extends BaseTest {
	private static final String BID_ID = "bidId";
	private static final String SUPP_FACT_SCORE = "suppFactScore";
	private static final String DISC_MARG_FILE = "discMargFilePath";

	private static final int ROW_NOT_FOUND = 0;

	private MyTaskPage myTaskPage;
	private ArchivePage archivePage;
	private FlaggingBidDataPage flaggingBidPage;
	private BidQPage bidQPage;
	private SupportDataPage suppDataPage;

	private int archiveRowIndex;

	@Test(priority = 1)
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
//		loginPage.userLogin(getLegalUsername(), getLegalPassword());
	}

	/**
	 * To verify that newly loaded SQO non focus bid is automatically released to
	 * archive.
	 * 
	 * 
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any role.
	 * 
	 * 3. Go to Archive/Non focus Tab.
	 * 
	 * 4. Search for the newly loaded SQO non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Archive tab.
	 * 
	 * 
	 */
	@Test(priority = 2)
	private void testNonFocusFlagging() {
		myTaskPage = new MyTaskPage();
		archivePage = new ArchivePage();

		/* Get the test data from excel */
		String tcNumber = "TC_BSSW-001";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println();
		System.out.println("==================== Test Item: " + tcNumber + " ====================");
		System.out.println(
				"DESCRIPTION: Verify that newly loaded SQO non focus bid is automatically released to archive.\n");

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		System.out.println("Currently at: " + SummaryPageTabsE.ARCHIVE.getLabel() + " tab");

		String newlyLoadedBidId = testData.get(BID_ID);

		archiveRowIndex = archivePage.searchRowIndex(driver, newlyLoadedBidId);

		String actualArchiveBidId = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.BID_ID_COL);

		System.out.println("\nData Source     - Bid Id: " + newlyLoadedBidId);
		System.out.println("BRETWEB Display - Bid Id:   " + actualArchiveBidId + "\n");

		archivePage.clickCheckbox(archiveRowIndex);

		Assert.assertTrue(archiveRowIndex > ROW_NOT_FOUND);
		Assert.assertEquals(actualArchiveBidId, newlyLoadedBidId);
		System.out.println("Verified that newly loaded SQO non focus bid is automatically released to archive.\n");

		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

//		archivePage.clickDetailLink(driver, archiveRowIndex);
//		archivePage.clickDetailLink(driver, 4);
		driver.get("https://prdgadalgwas01.w3-969.ibm.com:9443/systems/bret/SBidRiskController?page=detail&bid=080101BR&source=BPMS&batchId=-991202\r\n");
			}

	/**
	 * To verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any role.
	 * 
	 * 3. Go to Archive/Non focus Tab.
	 * 
	 * 4. Search for the newly loaded SQO non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Archive tab.
	 * 
	 * 6. Go to bid details.
	 * 
	 * 7. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * @throws InterruptedException 
	 */
	@Test(priority = 3)
	public void testNonFocusSupportingFactors() throws InterruptedException {
		flaggingBidPage = new FlaggingBidDataPage(driver);
		suppDataPage = new SupportDataPage(driver);

		String tcNumber = "TC_BSSW-002";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println();
		System.out.println("==================== Test Item: " + tcNumber + " ====================");
		System.out.println(
				"DESCRIPTION: Verify that score under the supporting factors Tab is correctly displayed on the summary page of the Bid Detail.\n");

		System.out.println("Currently at: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel() + " tab");

		String expectedScore = testData.get(SUPP_FACT_SCORE);

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		setupNewURL(driver.getCurrentUrl());

		String supportingFactorScore = flaggingBidPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS, FlaggingBidDataPage.COL_SCORE);

		System.out.println("Data Source     - Supporting Factors Score: " + expectedScore);
		System.out.println("BRETWEB Display - Supporting Factors Score: " + supportingFactorScore);
		System.out.println();

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		System.out.println("\nNavigating FROM: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);
		Thread.sleep(5000);
		
		/* Take a picture */
		suppDataPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
				.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.SUPPORT_DATA.getLabel(), this.getClass()));

		int computedSuppDataScore = suppDataPage.computeTotalScore(driver);

		System.out.println("\nVerify that the computed supporting factors is correct.");
		System.out.println("Supporting Factors Score at Flagging and Data Tab:  " + expectedScore);
		System.out.println("Supporting Factors Score at Support Data Tab:       " + computedSuppDataScore);

		Assert.assertTrue(expectedScore.equalsIgnoreCase(supportingFactorScore));
		Assert.assertTrue(supportingFactorScore.equalsIgnoreCase(String.valueOf(computedSuppDataScore)));
		System.out.println(
				"Verified that score under the supporting factors Tab is correctly displayed on the summary page of the Bid Detail.\n");
	}

	/**
	 * To verify that the score of the bid is being added to the total bid score and
	 * being displayed correctly on the summary page of bid detail.
	 * 
	 * 
	 * 
	 * 
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any role.
	 * 
	 * 3. Go to Archive/Non focus Tab.
	 * 
	 * 4. Search for the newly loaded SQO non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Archive tab.
	 * 
	 * 6. Go to bid details.
	 * 
	 * 7. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 8. Verify that the scores form the focus factors are being added to the total
	 * bid score and being displayed correctly on the summary page of bid detail.
	 * 
	 * -- Flagging and Bid Data tab
	 * 
	 * -- Bid Q tab
	 */
	@Test(priority = 4)
	public void testNonFocusScores() {
		suppDataPage = new SupportDataPage(driver);
		flaggingBidPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "TC_BSSW-003";

		System.out.println("==================== Test Item: " + tcNumber + " ====================\n");
		System.out.println("DESCRIPTION: Verify that all score should be added to Total Bid Level Score.\n");

		System.out.println("Navigating FROM: " + DetailPageTabsE.SUPPORT_DATA.getLabel());
		suppDataPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		int totalBidLevelScoreComputed = flaggingBidPage.getTotalBidLevelScore();

		String totalBidValueScoreDisplay = flaggingBidPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE, FlaggingBidDataPage.COL_SCORE);

		System.out.println("Total Bid Level Score (Display):  " + totalBidValueScoreDisplay);
		System.out.println("Total Bid Level Score (Computed): " + totalBidLevelScoreComputed);

		Assert.assertEquals(String.valueOf(totalBidLevelScoreComputed), totalBidValueScoreDisplay);
		System.out.println("Verified that all score should be added to Total Bid Level Score.\n");

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		String displayBPCopsScore = flaggingBidPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);

		String tier1Ceid = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flaggingBidPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		System.out.println("\nNavigating FROM: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel() + " tab");

		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		System.out.println("Currently at: " + DetailPageTabsE.BID_Q.getLabel() + " tab");

		int computedBPCOPS = bidQPage.getTotalForBPCops(driver, tier1Ceid.equalsIgnoreCase(tier2Ceid));

		System.out.println("\nQuestions & BPCOPS (Display at Flagging and Bid Tab): " + displayBPCopsScore);
		System.out.println("Questions & BPCOPS (Computed from Bid Q Tab):         " + computedBPCOPS + "\n");
		Assert.assertEquals(String.valueOf(computedBPCOPS), displayBPCopsScore);
		System.out.println("Verified that all score should be added to Total Bid Level Score.\n");

		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());
	}

	/**
	 * To verify that correctLeading product (SWG for SW) ,bid date, region, country
	 * and line items are being displayed correctly on the bid detail page.
	 * 
	 * 
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any role.
	 * 
	 * 3. Go to Archive/Non focus Tab.
	 * 
	 * 4. Search for the newly loaded SQO non focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in Archive tab.
	 * 
	 * 6. Go to bid details.
	 * 
	 * 7. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 8. Verify that the score of the bid is being added to the total bid score and
	 * being displayed correctly on the summary page of bid detail.
	 * 
	 * 9. Verify that correctLeading product (SWG for SW) ,bid date, region, country
	 * and line items are being displayed correctly on the bid detail page.
	 */
	@Test(priority = 5)
	public void testNonFocusDisplay() {
		flaggingBidPage = new FlaggingBidDataPage(driver);

		String tcNumber = "TC_BSSW-004";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("\n==================== Test Item: " + tcNumber + " ====================");
		System.out.println("DESCRIPTION: Verify that correct details are being displayed in the Bid Detail page.\n");

		System.out.println("Navigating FROM: " + DetailPageTabsE.BID_Q.getLabel() + " tab");
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		String primaryBrand = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND);
		String bidDate = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_BID_DATE);
		String region = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_REGION);
		String country = flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_COUNTRY_NAME);

		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		//		System.out.println("\n========== " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel() + " tab ==========");
		System.out.println("Verify that correct Leading product (SWG for SW) ,bid date, region, country and");
		System.out.println("line items are being displayed correctly on the bid details page.");
		System.out.println();
		System.out.println("Primary Brand (Expected): " + expectedPrimaryBrand);
		System.out.println("Primary Brand (Display): " + primaryBrand);
		System.out.println("Bid Date (Expected): " + expectedBidDate);
		System.out.println("Bid Date (Display): " + bidDate);
		System.out.println("Region (Expected): " + expectedRegion);
		System.out.println("Region (Display): " + region);
		System.out.println("Country (Expected): " + expectedCountry);
		System.out.println("Country (Display): " + country);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		System.out.println();
		System.out.println("Navigating FROM: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel() + " tab");
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);

		System.out.println();
		System.out.println("Verify that the displayed Discount and Margin Line items are correct.");
		boolean isDiscMargValid = checkDiscountMarginValidity(testData.get(DISC_MARG_FILE), tcNumber);
		Assert.assertTrue(isDiscMargValid);
		System.out.println();

		System.out.println("Discount + Margin display is valid: " + isDiscMargValid);

		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());
	}

	/**
	 * Returns if the Discount + Margin tab display is valid by comparing the BRET
	 * Web display to the expected CSV data.
	 * 
	 * @param filePath
	 *            of the expected CSV data - exported from the DB.
	 * @return true if the CSV data extract matches the BRET Web Display.
	 */
	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) {
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);

		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(),
				BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}

	/**
	 * Method to take screenshot.
	 * 
	 * @param tcNumber
	 * @param tabLabel
	 */
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}

}
