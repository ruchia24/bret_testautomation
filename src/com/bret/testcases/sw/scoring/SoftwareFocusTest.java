package com.bret.testcases.sw.scoring;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.SupportDataPage;
import com.bret.testcases.hw.scoring.IndicatorCheck;
import com.bret.util.BretTestUtils;

public class SoftwareFocusTest extends BaseTest {

	private static final String BID_ID_FOR_REVIEWER = "bidIdReviewer";
	private static final String BID_ID_FOR_ADMIN = "bidIdAdmin";
	private static final String REVIEWER_DECISION_REV = "reviewerDecisionReviewer";
	private static final String REVIEWER_DECISION_ADMIN = "reviewerDecisionAdmin";
	private static final String DISC_MARG_FILE = "discMargFilePath";

	MyTaskPage myTaskPage;
	FlaggingBidDataPage flagAndBid_page;
	DetailPage detailPage;
	SupportDataPage supportdataPage;

	@Test(priority = 1, description = "Log In")
	public void logInAsAdmin() {
		BRETLoginPage logIn = new BRETLoginPage(driver);
	//	logIn.userLogin(getAdminUsername(), getAdminPassword());
//		logIn.userLogin(getLegalUsername(), getLegalPassword());
		logIn.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	}

	/**
	 * 1153789: [BSSW-006] Focus Bids: ABCD indicators in Summary Page.
	 * 
	 * To verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * Step/s: For Focus Bid with status = NEW
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4. Search for the newly loaded Focus bid with status = NEW.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * For Focus Bid with status = Waiting for Pricing
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4. Search for the newly loaded Focus bid with status = Waiting for Pricing.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 2, description = "")
	public void focusBidsABCDindicatorCheckAsBidAdministrator() throws InterruptedException {
		String tcNumber = "TC_BSSW-006";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("================= START [BSSW-006] Focus Bids: Flagging ==================\n");

		myTaskPage = new MyTaskPage();

		myTaskPage.closeNPSSurvey(driver);

		System.out.println("\nYou are currently at My Task Tab - Logged in as an ADMIN\n");

		// TODO: click My Privilege
		// myTaskPage.userOption(driver, SummaryPage.SHOW_PRIVILEGE);

		// driver.findElement(By.xpath(".//*[@id='idx_form_DropDownLink_0_link']")).click();
		// driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_0_text']")).click();

		/* Currently at MY TASK TAB */
		/* GET the row number of bid */

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(BID_ID_FOR_ADMIN));

		/* CLICK on the radio button / check box */
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		/* Hack on getting the column value - Added 6/11/2018 */
		int colReviewerDecision = MyTaskPage.REVIEWER_DECISION_COL - 1;

		/* GET the the value: BID, REVIEWER DECISION - MYTASK */
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver, rowAtMyTaskTab, MyTaskPage.BID_ID_COL);
		// String bidAdministratorDecisionAtMyTaskTab =
		// myTaskPage.getValueAtIndex(driver,
		// rowAtMyTaskTab, MyTaskPage.REVIEWER_DECISION_COL);

		/* Hack on getting the column value - Added 6/11/2018 */
		String bidAdministratorDecisionAtMyTaskTab = myTaskPage.getValueAtIndex(driver, rowAtMyTaskTab,
				colReviewerDecision);

		/* GET the the value: BID, REVIEWER DECISION - Excel */
		String bidExpected = testData.get(BID_ID_FOR_ADMIN);
		String reviewerDecisionExpected = testData.get(REVIEWER_DECISION_ADMIN);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		System.out.println("bidAtMyTaskTab: " + bidAtMyTaskTab);
		System.out.println("bidAdministratorDecisionAtMyTaskTab: " + bidAdministratorDecisionAtMyTaskTab);

		Assert.assertEquals(bidAtMyTaskTab, bidExpected);
		Assert.assertEquals(bidAdministratorDecisionAtMyTaskTab, reviewerDecisionExpected);

		/* CHECK for the indicators in Properties File. True=Present, False=Not */
		boolean aExpected = Boolean.valueOf(testData.get("A_Admin"));
		boolean bExpected = Boolean.valueOf(testData.get("B_Admin"));
		boolean cExpected = Boolean.valueOf(testData.get("C_Admin"));
		boolean dExpected = Boolean.valueOf(testData.get("D_Admin"));
		
		
		System.out.println("\nINDICATOR\tDATA INPUT (Expected)\tMY TASK TAB (Actual)");
		System.out.println("    A:\t\t\t" + aExpected );
		System.out.println("    B:\t\t\t" + bExpected );
		System.out.println("    C:\t\t\t" + cExpected );
		System.out.println("    D:\t\t\t" + dExpected );
		System.out.println();
		
		
		/* CHECK if Indicators are present. True=Present, False=Not */
		boolean aActual = myTaskPage.aHasMarker(rowAtMyTaskTab);
		boolean bActual = myTaskPage.bHasMarker(rowAtMyTaskTab);
		boolean cActual = myTaskPage.cHasMarker(rowAtMyTaskTab);
		boolean dActual = myTaskPage.dHasMarker(rowAtMyTaskTab);

		System.out.println("TEST");

		System.out.println("\nBID VALUE ON: ");
		System.out.println("Excel file  (Expected) - " + testData.get(BID_ID_FOR_ADMIN));
		System.out.println("My Task Tab (Actual)   - " + bidAtMyTaskTab);

		System.out.println("\nINDICATOR\tDATA INPUT (Expected)\tMY TASK TAB (Actual)");
		System.out.println("    A:\t\t\t" + aExpected + "\t\t\t" + aActual);
		System.out.println("    B:\t\t\t" + bExpected + "\t\t\t" + bActual);
		System.out.println("    C:\t\t\t" + cExpected + "\t\t\t" + cActual);
		System.out.println("    D:\t\t\t" + dExpected + "\t\t\t" + dActual);
		System.out.println();

		/* COMPARE the Indicators in My Task Tab and in Excel */

		Assert.assertEquals(aActual, aExpected);
		Assert.assertEquals(bActual, bExpected);
		Assert.assertEquals(cActual, cExpected);
		Assert.assertEquals(dActual, dExpected);


	}

	@Test(priority = 3)
	public void logInAsReviewer() throws InterruptedException {
		BRETLoginPage logIn = new BRETLoginPage(driver);
		
		/* LOG OUT */
		driver.findElement(By.xpath(".//*[@id='idx_form_DropDownLink_0_link']")).click();
		driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_1_text']")).click();
		driver.findElement(By.xpath(".//*[@id='dijit_layout_ContentPane_0']/p/a/span")).click();
		
		Thread.sleep(10000);
		logIn.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
//		logIn.userLogin(getLegalUsername(), getLegalPassword());
	}

	@Test(priority = 4)
	public void focusBidsABCDindicatorCheckAsLeadReviewer() throws InterruptedException {
		String tcNumber = "TC_BSSW-006";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		myTaskPage = new MyTaskPage();

		System.out.println("\nYou are currently at My Task Tab - Logged in as a REVIEWER\n");

		// TODO: click My Privilege
		// myTaskPage.userOption(driver, SummaryPage.SHOW_PRIVILEGE);
		// driver.findElement(By.xpath(".//*[@id='idx_form_DropDownLink_0_link']")).click();
		// driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_0_text']")).click();

		/* Currently at MY TASK TAB */
		/* GET the row number of bid */
		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(BID_ID_FOR_REVIEWER));

		/* CLICK on the radio button / check box */
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		/* GET the the value: BID, REVIEWER DECISION - MYTASK */
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver, rowAtMyTaskTab, MyTaskPage.BID_ID_COL);
		String reviewerDecisionAtMyTaskTab = myTaskPage.getValueAtIndex(driver, rowAtMyTaskTab,
				MyTaskPage.REVIEWER_DECISION_COL);

		/* GET the the value: BID, REVIEWER DECISION - Excel */
		String bidExpected = testData.get(BID_ID_FOR_REVIEWER);
		String reviewerDecisionExpected = testData.get(REVIEWER_DECISION_REV);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertEquals(bidAtMyTaskTab, bidExpected);
		Assert.assertEquals(reviewerDecisionAtMyTaskTab, reviewerDecisionExpected);

		/* CHECK if Indicators are present. True=Present, False=Not */
		boolean aActual = myTaskPage.aHasMarker(rowAtMyTaskTab);
		boolean bActual = myTaskPage.bHasMarker(rowAtMyTaskTab);
		boolean cActual = myTaskPage.cHasMarker(rowAtMyTaskTab);
		boolean dActual = myTaskPage.dHasMarker(rowAtMyTaskTab);

		/* CHECK for the indicators in Properties File. True=Present, False=Not */
		boolean aExpected = Boolean.valueOf(testData.get("A_Rev"));
		boolean bExpected = Boolean.valueOf(testData.get("B_Rev"));
		boolean cExpected = Boolean.valueOf(testData.get("C_Rev"));
		boolean dExpected = Boolean.valueOf(testData.get("D_Rev"));

		System.out.println("\nBID VALUE ON: ");
		System.out.println("Excel file  (Expected) - " + testData.get(BID_ID_FOR_REVIEWER));
		System.out.println("My Task Tab (Actual)   - " + bidAtMyTaskTab);

		System.out.println("\nINDICATOR\tDATA INPUT (Expected)\tMY TASK TAB (Actual)");
		System.out.println("    A:\t\t\t" + aExpected + "\t\t\t" + aActual);
		System.out.println("    B:\t\t\t" + bExpected + "\t\t\t" + bActual);
		System.out.println("    C:\t\t\t" + cExpected + "\t\t\t" + cActual);
		System.out.println("    D:\t\t\t" + dExpected + "\t\t\t" + dActual);
		System.out.println();

		/* COMPARE the Indicators in My Task Tab and in Excel */

		Assert.assertEquals(aActual, aExpected);
		Assert.assertEquals(bActual, bExpected);
		Assert.assertEquals(cActual, cExpected);
		Assert.assertEquals(dActual, dExpected);

	}

	/**
	 * 1153794: [BSSW-007] Focus Bids: Focus factors
	 * 
	 * To verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary page of
	 * the bid detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer role/Admin Role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4. Search for the newly loaded Focus bid with status = NEW.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Go to Bid details.
	 * 
	 * 8. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary page of
	 * the bid detail.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5)
	public void focusBidsFocusFactors() throws InterruptedException {

		String tcNumber = "TC_BSSW-007";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("============== START [BSSW-007] Focus Bids: Focus factors ==============\n");
		System.out.println(
				"DESCRIPTION: To verify that the correct focus factors are being flagged and being \ndisplayed on Factors under Flagging and Bid Data tab on the summary page of the bid detail.\n");

		/* YOU ARE AT MY TASK TAB */
		System.out.println("You are currently at My Task Tab");
		myTaskPage = new MyTaskPage();

		/* GET row of the Bid and use it to click on the details of the bid */
		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(BID_ID_FOR_REVIEWER));
		String bidAtMyTask = myTaskPage.getValueAtIndex(driver, rowAtMyTaskTab, myTaskPage.BID_ID_COL);

		/* CHECK first if the indicators are Active on MYTASK tab */
		boolean aActual, bActual, cActual, dActual;
		aActual = myTaskPage.aHasMarker(rowAtMyTaskTab);
		bActual = myTaskPage.bHasMarker(rowAtMyTaskTab);
		cActual = myTaskPage.cHasMarker(rowAtMyTaskTab);
		dActual = myTaskPage.dHasMarker(rowAtMyTaskTab);

		/*
		 * CLICK on the chain to see bid details
		 * 
		 * TO DO: add xpath to Detail Page - navigate
		 */

		driver.findElement(By.xpath(
				".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rowAtMyTaskTab + "]/table/tbody/tr/td[4]/span"))
				.click();
		System.out.println("Navigating to Bid Details Page");

		/* GET the control of the current window */
		String winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		/* you are now at BID Details Page */
		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		IndicatorCheck focusFactors = new IndicatorCheck();

		System.out.println("\nNow Checking on Indicators");
		/* Evaluate Indicator A */
		boolean a_indicator = focusFactors.aChecker(driver, bidAtMyTask, aActual);

		Thread.sleep(10000);
		/* Evaluate Indicator B */
		boolean bIndicator = focusFactors.bChecker(driver, bidAtMyTask, bActual, tcNumber);

		Thread.sleep(10000);
		/* Evaluate Indicator C */
		boolean cIndicator = focusFactors.cChecker(driver, bidAtMyTask, cActual);

		Thread.sleep(10000);
		/* Evaluate Indicator C */
		boolean dIndicator = focusFactors.dChecker(driver, bidAtMyTask, dActual);

		Assert.assertEquals(a_indicator, true);
		Assert.assertEquals(bIndicator, true);
		Assert.assertEquals(cIndicator, true);
		Assert.assertEquals(dIndicator, true);

		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);

		setupNewURL(driver.getCurrentUrl());
		/* YOU ARE AT FLAGGING AND BID DATA TAB */

	}

	/**
	 * 1153795: [BSSW-008] Focus Bids: Supporting Factors
	 * 
	 * To verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer role/Admin Role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4. Search for the newly loaded Focus bid with status = NEW.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Go to Bid details.
	 * 
	 * 8. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary page of
	 * the bid detail.
	 * 
	 * 9. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * @throws InterruptedException
	 * 
	 */
	@Test(priority = 6)
	public void focusBidsSupportingFactors() throws InterruptedException {

		String tcNumber = "TC_BSSW-008";

		/* YOU ARE AT FLAGGING AND BID DATA TAB */

		System.out.println("============ START [BSSW-008] Focus Bids: Supporting Factors ============\n");
		System.out.println(
				"DESCRIPTION: To verify that the score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.\n");
		flagAndBid_page = new FlaggingBidDataPage(driver);

		/*
		 * actualScore is the score in Flagging and Bid Data Tab (CURRENTLY AT FLAGGING
		 * AND BID DATA TAB)
		 */
		String actualScore = flagAndBid_page.getValueAtIndex(driver, FlaggingBidDataPage.ROW_SUPPORTING_FACTORS,
				FlaggingBidDataPage.COL_SCORE);

		Thread.sleep(3000);
		System.out.println("You are currently at Flagging and Bid Data Tab");

		/* NAVIGATE to Support Data Tab to get the scores */
		detailPage = new DetailPage();
		detailPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);

		Thread.sleep(5000);
		supportdataPage = new SupportDataPage(driver);

		/* SCREENSHOT, testcase#, currentpage */
		supportdataPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
				.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.SUPPORT_DATA.getLabel(), this.getClass()));

		/* expectedScore is the score in Support Data Tab */
		int expectedScore = supportdataPage.computeTotalScore(driver);

		System.out.println("Supporting Factors Score in Flagging and Bid Data Tab (Actual) : " + actualScore);
		System.out.println("Supporting Factors Score in Support Data Tab (Expected)      : " + expectedScore);

		Assert.assertTrue(actualScore.equalsIgnoreCase(String.valueOf(expectedScore)));

		Thread.sleep(5000);
		/* YOU ARE AT SUPPORT DATA PAGE */

	}

	/**
	 * 1153796: [BSSW-009] Focus Bids: Scores
	 * 
	 * To verify that the score of the bid is being added to the total bid score and
	 * being displayed correctly on the summary page of bid detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer role/Admin Role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4. Go to Bid details.
	 * 
	 * 5. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 6. Verify that the scores form the focus factors are being added to the total
	 * bid score and being displayed correctly on the summary page of bid detail.
	 * 
	 * -- Flagging and Bid Data tab
	 * 
	 * -- Bid Q tab
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 7)
	public void focusBidsScores() throws InterruptedException {
		String tcNumber = "TC_BSSW-009";
		SupportDataPage supportdata_page = new SupportDataPage(driver);

		/* NAVIGATE to Flagging Bid Data */
		supportdata_page.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		System.out.println("============ START [BSHW-009] Focus Bids: Scores ============\n");
		System.out.println(
				"DESCRIPTION: To verify that the score of the bid is being added to the total bid score and being displayed correctly on the summary page of bid detail.\n");

		Thread.sleep(15000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/* actual_score is the score in Flagging and Bid Data Tab */
		String total_bidscore_actual = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE, FlaggingBidDataPage.COL_SCORE);

		/* expected_score is the summed up values in Flagging and Bid Data Tab */
		int total_bidscore_expected = flagAndBid_page.getTotalBidLevelScore();

		/* Checking for the Value of the Total Bid Level Score */
		System.out.println("Total Bid Level Score (actual)     : " + total_bidscore_actual);
		System.out.println("Total Bid Level Score (calculated) : " + total_bidscore_expected);
		System.out.println();

		Assert.assertTrue(total_bidscore_actual.equalsIgnoreCase(String.valueOf(total_bidscore_expected)));

		/* get the score value at index of Questions and BPCOPS */
		String qbpcops_actual = flagAndBid_page.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);

		String tier1Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		Thread.sleep(5000);

		System.out.println("Navigating to BID Q Page to get the QBPCOPS Expected Score");
		/* NAVIGATE to Bid Q's to get the score */
		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.BID_Q);

		// ---- > having black page while taking screenshot
		Thread.sleep(10000);
		/* SCREENSHOT, testcase#, currentpage */
		// takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());
		Thread.sleep(5000);

		/* qbpcops_expected is the summed up values in BidQ Tab */
		BidQPage bidq_page = new BidQPage(driver);
		int qbpcops_expected = bidq_page.getTotalForBPCops(driver, tier1Ceid.equalsIgnoreCase(tier2Ceid));

		/* Checking for the QBCPCOPS Score */
		System.out.println("\nQuestions & BPCOPS (actual)   : " + qbpcops_actual);
		System.out.println("Questions & BPCOPS (expected) : " + qbpcops_expected);
		System.out.println();

		Assert.assertTrue(qbpcops_actual.equalsIgnoreCase(String.valueOf(qbpcops_expected)));

		detail_page.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		/* YOU ARE NOW AT FLAGGING BID DATA PAGE */
	}

	/**
	 * 1153803: [BSSW-010] Focus Bids: Display
	 * 
	 * To verify that correctLeading product (SWG for SW) ,bid date, region, country
	 * and line items are being displayed correctly on the bid detail page.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer role/Admin Role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4. Search for the newly loaded Focus bid with status = NEW.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Go to Bid details.
	 * 
	 * 8. Verify that correctLeading product (SWG for SW) ,bid date, region, country
	 * and line items are being displayed correctly on the bid detail page.
	 * 
	 */

	@Test(priority = 8)
	public void focusBidsDisplay() {

		String tcNumber = "TC_BSSW-010";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		System.out.println("============ START [BSSW-010] Focus Bids: Display ============\n");

		/* YOU ARE CURRENTLY AT FLAGGING BID DATA PAGE */

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Getting expected values from Excel File */
		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		flagAndBid_page = new FlaggingBidDataPage(driver);

		/* Getting expected values from Flagging and Bid DAta Tab */
		String primaryBrand = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND);

		String bidDate = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_BID_DATE);

		String region = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_REGION);

		String country = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_COUNTRY_NAME);

		System.out.println("\n  DETAILS\tDATA INPUT (Expected)\tFLAGGING AND BID DATA TAB (Actual)");
		System.out.println("Primary Brand:\t\t" + expectedPrimaryBrand + "\t\t" + primaryBrand);
		System.out.println("Country Name :\t\t" + expectedCountry + "\t\t" + country);
		System.out.println("Region       :\t\t" + expectedRegion + "\t\t" + region);
		System.out.println("Bid Date     :\t\t" + expectedBidDate + "\t\t" + bidDate);
		System.out.println();

		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		/* NAVIGATE to Bid Margin And Discount */
		// --------->>> Added 9/27/2017

		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);
		boolean isDiscMargValid = checkDiscountMarginValidity(testData.get(DISC_MARG_FILE), tcNumber);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());

		Assert.assertTrue(isDiscMargValid);

		System.out.println("===============================================");
	}

	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) { // --------->>>
		// Added
		// 9/27/2017
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);

		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(),
				BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}

	@AfterTest
	public void exit() throws InterruptedException {
		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);
		driver.quit();
	}
}
