package com.bret.testcases.sw.scoring;

import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.MyTaskPage;
import com.bret.util.BretTestUtils;

/**
 * 
 */
public class SoftwareSQOChannelOverride extends BaseTest {

	private static final String BID_ID_WITH_REASON_CODE = "bidIdWithRC";
	private static final String BID_ID_WITHOUT_REASON_CODE = "bidIdWithOutRC";

	private static final String CHANNEL_CODE_REASON = "channelCodeAndReason";

	private MyTaskPage myTaskPage;
	private BidQPage bidQPage;
	private DetailPage detailPage;

	@Test(priority = 1)
	public void login() throws InterruptedException {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		// loginPage.userLogin(getAdminUsername(), getAdminPassword());
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());

	}

	/**
	 * 1254486: [BSSW-017] BRET Web Details > SQO Channel Override Reason: Field
	 * display for SW bids
	 * 
	 * To verify that SQO Channel Override Reason is displayed on BRET web for
	 * software bids.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using a role that has access to Bid details.
	 * 
	 * 3. Go to Bid Details page of a SW bid.
	 * 
	 * 4. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 2, description = "[BSSW-017] BRET Web Details > SQO Channel Override Reason: Field")
	public void testChannelOverrideReasonDisplay() throws InterruptedException {

		myTaskPage = new MyTaskPage();
		detailPage = new DetailPage();

		String sheetName = "TC_BSSW-017-019 (SQO Override)";
		String tcNumber = "TC_BSSW-017";

		System.out.println("\n========== TC Number: " + tcNumber + " ==========\n");
		System.out.println(
				"[BSSW-017] BRET Web Details > SQO Channel Override Reason: Field \nTo verify that SQO Channel Override Reason is displayed on BRET web for software bids.\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(BID_ID_WITH_REASON_CODE));

		/* CLICK on the radio button / check box */
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		/* NAVIGATE to Flagging Bid and DAta Tab */
		System.out.println("\nNavigating TO: Detail Page");

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);
		Thread.sleep(3000);

		/* GET the control of the current window */
		String winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		/* NAVIGATE to Bid Q Tab */
		bidQPage = new BidQPage(driver);
		detailPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		Thread.sleep(5000);
		System.out.println();

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		boolean channelOverrideReasonPresent = bidQPage.channelOverrideReasonPresent(driver);

		Assert.assertEquals(true, channelOverrideReasonPresent);

		/* Currently at Bid Q Page */

	}

	/**
	 * 1254489: [BSSW-018] BRET Web Details > SQO Channel Override Reason: SQO bids
	 * with SQO Channel Override Reason
	 * 
	 * To verify that SQO Channel Override Reason is displayed on BRET web with
	 * correct CHNL_OVRRD_REAS_CODE and CHNL_OVRRD_REAS_CODE_DSCR values
	 * 
	 * Step/s:
	 * 
	 * A. Load an SQO bid with CHNL_OVRRD_REAS_CODE and CHNL_OVRRD_REAS_CODE_DSCR
	 * values.
	 * 
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using a role that has access to Bid details.
	 * 
	 * 3. Go to Bid Details page of the bid in stepA
	 * 
	 * 4. Go to Bid Q tab.
	 * 
	 * 5. Locate SQO Channel Override Reason field.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3, description = "[BSSW-018] BRET Web Details > SQO Channel Override Reason: SQO bids with SQO Channel Override Reason")
	public void testChannelOverrideReasonValidity() throws InterruptedException {

		String sheetName = "TC_BSSW-017-019 (SQO Override)";
		String tcNumber = "TC_BSSW-018";
		/* Currently at Bid Q Page */

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		System.out.println("\n========== TC Number: " + tcNumber + " ==========\n");
		System.out.println(
				"[BSSW-018] BRET Web Details > SQO Channel Override Reason: SQO bids with SQO Channel Override Reason\nTo verify that SQO Channel Override Reason is displayed on BRET web with correct CHNL_OVRRD_REAS_CODE and CHNL_OVRRD_REAS_CODE_DSCR values\n");

		bidQPage = new BidQPage(driver);

		String actualDescription = bidQPage.getChannelOverrideDesc();

		String expectedDescription = testData.get(CHANNEL_CODE_REASON);

		System.out.println("\nSQO Channel Code and Reason (expected) : " + expectedDescription);
		System.out.println("SQO Channel Code and Reason (actual)   : " + actualDescription + "\n");

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		Assert.assertEquals(expectedDescription, actualDescription);

	}

	@Test(priority = 4)
	public void logInAgain() throws InterruptedException {
		Thread.sleep(5000);
		System.out.println("\nLogging out...");
		logOutDetailsPage(driver);
		Thread.sleep(3000);

		System.out.println("Logging in again to check for the other test bid...");
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

	}

	/**
	 * 1254492: [BSSW-019] BRET Web Details > SQO Channel Override Reason: SQO bids
	 * without SQO Channel Override Reason
	 * 
	 * To verify that SQO Channel Override Reason is displayed on BRET web with no
	 * details onCHNL_OVRRD_REAS_CODE and CHNL_OVRRD_REAS_CODE_DSCR section.
	 * 
	 * Step/s:
	 * 
	 * A. Load an SQO bid WITHOUT CHNL_OVRRD_REAS_CODE and CHNL_OVRRD_REAS_CODE_DSCR
	 * values.
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using a role that has access to Bid details.
	 * 
	 * 3. Go to Bid Details page ofthe bid in stepA.
	 * 
	 * 4. Go to Bid Q tab.
	 * 
	 * 5. Locate SQO Channel Override Reason field.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5, description = "[BSSW-019] BRET Web Details > SQO Channel Override Reason: SQO bids without SQO Channel Override Reason")
	public void testChannelOverride() throws InterruptedException {

		String sheetName = "TC_BSSW-017-019 (SQO Override)";
		String tcNumber = "TC_BSSW-019";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		bidQPage = new BidQPage(driver);

		System.out.println("\n========== TC Number: " + tcNumber + " ==========\n");
		System.out.println(
				"1254492: [BSSW-019] BRET Web Details > SQO Channel Override Reason: SQO bids without SQO Channel Override Reason\nTo verify that SQO Channel Override Reason is displayed on BRET web with no details onCHNL_OVRRD_REAS_CODE and CHNL_OVRRD_REAS_CODE_DSCR section.\n");

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(BID_ID_WITHOUT_REASON_CODE));

		/* CLICK on the radio button / check box */
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		/* NAVIGATE to Flagging Bid and DAta Tab */
		System.out.println("\nNavigating TO: Detail Page");

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);
		Thread.sleep(3000);

		/* GET the control of the current window */
		String winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		/* NAVIGATE to Bid Q Tab */
		bidQPage = new BidQPage(driver);
		detailPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		boolean check = bidQPage.getChannelOverrideDesc().trim().isEmpty();
		System.out.println("SQO Channel Code and Reason field is empty: " + check);

		System.out.println("===============================================");

		Assert.assertTrue(check);

	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}

	@AfterTest
	public void exit() throws InterruptedException {
		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);
		driver.quit();
	}
}
