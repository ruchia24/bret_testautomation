package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETMyTaskPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform manual archive of non focus SQO bids.<br>
 * 
 */

public class ManualArchiveNonFocusSWbids extends BRETmainTestNGbase{
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 1, 1);  
	 
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 
	 @Test (description = "RSW-001, RSW-002: Test case that will verify that Administrator user is able to manually move Non Focus bid to Archive Tab.")
	 public void tcManualArchiveNonFocusSWBid() throws Exception {
		 //----------------------------------------------------------------------------- 
		 // RSW-001: Test case that will manually move Non Focus bid to Archive
		 // RSW-002: Test case that will check manually moved Non Focus bids in Archive
		 //----------------------------------------------------------------------------- 
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 // search a particular bid in My Task tab
		 BRETMyTaskPage findBidMyTask = new BRETMyTaskPage (driver);
		 findBidMyTask.bidFindonMyTasktab_NF(bidNumber); 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = ManualArchiveNonFocusSWbids.class.getSimpleName();
		 String methodName = ("TC_RSW-001_" + className);
		 screenShots.screenCaptures(methodName);
		 
		 //RSW-001
		 findBidMyTask.bidReleasetoArchive();
		 
		 // RSW-002: Perform search on archive tab
		 BRETArchivePage findBidArchive = new BRETArchivePage(driver);
		 findBidArchive.releasedtoArchiveChck_NF(bidNumber, userName);
			
		 //Screen capture 
		 String className2ndTC  = ManualArchiveNonFocusSWbids.class.getSimpleName();
		 String methodName2ndTC  = ("TC_RSW-002_" + className2ndTC);
		 screenShots .screenCaptures(methodName2ndTC);

	 }


}
