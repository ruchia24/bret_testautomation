package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETNonFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform rollback of non focus bids>
 * 
 */

public class RollBackNonFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 2, 1);  
	
	
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 
	 
	 @Test(description = "RSW_003,RSW_004: Test case that will verify that Administrator is able to rollback non focus bid from Archive Tab and bid is cheked in Non Focus tab. ")
	 public void tcRollbackNonFocusSWbid () throws Exception {
		 //------------------------------------------------------------------------------------
		 // RSW_003: This test case will perform the bid rollback for non Focus SW bids.
		 // RSW_004: This test case will checked the rolled back bid in Non Focus Tab
		 //------------------------------------------------------------------------------------
		 
		 String className = RollBackNonFocusSWbids.class.getSimpleName();
		 String methodName;  
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 // Search and Rollback the bid
		 // Capture the  page prior to rollback
		 BRETArchivePage rollBackBidSW = new BRETArchivePage (driver);
		 methodName = ("TC_RSW_003_" + className);                
		 
		 //RSW_003
		 rollBackBidSW.bidrollBack(bidNumber, methodName);
		 
		 //RSW_004: check rollback bids in Non Focus tab
		 BRETNonFocusPage rolledBackbidChck = new BRETNonFocusPage (driver);
		 rolledBackbidChck.rolledbackBidChck(bidNumber);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName2ndTC = ("TC_RHW_004_" + className);
		 screenShots.screenCaptures(methodName2ndTC);
				 
	 }


}
