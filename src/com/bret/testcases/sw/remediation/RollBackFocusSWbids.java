package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform rollback of focus bids>
 * 
 */

public class RollBackFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 10, 1);  
	
	
	 String userRole = "LeadReviewer";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 
	 
	 @Test(description = "RSW_020,RSW_021: Test case that will verify that Lead Reviewer is able to rollback focus bid from Archive Tab and bid is cheked in Focus tab. ")
	 public void tcRollbackFocusSWbid () throws Exception {
		 //------------------------------------------------------------------------------------
		 // RSW_020: This test case will perform the bid rollback for Focus SW bids.
		 // RSW_021: This test case will checked the rolled back bid in Focus Tab
		 //------------------------------------------------------------------------------------
		 
		 String className = RollBackFocusSWbids.class.getSimpleName();
		 String methodName;  
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 	 
		 // Search and Rollback the bid
		 // Capture the  page prior to rollback
		 BRETArchivePage rollBackBid = new BRETArchivePage (driver);
		 methodName = ("TC_RSW_020_" + className);                
		 
		 //RSW_020
		 rollBackBid.bidrollBack_F(bidNumber, methodName);
		 
		 //RSW_021: check rollback bids in Focus tab
		 BRETFocusPage rolledBackbidChck = new BRETFocusPage (driver);
		 rolledBackbidChck.rolledBackbidChck(bidNumber);
		 		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName2ndTC = ("TC_RSW_021_" + className);
		 screenShots.screenCaptures(methodName2ndTC);
				 
	 }


}
