package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will check that non remediated focus bid should not arvhive.
 * 
 */
public class AutoArchiveNonRemediatedFocusSWbid extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 11, 1);  
	 String bidStatusFromExcel = Excel.getCellValue (xlpath, sheet2, 11, 2);  
	 
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 String bidStatus = bidStatusFromExcel.trim();
	 
	 @Test (description = "Test case that will check that non remediated bid should not Auto Achived.")
	 public void tcAutoArchiveNonRemediatedFocusSWBid() throws Exception {
		 //----------------------------------------------------------------------------- 
		 // Test case that will check that non remediated bid should not Auto Achived.
		 //-----------------------------------------------------------------------------
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 // check bid in Focus Tab
		 BRETFocusPage nonRemediatedFocusBid =  new BRETFocusPage(driver);
		 nonRemediatedFocusBid.bidCheckFocusTab(bidNumber, bidStatus, "SW");  
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = AutoArchiveNonRemediatedFocusSWbid.class.getSimpleName();
		 String methodName = ("TC_nonRemFbid_" + className );
		 screenShots.screenCaptures(methodName);
		 
	 }

}
