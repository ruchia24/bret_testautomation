package com.bret.testcases.sw.remediation;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETMyTaskPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform the READY FOR REVIEW process by Adminsitrator role.<br>
 * This will verify the change on SQO bid from status W to N.
 * 
 */
public class WaitingForPricingToNew extends BRETmainTestNGbase  {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
//	 String sheet1 ="Credentials"; //Sheet name on excel
//	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
//	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 3, 1);  
	 String bidStatusFromExcel = Excel.getCellValue (xlpath, sheet2, 3, 2);  
	 
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String[] foundSWBid = new String[4];
	 String bidNumber = bidNumberFromExcel.trim();
	 String bidStatus = bidStatusFromExcel.trim();
	 
	 @Test (description = "RSW_005, RSW_006: Test case that will check that Adminsitrator user is able to move Waiting for Pricing bids to NEW.")
	 public void tcSetWaitingForPrcingToNew () throws Exception {
		 //---------------------------------------------------------------------------------------------------------
		 // RSW_005 and RSW_006: This test case will check that Waiting for Pricing bids is available for Administrator.
		 // Adminsitrator is able to READY for REVIEW the bid
		 //---------------------------------------------------------------------------------------------------------
 
	 
		 //Performs the Login event - Admin
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 //Find the bid in My Task
		 BRETMyTaskPage findSWbid = new BRETMyTaskPage(driver);
		 foundSWBid = findSWbid.bidFindonMyTasktabSW(bidNumber);
		 
		 Assert.assertEquals(foundSWBid[2], bidStatus, "Bid Status is not waiting for prcing.");
		 		 
 		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = WaitingForPricingToNew.class.getSimpleName();
		 String methodName = ("TC_RSW_005a_6_" + className);
		 screenShots.screenCaptures(methodName);
		 
		 BRETMyTaskPage rdyReview = new BRETMyTaskPage(driver);
		 rdyReview.bidReadyforRview(bidNumber);
		 		 
	 }

	 @Test (description = "RSW_007: Test case that will check correct detaoils on the bid where status is already set to NEW.")
	 public void tcVerifyTheNewStatus () throws Exception {
		 //---------------------------------------------------------------------------------------------------------
		 // RSW_007: This test case will check Waiting for Pricing to New status change.
		 //---------------------------------------------------------------------------------------------------------
 	 
		 
		 BRETFocusPage chckNewbid = new BRETFocusPage(driver);
		 chckNewbid.bidCheckFocusTab(bidNumber, "New", "swNEW");
		 
 		 
 		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = WaitingForPricingToNew.class.getSimpleName();
		 String methodName = ("TC_RSW_007_" + className);
		 screenShots.screenCaptures(methodName);
		
		 
	 }
	 

}
