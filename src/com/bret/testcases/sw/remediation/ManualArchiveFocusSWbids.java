package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETMyTaskPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform manual archive of focus SQO bids.<br>
 * 
 */

public class ManualArchiveFocusSWbids extends BRETmainTestNGbase{
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 9, 1);  
	 
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 
	 @Test (description = "RSW-018, RSW-019: Test case that will verify that Administrator user is able to manually move Focus bid to Archive Tab.")
	 public void tcManualArchiveFocusSWBid() throws Exception {
		 //----------------------------------------------------------------------------- 
		 // RSW-018: Test case that will manually move Focus bid to Archive
		 // RSW-019: Test case that will check manually moved Focus bids in Archive
		 //----------------------------------------------------------------------------- 
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 String className = ManualArchiveFocusSWbids.class.getSimpleName();
		 String methodName = ("TC_RSW-018_" + className  );
		 
 		 // RSW-018: release bid to archive
		 BRETMyTaskPage releaseFocusBidtoArchive = new BRETMyTaskPage (driver);
		 releaseFocusBidtoArchive.releaseFbid2Archive(bidNumber, methodName); 
		 
		 // RSW-019: Perform search on archive tab
		 BRETArchivePage checkBidinArchive =  new BRETArchivePage(driver);
		 checkBidinArchive.releasedtoArchiveChck_F(bidNumber, userName);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName2ndTC = ("TC_RSW-019_" + className );
		 screenShots.screenCaptures(methodName2ndTC);
				 
		 

	 }


}
