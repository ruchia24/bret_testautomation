package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will check Auto Archiving of non focus bids.
 * 
 */
public class AutoArchiveNonFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 1, 1);  
	 
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();

	 
	 @Test (description = "RSW-002: Test case that will check that non focus bid is Auto Achived.")
	 public void tcAutoArchiveNonFocusSWBid() throws Exception {
		 //----------------------------------------------------------------------------- 
		 // RSW-002: Test case that will check that on focus bid is Auto Achived.
		 //-----------------------------------------------------------------------------
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 // check bid in Archive
		 BRETArchivePage autoReleaseNonFocusBid =  new BRETArchivePage(driver);
		 autoReleaseNonFocusBid.autoreleasedtoArchiveChck_NF(bidNumber, userName);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = AutoArchiveNonFocusSWbids.class.getSimpleName();
		 String methodName = ("TC_RSW-002_" + className );
		 screenShots.screenCaptures(methodName);
		 
	 }

}
