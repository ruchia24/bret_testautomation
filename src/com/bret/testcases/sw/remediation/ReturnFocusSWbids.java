package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform remediation process.<br>
 * Remediation Outcome = Return
 * 
 */
public class ReturnFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 5, 1);  
	 String bidStatusFromExcel = Excel.getCellValue (xlpath, sheet2, 5, 2);  
	 String bidCommentFromExcel = Excel.getCellValue (xlpath, sheet2, 5, 3); 
	 
	 String userRole = "Reviewer";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 String bidStatus = bidStatusFromExcel.trim();
	 String bidComment = bidCommentFromExcel.trim();
	 String brand = "SW";

	 
	 @Test (description = "RSW-010, RSW-011: Test case that will perform bid remediation RETURN.")
		public void tcReturnFocusSWBid () throws Exception {
		 //-------------------------------------------------------------------------------------  
		 // RSW-010, RSW-011: Test case that will perform bid Remediation =  Return 
	     // and then checked the remediated bid in Summary and Bid Details page
		 //-------------------------------------------------------------------------------------
			
			 String className = ReturnFocusSWbids.class.getSimpleName();
			 String methodNameDetailsCheck = ("TC_RSW-010_" + className);
			 String methodNameSummaryCheck = ("TC_RSW-011_" + className);
			 
			 //Performs the Login event
			 BRETValidLogin userLogin = new BRETValidLogin(driver);  
			 userLogin.userValidLogin(userName, userPassword);
			 
			 // Return a bid 
			 BRETFocusPage returnBid = new BRETFocusPage (driver);
			 returnBid.bidRemediation(bidNumber, userName, bidStatus, bidComment); 
		 	 
			 // check released bid in Focus tab
			 returnBid.bidRemediationCheckFocusTab (bidNumber, userName, bidStatus); 
			 
			 //Screen Capture
			 ScreenCapture screenShots = new ScreenCapture(driver);  
			 screenShots.screenCaptures(methodNameSummaryCheck);
					 
			 // Check on Bid Details after refresh
			 returnBid.bidRemediatonChckbidDetails(bidNumber, bidStatus, bidComment,brand, methodNameDetailsCheck);
				 
			 
	}
	 
}
