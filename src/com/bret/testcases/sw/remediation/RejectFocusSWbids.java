package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform remediation process.<br>
 * Remediation Outcome = Reject 
 * 
 */
public class RejectFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 6, 1);  
	 String bidStatusFromExcel = Excel.getCellValue (xlpath, sheet2, 6, 2);  
	 String bidCommentFromExcel = Excel.getCellValue (xlpath, sheet2, 6, 3); 
	 
	 String userRole = "Reviewer";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 String bidStatus = bidStatusFromExcel.trim();
	 String bidComment = bidCommentFromExcel.trim();
	 String brand = "SW";

	 
	 @Test (description = "RSW-012, RSW-013: Test case that will perform bid remediation REJECT.")
		public void tcRejectFocusSWBid () throws Exception {
		 //-------------------------------------------------------------------------------------  
		 // RSW-012, RSW-013: Test case that will perform bid Remediation =  Reject 
	     // and then checked the remediated bid in Summary and Bid Details page
		 //-------------------------------------------------------------------------------------
			
			 String className = RejectFocusSWbids.class.getSimpleName();
			 String methodNameDetailsCheck = ("TC_RSW-012_" + className);
			 String methodNameSummaryCheck = ("TC_RSW-013_" + className);
			 
			 //Performs the Login event
			 BRETValidLogin userLogin = new BRETValidLogin(driver);  
			 userLogin.userValidLogin(userName, userPassword);
			 
			 // Reject a bid 
			 BRETFocusPage rejectBid = new BRETFocusPage (driver);
			 rejectBid.bidRemediation(bidNumber, userName, bidStatus, bidComment); 
		 	 
			 // check released bid in Focus tab
			 rejectBid.bidRemediationCheckFocusTab (bidNumber, userName, bidStatus); 
			 
			 //Screen Capture
			 ScreenCapture screenShots = new ScreenCapture(driver);  
			 screenShots.screenCaptures(methodNameSummaryCheck);
					 
			 // Check on Bid Details after refresh
			 rejectBid.bidRemediatonChckbidDetails(bidNumber, bidStatus, bidComment,brand, methodNameDetailsCheck);
				 
			 
	}
	 
}
