package com.bret.testcases.export;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.obj.export.ExportDetailData;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.BRETAdvSearchPage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.ExportAdvancedSearchProcessor;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class ExportAdvancedSearchTest extends BaseTest {

	private static final String DOWNLOAD_PATH_PROP = "exportDownloadPath";
	private static final String ADVANCED_SEARCH_PROP = "exportAdvancedSearchSummary";

	private ExportAdvancedSearchProcessor exportAdvancedSearchProc = new ExportAdvancedSearchProcessor();

	//
	@Test(priority = 1)
	public void testExportAdvSearch() throws InterruptedException {
		/*
		 * "1. Launch BRET web. 2.Login using Admin/Reviewer role . 3. Go to Advanced
		 * Search tab. 4. Enter valid search criteria 5. Click export to csv button. "
		 * 
		 */
		// Log in as Reviewer

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so need to navigate to
		 * Non_focus tab
		 */

		MyTaskPage myTaskPage = new MyTaskPage();
		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);
		
		myTaskPage.selectSourceSystem(driver, MyTaskPage.SOURCE_SYS_BPMS);
		pausePage();
		
		int firstIndex = 1;
		
		String bidId = myTaskPage.getValueAtIndex(driver, firstIndex, MyTaskPage.BID_ID_COL);
		String additionalId = myTaskPage.getValueAtIndex(driver, firstIndex, MyTaskPage.ADDITIONAL_ID_COL);
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);
		AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage();
		BRETAdvSearchPage bretAdvSearch = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_EXPT-010";

		pausePage();

		/*
		 * To input Advanced Search Details
		 */
		// Bid ID
		// driver.findElement(By.id("//*[@id=\"locateBidInputBox\"]")).sendKeys("Feb15_BzNa");

		String advSearchBidIdFieldXpath = BRETAdvSearchPage.XPATH_BID_ID;
		String advSearchAdditionalIdFieldXpath = BRETAdvSearchPage.XPATH_ADDITIONAL_ID;

		bretAdvSearch.setInputField(advSearchBidIdFieldXpath, bidId);

		// additional ID
		// driver.findElement(By.id("//*[@id=\"locateAdditionalInputBox\"]")).sendKeys("0017350436");

		bretAdvSearch.setInputField(advSearchAdditionalIdFieldXpath, additionalId);

		takeScreenshot(tcNumber + "Search Criteria", SummaryPageTabsE.ADVANCED_SEARCH.getLabel(), this.getClass());

		bretAdvSearch.clickSearch();

//		Thread.sleep(5000);

		pausePage(5000);

		takeScreenshot(tcNumber + "Search Criteria", SummaryPageTabsE.ADVANCED_SEARCH.getLabel(), this.getClass());
		pausePage(6000);

		int advSearchFirstIndex = 1;

		// Click Checkbox of the Search result

		bretAdvSearch.clickCheckbox(advSearchFirstIndex);

		takeScreenshot(tcNumber + "Checkbox", SummaryPageTabsE.ADVANCED_SEARCH.getLabel(), this.getClass());

//		Thread.sleep(5000);

		pausePage(5500);

		/*
		 * Click Export Button - Automatically saves the CSV File - no more pop up
		 * message
		 */
		advancedSearchPage.clickExportToCsvButton(driver);
		
		
		pausePage(5500);

		String flag = bretAdvSearch.getSearchResultValue(driver, BRETAdvSearchPage.FLAG_COL);
		String reviewerDecision = bretAdvSearch.getSearchResultValue(driver, BRETAdvSearchPage.REVIEWER_DECISION_COL);
		String reviewer = bretAdvSearch.getSearchResultValue(driver, BRETAdvSearchPage.REVIEWER_COL);
		String reviewerReleasedOn = bretAdvSearch.getSearchResultValue(driver, BRETAdvSearchPage.RELEASED_ON_COL);

		
		
		bretAdvSearch.clickDetailLinkSingleRecord(driver);

		pausePage(5500);

		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage(8000);

		ExportDetailData exportedCsv = getDataFromExportCSV(ADVANCED_SEARCH_PROP);
		ExportDetailData bretWebData = getDataFromBretWebFields(flaggingBidPage , flag , bidId , additionalId , reviewerDecision , reviewer , reviewerReleasedOn);

		List<String> dataDifference = exportAdvancedSearchProc.getExportedDataDifference(exportedCsv, bretWebData);

		int dataDiffCount = dataDifference.size();

		if (dataDiffCount > 0) {
			for (String data : dataDifference) {
				System.out.println(data);
			}
		}

		Assert.assertEquals(0, dataDiffCount);

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}


	private ExportDetailData getDataFromExportCSV(String propertyKey) {
		return exportAdvancedSearchProc
				.getExportDetailData(composeExportedFilepath(BretTestUtils.getPropertyValue(propertyKey)));
	}

	private String composeExportedFilepath(String fileName) {
		return BretTestUtils.getPropertyValue(DOWNLOAD_PATH_PROP) + fileName;
	}

	private ExportDetailData getDataFromBretWebFields(FlaggingBidDataPage flaggingBidPage, String flag, String bidId,
			String additionalId, String reviewerDecision, String reviewer, String reviewerReleasedOn) {

		ExportDetailData bretWebData = new ExportDetailData();

		// Y = Focus , N= Non Focus , E = Error 
		String type = "From BRET Web";
		
		if (flag.equalsIgnoreCase("Y")) {
			type = "focus";
		} else if (flag.equalsIgnoreCase("N")){
			type = "nonfocus";
		}
		else {
			type = "error";
		}


		bretWebData.setType(type);

		bretWebData.setAdditionalId(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_ADDITIONAL_ID));
		bretWebData.setBidDate(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_BID_DATE));
		bretWebData.setSourceSystem(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_SOURCE_SYSTEM));		
		bretWebData.setRegion(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_REGION));
		bretWebData.setCountryName(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_COUNTRY_NAME));
		bretWebData.setReviewerDecision(reviewerDecision);
		bretWebData.setReviewer(reviewer.trim());
		bretWebData.setReviewerReleasedOn(reviewerReleasedOn.trim());
		bretWebData.setDistributor(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR));
		bretWebData.setCustomerFacingBP(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACING_BP));
		bretWebData.setCustomerName(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_NAME));
		bretWebData.setDistributorCeid(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID));
		bretWebData.setCustomerFacingBPCeid(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID));
		
		
		// navigate to Remediation log Tab
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		RemediationLogPage remediationLog = new RemediationLogPage(driver);

		bretWebData.setBidId(remediationLog.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_ID));
		
		bretWebData.setConfidentiality(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONFIDENTIALITY));
		bretWebData.setRouteToMarket(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_ROUTE_TO_MARKET));
		bretWebData.setOffshorePaymentTerms(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_OFFSHORE_PAYMENT_TERMS));
		bretWebData.setBundledSolutions(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BUNDLED_SOLUTIONS));
		bretWebData.setContingencyFee(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONTINGENCY_FEE));
		bretWebData.setSoleSourceProc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_SOLE_SOURCE_PROC));
		bretWebData.setFocusBP(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_FOCUS_BP));
		bretWebData.setBpMargPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_PERC));
		bretWebData.setDiscountPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_PERC));
		bretWebData.setBpMarginTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_TOTAL));
		bretWebData.setDiscountTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_TOTAL));
		
		bretWebData.setOperationOwner(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_CONTACTS, RemediationLogPage.ROW_CONTACTS_OO));
		bretWebData.setOperationOwner(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_CONTACTS, RemediationLogPage.ROW_CONTACTS_BID_MGR));
		
		bretWebData.setT3Name(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_RTM, RemediationLogPage.ROW_RTM_T3_NAME));
		bretWebData.setT4Name(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_RTM, RemediationLogPage.ROW_RTM_T4_NAME));
		bretWebData.setT5Name(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_RTM, RemediationLogPage.ROW_RTM_T5_NAME));
		
		bretWebData.setBpmT1Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T1_FIN));
		bretWebData.setBpmT1Serv(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T1_SERV));
		bretWebData.setBpmT1Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T1_INS_FREIGHT));
		bretWebData.setBpmT1Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T1_PROFIT));
		bretWebData.setBpmT2Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T2_FIN));
		bretWebData.setBpmT2Serv(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T2_SERV));
		bretWebData.setBpmT2Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T2_INS_FREIGHT));
		bretWebData.setBpmT2Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T2_PROFIT));
		bretWebData.setBpmOther(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_OTHER));
		
		bretWebData.setLog(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_LOG));
		bretWebData.setChannelProgram(remediationLog.getRemediationValueAtIndex(driver, 
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CHANNEL_PROGRAM));
		bretWebData.setActionOwner(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_ACTION_OWNER));
		bretWebData.setClosed(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CLOSED));
		bretWebData.setClosedDate(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CLOSE_DATE));
		bretWebData.setOutcome(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION,
				RemediationLogPage.ROW_REM_OUTCOME));
		// Bid Expired = Y (If Outcome = Expired), N for other outcome
		String outcome = "From BRET Web";

		bretWebData.setCondition(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CONDITION));
		bretWebData.setTransparencyLetter(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_TRANSPARENCY_LETTER));
		bretWebData.setCycle(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CYCLE));
		bretWebData.setExceptionFlag(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_EXCEPTION_FLAG));
		
		bretWebData.setFulfilled(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_FULFILLED));
		bretWebData.setComment(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_COMMENT));
		
		String isExpired = "";

		if (outcome.equalsIgnoreCase("EXPIRED")) {
			isExpired = "Y";
		} else {
			isExpired = "N";
		}
		bretWebData.setBidExpired(isExpired);
		
		bretWebData.setExPreFulfillment(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_EX_PRE_FULFILMENT));
		bretWebData.setPostVerifyDate(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_POST_VERIFY_DATE));
		bretWebData.setPostShipAuditFollowUpStatus(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_POST_SHIP_AUDIT_FOLLOW_UP_STATUS));
		bretWebData.setWaitingForExtension(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_WAITING_FOR_EXTENSION));
		bretWebData.setProblemsWithChasing(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_PROBLEMS_WITH_CHASING));
		bretWebData.setRequestSentTooEarly(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_REQUEST_SENT_TOO_EARLY));
		bretWebData.setRealisticallyDue(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_REALISTICALLY_DUE));
		bretWebData.setTestNoteStartedYet(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_TEST_NOT_STARTED_YET));
		bretWebData.setPotentialIssues(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_POTENTIAL_ISSUES));
		bretWebData.setFollowUpLog(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_FOLLOW_UP_LOG));
		bretWebData.setDeadlineOfRequest(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_DEADLINE_OF_REQUEST));
		bretWebData.setBidEvent(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_FULFILLED));
		
		
		
		return bretWebData;

	}

}
