package com.bret.testcases.export;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.obj.export.ExportMyTaskData;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.ExportMyTaskProcessor;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class ExportMyTaskTest extends BaseTest {
	// Constants for property Key
	// Details found on brettest.properties

	private static final String DOWNLOAD_PATH_PROP = "exportDownloadPath";
	private static final String MY_TASK_PROP = "exportMyTaskSummary";


	private ExportMyTaskProcessor exportMyTaskProc = new ExportMyTaskProcessor();

	//
	@Test(priority = 1)
	public void testExportMyTask() {

		/*
		 * Test Case: EXPT-001 Steps: 1. Launch BRET web. 2. Login using Admin/Reviewer
		 * role . 3. Go to My task tab. 4. Click Bids to be exported 5. Click Export
		 * Button
		 */

		// Log in as Reviewer Role

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */

		MyTaskPage myTaskPage = new MyTaskPage();

		String tcNumber = "TC_EXPT-001";

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);
		pausePage();

		/*
		 * To identify what checkbox will be click and avoid Magic Number
		 */
		int myTaskPageFirstIndex = 1;

		/*
		 * This will click the 1st checkbox You can change the Check Box number
		 */
		myTaskPage.clickCheckbox(driver, myTaskPageFirstIndex);
		pausePage(1000);

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		/*
		 * This will click the export button The Download pop-up box will not appear due
		 * to FirefoxProfile settings
		 */
		myTaskPage.clickExportToCSVButton(driver);
		pausePage();

		// Save MyTask Summary page Values
		String flag = myTaskPage.getValueAtIndex(driver, myTaskPageFirstIndex, MyTaskPage.FLAG_COL);
		String reviewerDecision = myTaskPage.getValueAtIndex(driver, myTaskPageFirstIndex,
				MyTaskPage.REVIEWER_DECISION_COL);
		String reviewerReleasedOn = myTaskPage.getValueAtIndex(driver, myTaskPageFirstIndex,
				MyTaskPage.RELEASED_ON_COL);
		String ibmSellPrice = myTaskPage.getValueAtIndex(driver, myTaskPageFirstIndex, MyTaskPage.IBM_SELL_PRICE_COL);
		String marginOrDiscountFlag = myTaskPage.aHasMarker(myTaskPageFirstIndex) ? "Y" : "N";
		String bidQuestionsFlag = myTaskPage.bHasMarker(myTaskPageFirstIndex) ? "Y" : "N";
		String pbcHistoryFlag = myTaskPage.cHasMarker(myTaskPageFirstIndex) ? "Y" : "N";
		String compositeScoreFlag = myTaskPage.aHasMarker(myTaskPageFirstIndex) ? "Y" : "N";

		// To View Flagging Data Page to check Values from CSV are correct
		myTaskPage.clickDetailLink(driver, myTaskPageFirstIndex);
		pausePage(5500);
	
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage(8000);

		ExportMyTaskData exportedCsv = getDataFromExportCSV(MY_TASK_PROP);
		ExportMyTaskData bretWebData = getDataFromBretWebFields(flaggingBidPage, flag  ,   reviewerDecision ,
				  reviewerReleasedOn ,  ibmSellPrice ,   marginOrDiscountFlag ,   bidQuestionsFlag ,   pbcHistoryFlag ,   compositeScoreFlag);

		List<String> dataDifference = exportMyTaskProc.getExportedDataDifference(exportedCsv, bretWebData);

		int dataDiffCount = dataDifference.size();

		if (dataDiffCount > 0) {
			for (String data : dataDifference) {
				System.out.println(data);
			}
		}

		Assert.assertEquals(0, dataDiffCount);

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}

	private ExportMyTaskData getDataFromExportCSV(String propertyKey) {
		return exportMyTaskProc
				.getExportMyTaskData(composeExportedFilepath(BretTestUtils.getPropertyValue(propertyKey)));
	}

	private String composeExportedFilepath(String fileName) {
		return BretTestUtils.getPropertyValue(DOWNLOAD_PATH_PROP) + fileName;
	}

	private ExportMyTaskData getDataFromBretWebFields(FlaggingBidDataPage flaggingBidPage , String flag ,  String reviewerDecision ,
			String reviewerReleasedOn , String  ibmSellPrice , String marginOrDiscountFlag , String bidQuestionsFlag , String pbcHistoryFlag , String compositeScoreFlag) {
		
		ExportMyTaskData bretWebData = new ExportMyTaskData();

		// Y = Focus , N= Non Focus , E = Error 
				String type = "From BRET Web";
				
				if (flag.equalsIgnoreCase("Y")) {
					type = "focus";
				} else if (flag.equalsIgnoreCase("N")){
					type = "nonfocus";
				}
				else {
					type = "error";
				}

				bretWebData.setType(type);
				bretWebData.setSourceSystem(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_SOURCE_SYSTEM));
				bretWebData.setRegion(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_REGION));
//				bretWebData.setBidId(bidId);
				bretWebData.setAdditionalId(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_ADDITIONAL_ID));
				bretWebData.setBidDate(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_BID_DATE));
				bretWebData.setReviewerDecision(reviewerDecision);
				bretWebData.setReviewerReleasedOn(reviewerReleasedOn);
				bretWebData.setCountry(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_COUNTRY_NAME));
//				bretWebData.setIbmSellPrice(ibmSellPrice);
				bretWebData.setPrimaryBrand(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_PRIMARY_BRAND));
				bretWebData.setDistributor(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_DISTRIBUTOR));
				bretWebData.setCustomerFacingBP(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_FACING_BP));
				bretWebData.setCustomerName(
						flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_NAME));
				bretWebData.setMarginOrDiscountFlag(marginOrDiscountFlag);
				bretWebData.setBidQuestionsFlag(bidQuestionsFlag);
				bretWebData.setPbcHistoryFlag(pbcHistoryFlag);
				bretWebData.setCompositeScoreFlag(compositeScoreFlag);
			
		
		// Helps Navigate to another tab
		// navigate to Remediation log Tab
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		RemediationLogPage remediationLog = new RemediationLogPage(driver);
		
				bretWebData.setReviewerComments(
						remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT));
				bretWebData.setReadyToReviewOn(
						remediationLog.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_DATE));
				
				bretWebData.setRemediationLog(
						remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_LOG));
				bretWebData.setActionOwner(
						remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_ACTION_OWNER));
				
				//From MYTask Tab but error to portect from Non_foucs Bid that are in My Task Tab
				bretWebData.setBidId(
						remediationLog.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_ID));
				bretWebData.setReviewerDecision(
						remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME));
				
				
				
				
		return bretWebData;
		
		
	}
}
