package com.bret.testcases.export;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.obj.export.ExportFocusData;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.ExportFocusProcessor;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class ExportFocusTest extends BaseTest {

	private static final String DOWNLOAD_PATH_PROP = "exportDownloadPath";
	private static final String FOCUS_PROP = "exportFocusSummary";

	private ExportFocusProcessor exportFocusProc = new ExportFocusProcessor();

	@Test(priority = 3)
	public void testExportFocus() {

		/*
		 * Steps 1. Launch BRET web. 2. Login using Admin/Reviewer role . 3. Go to Non
		 * Focus tab. 4. Click checkbox of the corresponding bid/s to be exported 5.
		 * Click export to csv button.
		 */

		// Log in as Reviewer

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so need to navigate to
		 * Focus tab
		 */

		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
		FocusPage focusPage = new FocusPage();

		String tcNumber = "TC_EXPT-005";

		/* This closes the NPS Survey */
		focusPage.closeNPSSurvey(driver);
		pausePage(1000);

		/*
		 * To identify what checkbox will be click and avoid Magic Number
		 */
		int focusPageFirstIndex = 1;

		/* Click the first checkbox */
		focusPage.clickCheckbox(driver, focusPageFirstIndex);
		pausePage(1000);

		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel(), this.getClass());

		/*
		 * Click Export Button - Automatically saves the CSV File - no more pop up
		 * message
		 */
		focusPage.clickExportToCSVButton(driver);
		pausePage(1000);

		String flag = focusPage.getValueAtIndex(driver, focusPageFirstIndex, focusPage.FLAG_COL);
		String bidId = focusPage.getValueAtIndex(driver, focusPageFirstIndex, focusPage.BID_ID_COL);
		String reviewerDecision = focusPage.getValueAtIndex(driver, focusPageFirstIndex,
				focusPage.REVIEWER_DECISION_COL);
		String reviewer = focusPage.getValueAtIndex(driver, focusPageFirstIndex, focusPage.REVIEWER_COL);
		String reviewerReleasedOn = focusPage.getValueAtIndex(driver, focusPageFirstIndex, focusPage.RELEASED_ON_COL);
		String ibmSellPrice = focusPage.getValueAtIndex(driver, focusPageFirstIndex, focusPage.IBM_SELL_PRICE_COL);
		String marginOrDiscountFlag = focusPage.aHasMarker(focusPageFirstIndex) ? "Y" : "N";
		String bidQuestionsFlag = focusPage.bHasMarker(focusPageFirstIndex) ? "Y" : "N";
		String pbcHistoryFlag = focusPage.cHasMarker(focusPageFirstIndex) ? "Y" : "N";
		String compositeScoreFlag = focusPage.aHasMarker(focusPageFirstIndex) ? "Y" : "N";

		focusPage.clickDetailLink(driver, focusPageFirstIndex);
		pausePage(5500);

		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage(8000);

		ExportFocusData exportedCsv = getDataFromExportCSV(FOCUS_PROP);
		ExportFocusData bretWebData = getDataFromBretWebFields(flaggingBidPage, flag, bidId, ibmSellPrice,
				reviewerDecision, reviewer, reviewerReleasedOn, marginOrDiscountFlag, bidQuestionsFlag, pbcHistoryFlag,
				compositeScoreFlag);

		List<String> dataDifference = exportFocusProc.getExportedDataDifference(exportedCsv, bretWebData);

		int dataDiffCount = dataDifference.size();

		if (dataDiffCount > 0) {
			for (String data : dataDifference) {
				System.out.println(data);
			}
		}

		Assert.assertEquals(0, dataDiffCount);

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}

	private ExportFocusData getDataFromExportCSV(String propertyKey) {
		return exportFocusProc.getExportFocusData(composeExportedFilepath(BretTestUtils.getPropertyValue(propertyKey)));
	}

	private String composeExportedFilepath(String fileName) {
		return BretTestUtils.getPropertyValue(DOWNLOAD_PATH_PROP) + fileName;

	}

	private ExportFocusData getDataFromBretWebFields(FlaggingBidDataPage flaggingBidPage, String flag, String bidId,
			String ibmSellPrice, String reviewerDecision, String reviewer, String reviewerReleasedOn,
			String marginOrDiscountFlag, String bidQuestionsFlag, String pbcHistoryFlag, String compositeScoreFlag) {

		ExportFocusData bretWebData = new ExportFocusData();

		String type = "From BRET Web";

		if (flag.equalsIgnoreCase("Y")) {
			type = "focus";
		} else {
			type = "error";
		}

		bretWebData.setType(type);
		bretWebData
				.setSourceSystem(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_SOURCE_SYSTEM));
		bretWebData.setRegion(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_REGION));
		bretWebData.setBidId(bidId);
		bretWebData
				.setAdditionalId(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_ADDITIONAL_ID));
		bretWebData.setBidDate(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_BID_DATE));
		bretWebData.setReviewerDecision(reviewerDecision);
		bretWebData.setReviewer(reviewer);
		bretWebData.setReviewerReleasedOn(reviewerReleasedOn);
		bretWebData.setCountry(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_COUNTRY_NAME));
		bretWebData.setIbmSellPrice(ibmSellPrice);
		bretWebData
				.setPrimaryBrand(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_PRIMARY_BRAND));
		bretWebData.setDistributor(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_DISTRIBUTOR));
		bretWebData.setCustomerFacingBP(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_FACING_BP));
		bretWebData
				.setCustomerName(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_NAME));
		bretWebData.setMarginOrDiscountFlag(marginOrDiscountFlag);
		bretWebData.setBidQuestionsFlag(bidQuestionsFlag);
		bretWebData.setPbcHistoryFlag(pbcHistoryFlag);
		bretWebData.setCompositeScoreFlag(compositeScoreFlag);

		// Helps Navigate to another tab
		// navigate to Remediation log Tab
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		RemediationLogPage remediationLog = new RemediationLogPage(driver);

		bretWebData.setReviewerComments(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT));
		bretWebData.setReadyToReviewOn(
				remediationLog.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_DATE));
		bretWebData.setConfidentiality(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONFIDENTIALITY));
		bretWebData.setConfidentiality(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONFIDENTIALITY));
		bretWebData.setRouteToMarket(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_ROUTE_TO_MARKET));
		bretWebData.setOffshorePaymentTerms(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_OFFSHORE_PAYMENT_TERMS));
		bretWebData.setBundledSolutions(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BUNDLED_SOLUTIONS));
		bretWebData.setContingencyFee(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONTINGENCY_FEE));
		bretWebData.setSoleSourceProc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_SOLE_SOURCE_PROC));
		bretWebData.setFocusBP(remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_FOCUS_BP));
		bretWebData.setBpMargPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_PERC));
		bretWebData.setDiscountPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_PERC));
		bretWebData.setBpMarginTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_TOTAL));
		bretWebData.setDiscountTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_TOTAL));
		bretWebData.setRemediationLog(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_LOG));
		bretWebData.setActionOwner(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_ACTION_OWNER));
		bretWebData.setRemediationFulfilled(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_FOLLOWUP, RemediationLogPage.ROW_FOLLOWUP_FULFILLED));
		bretWebData.setRemediationComment(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_FOLLOWUP, RemediationLogPage.ROW_FOLLOWUP_COMMENT));

		return bretWebData;
	}
}
