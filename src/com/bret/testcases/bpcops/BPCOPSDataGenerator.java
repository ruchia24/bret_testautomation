package com.bret.testcases.bpcops;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.bret.util.ConstUtils;
import com.bret.util.JDBCConnection;

public class BPCOPSDataGenerator {
	

	
	final static Logger LOGGER = Logger.getLogger(BPCOPSDataGenerator.class);
	
	public BPCOPSDataGenerator() {
		
	}	

	@Test
	public void run()  {
		
	// Check first if the ceid "Dummy00" is still usable: If no results generated, insert this ceid in bpcops table:
	JDBCConnection.insertIfNoExistingData(GeneralBPCOPSQueries.selectCEIDQuery, GeneralBPCOPSQueries.insertCEIDQuery, ConstUtils.testCEIDSevZero, String.valueOf(ConstUtils.INDEX_ZERO));
	
	// Check first if the ceid "Dummy01" is still usable: If no results generated, insert this ceid in bpcops table:
//	JDBCConnection.insertIfNoExistingData(GeneralBPCOPSQueries.selectCEIDQuery, GeneralBPCOPSQueries.insertCEIDQuery, ConstUtils.testCEIDSevOne, String.valueOf(ConstUtils.INDEX_ONE));
	
	// Check first if the ceid "Dummy02" is still usable: If no results generated, insert this ceid in bpcops table:
//	JDBCConnection.insertIfNoExistingData(GeneralBPCOPSQueries.selectCEIDQuery, GeneralBPCOPSQueries.insertCEIDQuery, ConstUtils.testCEIDSevTwo, String.valueOf(ConstUtils.INDEX_TWO));
		
	// Check first if the ceid "Dummy03" is still usable: If no results generated, insert this ceid in bpcops table:
//	JDBCConnection.insertIfNoExistingData(GeneralBPCOPSQueries.selectCEIDQuery, GeneralBPCOPSQueries.insertCEIDQuery, ConstUtils.testCEIDSevThree, String.valueOf(ConstUtils.INDEX_THREE));
	
	// Check first if the ceid "Dummy04" is still usable: If no results generated, insert this ceid in bpcops table:
//	JDBCConnection.insertIfNoExistingData(GeneralBPCOPSQueries.selectCEIDQuery, GeneralBPCOPSQueries.insertCEIDQuery, ConstUtils.testCEIDSevFour, String.valueOf(ConstUtils.INDEX_FOUR));
		
	// Check first if the ceid "Dummy05" is still usable: If no results generated, insert this ceid in bpcops table:
//	JDBCConnection.insertIfNoExistingData(GeneralBPCOPSQueries.selectCEIDQuery, GeneralBPCOPSQueries.insertCEIDQuery, ConstUtils.testCEIDSevFive, String.valueOf(ConstUtils.INDEX_FIVE));
		
		
		
	try {
		LOGGER.info("Begin SEV 0 : Insertion of Test Data for Severity 0");
//		JDBCConnection.executeSqlScript(".//src//com//bret//testcases//bpcops//sqlScripts//[BPCOPS-001-004]Sev0.sql");
		
		LOGGER.info("Begin SEV 1 : Insertion of Test Data for Severity 1");
//		JDBCConnection.executeSqlScript(".//src//com//bret//testcases//bpcops//sqlScripts//[BPCOPS-005-008]Sev1.sql");
		
		LOGGER.info("Begin SEV 2 : Insertion of Test Data for Severity 2");
//		JDBCConnection.executeSqlScript(".//src//com//bret//testcases//bpcops//sqlScripts//[BPCOPS-009-012]Sev2.sql");
		
		LOGGER.info("Begin SEV 3 : Insertion of Test Data for Severity 3");
//		JDBCConnection.executeSqlScript(".//src//com//bret//testcases//bpcops//sqlScripts//[BPCOPS-013-016]Sev3.sql");
		
		LOGGER.info("Begin SEV 4 : Insertion of Test Data for Severity 4");
//		JDBCConnection.executeSqlScript(".//src//com//bret//testcases//bpcops//sqlScripts//[BPCOPS-017-020]Sev4.sql");
		
		LOGGER.info("Begin SEV 5 : Insertion of Test Data for Severity 5");
//		JDBCConnection.executeSqlScript(".//src//com//bret//testcases//bpcops//sqlScripts//[BPCOPS-021-024]Sev5.sql");
		
//	} catch (IOException | ClassNotFoundException e) {
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	LOGGER.info("Call shell script to move test data from landing table to persistent tables");	
	JDBCConnection.callShellScript();		
	LOGGER.info("FInished Shell Script Execution");
	}
}
