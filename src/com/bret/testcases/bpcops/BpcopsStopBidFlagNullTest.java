package com.bret.testcases.bpcops;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class BpcopsStopBidFlagNullTest extends BaseTest {
	private static final String BID_ID1 = "null_nonFocusBid";
	private static final String BID_ID2 = "null_focusBid";
	private static final String BID_ID1_FLAG = "null_nonfocusBidFlag";
	private static final String BID_ID2_FLAG = "null_focusBidFlag";
	private static final String FACTORS = "null_factor";
	private static final String OUTCOME_COMMENT = "null_outcomeComment";
	private static final int ROW_NOT_FOUND = 0;
	private int myTaskRowIndex = ROW_NOT_FOUND;
	private static String winHandleBefore;
	private static final String sheetName = "TC_BPCOPS-001-030";
	private int archiveRowIndex;
	
	private MyTaskPage myTaskPage;
	private ArchivePage archivePage;
	private FlaggingBidDataPage flaggingBidPage;
	private RemediationLogPage remediationLogPage;

	@BeforeTest
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	}
	
	/**
	 * [BPCOPS-029] China BPCOPS > Stop bid flag = NULL: Bid Flagging and Focus Factors
	 * Steps:
	 * 1. Launch BRET web.
	 * 2. Login using Lead reviewer role.
	 * 3. Search for the newly loaded bids.
	 *  - if sev 0: non focus
	 *  - if sev 1,2,3: non focus, focus
	 *  - if sev 4,5,TERM: focus
	 *  4. Check summary tab and bids details page of the selected bids.
	 * 
	 * @param detailLinkPart
	 * @throws InterruptedException
	 */

	@Test(priority = 1, description = "[BPCOPS-029] China BPCOPS > Stop bid flag = NULL: Bid Flagging and Focus Factors - "
			+ "To verify that bids are flag as non focus for those bids released for sampling, and flag as focus for bids not released. "
			+ "Verify that all bids has focus factor: "
			+ "- Sev 0,1,2,3: BPCOPS: Tier1 (or Tier2) Investigation Findings"
			+ "- Sev 4,5,TERM: BPCOPS: Tier2(Tier1) Terminated/ to be terminated BP in RTM")
	public void testNonFocusFlagging() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		archivePage = new ArchivePage();
		flaggingBidPage = new FlaggingBidDataPage(driver);
		remediationLogPage = new RemediationLogPage(driver);

		/* NAVIGATE TO ARCHIVE TAB*/
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
		
		pausePage();
		
		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-029";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String newlyLoadedBidIdNonFocus = testData.get(BID_ID1);
		String expectedFlagNonFocus = testData.get(BID_ID1_FLAG);
		String expectedFactor = testData.get(FACTORS);

		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.ARCHIVE.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in Archive tab");
		System.out.println("Verify that the newly loaded bid is flagged as non focus");
		
		/* SEARCH FOR THE NEWLY LOADED FOCUS BID */
		archiveRowIndex = archivePage.searchRowIndex(driver, testData.get(BID_ID1));
		String bidAtArchiveTab = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.BID_ID_COL);
		String bidFlag1 = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.FLAG_COL);

		/* CLICK CHECK BOX FOR THE SELECTED BID */
		archivePage.clickCheckbox(archiveRowIndex);

		Assert.assertEquals(bidAtArchiveTab, newlyLoadedBidIdNonFocus);
		Assert.assertEquals(bidFlag1, expectedFlagNonFocus);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		System.out.println("NON FOCUS BID");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdNonFocus);
		System.out.println("> Bid id (actual): " + bidAtArchiveTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlagNonFocus);
		System.out.println("> Flag (actual): " + bidFlag1 +"\n\n");

		/* SCREENSHOT FOR THE BID SUMMARY (MY TASK TAB) */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		archivePage.clickDetailLink(driver, archiveRowIndex);
		
		/* GET CONTROL OF CURRENT WINDOW */
		winHandleBefore = driver.getWindowHandle();
		
		/* SWITCH CONTROL TO NEW WINDOW */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();

		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
	}	
	
	/**
	 * [BPCOPS-030] China BPCOPS > Stop bid flag = NULL: Outcome comments for released bids
	 * Steps:
	 * 1. Go to Remediation log of the released bid.
	 * 
	 */
	@Test(priority = 2, description = "[BPCOPS-030] China BPCOPS > Stop bid flag = NULL: Outcome comments for released bids")
	public void testNonFocusOutcomeComment() throws InterruptedException {
		remediationLogPage = new RemediationLogPage(driver);
		flaggingBidPage = new FlaggingBidDataPage(driver);

		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		String actualOutcomeComment = remediationLogPage.getRemediationValueAtIndex(driver,
			RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT);

		System.out.println("\nChecking outcome comment: ");

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-030";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String expectedOutcomeComment = testData.get(OUTCOME_COMMENT);

		System.out.println("\n> Expected Outcome Comment: " + expectedOutcomeComment);
		System.out.println("> Actual Outcome Comment  : " + actualOutcomeComment +"\n\n");

		Assert.assertEquals(actualOutcomeComment, expectedOutcomeComment);

		remediationLogPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
			.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass()));

		/* SCREENSHOT OF REMEDIATION LOG TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());
		Thread.sleep(5000);
			
		driver.close();
	}
		
		/* BACK TO BPCOPS-029 FOR NON FOCUS BID */
	@Test(priority = 3, description = "[BPCOPS-029] China BPCOPS > Stop bid flag = NULL: Bid Flagging and Focus Factors - "
			+ "To verify that bids are flag as non focus for those bids released for sampling, and flag as focus for bids not released. "
			+ "Verify that all bids has focus factor: "
			+ "- Sev 0,1,2,3: BPCOPS: Tier1 (or Tier2) Investigation Findings"
			+ "- Sev 4,5,TERM: BPCOPS: Tier2(Tier1) Terminated/ to be terminated BP in RTM")
	public void testFocusFlagging() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-029";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		/* GO BACK TO BRET WEB MAIN PAGE*/
		String newlyLoadedBidIdFocus = testData.get(BID_ID2);
		String expectedFlagFocus = testData.get(BID_ID2_FLAG);
		String expectedFactor = testData.get(FACTORS);
		
		/* SWITCH BACK TO ORIGINAL WINDOW */
		driver.switchTo().window(winHandleBefore);
		
		archivePage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
		
		Thread.sleep(3000);

		/* THIS CLOSES THE NPS SURVEY */
		myTaskPage.closeNPSSurvey(driver);
		
		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.MY_TASK.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in My Task Tab");
		System.out.println("Verify that the newly loaded bid is flagged as focus");

		/* SEARCH FOR THE NON FOCUS BID*/
		myTaskRowIndex = myTaskPage.searchRowIndex(driver, newlyLoadedBidIdFocus);
		
		/* CLICK CHECK BOX FOR THE SELECTED BID */
		myTaskPage.clickCheckbox(myTaskRowIndex);
		
		String bidAtArchiveTab = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.BID_ID_COL);
		String bidFlag1 = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.FLAG_COL);

		Assert.assertEquals(bidAtArchiveTab, newlyLoadedBidIdFocus);
		Assert.assertEquals(bidFlag1, expectedFlagFocus);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		System.out.println("FOCUS BID");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdFocus);
		System.out.println("> Bid id (actual): " + bidAtArchiveTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlagFocus);
		System.out.println("> Flag (actual): " + bidFlag1 +"\n\n");

		/* SCREENSHOT FOR THE BID SUMMARY (ARCHIVE TAB) */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		myTaskPage.clickDetailLink(driver, myTaskRowIndex);
		
		/* SWITCH CONTRO TO NEW WINDOW - FLAGGING AND BID DATA PAGE */
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();
		
		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB*/
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
	}
}
