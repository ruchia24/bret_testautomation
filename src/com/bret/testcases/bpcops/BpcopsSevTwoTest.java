
package com.bret.testcases.bpcops;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class BpcopsSevTwoTest extends BaseTest {
	private static final String BID_ID1 = "sev2_nonFocusBid";
	private static final String BID_ID2 = "sev2_focusBid";
	private static final String BID_ID1_FLAG = "sev2_nonFocusBidFlag";
	private static final String BID_ID2_FLAG = "sev2_focusBidFlag";
	private static final String BPCOPS_DISTRIBUTOR_CATEGORY_VALUE = "sev2_distributorCategoryValue";
	private static final String BPCOPS_CUSTOMER_CATEGORY_VALUE = "sev2_customerCategoryValue";
	private static final String FACTORS = "sev2_factor";
	private static final String OUTCOME_COMMENT = "sev2_outcomeComment";
	private static final int ROW_NOT_FOUND = 0;
	private int myTaskRowIndex = ROW_NOT_FOUND;
	private static String winHandleBefore;
	private static final String sheetName = "TC_BPCOPS-001-030";
	private int archiveRowIndex;
	
	private MyTaskPage myTaskPage;
	private ArchivePage archivePage;
	private FlaggingBidDataPage flaggingBidPage;
	private BidQPage bidQPage;
	private RemediationLogPage remediationLogPage;

	@BeforeTest
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	}
	
	/**
	 * [BPCOPS-010] Random Sampling > Severity Level 2: Bid Flagging and Focus factors
	 * Steps:
	 * 1. Launch BRET web.
	 * 2. Login using Lead reviewer role.
	 * 3. Search for the newly loaded bids. - non focus - focus
	 * 4. Check summary tab and bids details page of the selected bids.
	 * 
	 * @param detailLinkPart
	 * @throws InterruptedException
	 */

	@Test(priority = 1, description = "[BPCOPS-010] Random Sampling > Severity Level 2: Bid Flagging and Focus factors - "
			+ "To verify that bids are flag as non focus for those bids released for sampling, and flag as focus for bids not released. "
			+ "Verify that all bids has focus factor: BPCOPS: Tier1 (or Tier2) Investigation Findings")
	public void testFocusFlagging() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		archivePage = new ArchivePage();
		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-010";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String newlyLoadedBidIdFocus = testData.get(BID_ID2);
		String expectedFlagFocus = testData.get(BID_ID2_FLAG);
		String expectedFactor = testData.get(FACTORS);

		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.MY_TASK.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in My Task Tab");
		System.out.println("Verify that the newly loaded bid is flagged as focus");
		
		/* SEARCH FOR THE NEWLY LOADED FOCUS BID */
		myTaskRowIndex = myTaskPage.searchRowIndex(driver, testData.get(BID_ID2));
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.BID_ID_COL);
		String bidFlag2 = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.FLAG_COL);

		/* CLICK CHECK BOX FOR THE SELECTED BID */
		myTaskPage.clickCheckbox(myTaskRowIndex);

		Assert.assertEquals(bidAtMyTaskTab, newlyLoadedBidIdFocus);
		Assert.assertEquals(bidFlag2, expectedFlagFocus);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		System.out.println("FOCUS BID");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdFocus);
		System.out.println("> Bid id (actual): " + bidAtMyTaskTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlagFocus);
		System.out.println("> Flag (actual): " + bidFlag2 +"\n\n");

		/* SCREENSHOT FOR THE BID SUMMARY (MY TASK TAB) */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		myTaskPage.clickDetailLink(driver, myTaskRowIndex);
		
		/* GET CONTROL OF CURRENT WINDOW */
		winHandleBefore = driver.getWindowHandle();
		
		/* SWITCH CONTROL TO NEW WINDOW */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();

		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
	}
	
		/* BPCOPS CATEGORY FOR FOCUS BID (BPCOPS-012) */
	@Test(priority = 2, description = "[BPCOPS-012] BRET Web Details > BPCOPS Category: Severity Level 2")
	public void testBPCOPSCategoryFocus() throws InterruptedException {
		flaggingBidPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		/* NAVIGATE TO BID Q TAB */
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-012";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		String expectedDistributorCategoryValue = testData.get(BPCOPS_DISTRIBUTOR_CATEGORY_VALUE);
		String expectedCustomerCategoryValue = testData.get(BPCOPS_CUSTOMER_CATEGORY_VALUE);

		/* GET VALUE OF BPCOPS CATEGORY FROM BID Q TAB */
		String actualDistributorCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR, BidQPage.COL_HIST_BPC_OPS_CAT);
		String actualCustomerCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP, BidQPage.COL_HIST_BPC_OPS_CAT);

		System.out.println("\nChecking BPCOPS Category: \n");
		
		System.out.println("> Expected bpcops category value for T1: "   + expectedDistributorCategoryValue);
		System.out.println("> Actual bpcops category value for T1  : "     + actualDistributorCategoryValue);

		System.out.println("\n> Expected bpcops category value for T2: " + expectedCustomerCategoryValue);
		System.out.println("> Actual bpcops category value for T2  : " + actualCustomerCategoryValue +"\n\n");

		Assert.assertTrue(expectedDistributorCategoryValue.contains(actualDistributorCategoryValue), "Factor cannot be found.\n");
		Assert.assertTrue(expectedCustomerCategoryValue.contains(actualCustomerCategoryValue), "Factor cannot be found.\n");
		
		/* SCREENSHOT OF BID Q TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel(), this.getClass());
		Thread.sleep(5000);
		
		driver.close();
	}
		
		/* BACK TO BPCOPS-010 FOR NON FOCUS BID */
	@Test(priority = 3, description = "[BPCOPS-010] Random Sampling > Severity Level 2: Bid Flagging and Focus factors - "
				+ "To verify that bids are flag as non focus for those bids released for sampling, and flag as focus for bids not released. "
				+ "Verify that all bids has focus factor: BPCOPS: Tier1 (or Tier2) Investigation Findings")
	public void testnonFocusFlagging() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		archivePage = new ArchivePage();
		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-010";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		/* GO BACK TO BRET WEB MAIN PAGE*/
		String newlyLoadedBidIdNonfocus = testData.get(BID_ID1);
		String expectedFlagNonfocus = testData.get(BID_ID1_FLAG);
		String expectedFactor = testData.get(FACTORS);
		
		/* SWITCH BACK TO ORIGINAL WINDOW */
		driver.switchTo().window(winHandleBefore);
		
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
		
		Thread.sleep(3000);

		/* THIS CLOSES THE NPS SURVEY */
		archivePage.closeNPSSurvey(driver);
		
		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.ARCHIVE.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in Archive Tab");
		System.out.println("Verify that the newly loaded bid is flagged as non focus");

		/* SEARCH FOR THE NON FOCUS BID*/
		archiveRowIndex = archivePage.searchRowIndex(driver, newlyLoadedBidIdNonfocus);
		
		/* CLICK CHECK BOX FOR THE SELECTED BID */
		archivePage.clickCheckbox(archiveRowIndex);
		
		String bidAtArchiveTab = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.BID_ID_COL);
		String bidFlag1 = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.FLAG_COL);

		Assert.assertEquals(bidAtArchiveTab, newlyLoadedBidIdNonfocus);
		Assert.assertEquals(bidFlag1, expectedFlagNonfocus);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		System.out.println("NON FOCUS BID");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdNonfocus);
		System.out.println("> Bid id (actual): " + bidAtArchiveTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlagNonfocus);
		System.out.println("> Flag (actual): " + bidFlag1 +"\n\n");

		/* SCREENSHOT FOR THE BID SUMMARY (ARCHIVE TAB) */
		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		archivePage.clickDetailLink(driver, archiveRowIndex);
		
		/* SWITCH CONTRO TO NEW WINDOW - FLAGGING AND BID DATA PAGE */
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();
		
		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB*/
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
	}
	
	/**
	 * [BPCOPS-011] Random Sampling > Severity Level 2: Outcome comments for released bids
	 * Steps:
	 * 1. Go to Remediation log of the released bid.
	 * 
	 * @throws InterruptedException
	 */

	@Test(priority = 4, description = "[BPCOPS-011] Random Sampling > Severity Level 2: Outcome comments for released bids")
	public void testNonFocusOutcomeComment() throws InterruptedException {
		remediationLogPage = new RemediationLogPage(driver);
		flaggingBidPage = new FlaggingBidDataPage(driver);

		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		String actualOutcomeComment = remediationLogPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT);

		System.out.println("\nChecking outcome comment: ");

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-011";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String expectedOutcomeComment = testData.get(OUTCOME_COMMENT);

		System.out.println("\n> Expected Outcome Comment: " + expectedOutcomeComment);
		System.out.println("> Actual Outcome Comment  : " + actualOutcomeComment +"\n\n");

		Assert.assertEquals(actualOutcomeComment, expectedOutcomeComment);

		remediationLogPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
				.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass()));

		/* SCREENSHOT OF REMEDIATION LOG TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());
		Thread.sleep(5000);
	}

	/**
	 * [BPCOPS-012] BRET Web Details > BPCOPS Category: Severity Level 2
	 * Steps:
	 * 1. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */
	
	@Test(priority = 5, description = "[BPCOPS-012] BRET Web Details > BPCOPS Category: Severity Level 2")
	public void testBPCOPSCategoryNonfocus() throws InterruptedException {
		remediationLogPage = new RemediationLogPage(driver);
		bidQPage = new BidQPage(driver);

		/* NAVIGATE TO BID Q TAB */
		remediationLogPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-012";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		String expectedDistributorCategoryValue = testData.get(BPCOPS_DISTRIBUTOR_CATEGORY_VALUE);
		String expectedCustomerCategoryValue = testData.get(BPCOPS_CUSTOMER_CATEGORY_VALUE);

		/* GET VALUE OF BPCOPS CATEGORY FROM BID Q TAB */
		String actualDistributorCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR, BidQPage.COL_HIST_BPC_OPS_CAT);
		String actualCustomerCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP, BidQPage.COL_HIST_BPC_OPS_CAT);

		System.out.println("\nChecking BPCOPS Category: \n");
		
		System.out.println("> Expected bpcops category value for T1: "   + expectedDistributorCategoryValue);
		System.out.println("> Actual bpcops category value for T1  : "   + actualDistributorCategoryValue);

		System.out.println("\n> Expected bpcops category value for T2: " + expectedCustomerCategoryValue);
		System.out.println("> Actual bpcops category value for T2  : " + actualCustomerCategoryValue +"\n\n");

		Assert.assertTrue(expectedDistributorCategoryValue.contains(actualDistributorCategoryValue), "Factor cannot be found.\n");
		Assert.assertTrue(expectedCustomerCategoryValue.contains(actualCustomerCategoryValue), "Factor cannot be found.\n");
	
		/* SCREENSHOT FOR BID Q TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel(), this.getClass());
		Thread.sleep(5000);
	}
}
