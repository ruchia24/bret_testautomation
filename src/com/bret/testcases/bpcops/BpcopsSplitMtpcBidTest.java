package com.bret.testcases.bpcops;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class BpcopsSplitMtpcBidTest extends BaseTest {
	/*SPLIT BID*/
	private static final String BID_ID1 = "split_Bid";
	private static final String BID_ID1_FLAG = "split_BidFlag";
	private static final String FACTORS1 = "split_factor";
	private static final String OUTCOME_COMMENT = "split_outcomeComment";
	private static final String BPCOPS_DISTRIBUTOR_CATEGORY_VALUE1 = "split_distributorCategoryValue";
	private static final String BPCOPS_CUSTOMER_CATEGORY_VALUE1 = "split_customerCategoryValue";
	/*MTPC*/
	private static final String BID_ID2 = "mtpc_Bid";
	private static final String BID_ID2_FLAG = "mtpc_BidFlag";
	private static final String FACTORS2 = "mtpc_factor";
	private static final String BPCOPS_DISTRIBUTOR_CATEGORY_VALUE2 = "mtpc_distributorCategoryValue";
	private static final String BPCOPS_CUSTOMER_CATEGORY_VALUE2 = "mtpc_customerCategoryValue";
	private static String winHandleBefore;

	private static final String sheetName = "TC_BPCOPS-001-030";
	private int myTaskRowIndex;

	private MyTaskPage myTaskPage;
	private FlaggingBidDataPage flaggingBidPage;
	private BidQPage bidQPage;
	private RemediationLogPage remediationLogPage;

	@BeforeTest
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	}
	
	/**
	 * [BPCOPS-026] Split Bids: BPCOPS on Split Bids
	 * Steps:
	 * 1. Launch BRET web.
	 * 2. Login using Lead reviewer role.
	 * 3. Search for the newly loaded split bid.
	 * 
	 */

	@Test(priority = 1, description = "[BPCOPS-026] Split Bids: BPCOPS on Split Bids - "
			+ "To verify that loaded split bids will be treated as 'normal' bids and will go through BPCOPS logic.")
	public void testRandomSamplingInSPLITBid() throws InterruptedException {
			myTaskPage = new MyTaskPage();
			flaggingBidPage = new FlaggingBidDataPage(driver);
			bidQPage = new BidQPage(driver);
			remediationLogPage = new RemediationLogPage(driver);

		/* GET TEST DATA FROM EXCEL */
			String tcNumber = "BPCOPS-026";
			Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String newlyLoadedBidId = testData.get(BID_ID1);
		String expectedFactor = testData.get(FACTORS1);
		String expectedFlag = testData.get(BID_ID1_FLAG);
		String expectedDistributorCategoryValue = testData.get(BPCOPS_DISTRIBUTOR_CATEGORY_VALUE1);
		String expectedCustomerCategoryValue = testData.get(BPCOPS_CUSTOMER_CATEGORY_VALUE1);

		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.MY_TASK.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in My Task Tab");
		
		/* SEARCH FOR THE NEWLY LOADED BID*/
		myTaskRowIndex = myTaskPage.searchRowIndex(driver, testData.get(BID_ID1));
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.BID_ID_COL);
		String bidFlag = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.FLAG_COL);
		
		/* CLICK CHECK BOX FOR THE SELECTED BID */
		myTaskPage.clickCheckbox(myTaskRowIndex);
		
		Assert.assertEquals(bidAtMyTaskTab, newlyLoadedBidId);
		Assert.assertEquals(bidFlag, expectedFlag);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidId);
		System.out.println("> Bid id (actual): " + bidAtMyTaskTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlag);
		System.out.println("> Flag (actual): " + bidFlag +"\n\n");

		/* SCREENSHOT OF SUMMARY PAGE - MY TASK TAB */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		myTaskPage.clickDetailLink(driver, myTaskRowIndex);
		
		/* GET CONTROL OF CURRENT WINDOW */
		winHandleBefore = driver.getWindowHandle();
		
		/* SWITCH CONTROL TO NEW WINDOW */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();
			
		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
			
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB*/
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
		
		/* CHECK BPCOPS CATEGORY FOR SPLIT BID */
		/* NAVIGATE TO BID Q TAB */
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		/* GET VALUE OF BPCOPS CATEGORY FROM BID Q TAB */
		String actualDistributorCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR, BidQPage.COL_HIST_BPC_OPS_CAT);
		String actualCustomerCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP, BidQPage.COL_HIST_BPC_OPS_CAT);

		System.out.println("\nChecking BPCOPS Category: \n");
		
		System.out.println("> Expected bpcops category value for T1: "   + expectedDistributorCategoryValue);
		System.out.println("> Actual bpcops category value for T1  : "   + actualDistributorCategoryValue);
		
		System.out.println("\n> Expected bpcops category value for T2: " + expectedCustomerCategoryValue);
		System.out.println("> Actual bpcops category value for T2  : " + actualCustomerCategoryValue +"\n\n");

		Assert.assertTrue(expectedDistributorCategoryValue.contains(actualDistributorCategoryValue), "Factor cannot be found.\n");
		Assert.assertTrue(expectedCustomerCategoryValue.contains(actualCustomerCategoryValue), "Factor cannot be found.\n");
		
		/* SCREENSHOT FOR BID Q TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel(), this.getClass());
		Thread.sleep(5000);
		
		/* CHECKING OUTCOME COMMENT */
		bidQPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		String actualOutcomeComment = remediationLogPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT);

		System.out.println("\nChecking outcome comment: ");

		String expectedOutcomeComment = testData.get(OUTCOME_COMMENT);

		System.out.println("\n> Expected Outcome Comment: " + expectedOutcomeComment);
		System.out.println("> Actual Outcome Comment  : " + actualOutcomeComment +"\n\n");

		Assert.assertEquals(actualOutcomeComment, expectedOutcomeComment);

		remediationLogPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
				.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass()));

		/* SCREENSHOT OF REMEDIATION LOG TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());
		Thread.sleep(5000);
		
		driver.close();
	}
	
	/**
	 * [BPCOPS-027] MTPC Bids: BPCOPS on MTPC Bids
	 * Steps:
	 * 1. Launch BRET web.
	 * 2. Login using Lead reviewer role.
	 * 3. Search for the newly loaded MTPC bid.
	 * 
	 * @throws InterruptedException 
	 * 
	 */
	
	@Test(priority = 2, description = "[BPCOPS-027] MTPC Bids: BPCOPS on MTPC Bids - "
			+ "To verify that MTPC bidis flagged as Focus and will not be affected by BPCOPS severity level.")
	public void testRandomSamplingInMTPCBid() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flaggingBidPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);
		remediationLogPage = new RemediationLogPage(driver);
		
		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-027";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		String newlyLoadedBidId = testData.get(BID_ID2);
		String expectedFactor = testData.get(FACTORS2);
		String expectedFlag = testData.get(BID_ID2_FLAG);
		String expectedDistributorCategoryValue = testData.get(BPCOPS_DISTRIBUTOR_CATEGORY_VALUE2);
		String expectedCustomerCategoryValue = testData.get(BPCOPS_CUSTOMER_CATEGORY_VALUE2);
		
		/* SWITCH BACK TO ORIGINAL WINDOW */
		driver.switchTo().window(winHandleBefore);
				
		Thread.sleep(3000);

		/* THIS CLOSES THE NPS SURVEY */
		myTaskPage.closeNPSSurvey(driver);

		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.MY_TASK.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in My Task Tab");
		
		/* SEARCH FOR THE NEWLY LOADED BID*/
		myTaskRowIndex = myTaskPage.searchRowIndex(driver, testData.get(BID_ID2));
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.BID_ID_COL);
		String bidFlag = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.FLAG_COL);
		
		/* CLICK CHECK BOX FOR THE SELECTED BID */
		myTaskPage.clickCheckbox(myTaskRowIndex);
		
		Assert.assertEquals(bidAtMyTaskTab, newlyLoadedBidId);
		Assert.assertEquals(bidFlag, expectedFlag);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidId);
		System.out.println("> Bid id (actual): " + bidAtMyTaskTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlag);
		System.out.println("> Flag (actual): " + bidFlag +"\n\n");

		/* SCREENSHOT OF SUMMARY PAGE - MY TASK TAB */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		pausePage();
		
		myTaskPage.clickDetailLink(driver, myTaskRowIndex);
		
		/* GET CONTROL OF CURRENT WINDOW */
		winHandleBefore = driver.getWindowHandle();
		
		/* SWITCH CONTROL TO NEW WINDOW */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();
		
		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB*/
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
		
		/* CHECK BPCOPS CATEGORY FOR SPLIT BID */
		/* NAVIGATE TO BID Q TAB */
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		/* Get the value of bpcops category from Bid Q Tab */
		String actualDistributorCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR, BidQPage.COL_HIST_BPC_OPS_CAT);
		String actualCustomerCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP, BidQPage.COL_HIST_BPC_OPS_CAT);

		System.out.println("\nChecking BPCOPS Category: \n");
		
		System.out.println("> Expected bpcops category value for T1: "   + expectedDistributorCategoryValue);
		System.out.println("> Actual bpcops category value for T1  : "   + actualDistributorCategoryValue);
		
		System.out.println("\n> Expected bpcops category value for T2: " + expectedCustomerCategoryValue);
		System.out.println("> Actual bpcops category value for T2  : " + actualCustomerCategoryValue +"\n\n");

		Assert.assertTrue(expectedDistributorCategoryValue.contains(actualDistributorCategoryValue), "Factor cannot be found.\n");
		Assert.assertTrue(expectedCustomerCategoryValue.contains(actualCustomerCategoryValue), "Factor cannot be found.\n");
		
		/* SCREENSHOT FOR BID Q TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel(), this.getClass());
		Thread.sleep(5000);
	}
}
