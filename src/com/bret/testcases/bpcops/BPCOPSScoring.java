package com.bret.testcases.bpcops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.util.BretTestUtils;

/**
 * 
 * @author LhoydCastillo March 2, 2018
 */
public class BPCOPSScoring extends BaseTest {

	private static final String TIER1_TIER2_BID_ID = "tier1and2bidId";
	private static final String TIER1_TIER2_SAME_CEID_BID_ID = "tier1and2SameBidId";
	private static final String TIER1_TIER2_DIFFERENT_CEID_BID_ID = "tier1and2DifferentBidId";
	private static final String TIER3_BID_ID = "tier3BidId";
	private static final String TIER3_TIER4_SAME_CEID_BID_ID = "sameTier3Tier4BidId";
	private static final String TIER3_TIER4_DIFFERENT_CEID_BID_ID = "differentTier3Tier4BidId";
	private static final String TIER3_SAME_CEID = "sameTier3ceid";
	private static final String TIER4_SAME_CEID = "sameTier4ceid";

	private MyTaskPage myTaskPage;
	private FlaggingBidDataPage flagBidDataPage;
	private BidQPage bidQPage;

	static String winHandleBefore;

	@BeforeTest
	public void logInAsAdministrator() {
		BRETLoginPage logIn = new BRETLoginPage(driver);
		logIn.userLogin(getAdminUsername(), getAdminPassword());
	}

	// TIER 1 AND TIER 2
	/**
	 *
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Currently on Bid Details of any of the newly loaded bid
	 * for any sev levels that T1/T2 are in BPC_OPS.FLAGS table and CEID is not NULL
	 *
	 * Step: 1. Go to Bid Q tab.
	 *
	 * @throws InterruptedException
	 *
	 */
	@Test(priority = 1, description = "Verify that bids whose T1/2 CEID are in BPC_OPS.FLAGS should have correct Outlier and score on Bid Q Tab.")
	public void tierOneTwoOutlierAndScore() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String sheetName = "TC_BPCOPS-Scoring";
		String tcNumber = "BPCOPS-032";

		System.out.println(
				"\n================= START [BPCOPS-032] BPCOPS Scoring > T1/2 on BPCOPS: Outlier and score on Bid Q Tab ==================\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(TIER1_TIER2_BID_ID));

		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);

		/* GET the control of the current window */
		winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		/* Check if one of the ceid's is NULL */
		Boolean ceidNotNull = false;
		if (tier1Ceid != null && tier2Ceid != null) {
			ceidNotNull = true;
		}

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid:    " + tier2Ceid + "\n");

		Assert.assertTrue(ceidNotNull, "One of the ceid's is NULL.");

		Thread.sleep(3000);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier1ScoreAndFlaggingAtBidQPage(driver);
		getTier2ScoreAndFlaggingAtBidQPage(driver);

		/*
		 * This is to check if Outlier Status and Score are correct: 0 Score = Outlier
		 * Status N
		 */

		Assert.assertTrue(bidQPage.distributorConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.distributorTierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");

		System.out.println("Distributor Confidentiality has correct score and flagging: "
				+ bidQPage.distributorConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Distributor Tier Findings has correct score and flagging:   "
				+ bidQPage.distributorTierInvestigationScoreAndOutlierHasCorrectFlagging());
		System.out.println("Customer Confidentiality has correct score and flagging:    "
				+ bidQPage.customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Distributor Tier Findings has correct score and flagging:   "
				+ bidQPage.customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging());
		System.out.println();

	}

	/**
	 *
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Currently on Bid Details of any of the newly loaded bid
	 * for any sev levels that T1/T2 are in BPC_OPS.FLAGS table and CEID is not NULL
	 *
	 * 1. Go to Flagging and Bid Data Tab.
	 *
	 * @throws InterruptedException
	 */
	@Test(priority = 1, description = "Verify that loaded bid's Questions & BPCOPS score should include T1/T2 BPCOPS score.")
	public void tierOneTwoOutlierAndScoreIncluded() throws InterruptedException {
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-033";

		System.out.println(
				"\n=================START [BPCOPS-033] BPCOPS Scoring > T1/2 on BPCOPS: Questions & BPCOPS score to include BPCOPS score ==================\n");

		/*
		 * navigate to FlaggingBidDataPage: get CEID and Score of QuestionAndBPCOPS
		 */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		boolean isCeidSame = false;
		String questionAndBpcopsScoreAtFlagbidPageScore = "";

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		/* Check if DistributorCeid = CustomerCeid */
		if (tier1Ceid.equals(tier2Ceid)) {
			isCeidSame = true;
		}

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid: " + tier2Ceid);

		questionAndBpcopsScoreAtFlagbidPageScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_QUEST_BPCOPS, FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScoreAtFlagbidPageScore);

		/* Compute the score at Bid Q Page */
		int computedValue = bidQPage.getTotalForBPCops(driver, isCeidSame);

		System.out.println("\nQuestions & BPCOPS Score at Flagging and Bid Data Page: " + scoreAtFlagbidPageScore);
		System.out.println("Computed Questions & BPCOPS Score at BidQ Page: " + computedValue);

		Assert.assertEquals(computedValue, scoreAtFlagbidPageScore,
				"Score at Flagging and Bid Data Page is not equal to the computed value at BidQ Page!");
	}

	/**
	 *
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Currently on Bid Details of any of the newly loaded bid
	 * for any sev levels that T1/T2 are in BPC_OPS.FLAGS table and CEID is not NULL
	 *
	 * 1. Go to Flagging and Bid Data Tab.
	 *
	 * @throws InterruptedException
	 */
	@Test(priority = 1, description = "Verify that loaded bid's Total Bid Score should include T1/T2 BPCOPS score.")
	public void tierOneTwoOutlierAndScoreIncludedOnTotalBidLevelScore() throws InterruptedException {
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-034";

		System.out.println(
				"\n=================START [BPCOPS-034] BPCOPS Scoring > T1/2 on BPCOPS: Total bid score correct calculations ==================\n");

		/* Get total bid score */
		int totalScore = flagBidDataPage.getTotalBidLevelScore();

		/* Get score of Questions & BPCOPS */
		String questionAndBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScore);

		/* Get score of other factors */
		String bidMarginPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginPercentScoreInt = Integer.parseInt(bidMarginPercentScore);

		String bidMarginValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginValueScoreInt = Integer.parseInt(bidMarginValueScore);

		String bidDiscountPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountPercentScoreInt = Integer.parseInt(bidDiscountPercentScore);

		String bidDiscountValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountValueScoreInt = Integer.parseInt(bidDiscountValueScore);

		String supportingFactorsScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS, FlaggingBidDataPage.COL_SCORE);
		int supportingFactorsScoreInt = Integer.parseInt(supportingFactorsScore);

		/* Compute the bpcops score */
		int computedBPCOPSscore = totalScore - bidMarginPercentScoreInt - bidMarginValueScoreInt
				- bidDiscountPercentScoreInt - bidDiscountValueScoreInt - supportingFactorsScoreInt;

		System.out.println("Total Bid Level Score:    " + totalScore);
		System.out.println("Bid Margin, % Score:      " + bidMarginPercentScoreInt);
		System.out.println("Bid Margin, $ Score:      " + bidMarginValueScoreInt);
		System.out.println("Bid Discount, % Score:    " + bidDiscountPercentScoreInt);
		System.out.println("Bid Discount, $ Score:    " + bidDiscountValueScoreInt);
		System.out.println("Questions & BPCOPS Score: " + scoreAtFlagbidPageScore);
		System.out.println("Supporting Factors Score: " + supportingFactorsScoreInt);

		System.out.println(
				"\nTo check if BPCOPS Score was included on the total bid level score computation, use formula: \n Questions & BPCOPS = Total Bid Level Score - Bid Margin, % - Bid Margin, $ - Bid Discount, % - Bid Discount, $ - Supporting	 Factors");

		System.out.println("\nComputed Questions & BPCOPS Score:  " + computedBPCOPSscore);
		System.out.println("Actual Questions & BPCOPS Score:    " + scoreAtFlagbidPageScore + "\n\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertEquals(scoreAtFlagbidPageScore, computedBPCOPSscore,
				"Questions & BPCOPS Score was not included in the computation of Total Bid	 Level Score!");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* CLOSE current tab and go back to original tab */
		Thread.sleep(5000);
		driver.close();

	}

	// TIER 1 AND TIER 2 - SAME CEID
	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 *
	 * 1. Go to Bid Q tab.
	 *
	 * @throws InterruptedException
	 */
	@Test(priority = 2, description = "Verify that bids whose T1 and T2 (same CEID) on BPC_OPS.FLAGS table should have the correct Outlier and score displayed on Bid Q Tab.")
	public void tierOneTwoSameCeidOutlierScoreInBidQ() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		/* GET the control of the current window and refresh page */
		driver.switchTo().window(winHandleBefore);
		driver.navigate().refresh();

		String sheetName = "TC_BPCOPS-Scoring";
		String tcNumber = "BPCOPS-036";

		System.out.println(
				"\n================= START [BPCOPS-036] BPCOPS Scoring > T1 and T2 on BPCOPS (same CEID): Outlier and score on Bid Q Tab ==================\n");

		Thread.sleep(5000);
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(TIER1_TIER2_SAME_CEID_BID_ID));

		Thread.sleep(3000);
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid:    " + tier2Ceid + "\n");

		/* Check if DistributorCeid = CustomerCeid */
		boolean ceidIsSame = false;
		if (tier1Ceid.equals(tier2Ceid)) {
			ceidIsSame = true;
		}

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertTrue(ceidIsSame, "Distributor's CEID and Customer's CEID are NOT the same!");

		Thread.sleep(3000);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier1ScoreAndFlaggingAtBidQPage(driver);
		getTier2ScoreAndFlaggingAtBidQPage(driver);
		/*
		 * This is to check if Outlier Status and Score are correct: 0 Score = Outlier
		 * Status N
		 */

		Assert.assertTrue(bidQPage.distributorConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.distributorTierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");

		System.out.println("Distributor Confidentiality has correct score and flagging: "
				+ bidQPage.distributorConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Distributor Tier Findings has correct score and flagging:   "
				+ bidQPage.distributorTierInvestigationScoreAndOutlierHasCorrectFlagging());
		System.out.println("Customer Confidentiality has correct score and flagging:    "
				+ bidQPage.customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Distributor Tier Findings has correct score and flagging:   "
				+ bidQPage.customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging());
		System.out.println();

	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 *
	 * 1. Go to Flagging and Bid Data Tab.
	 *
	 * @throws InterruptedException
	 */
	@Test(priority = 2, description = "Verify that Questions & BPCOPS score will include only	one BPCOPS	score on bids bids	with T1	and T2 CEID (same CEID) on BPC_OPS.FLAGS table.")
	public void tierOneTwoSameCeidOutlierScoreIncludeOnlyOne() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-037";

		System.out.println(
				"\n================= START [BPCOPS-037] BPCOPS Scoring > T1 and T2 on BPCOPS (same CEID): Questions & BPCOPS score to include only one BPCOPS score==================\n");

		/* NAVIGATE to Flagging and Bid Data Tab */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		/* Check if DistributorCeid = CustomerCeid */
		boolean ceidIsSame = false;
		if (tier1Ceid.equals(tier2Ceid)) {
			ceidIsSame = true;
		}

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid:    " + tier2Ceid + "\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertTrue(ceidIsSame, "Distributor's CEID and Customer's CEID are NOT the same!");

		/* GET Questions & BPCOPS at Flagging Bid Data Page */
		String actualQandBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int actualQandBpcopsScoreInt = Integer.parseInt(actualQandBpcopsScore);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier1ScoreAndFlaggingAtBidQPage(driver);
		getTier2ScoreAndFlaggingAtBidQPage(driver);
		
		int historyScore = bidQPage.computeHistoryScore(driver, ceidIsSame);
		int relatedQuestionsScore = bidQPage.computeRelatedQuestionScore(driver);
		int expectedScoreOfBPCOPS = actualQandBpcopsScoreInt - relatedQuestionsScore;

		/* To verify that only one Vertical Score is included */
		System.out.println(
				"\nTo check if the BPCOPS Score on the Total Bid Level Score only included one Vertical Value/Score of same ceid, compute using formula: \nHistory Score on Bid Q Page = Questions & BPCOPS Score on Flagging Bid Data Page - Bid Related Questions on Bid Q Page");
		System.out.println("\nQuestions & BPCOPS Score on Flagging Bid Data Page: " + actualQandBpcopsScore);
		System.out.println("History Score on Bid Q Page: " + historyScore);
		System.out.println("Bid Related Questions on Bid Q Page: " + relatedQuestionsScore);
		System.out.println(expectedScoreOfBPCOPS + " = " + actualQandBpcopsScore + " - " + relatedQuestionsScore);
		System.out.println(expectedScoreOfBPCOPS + " = " + historyScore);

		Assert.assertEquals(expectedScoreOfBPCOPS, historyScore, "Calculation of History Score inlcuded both CEID's!");
	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 *
	 * 1. Go to Flagging and Bid Data Tab.
	 *
	 * @throws InterruptedException
	 */
	@Test(priority = 2, description = "Verify that Total Bid Score should include only one BPCOPS score on bids bids with T1 and T2 CEID (same CEID) on BPC_OPS.FLAGS table.")
	public void tierOneTwoSameCeidTotalBidScoreCalculation() throws InterruptedException {
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-038";

		System.out.println(
				"\n=================[BPCOPS-038] BPCOPS Scoring > T1 and T2 on BPCOPS (same CEID): Total bid score correct calculations\n");

		/* NAVIGATE to Bid Q Tab */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		/* Check if DistributorCeid = CustomerCeid */
		boolean ceidIsSame = false;
		if (tier1Ceid.equals(tier2Ceid)) {
			ceidIsSame = true;
		}

		Assert.assertTrue(ceidIsSame, "Distributor's CEID and Customer's CEID are NOT the same!");

		/* Get total bid score */
		int totalScore = flagBidDataPage.getTotalBidLevelScore();

		/* Get score of Questions & BPCOPS */
		String questionAndBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScore);

		/* Get score of other factors */
		String bidMarginPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginPercentScoreInt = Integer.parseInt(bidMarginPercentScore);

		String bidMarginValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginValueScoreInt = Integer.parseInt(bidMarginValueScore);

		String bidDiscountPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountPercentScoreInt = Integer.parseInt(bidDiscountPercentScore);

		String bidDiscountValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountValueScoreInt = Integer.parseInt(bidDiscountValueScore);

		String supportingFactorsScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS, FlaggingBidDataPage.COL_SCORE);
		int supportingFactorsScoreInt = Integer.parseInt(supportingFactorsScore);

		/* Compute the bpcops score */
		int computedBPCOPSscore = totalScore - bidMarginPercentScoreInt - bidMarginValueScoreInt
				- bidDiscountPercentScoreInt - bidDiscountValueScoreInt - supportingFactorsScoreInt;

		System.out.println("Total Bid Level Score:    " + totalScore);
		System.out.println("Bid Margin, % Score:      " + bidMarginPercentScoreInt);
		System.out.println("Bid Margin, $ Score:      " + bidMarginValueScoreInt);
		System.out.println("Bid Discount, % Score:    " + bidDiscountPercentScoreInt);
		System.out.println("Bid Discount, $ Score:    " + bidDiscountValueScoreInt);
		System.out.println("Questions & BPCOPS Score: " + scoreAtFlagbidPageScore);
		System.out.println("Supporting Factors Score: " + supportingFactorsScoreInt);

		System.out.println(
				"\nTo check if BPCOPS Score was included on the total bid level score computation, use formula: \n Questions & BPCOPS = Total Bid Level Score - Bid Margin, % - Bid Margin, $ - Bid Discount, % - Bid Discount, $ - Supporting Factors");
		System.out.println("\nComputed Questions & BPCOPS Score:   " + computedBPCOPSscore);
		System.out.println("Actual Questions & BPCOPS Score:     " + scoreAtFlagbidPageScore + "\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertEquals(scoreAtFlagbidPageScore, computedBPCOPSscore,
				"Questions & BPCOPS Score was not included in the computation of Total Bid	 Level Score!");

		/* CLOSE current tab and go back to original tab */
		Thread.sleep(5000);
		driver.close();

	}

	// TIER 1 AND TIER 2 - DIFFERENT CEID

	/**
	 * 
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3, description = "Verify that bids whose T1 and T2 different CEID) on BPC_OPS.FLAGS table should have the correct Outlier and score displayed on Bid Q Tab.")
	public void tierOneTwoDifferentCeidOutlierScoreInBidQ() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		/* GET the control of the current window and refresh page */
		driver.switchTo().window(winHandleBefore);
		driver.navigate().refresh();

		String sheetName = "TC_BPCOPS-Scoring";
		String tcNumber = "BPCOPS-040";

		System.out.println(
				"\n================= START [BPCOPS-040] BPCOPS Scoring > T1 and T2 on BPCOPS (different CEID): Outlier and score on Bid Q Tab ==================\n");

		Thread.sleep(5000);
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(TIER1_TIER2_DIFFERENT_CEID_BID_ID));

		Thread.sleep(3000);
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid:    " + tier2Ceid + "\n");

		/* Check if DistributorCeid != CustomerCeid */
		boolean ceidIsSame = true;
		if (tier1Ceid.equals(tier2Ceid)) {
			ceidIsSame = false;
		}

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertTrue(ceidIsSame, "Distributor's CEID and Customer's CEID are NOT the same!");

		Thread.sleep(3000);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier1ScoreAndFlaggingAtBidQPage(driver);
		getTier2ScoreAndFlaggingAtBidQPage(driver);
		/*
		 * This is to check if Outlier Status and Score are correct: 0 Score = Outlier
		 * Status N
		 */

		Assert.assertTrue(bidQPage.distributorConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.distributorTierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");

		System.out.println("Distributor Confidentiality has correct score and flagging: "
				+ bidQPage.distributorConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Distributor Tier Findings has correct score and flagging:   "
				+ bidQPage.distributorTierInvestigationScoreAndOutlierHasCorrectFlagging());
		System.out.println("Customer Confidentiality has correct score and flagging:    "
				+ bidQPage.customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Distributor Tier Findings has correct score and flagging:   "
				+ bidQPage.customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging());
		System.out.println();

	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Flagging and Bid Data Tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3, description = "Verify that Questions & BPCOPS score will include both BPCOPS score on bids bids with T1 and T2 CEID (different CEID) on BPC_OPS.FLAGS table.")
	public void tierOneTwoDifferentCeidOutlierScoreIncludeOnlyOne() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-041";

		System.out.println(
				"\n================= [BPCOPS-041] BPCOPS Scoring > T1 and T2 on BPCOPS (different CEID): Questions & BPCOPS score to include only one BPCOPS score ==================\n");

		/* NAVIGATE to Flagging and Bid Data Tab */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		/* Check if DistributorCeid = CustomerCeid */
		boolean ceidNotSame = false;
		if (!tier1Ceid.equals(tier2Ceid)) {
			ceidNotSame = true;
		}

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid:    " + tier2Ceid + "\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertTrue(ceidNotSame, "Distributor's CEID and Customer's CEID are the same!");

		/* GET Questions & BPCOPS at Flagging Bid Data Page */
		String actualQandBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int actualQandBpcopsScoreInt = Integer.parseInt(actualQandBpcopsScore);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier1ScoreAndFlaggingAtBidQPage(driver);
		getTier2ScoreAndFlaggingAtBidQPage(driver);

		int historyScore = bidQPage.computeHistoryScore(driver, ceidNotSame);
		int relatedQuestionsScore = bidQPage.computeRelatedQuestionScore(driver);
		int expectedScoreOfBPCOPS = actualQandBpcopsScoreInt - relatedQuestionsScore;

		/* To verify that only one Vertical Score is included */
		System.out.println(
				"\nTo check if the BPCOPS Score on the Total Bid Level Score only included one Vertical Value/Score of same ceid, compute using formula: \nHistory Score on Bid Q Page = Questions & BPCOPS Score on Flagging Bid Data Page - Bid Related Questions on Bid Q Page");
		System.out.println("\nQuestions & BPCOPS Score on Flagging Bid Data Page: " + actualQandBpcopsScore);
		System.out.println("History Score on Bid Q Page: " + historyScore);
		System.out.println("Bid Related Questions on Bid Q Page: " + relatedQuestionsScore);
		System.out.println(expectedScoreOfBPCOPS + " = " + actualQandBpcopsScore + " - " + relatedQuestionsScore);
		System.out.println(expectedScoreOfBPCOPS + " = " + historyScore);

		Assert.assertEquals(expectedScoreOfBPCOPS, historyScore, "Calculation of History Score inlcuded both CEID's!");
	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Flagging and Bid Data Tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3, description = "Verify that Total Bid Score should include both BPCOPS score on bids bids with T1 and T2 CEID (different CEID) on BPC_OPS.FLAGS table.")
	public void tierOneTwoDifferentCeidTotalBidScoreCalculation() throws InterruptedException {
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-042";

		System.out.println(
				"\n================= [BPCOPS-042] BPCOPS Scoring > T1 and T2 on BPCOPS (different CEID): Total bid score correct calculations \n");

		/* NAVIGATE to Bid Q Tab */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		System.out.println("\nDistributor ceid: " + tier1Ceid);
		System.out.println("Customer ceid:    " + tier2Ceid + "\n");

		/* Check if DistributorCeid != CustomerCeid */
		boolean ceidIsSame = true;
		if (tier1Ceid.equals(tier2Ceid)) {
			ceidIsSame = false;
		}

		Thread.sleep(3000);
		Assert.assertTrue(ceidIsSame, "Distributor's CEID and Customer's CEID are NOT the same!");

		/* Get total bid score */
		int totalScore = flagBidDataPage.getTotalBidLevelScore();

		/* Get score of Questions & BPCOPS */
		String questionAndBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScore);

		/* Get score of other factors */
		String bidMarginPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginPercentScoreInt = Integer.parseInt(bidMarginPercentScore);

		String bidMarginValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginValueScoreInt = Integer.parseInt(bidMarginValueScore);

		String bidDiscountPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountPercentScoreInt = Integer.parseInt(bidDiscountPercentScore);

		String bidDiscountValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountValueScoreInt = Integer.parseInt(bidDiscountValueScore);

		String supportingFactorsScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS, FlaggingBidDataPage.COL_SCORE);
		int supportingFactorsScoreInt = Integer.parseInt(supportingFactorsScore);

		/* Compute the bpcops score */
		int computedBPCOPSscore = totalScore - bidMarginPercentScoreInt - bidMarginValueScoreInt
				- bidDiscountPercentScoreInt - bidDiscountValueScoreInt - supportingFactorsScoreInt;

		System.out.println("Total Bid Level Score:    " + totalScore);
		System.out.println("Bid Margin, % Score:      " + bidMarginPercentScoreInt);
		System.out.println("Bid Margin, $ Score:      " + bidMarginValueScoreInt);
		System.out.println("Bid Discount, % Score:    " + bidDiscountPercentScoreInt);
		System.out.println("Bid Discount, $ Score:    " + bidDiscountValueScoreInt);
		System.out.println("Questions & BPCOPS Score: " + scoreAtFlagbidPageScore);
		System.out.println("Supporting Factors Score: " + supportingFactorsScoreInt);

		System.out.println(
				"\nTo check if BPCOPS Score was included on the total bid level score computation, use formula: \n Questions & BPCOPS = Total Bid Level Score - Bid Margin, % - Bid Margin, $ - Bid Discount, % - Bid Discount, $ - Supporting Factors");
		System.out.println("\nComputed Questions & BPCOPS Score: " + computedBPCOPSscore);
		System.out.println("Actual Questions & BPCOPS Score: " + scoreAtFlagbidPageScore + "\n\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertEquals(scoreAtFlagbidPageScore, computedBPCOPSscore,
				"Questions & BPCOPS Score was not included in the computation of Total Bid	 Level Score!");

		/* CLOSE current tab and go back to original tab */
		Thread.sleep(5000);
		driver.close();

	}

	/**
	 * 
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 4, description = "Verify that bids whose T3 CEID  on BPC_OPS.FLAGS table should have the corect Outlier and score displayed on Bid Q Tab.")
	public void tierThreeOutlierAndScore() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		/* GET the control of the current window and refresh page -- Added 3/15/2018 */
		driver.switchTo().window(winHandleBefore);
		driver.navigate().refresh();

		String sheetName = "TC_BPCOPS-Scoring";
		String tcNumber = "BPCOPS-044";

		System.out.println(
				"\n================= [BPCOPS-044] BPCOPS Scoring > T3 only on BPCOPS: Outlier and score on Bid Q Tab ==================\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(TIER3_BID_ID));

		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);

		/* GET the control of the current window */
		winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(3000);
		String tier3Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);

		/* Check if one of the ceid's is NULL */
		Boolean ceidNotNull = false;
		if (tier3Ceid != null) {
			ceidNotNull = true;
		}

		System.out.println("\nDistributor ceid: \n" + tier3Ceid);

		Assert.assertTrue(ceidNotNull, "Additional BP (CEID) / Tier 3 is NULL.");
		Thread.sleep(3000);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		boolean tier3isPresent = bidQPage.tier3isPresent(driver);
		Assert.assertTrue(tier3isPresent, "Tier 3 is not Present!");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier3ScoreAndFlaggingAtBidQPage(driver);

		/*
		 * This is to check if Outlier Status and Score are correct: 0 Score = Outlier
		 * Status N
		 */

		Assert.assertTrue(bidQPage.additionalBPTier3ConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.additionalBPTier3TierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");

		System.out.println("Additional BP Tier 3 Confidentiality has correct score and flagging:    "
				+ bidQPage.additionalBPTier3ConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Additional BP Tier 3 Tier Findings has correct score and flagging:   "
				+ bidQPage.additionalBPTier3TierInvestigationScoreAndOutlierHasCorrectFlagging());

		System.out.println();
	}

	/**
	 * 
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Flagging and Bid Data Tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 4, description = "Verify that Questions & BPCOPS score will include BPCOPS score on bids with T3 CEID on BPC_OPS.FLAGS table.")
	public void tierThreeOutlierAndScoreIncluded() throws InterruptedException {
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-045";

		System.out.println(
				"\n=================START [BPCOPS-045] BPCOPS Scoring > T3 only on BPCOPS: Questions & BPCOPS score to include BPCOPS score ==================\n");

		/*
		 * navigate to FlaggingBidDataPage: get CEID and Score of QuestionAndBPCOPS
		 */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		boolean isCeidSame = false;
		String questionAndBpcopsScoreAtFlagbidPageScore = "";

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Thread.sleep(3000);
		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);
		String tier3Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);

		/* Check if CEID's are equal */
		if (tier1Ceid.equals(tier2Ceid)) {
			isCeidSame = true;
		}

		System.out.println("\nDistributor ceid:    " + tier1Ceid);
		System.out.println("Customer ceid:       " + tier2Ceid);
		System.out.println("Additional BP ceid:  \n" + tier3Ceid);

		Thread.sleep(3000);
		questionAndBpcopsScoreAtFlagbidPageScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_QUEST_BPCOPS, FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScoreAtFlagbidPageScore);

		/* Compute the score at Bid Q Page */
		int computedValue = bidQPage.getTotalForBPCops(driver, isCeidSame);

		System.out.println("\nQuestions & BPCOPS Score at Flagging and Bid Data Page: " + scoreAtFlagbidPageScore);
		System.out.println("Computed Questions & BPCOPS Score at BidQ Page: " + computedValue);

		Assert.assertEquals(computedValue, questionAndBpcopsScoreAtFlagbidPageScore,
				"Score at Flagging and Bid Data Page is not equal to the computed value at BidQ Page!");

	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Flagging and Bid Data Tab.s
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 4, description = "Verify that Total Bid Score should include BPCOPS score on bids with T3 CEID on BPC_OPS.FLAGS table.")
	public void tierThreeOutlierAndScoreIncludedOnTotalBidLevelScore() throws InterruptedException {

		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-046";

		System.out.println(
				"\n================= START [BPCOPS-046] BPCOPS Scoring > T3 only on BPCOPS: Total bid score correct calculations ==================\n");

		/* Get total bid score */
		int totalScore = flagBidDataPage.getTotalBidLevelScore();

		/* Get score of Questions & BPCOPS */
		String questionAndBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScore);

		/* Get score of other factors */
		String bidMarginPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginPercentScoreInt = Integer.parseInt(bidMarginPercentScore);

		String bidMarginValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginValueScoreInt = Integer.parseInt(bidMarginValueScore);

		String bidDiscountPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountPercentScoreInt = Integer.parseInt(bidDiscountPercentScore);

		String bidDiscountValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountValueScoreInt = Integer.parseInt(bidDiscountValueScore);

		String supportingFactorsScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS, FlaggingBidDataPage.COL_SCORE);
		int supportingFactorsScoreInt = Integer.parseInt(supportingFactorsScore);

		/* Compute the bpcops score */
		int computedBPCOPSscore = totalScore - bidMarginPercentScoreInt - bidMarginValueScoreInt
				- bidDiscountPercentScoreInt - bidDiscountValueScoreInt - supportingFactorsScoreInt;

		System.out.println("Total Bid Level Score:    " + totalScore);
		System.out.println("Bid Margin, % Score:      " + bidMarginPercentScoreInt);
		System.out.println("Bid Margin, $ Score:      " + bidMarginValueScoreInt);
		System.out.println("Bid Discount, % Score:    " + bidDiscountPercentScoreInt);
		System.out.println("Bid Discount, $ Score:    " + bidDiscountValueScoreInt);
		System.out.println("Questions & BPCOPS Score: " + scoreAtFlagbidPageScore);
		System.out.println("Supporting Factors Score: " + supportingFactorsScoreInt);

		System.out.println(
				"\nTo check if BPCOPS Score was included on the total bid level score computation, use formula: \n Questions & BPCOPS = Total Bid Level Score - Bid Margin, % - Bid Margin, $ - Bid Discount, % - Bid Discount, $ - Supporting	 Factors");

		System.out.println("\nComputed Questions & BPCOPS Score:  " + computedBPCOPSscore);
		System.out.println("Actual Questions & BPCOPS Score:    " + scoreAtFlagbidPageScore + "\n\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertEquals(scoreAtFlagbidPageScore, computedBPCOPSscore,
				"Questions & BPCOPS Score was not included in the computation of Total Bid	 Level Score!");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* CLOSE current tab and go back to original tab */
		Thread.sleep(5000);
		driver.close();
	}

	/* SAME CEID T3 T4 */
	/**
	 * 
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5, description = "Verify that Bids whose T3 and T4 (same CEID) on BPC_OPS.FLAGS table should have the correct  Outlier and score displayed on Bid Q Tab.")
	public void tierThreeFourSameCeidOutlierAndScoreInBidQ() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		/* GET the control of the current window and refresh page */
		driver.switchTo().window(winHandleBefore);
		driver.navigate().refresh();

		String sheetName = "TC_BPCOPS-Scoring";
		String tcNumber = "BPCOPS-048";

		System.out.println(
				"\n================= START [BPCOPS-048] BPCOPS Scoring > T3 and T4 on BPCOPS (same CEID): Outlier and score on Bid Q Tab ==================\n");

		Thread.sleep(5000);

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(TIER3_TIER4_SAME_CEID_BID_ID));
		String tier3ceid = testData.get(TIER3_SAME_CEID);
		String tier4ceid = testData.get(TIER4_SAME_CEID);

		Thread.sleep(3000);
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		myTaskPage.clickDetailLink(driver, rowAtMyTaskTab);

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(3000);

		String additionalBPceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);

		System.out.println("\nAdditional BP CEID's are: " + additionalBPceid);

		/* Check if DistributorCeid = CustomerCeid */
		boolean ceidIsSame = false;
		if (additionalBPceid.contains(tier3ceid) && additionalBPceid.contains(tier4ceid)) {
			ceidIsSame = true;
		}

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertTrue(ceidIsSame, "Additional BP's CEID are NOT the same!");
		Thread.sleep(3000);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		getTier3ScoreAndFlaggingAtBidQPage(driver);
		getTier4ScoreAndFlaggingAtBidQPage(driver);
	
		/*
		 * This is to check if Outlier Status and Score are correct: 0 Score = Outlier
		 * Status N
		 */

		Assert.assertTrue(bidQPage.additionalBPTier3ConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.additionalBPTier3TierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");

		Assert.assertTrue(bidQPage.additionalBPTier4ConfidentialityScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");
		Assert.assertTrue(bidQPage.additionalBPTier4TierInvestigationScoreAndOutlierHasCorrectFlagging(),
				"Incorrect flagging of score and outlier status!");

		System.out.println("Additional BP Tier 3 Confidentiality has correct score and flagging:    "
				+ bidQPage.additionalBPTier3ConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Additional BP Tier 3 Tier Findings has correct score and flagging:   "
				+ bidQPage.additionalBPTier3TierInvestigationScoreAndOutlierHasCorrectFlagging());

		System.out.println("Additional BP Tier 4 Confidentiality has correct score and flagging:    "
				+ bidQPage.additionalBPTier3ConfidentialityScoreAndOutlierHasCorrectFlagging());
		System.out.println("Additional BP Tier 4 Tier Findings has correct score and flagging:   "
				+ bidQPage.additionalBPTier3TierInvestigationScoreAndOutlierHasCorrectFlagging());

	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Flagging and Bid Data Tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5, description = "[BPCOPS-049] BPCOPS Scoring > T3 and T4 on BPCOPS (same CEID): Questions & BPCOPS score to include only one BPCOPS score")
	public void tierThreeFourSameCeidOutlierAndScoreIncludeOnlyOne() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-049";

		System.out.println(
				"\n================= START [BPCOPS-049] BPCOPS Scoring > T3 and T4 on BPCOPS (same CEID): Questions & BPCOPS score to include only one BPCOPS score ==================\n");

		/* NAVIGATE to Flagging and Bid Data Tab */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		Thread.sleep(5000);

		String[] tier3Andtier4ceid = this.getTier3andTier4ceid(driver);

		String tier3ceid = tier3Andtier4ceid[0];
		String tier4ceid = tier3Andtier4ceid[1];

		/* Check if DistributorCeid = CustomerCeid */
		boolean ceidIsSame = false;
		if (tier3ceid.equals(tier4ceid)) {
			ceidIsSame = true;
		}

		Thread.sleep(5000);

		System.out.println("\nAdditional BP ceid (Tier 3): " + tier3ceid);
		System.out.println("Additional BP ceid (Tier 4): " + tier4ceid + "\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertTrue(ceidIsSame, "TIER 3 CEID and TIER 4 CEID are NOT the same!");

		/* GET Questions & BPCOPS at Flagging Bid Data Page */
		String actualQandBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int actualQandBpcopsScoreInt = Integer.parseInt(actualQandBpcopsScore);

		/* Get the current rows */
		List<Integer> counterRows = getCountedRows(driver);

		/* NAVIGATE to Bid Q Tab */
		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		/**
		 * TODO: create methods to handle different combinations of Tier 1 to 4 similar
		 * ceids
		 */
		String distribConfiScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_SCORE_1);

		String distribTierScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_SCORE_2);

		String custConfiScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_SCORE_1);

		String custTierScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_SCORE_2);
		
		String additionalBPtier3ConfiScore = bidQPage.getValueAtIndexHistory(driver,
				BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3, BidQPage.COL_HIST_SCORE_1);

		String additionalBPtier3TierScore = bidQPage.getValueAtIndexHistory(driver,
				BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3, BidQPage.COL_HIST_SCORE_2);

		String additionalBPtier4ConfiScore = bidQPage.getValueAtIndexHistory(driver,
				BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4, BidQPage.COL_HIST_SCORE_1);

		String additionalBPtier4TierScore = bidQPage.getValueAtIndexHistory(driver,
				BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4, BidQPage.COL_HIST_SCORE_2);

		System.out.println("\nDistributor Confidentiality Score :    " + distribConfiScore);
		System.out.println("Distributor Tier Investigation Score : " + distribTierScore);
		System.out.println("\nCustomer Confidentiality Score :    " + custConfiScore);
		System.out.println("Customer Tier Investigation Score : " + custTierScore);

		System.out.println("\nAdditional BP Tier 3 Confidentiality Score :    " + additionalBPtier3ConfiScore);
		System.out.println("Additional BP Tier 3 Tier Investigation Score : " + additionalBPtier3TierScore);
		System.out.println("\nAdditional BP Tier 4 Confidentiality Score :    " + additionalBPtier4ConfiScore);
		System.out.println("Additional BP Tier 4 Tier Investigation Score : " + additionalBPtier4TierScore);

		int historyScore = bidQPage.computeHistoryScore(driver, counterRows);
		int relatedQuestionsScore = bidQPage.computeRelatedQuestionScore(driver);
		int expectedScoreOfBPCOPS = actualQandBpcopsScoreInt - relatedQuestionsScore;

		/* To verify that only one Vertical Score is included */
		System.out.println(
				"\nTo check if the BPCOPS Score on the Total Bid Level Score only included one Vertical Value/Score of same ceid, compute using formula: \nHistory Score on Bid Q Page = Questions & BPCOPS Score on Flagging Bid Data Page - Bid Related Questions on Bid Q Page");
		System.out.println("\nQuestions & BPCOPS Score on Flagging Bid Data Page: " + actualQandBpcopsScore);
		System.out.println("History Score on Bid Q Page: " + historyScore);
		System.out.println("Bid Related Questions on Bid Q Page: " + relatedQuestionsScore);
		System.out.println(expectedScoreOfBPCOPS + " = " + actualQandBpcopsScore + " - " + relatedQuestionsScore);
		System.out.println(expectedScoreOfBPCOPS + " = " + historyScore);

		Assert.assertEquals(expectedScoreOfBPCOPS, historyScore, "Calculation of History Score inlcuded both CEID's!");
	}

	/**
	 * - Logged in BRET Web using any role with access to My Task/Focus/Non
	 * Focus/Archive tab - Newly loaded bids are selected - Currently on Bid Details
	 * of any of the newly loaded bid
	 * 
	 * 1. Go to Flagging and Bid Data Tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5, description = "Verify that Total Bid Score should include BPCOPS score on bids with T3 CEID on BPC_OPS.FLAGS table.")
	public void tierThreeFourSameCeidOutlierAndScoreIncludedOnTotalBidLevelScore() throws InterruptedException {

		flagBidDataPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		String tcNumber = "BPCOPS-050";

		System.out.println(
				"\n================= START [BPCOPS-050] BPCOPS Scoring > T3 and T4 on BPCOPS (same CEID): Total bid score correct calculations core  ==================\n");

		/* NAVIGATE to Bid Q Tab */
		bidQPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);

		/* Get total bid score */
		int totalScore = flagBidDataPage.getTotalBidLevelScore();

		/* Get score of other factors */
		String questionAndBpcopsScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		int scoreAtFlagbidPageScore = Integer.parseInt(questionAndBpcopsScore);

		String bidMarginPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginPercentScoreInt = Integer.parseInt(bidMarginPercentScore);

		String bidMarginValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidMarginValueScoreInt = Integer.parseInt(bidMarginValueScore);

		String bidDiscountPercentScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountPercentScoreInt = Integer.parseInt(bidDiscountPercentScore);

		String bidDiscountValueScore = flagBidDataPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_VALUE,
				FlaggingBidDataPage.COL_SCORE);
		int bidDiscountValueScoreInt = Integer.parseInt(bidDiscountValueScore);

		String supportingFactorsScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS, FlaggingBidDataPage.COL_SCORE);
		int supportingFactorsScoreInt = Integer.parseInt(supportingFactorsScore);

		/* Compute the bpcops score */
		int computedBPCOPSscore = totalScore - bidMarginPercentScoreInt - bidMarginValueScoreInt
				- bidDiscountPercentScoreInt - bidDiscountValueScoreInt - supportingFactorsScoreInt;

		System.out.println("Total Bid Level Score:    " + totalScore);
		System.out.println("Bid Margin, % Score:      " + bidMarginPercentScoreInt);
		System.out.println("Bid Margin, $ Score:      " + bidMarginValueScoreInt);
		System.out.println("Bid Discount, % Score:    " + bidDiscountPercentScoreInt);
		System.out.println("Bid Discount, $ Score:    " + bidDiscountValueScoreInt);
		System.out.println("Questions & BPCOPS Score: " + scoreAtFlagbidPageScore);

		System.out.println("Supporting Factors Score: " + supportingFactorsScoreInt);

		System.out.println(
				"\nTo check if BPCOPS Score was included on the total bid level score computation, use formula: \n Questions & BPCOPS = Total Bid Level Score - Bid Margin, % - Bid Margin, $ - Bid Discount, % - Bid Discount, $ - Supporting	 Factors");

		System.out.println("\nComputed Questions & BPCOPS Score:  " + computedBPCOPSscore);
		System.out.println("Actual Questions & BPCOPS Score:    " + scoreAtFlagbidPageScore + "\n\n");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		Assert.assertEquals(scoreAtFlagbidPageScore, computedBPCOPSscore,
				"Questions & BPCOPS Score was not included in the computation of Total Bid	 Level Score!");

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* CLOSE current tab and go back to original tab */
		Thread.sleep(5000);
		driver.close();
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}

	private List<Integer> getCountedRows(WebDriver driver) {
		flagBidDataPage = new FlaggingBidDataPage(driver);

		Map<String, Integer> ceidMap = new HashMap<>();

		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		String tier3tier4Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);

		String[] tier3tier4Ceidseparated = tier3tier4Ceid.replace("(", "").replace(")", "").split("\n");

		String tier3Ceid = tier3tier4Ceidseparated[0];
		String tier4Ceid = tier3tier4Ceidseparated[1];

		ceidMap.put(tier1Ceid, BidQPage.ROW_HIST_DISTRIBUTOR);
		ceidMap.put(tier2Ceid, BidQPage.ROW_HIST_CUST_FACING_BP);
		ceidMap.put(tier3Ceid, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3);
		ceidMap.put(tier4Ceid, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4);

		List<Integer> countedRow = new ArrayList<Integer>(ceidMap.values());

		return countedRow;
	}

	private String[] getTier3andTier4ceid(WebDriver driver) {
		String tier3tier4Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);

		String[] tier3tier4Ceidseparated = tier3tier4Ceid.replace("(", "").replace(")", "").split("\n");

		String tier3Ceid = tier3tier4Ceidseparated[0];
		String tier4Ceid = tier3tier4Ceidseparated[1];

		return new String[] { tier3Ceid, tier4Ceid };
	}

	private void getTier1ScoreAndFlaggingAtBidQPage(WebDriver driver) {
		bidQPage = new BidQPage(driver);

		String distribConfiScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_SCORE_1);
		String distribConfiOutlier = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_OUTLIER_STATUS_1);

		String distribTierScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_SCORE_2);
		String distribTierOutlier = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR,
				BidQPage.COL_HIST_OUTLIER_STATUS_2);

		System.out.println("\nDistributor Confidentiality Score:      " + distribConfiScore);
		System.out.println("Distributor Confidentiality Outlier:    " + distribConfiOutlier);
		System.out.println("Distributor Tier Investigation Score:   " + distribTierScore);
		System.out.println("Distributor Tier Investigation Outlier: " + distribTierOutlier);

	}

	private void getTier2ScoreAndFlaggingAtBidQPage(WebDriver driver) {
		bidQPage = new BidQPage(driver);

		String custConfiScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_SCORE_1);
		String custConfiOutlierStatus = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_OUTLIER_STATUS_1);

		String custTierScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_SCORE_2);
		String custTierOutlierStatus = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP,
				BidQPage.COL_HIST_OUTLIER_STATUS_2);

		System.out.println("\nCustomer Confidentiality Score :      " + custConfiScore);
		System.out.println("Customer Confidentiality Outlier :    " + custConfiOutlierStatus);
		System.out.println("Customer Tier Investigation Score :   " + custTierScore);
		System.out.println("Customer Tier Investigation Outlier : " + custTierOutlierStatus + "\n");

	}

	private void getTier3ScoreAndFlaggingAtBidQPage(WebDriver driver) {
		bidQPage = new BidQPage(driver);

		String tier3ConfiScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3,
				BidQPage.COL_HIST_SCORE_1);
		String tier3ConfiOutlierStatus = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3,
				BidQPage.COL_HIST_OUTLIER_STATUS_1);
		String tier3TierScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3,
				BidQPage.COL_HIST_SCORE_2);
		String tier3TierOutlierStatus = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_3,
				BidQPage.COL_HIST_OUTLIER_STATUS_2);

		System.out.println("\nAdditional BP Tier 3 Confidentiality Score :      " + tier3ConfiScore);
		System.out.println("Additional BP Tier 3 Confidentiality Outlier :    " + tier3ConfiOutlierStatus);
		System.out.println("Additional BP Tier 3 Tier Investigation Score :   " + tier3TierScore);
		System.out.println("Additional BP Tier 3 Tier Investigation Outlier : " + tier3TierOutlierStatus + "\n");

	}

	private void getTier4ScoreAndFlaggingAtBidQPage(WebDriver driver) {
		bidQPage = new BidQPage(driver);

		String tier4ConfiScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4,
				BidQPage.COL_HIST_SCORE_1);
		String tier4ConfiOutlierStatus = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4,
				BidQPage.COL_HIST_OUTLIER_STATUS_1);
		String tier4TierScore = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4,
				BidQPage.COL_HIST_SCORE_2);
		String tier4TierOutlierStatus = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_ADDITIONAL_BP_TIER_4,
				BidQPage.COL_HIST_OUTLIER_STATUS_2);

		System.out.println("\nAdditional BP Tier 4 Confidentiality Score :      " + tier4ConfiScore);
		System.out.println("Additional BP Tier 4 Confidentiality Outlier :    " + tier4ConfiOutlierStatus);
		System.out.println("Additional BP Tier 4 Tier Investigation Score :   " + tier4TierScore);
		System.out.println("Additional BP Tier 4 Tier Investigation Outlier : " + tier4TierOutlierStatus + "\n");

	}

}
