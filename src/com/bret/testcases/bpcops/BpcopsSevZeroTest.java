package com.bret.testcases.bpcops;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class BpcopsSevZeroTest extends BaseTest {
	protected static final String BID_ID1 = "sev0_nonFocusBid";
	protected static final String BID_ID1_FLAG = "sev0_nonFocusBidFlag";
	private static final String FACTORS = "sev0_factor";
	private static final String OUTCOME_COMMENT = "sev0_outcomeComment";
	private static final String BPCOPS_DISTRIBUTOR_CATEGORY_VALUE = "sev0_distributorCategoryValue";
	private static final String BPCOPS_CUSTOMER_CATEGORY_VALUE = "sev0_customerCategoryValue";
	private static final String sheetName = "TC_BPCOPS-001-030";
	private int archiveRowIndex;
	
	private MyTaskPage myTaskPage;
	private ArchivePage archivePage;
	private FlaggingBidDataPage flaggingBidPage;
	private BidQPage bidQPage;
	private RemediationLogPage remediationLogPage;

	@BeforeTest
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	}
	
	/**
	 * [BPCOPS-002] Random Sampling > Severity Level 0: Bid Flagging and Focus factors
	 * Steps:
	 * 1. Launch BRET web.
	 * 2. Login using Lead reviewer role.
	 * 3. Search for the newly loaded bids.
	 * 4. Check summary tab and bids details page of the selected bids.
	 * 
	 * @param detailLinkPart
	 * @throws InterruptedException
	 */

	@Test(priority = 1, description = "[BPCOPS-002] Random Sampling > Severity Level 0: Bid Flagging and Focus factors - "
			+ "To verify that all bids are flagged as Non focus and has focus factor on the bid: BPCOPS: Tier1 (or Tier2) Investigation Findings")
	public void testNonFocusFlagging() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		archivePage = new ArchivePage();
		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-002";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String newlyLoadedBidIdNonfocus = testData.get(BID_ID1);
		String expectedFactor = testData.get(FACTORS);
		String expectedFlag = testData.get(BID_ID1_FLAG);

		/* NAVIGATE TO ARCHIVE TAB*/
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		Thread.sleep(5000);
		
		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.ARCHIVE.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in Archive Tab");
		System.out.println("DESCRIPTION: To verify that all bids are flagged as Non focus and has focus factor on the bid: BPCOPS: Tier1 (or Tier2) Investigation Findings\n");

		/* SEARCH FOR THE NEWLY LOADED NON FOCUS BID*/
		archiveRowIndex = archivePage.searchRowIndex(driver, newlyLoadedBidIdNonfocus);
		
		/* CLICK CHECK BOX FOR THE SELECTED BID */
		archivePage.clickCheckbox(archiveRowIndex);
		
		String bidAtArchiveTab = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.BID_ID_COL);
		String bidFlag = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.FLAG_COL);

		Assert.assertEquals(bidAtArchiveTab, newlyLoadedBidIdNonfocus);
		Assert.assertEquals(bidFlag, expectedFlag);		

		System.out.println("\nChecking BID ID and Flagging: ");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdNonfocus);
		System.out.println("> Bid id (actual): " + bidAtArchiveTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlag);
		System.out.println("> Flag (actual): " + bidFlag +"\n\n");

		/* SCREENSHOT OF SUMMARY PAGE - ARCHIVE TAB */
		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		archivePage.clickDetailLink(driver, archiveRowIndex);

		/* SWITCH CONTROL TO NEW WINDOW - FLAGGING AND BID DATA TAB */
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();
		
		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();
		
		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB*/
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
	}
	
	/**
	 * [BPCOPS-003] Random Sampling > Severity Level 0: Outcome comments for released bids
	 * Steps:
	 * 1. Go to Remediation log of the released bid.
	 * 
	 * @throws InterruptedException
	 */

	@Test(priority = 2, description = "[BPCOPS-003] Random Sampling > Severity Level 0: Outcome comments for released bids")
	public void testNonFocusOutcomeComment() throws InterruptedException {
		remediationLogPage = new RemediationLogPage(driver);
		flaggingBidPage = new FlaggingBidDataPage(driver);

		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);

		String actualOutcomeComment = remediationLogPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT);

		System.out.println("\nChecking outcome comment: ");

		String tcNumber = "BPCOPS-003";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String expectedOutcomeComment = testData.get(OUTCOME_COMMENT);

		System.out.println("\n> Expected Outcome Comment: " + expectedOutcomeComment);
		System.out.println("> Actual Outcome Comment  : " + actualOutcomeComment +"\n\n");

		Assert.assertEquals(actualOutcomeComment, expectedOutcomeComment);

		remediationLogPage.scrollingScreenshot(driver, getScreenshotPath(), BretTestUtils
				.getImgFilenameNoFormat(tcNumber + DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass()));

		/* SCREENSHOT OF REMEDIATION LOG TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel(), this.getClass());
		Thread.sleep(5000);
	}

	/**
	 * [BPCOPS-004] BRET Web Details > BPCOPS Category: Severity Level 0
	 * Steps:
	 * 1. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */

	@Test(priority = 3, description = "[BPCOPS-004] BRET Web Details > BPCOPS Category: Severity Level 0")
	public void testBPCOPSCategoryNonfocus() throws InterruptedException {
		remediationLogPage = new RemediationLogPage(driver);
		bidQPage = new BidQPage(driver);

		/* NAVIGATE TO BID Q TAB */
		remediationLogPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		String tcNumber = "BPCOPS-004";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		String expectedDistributorCategoryValue = testData.get(BPCOPS_DISTRIBUTOR_CATEGORY_VALUE);
		String expectedCustomerCategoryValue = testData.get(BPCOPS_CUSTOMER_CATEGORY_VALUE);

		/* BPCOPS CATEGORY FOR NON FOCUS BID */
		String actualDistributorCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR, BidQPage.COL_HIST_BPC_OPS_CAT);
		String actualCustomerCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP, BidQPage.COL_HIST_BPC_OPS_CAT);

		System.out.println("\nChecking BPCOPS Category: \n");
		
		System.out.println("> Expected bpcops category value for T1: " + expectedDistributorCategoryValue);
		System.out.println("> Actual bpcops category value for T1  : " + actualDistributorCategoryValue);

		System.out.println("\n> Expected bpcops category value for T2: " + expectedCustomerCategoryValue);
		System.out.println("> Actual bpcops category value for T2  : " + actualCustomerCategoryValue +"\n\n");

		Assert.assertTrue(expectedDistributorCategoryValue.contains(actualDistributorCategoryValue), "Factor cannot be found.\n");
		Assert.assertTrue(expectedCustomerCategoryValue.contains(actualCustomerCategoryValue), "Factor cannot be found.\n");

		/* SCREENSHOT FOR BID Q TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel(), this.getClass());
		Thread.sleep(5000);
	}
}
