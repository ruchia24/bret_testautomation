//package com.bret.testcases.bpcops;
//
//import java.util.Map;
//
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//import com.bret.base.BaseBpcopsTest;
//import com.bret.enums.DetailPageTabsE;
//import com.bret.enums.SummaryPageTabsE;
//import com.bret.pages.ArchivePage;
//import com.bret.pages.BRETLoginPage;
//import com.bret.pages.FlaggingBidDataPage;
//import com.bret.pages.MyTaskPage;
//import com.bret.util.BretTestUtils;
//
//public class BpcopsSevZeroToTerm extends BaseBpcopsTest {
//	
//	/**
//	 * [BPCOPS-002] Random Sampling > Severity Level 0: Bid Flagging and Focus factors
//	 * Steps:
//	 * 1. Launch BRET web.
//	 * 2. Login using any role with access to My Task/Focus/Non Focus/Archive tab.
//	 * 3. Search for the newly loaded bids.
//	 * 4. Check summary tab and bids details page of the selected bids.
//	 * 
//	 * @param detailLinkPart
//	 * @throws InterruptedException
//	 */
//	
//	@Test(priority = 1)
//	public void testNonFocusFlagging() {
//		String tcNumber = "TC_RSW-002";
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
//		MyTaskPage myTaskPage = new MyTaskPage();
//		ArchivePage archivePage = new ArchivePage();
//		String newlyLoadedBidIdNonfocus = testData.get(BID_ID1);
//		String expectedFlag = testData.get(BID_ID1_FLAG);
//
//		BRETLoginPage loginPage = new BRETLoginPage(driver);
//		loginPage.userLogin(getAdminUsername(), getAdminPassword());
//		
//		/* NAVIGATE TO ARCHIVE TAB*/
//		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
//
//		pausePage();
//		
//		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
//		System.out.println("\n========== " + SummaryPageTabsE.ARCHIVE.getLabel() + " tab ==========");
//		System.out.println("Verify that the newly loaded bid is present in Archive Tab");
//		System.out.println("DESCRIPTION: To verify that all bids are flagged as Non focus and has focus factor on the bid: BPCOPS: Tier1 (or Tier2) Investigation Findings\n");
//
//		/* SEARCH FOR THE NEWLY LOADED NON FOCUS BID*/
//		archiveRowIndex = archivePage.searchRowIndex(driver, newlyLoadedBidIdNonfocus);
//		
//		/* CLICK CHECK BOX FOR THE SELECTED BID */
//		archivePage.clickCheckbox(archiveRowIndex);
//		
//		String bidAtArchiveTab = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.BID_ID_COL);
//		String bidFlag = archivePage.getValueAtIndex(driver, archiveRowIndex, ArchivePage.FLAG_COL);
//
//		Assert.assertEquals(bidAtArchiveTab, newlyLoadedBidIdNonfocus);
//		Assert.assertEquals(bidFlag, expectedFlag);
//
//		System.out.println("\nChecking BID ID and Flagging: ");
//		
//		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdNonfocus);
//		System.out.println("> Bid id (actual): " + bidAtArchiveTab +"\n");
//
//		System.out.println("> Expected Flag: " + expectedFlag);
//		System.out.println("> Flag (actual): " + bidFlag +"\n\n");
//
//		/* SCREENSHOT OF SUMMARY PAGE - ARCHIVE TAB */
//		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel(), this.getClass());
//		
//		pausePage();
//		
//		archivePage.clickDetailLink(driver, archiveRowIndex);
//
//		/* SWITCH CONTROL TO NEW WINDOW - FLAGGING AND BID DATA TAB */
//		pausePage();
//		switchToNewWindow();
//		setupNewURL(driver.getCurrentUrl());
//		pausePage();
//		
//		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
//		
//		String actualFactorFocus = flaggingBidPage.getFactorsValue();
//		
//		System.out.println("\n\nChecking Focus Factor: ");
//		
////		System.out.println("\n> Expected Factor: " + expectedFactor);
//		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
////		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");
//
//		pausePage();
//		
//		/* SCREENSHOT OF FLAGGING AND BID DATA TAB*/
//		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
//		pausePage();
//	}
//}
