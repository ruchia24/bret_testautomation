package com.bret.enums;

public enum DetailPageTabsE {
	FLAGGING_BID_DATA("Flagging and Bid Data"), DISCOUNT_MARGIN(
			"Discount + Margin"), BID_Q("Bid Qs"), SUPPORT_DATA("Support Data"), BELOW_CLIP_BIDS(
			"Below Clip Bids"), REMEDIATION_LOG("Remediation Log");

	private String label;

	private DetailPageTabsE(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
