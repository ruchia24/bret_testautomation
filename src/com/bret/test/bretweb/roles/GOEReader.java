package com.bret.test.bretweb.roles;

import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETDashboardLoginPage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.DetailedReportPage;
import com.bret.pages.FocusPage;
import com.bret.pages.GoeDeterminationPage;
import com.bret.pages.ManualReviewPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SummaryPage;
import com.bret.pages.UserDetailPage;
import com.bret.pages.UserManagementPage;
import com.bret.util.BretTestUtils;

public class GOEReader extends BaseTest{

	private MyTaskPage myTaskPage;
	private SummaryPage summaryPage;
	private NonFocusPage nonFocusPage;
	private FocusPage focusPage;
	private ArchivePage archivePage;
	private AdvancedSearchPage advancedSearch;
	private ManualReviewPage manualReviewPage;
	private GoeDeterminationPage goeDeterminationPage;
	private UserManagementPage userManagementPage;
	private UserDetailPage userDetailPage;
	private DetailedReportPage detailedReportPage;


	@BeforeTest
	public void loginAsReaderDetail()  throws TimeoutException, InterruptedException, ExecutionException  {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	
		try {
			WebDriverWait wait=new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.titleContains("Special Bid Risk Analysis - Bid Summary"));
	
		} catch (Exception e) {
			throw new TimeoutException("Invalid Username or Password");
		}
	}


//	@Test(priority = 1)
//	public void showAccessDetailsSourceSystem() throws InterruptedException {
//		String tcNumber = "TC_BWRGR-001 ";
//		displayLogHeader(tcNumber);
//		
//		myTaskPage = new MyTaskPage();
//		myTaskPage.clickUserName(driver);
//		myTaskPage.clickShowMyPrivilege(driver);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//	}
//	
//	@Test(priority = 2)
//	public void showAccessDetailsRegion() throws InterruptedException {
//
//		String tcNumber = "TC_BWRGR-002 ";
//		displayLogHeader(tcNumber);
//		
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//		Thread.sleep(3000);
//
//		WebElement closeButton = driver.findElement(By.xpath("//*[@id=\"dijit_form_Button_2_label\"]"));
//		closeButton.click();
//	}
//	
	@Test(priority = 3)
	public void showMyTaskTab() throws InterruptedException, TimeoutException {

		String tcNumber = "TC_BWRGR-003 ";
		displayLogHeader(tcNumber);
		
		WebElement MyTaskTab = driver
				.findElement(By.xpath("//*[@id=\"dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_0\"]"));
		// My task tab xpath, to add it on My Task Page

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

	}
	
	@Test(priority = 4)
	public void archiveNonFocusBidAtMyTaskTab() throws InterruptedException {
	
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRGR-004 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

	}

	@Test(priority = 5)
	public void archiveFocusBidAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRGR-005 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());


	}
	
	@Test(priority = 6)
	public void changeWaitingForPricingToNewAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRGR-006 ";
		displayLogHeader(tcNumber);


		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

	}
	
	@Test(priority = 7)
	public void changeErrorToFocusAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRGR-007 ";
		displayLogHeader(tcNumber);


		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());


	}

	@Test(priority = 8)
	public void exportSummaryTableToCSVAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRGR-008 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());


	}
	
	@Test(priority = 9)
	public void showNonFocusTab() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRGR-009 ";
		displayLogHeader(tcNumber);

	
		
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());


	}
	
	@Test(priority = 10)
	public void archiveNonFocusBidAtNonFocusTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();

		String tcNumber = "TC_BWRGR-010 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());


	}
	
	@Test(priority = 11)
	public void exportSummaryTableToCSVAtNonFocusTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();

		String tcNumber = "TC_BWRGR-011 ";
		displayLogHeader(tcNumber);


		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

	}
	
	@Test(priority = 12)
	public void showFocusTab() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();

		String tcNumber = "TC_BWRGR-012 ";
		displayLogHeader(tcNumber);

		
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

	}
	
	 @Test(priority = 13)
	 public void archiveFocusBidAtFocusTab() throws InterruptedException {
		 summaryPage = new SummaryPage();
	 	focusPage = new FocusPage();
	
	 	String tcNumber = "TC_BWRGR-013 ";
	 	displayLogHeader(tcNumber);
	
	 	Thread.sleep(3000);
	 	takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

	 }
	 
	 @Test(priority = 14)
		public void exportSummaryTableToCSVAtFocusTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			focusPage = new FocusPage();

			String tcNumber = "TC_BWRGR-014 ";
			displayLogHeader(tcNumber);
			
	
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		
		}
	 
		@Test(priority = 15)
		public void clickStatusReportAtFocusTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			focusPage = new FocusPage();

			String tcNumber = "TC_BWRGR-015 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

			
		}

		@Test(priority = 16)
		public void showArchiveTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-016 ";
			displayLogHeader(tcNumber);

			
		
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		}
		
		@Test(priority = 17)
		public void exportSummaryTableToCSVAtArchiveTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			archivePage = new ArchivePage();

			String tcNumber = "TC_BWRGR-017 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		
		}

		@Test(priority = 18)
		public void exportStatusReportAtArchiveTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			archivePage = new ArchivePage();

			String tcNumber = "TC_BWRGR-018 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

			
		}
		
		@Test(priority = 19)
		public void uploadRemediationFollowUpButtonAtArchiveTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			archivePage = new ArchivePage();

			String tcNumber = "TC_BWRGR-019 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

			
		}
		
		@Test(priority = 20)
		public void showSearchTab() throws TimeoutException, InterruptedException, ExecutionException  {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-020 ";
			displayLogHeader(tcNumber);

			
			takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());

	
		}
		
		@Test(priority = 21)
		public void showWaitingBidsTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-021 ";
			displayLogHeader(tcNumber);

			takeScreenshot(tcNumber, SummaryPageTabsE.WAITING_BIDS.getLabel());

		}
		
		@Test(priority = 22)
		public void showErrorBidsTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-022 ";
			displayLogHeader(tcNumber);

		
			takeScreenshot(tcNumber, SummaryPageTabsE.ERROR_BIDS.getLabel());

		
		}
		
		@Test(priority = 23)
		public void showAdvancedSearchTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-023 ";
			displayLogHeader(tcNumber);

			takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());

		}

		@Test(priority = 24)
		public void exportSummaryTableToCSVAtAdvancedSearchTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			advancedSearch = new AdvancedSearchPage();

			String tcNumber = "TC_BWRGR-024 ";
			displayLogHeader(tcNumber);

			takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
			
		}
		
		@Test(priority = 25)
		public void showManualReviewTab() throws InterruptedException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-025 ";
			displayLogHeader(tcNumber);

			
			takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		}
		
		@Test(priority = 26)
		public void downloadTheLatestFileAtManualReviewTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			manualReviewPage = new ManualReviewPage();
		
			String tcNumber = "TC_BWRGR-026 ";
			displayLogHeader(tcNumber);

			
			takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		}

		@Test(priority = 27)
		public void uploadANewFileAtManualReviewTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			manualReviewPage = new ManualReviewPage();
		
			String tcNumber = "TC_BWRGR-027 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		}
		
		@Test(priority = 28)
		public void showGoeDeterminationTab() throws InterruptedException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRGR-028 ";
			displayLogHeader(tcNumber);

			takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

		

		}
		
		@Test(priority = 29)
		public void searchCmrAtGoeDeterminationTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			goeDeterminationPage = new GoeDeterminationPage();
		
			String tcNumber = "TC_BWRGR-029 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

			
		}
		
	@Test(priority = 34)
	public void showUserManagementPage() throws TimeoutException, InterruptedException, ExecutionException {
	
		summaryPage = new SummaryPage();
		userDetailPage = new UserDetailPage();
		
		String tcNumber = "TC_BWRGR-034 ";
		displayLogHeader(tcNumber);
		
	
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		
	
	}

	@Test(priority = 35)
	public void locateAddUserOnUserManagement() throws InterruptedException {
	
		userManagementPage = new UserManagementPage();
		
		String tcNumber = "TC_BWRGR-035 ";
		displayLogHeader(tcNumber);

	
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());

	}
		
	
	@Test(priority = 36)
	public void locateModifyUserOnUserManagement() throws TimeoutException, InterruptedException, ExecutionException {
	
		userDetailPage = new UserDetailPage();
		userManagementPage = new UserManagementPage();
		
		String tcNumber = "TC_BWRGR-036 ";

		displayLogHeader(tcNumber);
			
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
	
	}
	
	@Test(priority = 37)
	public void exportUsersToCSVAtUserManagementPage() throws InterruptedException {
		userManagementPage = new UserManagementPage();
		
		String tcNumber = "TC_BWRGR-037 ";
		displayLogHeader(tcNumber);
		
		/** For Access Moderator only
		userRegistrationPage.clickUserManagement(driver);
		**/
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());

	
	}
	
	@Test(priority = 38)
	public void detailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		
		String tcNumber = "TC_BWRGR-038 ";
		displayLogHeader(tcNumber);

		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		
	}

	/** This is for Reviewer and Lead Reviewer
	@Test(priority = 38)
	public void detailedReportPageTC() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRGR-038 ";
		displayLogHeader(tcNumber);

		boolean myTaskTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
			myTaskTabIsPresent = true;
		} catch (NoSuchElementException e) {
			myTaskTabIsPresent = false;
		}
		
		Assert.assertTrue(myTaskTabIsPresent, "My Task Tab is not Present!");
			
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='myTaskBidsGridDiv-2']")));
			
		} catch (Exception e) {
			throw new TimeoutException("My Task Tab is not fully loaded");
		}
		
		detailedReportPage.searchNewInMyTask2(driver,"New");
		
		boolean detailedReportPageIsPresent = false;
		
		Thread.sleep(5000);
		
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='idx_grid__PropertyRow_14']")));
			detailedReportPageIsPresent = true;
			
		} catch (Exception e) {
			detailedReportPageIsPresent = false;
			throw new TimeoutException("Detailed Report Page is not fully loaded");
		}
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		Assert.assertTrue(detailedReportPageIsPresent, "Detailed Report Page is not Present which is Not as Expected");
	
		System.out.println("Detailed Report Page is Present as Expected");
		Thread.sleep(5000);

	}
	**/
	
	
	@Test(priority = 39)
	public void takeThisBidAtDetailedReportPage() throws InterruptedException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRGR-039 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		
	}
	
	@Test(priority = 40)
	public void writeInRemediationLogTabAtDetailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRGR-040 ";
		displayLogHeader(tcNumber);

		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		
	}
	
	@Test(priority = 41)
	public void uploadAttachmentsAtDetailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRGR-041 ";
		displayLogHeader(tcNumber);

		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		
	}
		
	@Test(priority = 42)
	public void bidRollbackAtArchiveTab() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		archivePage = new ArchivePage();
		
		String tcNumber = "TC_BWRGR-042 ";
		displayLogHeader(tcNumber);

		
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

	
	}
	
	@Test(priority = 43)
	public void exportSummaryTableToCSVAtDetailedReportPage() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		

		String tcNumber = "TC_BWRGR-043 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

	}
	
	@Test(priority = 44)
	public void printReportViaPrintVersionAtDetailedReportPage() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		
		String tcNumber = "TC_BWRGR-044 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

	}
	
	@Test(priority = 45)
	public void exportNonFocusBelowClipToCSVAtDetailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRGR-045 ";
		displayLogHeader(tcNumber);
		
		
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		
	}
	
	@Test(priority = 46)
	public void viewBRETDashboardasBidSupport()  throws TimeoutException, InterruptedException, ExecutionException  {
		
		BRETDashboardLoginPage dashboardLoginPage = new BRETDashboardLoginPage(driver);
		
		String tcNumber = "TC_BWRGR-046 ";
		displayLogHeader(tcNumber);
		
		driver.navigate().to("https://bldbz173021.cloud.dst.ibm.com:9443/DashboardSrcSys/");
		
		dashboardLoginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	
		try {
			WebDriverWait wait=new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.titleContains("Bret Monitoring Dashboard"));
	
		} catch (Exception e) {
			System.out.println("PASSED - BRET Dashboard is Not Viewable as Expected");
//			throw new TimeoutException("Invalid Username or Password");
		}
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 100);
			System.out.println("Waiting for BRET Dashboard to load. This will take time, please wait...");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sourceNameText1\"]")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sourceNameText12\"]")));
			
		} catch (Exception e) {
			takeScreenshot(tcNumber, SummaryPageTabsE.BRET_DASHBOARD.getLabel());
			throw new TimeoutException("BRET Dashboard is not fully loaded");
		}
		
		Thread.sleep(8000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.BRET_DASHBOARD.getLabel());
		System.out.println("BRET Dashboard is Displayed as expected");
	}
	
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}


}
