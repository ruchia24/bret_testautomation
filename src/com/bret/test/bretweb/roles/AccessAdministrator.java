package com.bret.test.bretweb.roles;

//import java.util.List;
//import java.util.Map;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
//import com.bret.pages.BRETSearchPage;
import com.bret.pages.FocusPage;
import com.bret.pages.GoeDeterminationPage;
import com.bret.pages.ManualReviewPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SummaryPage;
import com.bret.pages.UserDetailPage;
import com.bret.pages.UserManagementPage;
import com.bret.pages.UserRegistrationPage;
import com.bret.util.BretTestUtils;

/**
 * June 20, 2018
 * 
 * @author DKAlba
 *
 */
public class AccessAdministrator extends BaseTest {

	private MyTaskPage myTaskPage;
	private SummaryPage summaryPage;
	private NonFocusPage nonFocusPage;
	private FocusPage focusPage;
	private ArchivePage archivePage;
//	private BRETSearchPage searchPage;
	private AdvancedSearchPage advancedSearchPage;
	private ManualReviewPage manualReviewPage;
	private GoeDeterminationPage goeDeterminationPage;
	private UserManagementPage userManagementPage;
	private UserRegistrationPage userRegistrationPage;
	private UserDetailPage userDetailPage;
	private WebDriverWait wait;
	
//	private static final String TEST_USER_ID = "userId";
	

	@BeforeTest
	public void loginAsAccessAdministrator() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAccessAdminUsername(), getAccessAdminPassword());
		
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();
		nonFocusPage = new NonFocusPage();
		focusPage = new FocusPage();
		archivePage = new ArchivePage();
//		searchPage = new BRETSearchPage(driver);
		advancedSearchPage = new AdvancedSearchPage();
		manualReviewPage = new ManualReviewPage();
		goeDeterminationPage = new GoeDeterminationPage();
		userManagementPage = new UserManagementPage();
		userRegistrationPage = new UserRegistrationPage();
		userDetailPage = new UserDetailPage();
		wait = new WebDriverWait(driver, 30);
	}
	
	@Test(priority = 1)
	public void showAccessDetailsSourceSystem() throws InterruptedException {
		String tcNumber = "TC_BWRAA-001 ";

		myTaskPage.clickUserName(driver);
		myTaskPage.clickShowMyPrivilege(driver);

		Thread.sleep(5000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		String accessDetails = myTaskPage.getAccessDetails(driver);
		
		Assert.assertTrue(accessDetails.contains("Access Source Systems: All"), "Access Source Systems is not set to All");
		System.out.println("Access Source System is set to All");
	}

	@Test(priority = 2)
	public void showAccessDetailsRegion() throws InterruptedException {

		String tcNumber = "TC_BWRAA-002 ";
		displayLogHeader(tcNumber);
		
 		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		Thread.sleep(3000);

		String accessDetails = myTaskPage.getAccessDetails(driver);
		Assert.assertTrue(accessDetails.contains("Access Regions: All"), "Access Region is not set to All");
		System.out.println("Access Region is set to All");
	
		myTaskPage.closeMyPrivilege(driver);

	}

	@Test(priority = 3)
	public void showMyTaskTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-003 ";
		displayLogHeader(tcNumber);
		WebElement MyTaskTab = driver
				.findElement(By.xpath("//*[@id=\"dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_0\"]"));
		// My task tab xpath, to add it on My Task Page

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		
		Assert.assertTrue(MyTaskTab.isDisplayed(), "My Task Tab is not enabled.");
		System.out.println("My Task Tab is displayed.");
				
	}

	@Test(priority = 4)
	public void archiveNonFocusBidAtMyTaskTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-004 ";
		displayLogHeader(tcNumber);
		String fieldText = myTaskPage.getBidFieldText(driver);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(fieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No bids available to archive");

	}

	@Test(priority = 5)
	public void archiveFocusBidAtMyTaskTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-005 ";
		displayLogHeader(tcNumber);
		String fieldText = myTaskPage.getBidFieldText(driver);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(fieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No bids available to archive");

	}

	@Test(priority = 6)
	public void changeWaitingForPricingToNewAtMyTaskTab() throws InterruptedException {;

		String tcNumber = "TC_BWRAA-006 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(bidFieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No bids available to archive");

	}

	@Test(priority = 7)
	public void changeErrorToFocusAtMyTaskTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-007 ";
		displayLogHeader(tcNumber);
		String fieldText = myTaskPage.getBidFieldText(driver);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(fieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No bids available to archive");

	}

	@Test(priority = 8)
	public void exportSummaryTableToCSVAtMyTaskTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-008 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		boolean exportButtonIsDisplayed = myTaskPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(!exportButtonIsDisplayed, "Export Button is displayed.");
		System.out.println("Export To CSV Button is not Displayed");

	}

	@Test(priority = 9)
	public void showNonFocusTab() throws InterruptedException {

		
		String tcNumber = "TC_BWRAA-009 ";
		displayLogHeader(tcNumber);

		boolean nonFocusBidsTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
			nonFocusBidsTabIsPresent = true;
		} catch (NoSuchElementException e) {
			nonFocusBidsTabIsPresent = false;
		}
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//*[@id=\"dijit_layout_ContentPane_1\"]"), "Total:" ));
		
		Thread.sleep(5000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		Assert.assertTrue(nonFocusBidsTabIsPresent, "Non Focus Tab is not Present!");
		System.out.println("Non Focus Tab is Present!");
//		Thread.sleep(3000);
	}

	@Test(priority = 10)
	public void archiveNonFocusBidAtNonFocusTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-010 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		boolean archiveButtonIsDisplayed = nonFocusPage.archiveButtonIsDisplayed(driver);

		Assert.assertTrue(!archiveButtonIsDisplayed, "Archive Button is displayed.");
		System.out.println("Export To CSV Button is not Displayed");

	}

	@Test(priority = 11)
	public void exportSummaryTableToCSVAtNonFocusTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-011 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		boolean exportButtonIsDisplayed = nonFocusPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(!exportButtonIsDisplayed, "Export Button is displayed.");
		System.out.println("Archive Button is not displayed");
	}

	@Test(priority = 12)
	public void showFocusTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-012 ";
		displayLogHeader(tcNumber);

		boolean focusBidsTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
			focusBidsTabIsPresent = true;
		} catch (NoSuchElementException e) {
			focusBidsTabIsPresent = false;
		}

		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//*[@id=\"dijit_layout_ContentPane_1\"]"), "Total:" ));
//		Thread.sleep(9000);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		Assert.assertTrue(focusBidsTabIsPresent, "Focus Tab is not Present!");
		System.out.println("Focus Tab is Present!");
//		Thread.sleep(5000);
	}

	 @Test(priority = 13)
	 public void archiveFocusBidAtFocusTab() throws InterruptedException {
	
	 	String tcNumber = "TC_BWRAA-013 ";
	 	displayLogHeader(tcNumber);
	
//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
	
	 	boolean archiveButtonIsDisplayed = nonFocusPage.archiveButtonIsDisplayed(driver);

	 	Assert.assertTrue(!archiveButtonIsDisplayed, "Archive Button is displayed.");
	 	System.out.println("Archive Button is not displayed");
	
	 }

	@Test(priority = 14)
	public void exportSummaryTableToCSVAtFocusTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-014 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		boolean exportButtonIsDisplayed = focusPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(!exportButtonIsDisplayed, "Export Button is not Displayed.");
		System.out.println("Export To CSV Button is not Displayed");

	}

	@Test(priority = 15)
	public void clickStatusReportAtFocusTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-015 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		boolean statusReportIsDisplayed = focusPage.statusReportButtonIsDisplayed(driver);
		
		System.out.println("statusReportIsDisplayed: " + statusReportIsDisplayed);
			
		Assert.assertTrue(!statusReportIsDisplayed, "Status Report Button is Displayed.");

	}

	@Test(priority = 16)
	public void showArchiveTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-016 ";
		displayLogHeader(tcNumber);

		boolean archiveTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
			archiveTabIsPresent = true;
		} catch (NoSuchElementException e) {
			archiveTabIsPresent = false;
		}

		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//*[@id=\"dijit_layout_ContentPane_1\"]"), "Total:" ));
		
		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		Assert.assertTrue(archiveTabIsPresent, "Archive Tab is not Present!");
		System.out.println("Archive Tab is Present!");
//		Thread.sleep(5000);
	}

	@Test(priority = 17)
	public void exportSummaryTableToCSVAtArchiveTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-017 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		boolean exportButtonIsDisplayed = archivePage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(!exportButtonIsDisplayed, "Export Button is not Displayed.");
		System.out.println("Export To CSV Button is not Displayed");
	}

	@Test(priority = 18)
	public void statusReportAtArchiveTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-018 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		boolean statusReportIsDisplayed = focusPage.statusReportButtonIsDisplayed(driver);
		
		System.out.println("statusReportIsDisplayed: " + statusReportIsDisplayed);
			
		Assert.assertTrue(!statusReportIsDisplayed, "Status Report Button is Displayed.");

	}

	@Test(priority = 19)
	public void exportUploadRemediationFollowUpAtArchiveTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-019 ";
		displayLogHeader(tcNumber);

//		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
		 

		boolean followUpButtonIsDisplayed = archivePage.uploadRemediationFollowUpButtonIsDisplayed(driver);

		Assert.assertTrue(!followUpButtonIsDisplayed, "Export Button is not Displayed.");
		System.out.println("Export To CSV Button is not Displayed");
	}
	
	@Test(priority = 20)
	public void showSearchTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-020 ";
		displayLogHeader(tcNumber);

		boolean searchTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
			searchTabIsPresent = true;
		} catch (NoSuchElementException e) {
			searchTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());

		Assert.assertTrue(searchTabIsPresent, "Search Tab is not Present!");
		System.out.println("Search Tab is Present!");
		Thread.sleep(5000);

	}
	
	@Test(priority = 21)
	public void showWaitingBidsTab() throws InterruptedException {
		summaryPage = new SummaryPage();
	
		String tcNumber = "TC_BWRAA-021 ";
		displayLogHeader(tcNumber);

		boolean waitingBidsTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.WAITING_BIDS);
			waitingBidsTabIsPresent = true;
		} catch (NoSuchElementException e) {
			waitingBidsTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.WAITING_BIDS.getLabel());

		Assert.assertTrue(waitingBidsTabIsPresent, "Waiting Bids Tab is not Present!");
		System.out.println("Waiting Bids Tab is Present!");
		Thread.sleep(5000);

	}
	
	@Test(priority = 22)
	public void showErrorBidsTab() throws InterruptedException {
		summaryPage = new SummaryPage();
	
		String tcNumber = "TC_BWRAA-022 ";
		displayLogHeader(tcNumber);

		boolean errorBidsTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.ERROR_BIDS);
			errorBidsTabIsPresent = true;
		} catch (NoSuchElementException e) {
			errorBidsTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.ERROR_BIDS.getLabel());

		Assert.assertTrue(errorBidsTabIsPresent, "Error Bids Tab is not Present!");
		System.out.println("Error Bids Tab is Present!");
		Thread.sleep(5000);

	}
	
	@Test(priority = 23)
	public void showAdvancedSearchTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-023 ";
		displayLogHeader(tcNumber);

		boolean advancedSearchTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);
			advancedSearchTabIsPresent = true;
		} catch (NoSuchElementException e) {
			advancedSearchTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());

		Assert.assertTrue(advancedSearchTabIsPresent, "Advanced Search Tab is not Present!");
		System.out.println("Advanced Search Tab is Present!");
		Thread.sleep(5000);

	}

	@Test(priority = 24)
	public void exportSummaryTableToCSVAtAdvancedSearchTab() throws InterruptedException {

		String tcNumber = "TC_BWRAA-024 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());

		boolean exportSummaryTableToCSVIsDisplayed = advancedSearchPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(!exportSummaryTableToCSVIsDisplayed, "Export to CSV Button is Displayed.");
		System.out.println("Export To CSV Button is not Displayed");
	}

	@Test(priority = 25)
	public void showManualReviewTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-025 ";
		displayLogHeader(tcNumber);

		boolean manualReviewTabIsPresent = false;
		
		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.MANUAL_REVIEW);
			manualReviewTabIsPresent = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			manualReviewTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		Assert.assertTrue(manualReviewTabIsPresent, "Manual Review Tab is not Present!");
		System.out.println("Manual Review Tab is Present!");
		Thread.sleep(5000);

	}
	
	@Test(priority = 26)
	public void downloadTheLatestFileAtManualReviewTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-026 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());
		
		boolean downloadTheLatestFileIsDisplayed = manualReviewPage.downloadTheLatestFileIsDisplayed(driver);
		
		Assert.assertFalse(downloadTheLatestFileIsDisplayed, "Download the latest file Button is Displayed.");
		System.out.println("Download the latest file Button is Not Displayed");

	}

	
	@Test(priority = 27, dependsOnMethods = {"showManualReviewTab"})
	public void uploadANewFileAtManualReviewTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-027 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		
		
		boolean uploadANewFileIsDisplayed = manualReviewPage.uploadANewFileIsDisplayed(driver);
				
		Assert.assertFalse(uploadANewFileIsDisplayed, "Upload a New file Button is Displayed.");
		System.out.println("Upload a New file Button is Not Displayed");

	}
	
	@Test(priority = 28)
	public void showGoeDeterminationTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-028 ";
		displayLogHeader(tcNumber);

		boolean goeDeterminationTabIsPresent = false;
		
		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.GOE_DETERMINATION);
			goeDeterminationTabIsPresent = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			goeDeterminationTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

		Assert.assertTrue(goeDeterminationTabIsPresent, "GOE Determination Tab is not Present!");
		System.out.println("GOE Determination Tab is Present!");
		Thread.sleep(5000);

	}
	

	@Test(priority = 29)
	public void searchCmrAtGoeDeterminationTab() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-029 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

		boolean searchCmrButtonIsDisplayed = goeDeterminationPage.searchButtonIsDisplayed(driver);
		
		Assert.assertTrue(searchCmrButtonIsDisplayed, "Search CMR Button is not Displayed.");
		System.out.println("search CMR Button is Displayed");

	}
	
	@Test(priority = 30)
	public void showUserManagement() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-034 ";
		String userManagementHeaderText = userDetailPage.getUserDetailPageText(driver);
		displayLogHeader(tcNumber);

		summaryPage.navigateToTab(driver, SummaryPageTabsE.USER_MANAGEMENT);
		
		Assert.assertTrue(userManagementHeaderText.contains("User Management"));
		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());

		System.out.println("User Management is Present!");
		Thread.sleep(3000);

	}
	
	@Test(priority = 31)
	public void locateAddUserOnUserManagement() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-035 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		boolean addUsersButtonIsDisplayed = userManagementPage.addUsersButtonIsDisplayed(driver);
		
		Assert.assertTrue(addUsersButtonIsDisplayed, "Add Users Button is not Displayed.");

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		
		userManagementPage.clickAddUsersButton(driver);
		System.out.println("User Registration is Present!");
		
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());

	}
	
	@Test(priority = 32)
	public void locateModifyUserOnUserManagement() throws InterruptedException {
	
		String tcNumber = "TC_BWRAA-036 ";
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
//		String testUserID = testData.get(TEST_USER_ID);
		String testUserID = "leonorfd@ph.ibm.com";
		displayLogHeader(tcNumber);
			
		userRegistrationPage.clickUserManagement(driver);
		
//		int chainBidDetails = userManagementPage.searchRowIndex(driver, testData.get(TEST_USER_ID));
		int chainBidDetails = userManagementPage.searchRowIndex(driver, testUserID);
		
//		System.out.println("/html/body/div[1]/div[3]/div/div[3]/div/div[3]/div[3]/div[2]/div[" + chainBidDetails + "]/table/tbody/tr/td[3]");
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		Thread.sleep(5000);
		
		driver.findElement(By.xpath(
				"/html/body/div[1]/div[3]/div/div[3]/div/div[3]/div[3]/div[2]/div[" + chainBidDetails + "]/table/tbody/tr/td[3]"))
				.click();
		
		Thread.sleep(3000);
		
		String userDetailPageHeaderText = userDetailPage.getUserDetailPageText(driver);

		Assert.assertTrue(userDetailPageHeaderText.contains("User Detail"));
		System.out.println("User Detail is Present!");
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		Thread.sleep(9000);		

		

	}
	
	@Test(priority = 33)
	public void exportUsersToCSVAtUserManagementPage() throws InterruptedException {

		String tcNumber = "TC_BWRAA-037 ";
		displayLogHeader(tcNumber);
		
		userRegistrationPage.clickUserManagement(driver);
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//*[@id=\"dijit_layout_ContentPane_1\"]"), "Total:" ));
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());

		boolean exportButtonIsDisplayed = userManagementPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(exportButtonIsDisplayed);
		System.out.println("Export To CSV Button is Displayed");
		
		userManagementPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
	}

	 private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
		BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	 }


}
