package com.bret.test.bretweb.roles;

import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETDashboardLoginPage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.DetailedReportPage;
import com.bret.pages.FocusPage;
import com.bret.pages.GoeDeterminationPage;
import com.bret.pages.ManualReviewPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SummaryPage;
import com.bret.pages.UserDetailPage;
import com.bret.pages.UserManagementPage;
import com.bret.util.BretTestUtils;

public class Reviewer extends BaseTest{

	private MyTaskPage myTaskPage;
	private SummaryPage summaryPage;
	private NonFocusPage nonFocusPage;
	private FocusPage focusPage;
	private ArchivePage archivePage;
	private AdvancedSearchPage advancedSearch;
	private ManualReviewPage manualReviewPage;
	private GoeDeterminationPage goeDeterminationPage;
	private UserManagementPage userManagementPage;
	private UserDetailPage userDetailPage;
	private DetailedReportPage detailedReportPage;


	@BeforeTest
	public void loginAsBidSupport()  throws TimeoutException, InterruptedException, ExecutionException  {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());
	
		try {
			WebDriverWait wait=new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.titleContains("Special Bid Risk Analysis - Bid Summary"));
	
		} catch (Exception e) {
			throw new TimeoutException("Invalid Username or Password");
		}
	}


	@Test(priority = 1)
	public void showAccessDetailsSourceSystem() throws InterruptedException {
		String tcNumber = "TC_BWRR-001 ";
		displayLogHeader(tcNumber);
		
		myTaskPage = new MyTaskPage();
		myTaskPage.clickUserName(driver);
		myTaskPage.clickShowMyPrivilege(driver);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

	}
	
	@Test(priority = 2)
	public void showAccessDetailsRegion() throws InterruptedException {

		String tcNumber = "TC_BWRR-002 ";
		displayLogHeader(tcNumber);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		Thread.sleep(3000);

		WebElement closeButton = driver.findElement(By.xpath("//*[@id=\"dijit_form_Button_2_label\"]"));
		closeButton.click();

	}
	
	@Test(priority = 3)
	public void showMyTaskTab() throws InterruptedException {

		String tcNumber = "TC_BWRR-003 ";
		displayLogHeader(tcNumber);
		
		WebElement MyTaskTab = driver
				.findElement(By.xpath("//*[@id=\"dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_0\"]"));
		// My task tab xpath, to add it on My Task Page

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(MyTaskTab.isDisplayed(), "My Task Tab is not enabled.");
		System.out.println("My Task Tab is displayed.");

	}
	
	@Test(priority = 4)
	public void archiveNonFocusBidAtMyTaskTab() throws InterruptedException {
	
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRR-004 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(bidFieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No Non-focus bids available to archive");

	}

	@Test(priority = 5)
	public void archiveFocusBidAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRR-005 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(bidFieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No Focus bids available to archive");

	}
	
	@Test(priority = 6)
	public void changeWaitingForPricingToNewAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRR-006 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(bidFieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No \"Waiting for Pricing\" bids available to change into \"New\"");

	}
	
	@Test(priority = 7)
	public void changeErrorToFocusAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRR-007 ";
		displayLogHeader(tcNumber);
		String bidFieldText = myTaskPage.getBidFieldText(driver);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		Assert.assertTrue(bidFieldText.contains("No items to display"), "There are bid/s displayed");
		System.out.println("No \"Error\" bids available to change into \"Focus\"");

	}

	@Test(priority = 8)
	public void exportSummaryTableToCSVAtMyTaskTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRR-008 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		boolean exportButtonIsDisplayed = myTaskPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(!exportButtonIsDisplayed, "Export Button is displayed.");
		System.out.println("Export To CSV Button is not Displayed");

	}
	
	@Test(priority = 9)
	public void showNonFocusTab() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		myTaskPage = new MyTaskPage();

		String tcNumber = "TC_BWRR-009 ";
		displayLogHeader(tcNumber);

		boolean nonFocusTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
			nonFocusTabIsPresent = true;
		} catch (NoSuchElementException e) {
			nonFocusTabIsPresent = false;
		}


		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='nonFocus_srcSystemSelect']")));
	
		} catch (Exception e) {
			throw new TimeoutException("Non-Focus Bids Tab is not fully loaded");
		}
		
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		Assert.assertTrue(nonFocusTabIsPresent, "Non Focus Tab is not Present!");
		System.out.println("Non-Focus Tab is displayed!");
		Thread.sleep(5000);
	}
	
	@Test(priority = 10)
	public void archiveNonFocusBidAtNonFocusTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();

		String tcNumber = "TC_BWRR-010 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		boolean archiveButtonIsDisplayed = nonFocusPage.archiveButtonIsDisplayed(driver);

		Assert.assertTrue(!archiveButtonIsDisplayed, "Archive Button is displayed.");
		System.out.println("Archive Button is not Displayed");

	}
	
	@Test(priority = 11)
	public void exportSummaryTableToCSVAtNonFocusTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();

		String tcNumber = "TC_BWRR-011 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		summaryPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
		
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		boolean exportButtonIsDisplayed = nonFocusPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(exportButtonIsDisplayed, "Export Button is not Displayed.");
		System.out.println("Export Button is Displayed.");

	}
	
	@Test(priority = 12)
	public void showFocusTab() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();

		String tcNumber = "TC_BWRR-012 ";
		displayLogHeader(tcNumber);

		boolean FocusTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
			FocusTabIsPresent = true;
		} catch (NoSuchElementException e) {
			FocusTabIsPresent = false;
		}

		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='focus_srcSystemSelect']")));
	
		} catch (Exception e) {
			throw new TimeoutException("Focus Bids Tab is not fully loaded");
		}
		
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		Assert.assertTrue(FocusTabIsPresent, "Focus Tab is not Present!");
		System.out.println("Focus Tab is Present!");
		Thread.sleep(5000);
	}
	
	 @Test(priority = 13)
	 public void archiveFocusBidAtFocusTab() throws InterruptedException {
	 summaryPage = new SummaryPage();
	 focusPage = new FocusPage();
	
	 String tcNumber = "TC_BWRR-013 ";
	 displayLogHeader(tcNumber);
	
	 Thread.sleep(3000);
	 takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
	
	 boolean archiveButtonIsDisplayed = nonFocusPage.archiveButtonIsDisplayed(driver);

	 Assert.assertTrue(!archiveButtonIsDisplayed, "Archive Button is displayed.");
	 System.out.println("Archive Button is not displayed in Focus Tab");
	
	 }
	 
	 @Test(priority = 14)
		public void exportSummaryTableToCSVAtFocusTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			focusPage = new FocusPage();

			String tcNumber = "TC_BWRR-014 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			summaryPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

			boolean exportButtonIsDisplayed = focusPage.exportButtonIsDisplayed(driver);

			Assert.assertTrue(exportButtonIsDisplayed, "Export to CSV Button is not Displayed.");
			System.out.println("Export To CSV Button is Displayed");
		}
	 
		@Test(priority = 15)
		public void clickStatusReportAtFocusTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			focusPage = new FocusPage();

			String tcNumber = "TC_BWRR-015 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

			boolean statusReportIsDisplayed = focusPage.statusReportButtonIsDisplayed(driver);
			
			System.out.println("statusReportIsDisplayed: " + statusReportIsDisplayed);
				
			Assert.assertTrue(statusReportIsDisplayed, "Export Button is Displayed.");

		}

		@Test(priority = 16)
		public void showArchiveTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-016 ";
			displayLogHeader(tcNumber);

			boolean archiveTabIsPresent = false;

			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
				archiveTabIsPresent = true;
			} catch (NoSuchElementException e) {
				archiveTabIsPresent = false;
			}

			try {
				WebDriverWait wait=new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='archive_srcSystemSelect']")));
		
			} catch (Exception e) {
				throw new TimeoutException("Archive Tab is not fully loaded");
			}
		
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

			Assert.assertTrue(archiveTabIsPresent, "Archive Tab is not Present!");
			System.out.println("Archive Tab is Present!");
			Thread.sleep(5000);

		}
		
		@Test(priority = 17)
		public void exportSummaryTableToCSVAtArchiveTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			archivePage = new ArchivePage();

			String tcNumber = "TC_BWRR-017 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

			boolean exportSummaryTableToCSVIsDisplayed = archivePage.exportButtonIsDisplayed(driver);

			Assert.assertTrue(exportSummaryTableToCSVIsDisplayed, "Export to CSV Button is not Displayed.");
			System.out.println("Export To CSV Button is Displayed");
		}

		@Test(priority = 18)
		public void exportStatusReportAtArchiveTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			archivePage = new ArchivePage();

			String tcNumber = "TC_BWRR-018 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

			boolean exportStatusReportIsDisplayed = archivePage.statusReportButtonIsDisplayed(driver);

			Assert.assertTrue(exportStatusReportIsDisplayed, "Export Status Report Button is not Displayed.");
			System.out.println("Export To CSV Button is Displayed");
		}
		
		@Test(priority = 19)
		public void uploadRemediationFollowUpButtonAtArchiveTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			archivePage = new ArchivePage();

			String tcNumber = "TC_BWRR-019 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

			boolean uploadRemediationFollowUpButtonIsDisplayed = archivePage.uploadRemediationFollowUpButtonIsDisplayed(driver);

			Assert.assertTrue(!uploadRemediationFollowUpButtonIsDisplayed, "Upload Remediation Follow Up Button is Displayed.");
			System.out.println("Upload Remediation Follow Up Button is Not Displayed.");
		}
		
		@Test(priority = 20)
		public void showSearchTab() throws TimeoutException, InterruptedException, ExecutionException  {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-020 ";
			displayLogHeader(tcNumber);

			boolean archiveTabIsPresent = false;

			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
				archiveTabIsPresent = true;
			} catch (NoSuchElementException e) {
				archiveTabIsPresent = false;
			}
			
			try {
				WebDriverWait wait=new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='bidSearchInputBox']")));
		
			} catch (Exception e) {
				throw new TimeoutException("Search Tab is not fully loaded");
			}

			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());

			Assert.assertTrue(archiveTabIsPresent, "Search Tab is not Present!");
			System.out.println("Search Tab is Present!");
			Thread.sleep(5000);

		}
		
		@Test(priority = 21)
		public void showWaitingBidsTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-021 ";
			displayLogHeader(tcNumber);

			boolean archiveTabIsPresent = false;

			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.WAITING_BIDS);
				archiveTabIsPresent = true;
			} catch (NoSuchElementException e) {
				archiveTabIsPresent = false;
			}
			
			try {
				WebDriverWait wait=new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='gridx_Grid_0-1']")));
		
			} catch (Exception e) {
				throw new TimeoutException("Waiting Bids Tab is not fully loaded");
			}
		

			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.WAITING_BIDS.getLabel());

			Assert.assertTrue(archiveTabIsPresent, "Waiting Bids Tab is not Present!");
			System.out.println("Waiting Bids Tab is Present!");
			Thread.sleep(5000);

		}
		
		@Test(priority = 22)
		public void showErrorBidsTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-022 ";
			displayLogHeader(tcNumber);

			boolean archiveTabIsPresent = false;

			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.ERROR_BIDS);
				archiveTabIsPresent = true;
			} catch (NoSuchElementException e) {
				archiveTabIsPresent = false;
			}

			try {
				WebDriverWait wait=new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='errorBidsGridDiv-1']")));
		
			} catch (Exception e) {
				throw new TimeoutException("Error Bids Tab is not fully loaded");
			}
			
			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ERROR_BIDS.getLabel());

			Assert.assertTrue(archiveTabIsPresent, "Error Bids Tab is not Present!");
			System.out.println("Error Bids Tab is Present!");
			Thread.sleep(5000);

		}
		
		@Test(priority = 23)
		public void showAdvancedSearchTab() throws TimeoutException, InterruptedException, ExecutionException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-023 ";
			displayLogHeader(tcNumber);

			boolean archiveTabIsPresent = false;

			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);
				archiveTabIsPresent = true;
			} catch (NoSuchElementException e) {
				archiveTabIsPresent = false;
			}
			
			try {
				WebDriverWait wait=new WebDriverWait(driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='locateBidSearchBtn']")));
		
			} catch (Exception e) {
				throw new TimeoutException("Advanced Search Tab is not fully loaded");
			}

			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());

			Assert.assertTrue(archiveTabIsPresent, "Advanced Search Tab is not Present!");
			System.out.println("Advanced Search Tab is Present!");
			Thread.sleep(5000);

		}

		@Test(priority = 24)
		public void exportSummaryTableToCSVAtAdvancedSearchTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			advancedSearch = new AdvancedSearchPage();

			String tcNumber = "TC_BWRR-024 ";
			displayLogHeader(tcNumber);
			
			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());

			boolean exportSummaryTableToCSVIsDisplayed = advancedSearch.exportButtonIsDisplayed(driver);

			Assert.assertTrue(exportSummaryTableToCSVIsDisplayed, "Export to CSV Button is not Displayed.");
			System.out.println("Export To CSV Button is Displayed");
		}
		
		@Test(priority = 25)
		public void showManualReviewTab() throws InterruptedException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-025 ";
			displayLogHeader(tcNumber);

			boolean manualReviewTabIsPresent = false;
			
			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.MANUAL_REVIEW);
				manualReviewTabIsPresent = true;
			} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
				manualReviewTabIsPresent = false;
			}

			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

			Assert.assertTrue(manualReviewTabIsPresent, "Manual Review Tab is Not Present!");
			System.out.println("Manual Review Tab is Present!");
			Thread.sleep(5000);

		}
		
		@Test(priority = 26)
		public void downloadTheLatestFileAtManualReviewTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			manualReviewPage = new ManualReviewPage();
		
			String tcNumber = "TC_BWRR-026 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

			boolean downloadTheLatestFileIsDisplayed = false;
					
			try {
				manualReviewPage.downloadTheLatestFileIsDisplayed(driver);
				downloadTheLatestFileIsDisplayed = true;
			} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
				downloadTheLatestFileIsDisplayed = false;
				System.out.println("Manual Review Tab is Not Present! ");
			}
			
			
			Assert.assertTrue(downloadTheLatestFileIsDisplayed, "Download the latest file Button is Not Displayed.");
			System.out.println("Download the latest file Button is Displayed");

		}

		

		@Test(priority = 27)
		public void uploadANewFileAtManualReviewTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			manualReviewPage = new ManualReviewPage();
		
			String tcNumber = "TC_BWRR-027 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

			boolean uploadANewIsDisplayed = false;
					
			try {
				manualReviewPage.downloadTheLatestFileIsDisplayed(driver);
				uploadANewIsDisplayed = true;
			} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
				uploadANewIsDisplayed = false;
				System.out.println("Manual Review Tab is Not Present! ");
			}
			
			Assert.assertTrue(uploadANewIsDisplayed, "Upload a New file Button is Not Displayed.");
			System.out.println("Upload a New file Button is Displayed");

		}
		
		@Test(priority = 28)
		public void showGoeDeterminationTab() throws InterruptedException {
			summaryPage = new SummaryPage();
		
			String tcNumber = "TC_BWRR-028 ";
			displayLogHeader(tcNumber);

			boolean manualReviewTabIsPresent = false;
			
			try {
				summaryPage.navigateToTab(driver, SummaryPageTabsE.GOE_DETERMINATION);
				manualReviewTabIsPresent = true;
			} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
				manualReviewTabIsPresent = false;
			}

			Thread.sleep(8000);
			takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

			Assert.assertTrue(manualReviewTabIsPresent, "GOE Determination Tab is Not Present!");
			System.out.println("GOE Determination Tab is Present!");
			Thread.sleep(5000);

		}
		

		@Test(priority = 29)
		public void searchCmrAtGoeDeterminationTab() throws InterruptedException {
			summaryPage = new SummaryPage();
			goeDeterminationPage = new GoeDeterminationPage();
		
			String tcNumber = "TC_BWRR-029 ";
			displayLogHeader(tcNumber);

			Thread.sleep(3000);
			takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

			boolean searchCmrButtonIsDisplayed = false;
					
			try {
				goeDeterminationPage.searchButtonIsDisplayed(driver);
				searchCmrButtonIsDisplayed = true;
			} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
				searchCmrButtonIsDisplayed = false;
				System.out.println("GOE Determination Tab is Not Present! ");
			}
			
			
			Assert.assertTrue(searchCmrButtonIsDisplayed, "Search CMR Button is Not Displayed.");
			System.out.println("Search CMR Button is Displayed");

		}
	@Test(priority = 34)
	public void showUserManagementPage() throws TimeoutException, InterruptedException, ExecutionException {
	
		summaryPage = new SummaryPage();
		userDetailPage = new UserDetailPage();
		
		String tcNumber = "TC_BWRR-034 ";
		displayLogHeader(tcNumber);

		summaryPage.navigateToTab(driver, SummaryPageTabsE.USER_MANAGEMENT);
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[3]/div/div[3]/div/div[3]/div[2]/div[2]/div/table/tbody/tr/td[1]/div/div[2]")));
			
		} catch (Exception e) {
			throw new TimeoutException("User Management Page is not fully loaded");
		}
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		
		String userManagementHeaderText = userDetailPage.getUserDetailPageText(driver);
		Assert.assertTrue(userManagementHeaderText.contains("User Management"),"User Management is not Present which is not as Expected!");
		System.out.println("User Management Page is Present which is as Expected");
		Thread.sleep(3000);

	}

	@Test(priority = 35)
	public void locateAddUserOnUserManagement() throws InterruptedException {
	
		userManagementPage = new UserManagementPage();
		
		String tcNumber = "TC_BWRR-035 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		boolean addUsersButtonIsDisplayed = userManagementPage.addUsersButtonIsDisplayed(driver);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		Assert.assertFalse(addUsersButtonIsDisplayed, "Add Users Button is Displayed which is Not as Expected.");
		System.out.println("Add Users Button is Not Displayed which is as Expected");
		
		/** The following code is for Access Administrator only
		userManagementPage.clickAddUsersButton(driver);
		System.out.println("User Registration is Present which is Not as Expected!");
		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		**/
	}
		
	
	@Test(priority = 36)
	public void locateModifyUserOnUserManagement() throws TimeoutException, InterruptedException, ExecutionException {
	
		userDetailPage = new UserDetailPage();
		userManagementPage = new UserManagementPage();
		
		String tcNumber = "TC_BWRR-036 ";
		String testUserID = "leonorfd@ph.ibm.com";
		displayLogHeader(tcNumber);
			
		/** For Access Moderator only
		userRegistrationPage.clickUserManagement(driver);
		**/
	
		Thread.sleep(5000);	
	
		int chainBidDetails = userManagementPage.searchRowIndex(driver, testUserID);
		
		boolean userDetailPageButtonIsDisplayed = userManagementPage.userDetailPageButtonIsDisplayed(driver,chainBidDetails);

		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		Assert.assertFalse(userDetailPageButtonIsDisplayed, "User Detail Page Button is displayed which is Not as Expected");
		System.out.println("User Detail Page Button is not displayed which is as Expected");
		
		/** For Access Moderator only
		String userDetailPageHeaderText = userDetailPage.getUserDetailPageText(driver);
		Assert.assertFalse(userDetailPageHeaderText.contains("User Detail"), "User Detail is Present which is Not as Expected");
		System.out.println("User Detail is Not Present which is as Expected");
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());
		**/
		
		Thread.sleep(5000);		
	}
	
	@Test(priority = 37)
	public void exportUsersToCSVAtUserManagementPage() throws InterruptedException {
		userManagementPage = new UserManagementPage();
		
		String tcNumber = "TC_BWRR-037 ";
		displayLogHeader(tcNumber);
		
		/** For Access Moderator only
		userRegistrationPage.clickUserManagement(driver);
		**/
		
		takeScreenshot(tcNumber, SummaryPageTabsE.USER_MANAGEMENT.getLabel());

		boolean exportButtonIsDisplayed = userManagementPage.exportButtonIsDisplayed(driver);

		Assert.assertTrue(exportButtonIsDisplayed, "Export To CSV Button is not Displayed which is Not as Expected");
		System.out.println("Export To CSV Button is Displayed which is as Expected");

	}
	
	@Test(priority = 38)
	public void detailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		
		String tcNumber = "TC_BWRR-038 ";
		displayLogHeader(tcNumber);

		boolean nonFocusBidsTabIsPresent = false;

		try {
			driver.navigate().to("https://bldbz173021.cloud.dst.ibm.com:9443/systems/bret/SBidRiskController");

			summaryPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
			nonFocusBidsTabIsPresent = true;
		} catch (NoSuchElementException e) {
			nonFocusBidsTabIsPresent = false;
		}
		
		Assert.assertTrue(nonFocusBidsTabIsPresent, "Non-focus Bids Tab is not Present!");
			
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='nonFocusBidsGridDiv-2']")));
			
		} catch (Exception e) {
			throw new TimeoutException("Non-Focus Bids Tab is not fully loaded");
		}
		

		boolean detailedReportPageIsPresent = false;
		
		Thread.sleep(5000);
		nonFocusPage.clickDetailLinkOf1stBid(driver);
		

		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='idx_grid__PropertyRow_14']")));
			detailedReportPageIsPresent = true;
			
		} catch (Exception e) {
			detailedReportPageIsPresent = false;
			throw new TimeoutException("Detailed Report Page is not fully loaded");
		}
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		Assert.assertTrue(detailedReportPageIsPresent, "Detailed Report Page is not Present which is not as Expected");
	
		System.out.println("Detailed Report Page is Present which is as Expected");
		Thread.sleep(5000);

	}

	/** This is for Reviewer and Lead Reviewer
	@Test(priority = 38)
	public void detailedReportPageTC() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRR-038 ";
		displayLogHeader(tcNumber);

		boolean myTaskTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
			myTaskTabIsPresent = true;
		} catch (NoSuchElementException e) {
			myTaskTabIsPresent = false;
		}
		
		Assert.assertTrue(myTaskTabIsPresent, "My Task Tab is not Present!");
			
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='myTaskBidsGridDiv-2']")));
			
		} catch (Exception e) {
			throw new TimeoutException("My Task Tab is not fully loaded");
		}
		
		detailedReportPage.searchNewInMyTask2(driver,"New");
		
		boolean detailedReportPageIsPresent = false;
		
		Thread.sleep(5000);
		
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='idx_grid__PropertyRow_14']")));
			detailedReportPageIsPresent = true;
			
		} catch (Exception e) {
			detailedReportPageIsPresent = false;
			throw new TimeoutException("Detailed Report Page is not fully loaded");
		}
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		Assert.assertTrue(detailedReportPageIsPresent, "Detailed Report Page is not Present!");
	
		System.out.println("Detailed Report Page is Present!");
		Thread.sleep(5000);

	}
	**/
	
	
	@Test(priority = 39)
	public void takeThisBidAtDetailedReportPage() throws InterruptedException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRR-039 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean takeThisBidAtDetailedReportPage = detailedReportPage.takeThisBidButtonIsDisplayed(driver);

		Assert.assertFalse(takeThisBidAtDetailedReportPage, "Take this bid Button is Displayed which is not as expected.");
		System.out.println("Take this bid Button is not Displayed as expected");
		
	}
	
	@Test(priority = 40)
	public void writeInRemediationLogTabAtDetailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRR-040 ";
		displayLogHeader(tcNumber);

		boolean remediationLogTabIsPresent = false;

		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.REMEDIATION_LOG);
			remediationLogTabIsPresent = true;
		} catch (NoSuchElementException e) {
			remediationLogTabIsPresent = false;
		}
		
		Assert.assertTrue(remediationLogTabIsPresent, "Remediation Log Tab is not Present!");
			
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='remediationEditDiv_titleNode']")));
			
		} catch (Exception e) {
			throw new TimeoutException("Remediation Log Tab is not fully loaded");
		}
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean editButtonAtDetailedReportPage = detailedReportPage.editRemediationButtonIsDisplayed(driver);

		Assert.assertFalse(editButtonAtDetailedReportPage, "Edit Remediation Log Button is Displayed which is not as Expected.");
		System.out.println("Edit Remediation Log Button is Not Displayed as Expected");

		Thread.sleep(3000);
	
	}
	
	@Test(priority = 41)
	public void uploadAttachmentsAtDetailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRR-041 ";
		displayLogHeader(tcNumber);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement UploadButton = driver.findElement(By.linkText("Upload"));
		js.executeScript("arguments[0].scrollIntoView();", UploadButton);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean uploadAttachmentsButtonAtDetailedReportPage = detailedReportPage.uploadAttachmentsButtonIsDisplayed(driver);

		Assert.assertFalse(uploadAttachmentsButtonAtDetailedReportPage, "Upload Attachments Button is Displayed which is not as Expected.");
		System.out.println("Upload Attachments Button is Not Displayed as Expected");
		Thread.sleep(3000);
		
	}
		
	@Test(priority = 42)
	public void bidRollbackAtArchiveTab() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		detailedReportPage = new DetailedReportPage();
		archivePage = new ArchivePage();
		
		String tcNumber = "TC_BWRR-042 ";
		displayLogHeader(tcNumber);

		boolean archiveTabIsPresent = false;
		
		try {
			driver.navigate().to("https://bldbz173021.cloud.dst.ibm.com:9443/systems/bret/SBidRiskController?tab=archive");
			summaryPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
			archiveTabIsPresent = true;
		} catch (NoSuchElementException e) {
			archiveTabIsPresent = false;
		}
		
		Assert.assertTrue(archiveTabIsPresent, "Archive Tab is not Present!");
			
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"archiveBidsGridDiv-2\"]")));
			
		} catch (Exception e) {
			throw new TimeoutException("Archive Tab is not fully loaded");
		}
			
		detailedReportPage.searchRemediatedBidInArchive(driver);

		boolean detailedReportPageIsPresent = false;
		
		Thread.sleep(3000);
		
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='idx_grid__PropertyRow_14']")));
			detailedReportPageIsPresent = true;
			
		} catch (Exception e) {
			detailedReportPageIsPresent = false;
			throw new TimeoutException("Detailed Report Page is not fully loaded");
		}
		
		Assert.assertTrue(detailedReportPageIsPresent, "Detailed Report Page is not Present!");
	
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean rollbackButtonAtDetailedReportPage = detailedReportPage.rollbackButtonIsDisplayed(driver);

		Assert.assertFalse(rollbackButtonAtDetailedReportPage, "Rollback Button is Displayed which is not as Expected.");
		System.out.println("Rollback Button is not Displayed as Expected");
		
		Thread.sleep(3000);
	
	}
	@Test(priority = 43)
	public void exportSummaryTableToCSVAtDetailedReportPage() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		

		String tcNumber = "TC_BWRR-043 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean exportSummaryTableToCSVIsDisplayed = detailedReportPage.exportCSVButtonIsDisplayed(driver);

		Assert.assertTrue(exportSummaryTableToCSVIsDisplayed, "Export to CSV Button is not Displayed which is Not as Expected");
		System.out.println("Export To CSV Button is Displayed which is as Expected");
	}
	
	@Test(priority = 44)
	public void printReportViaPrintVersionAtDetailedReportPage() throws InterruptedException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		
		String tcNumber = "TC_BWRR-044 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean printVersionButtonIsDisplayed = detailedReportPage.printVersionButtonIsDisplayed(driver);

		Assert.assertTrue(printVersionButtonIsDisplayed, "Print Version Button is not Displayed which is not as Expected.");
		System.out.println("Print Version Button is Displayed which is as Expected");
	}
	
	@Test(priority = 45)
	public void exportNonFocusBelowClipToCSVAtDetailedReportPage() throws TimeoutException, InterruptedException, ExecutionException {
		summaryPage = new SummaryPage();
		nonFocusPage = new NonFocusPage();
		detailedReportPage = new DetailedReportPage();
		
		String tcNumber = "TC_BWRR-045 ";
		displayLogHeader(tcNumber);
		
		Thread.sleep(3000);
		detailedReportPage.navigateToTab(driver, SummaryPageTabsE.BELOW_CLIP_BIDS);

		try {
			WebDriverWait wait=new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gridx_Grid_1-1\"]")));

		} catch (Exception e) {
			throw new TimeoutException("Below Clip Bids Tab is not fully loaded");
		}
		
		takeScreenshot(tcNumber, SummaryPageTabsE.DETAILED_REPORT.getLabel());

		boolean exportToCSVInBelowClipBidsTabIsDisplayed = detailedReportPage.exportToCSVInBelowClipBidsTabIsDisplayed(driver);

		Assert.assertTrue(exportToCSVInBelowClipBidsTabIsDisplayed, "Export to CSV Button in Below Clip Bids Tab is not Displayed which is not as Expected.");
		System.out.println("Export to CSV Button in Below Clip Bids Tab is Displayed which is as Expected");
		
		Thread.sleep(8000);
	}
	
	@Test(priority = 46)
	public void viewBRETDashboardasBidSupport()  throws TimeoutException, InterruptedException, ExecutionException  {
		
		BRETDashboardLoginPage dashboardLoginPage = new BRETDashboardLoginPage(driver);
		
		String tcNumber = "TC_BWRR-046 ";
		displayLogHeader(tcNumber);
		
		driver.navigate().to("https://bldbz173021.cloud.dst.ibm.com:9443/DashboardSrcSys/");
		
		dashboardLoginPage.userLogin(getReviewerUsername(), getReviewerPassword());
	
		try {
			WebDriverWait wait=new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.titleContains("Bret Monitoring Dashboard"));
	
		} catch (Exception e) {
			throw new TimeoutException("Invalid Username or Password");
		}
		
		try {
			WebDriverWait wait=new WebDriverWait(driver, 100);
			System.out.println("Waiting for BRET Dashboard to load. This will take time, please wait...");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sourceNameText1\"]")));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sourceNameText12\"]")));
			
		} catch (Exception e) {
			throw new TimeoutException("BRET Dashboard is not fully loaded");
		}
		
		Thread.sleep(8000);
		
		takeScreenshot(tcNumber, SummaryPageTabsE.BRET_DASHBOARD.getLabel());
		System.out.println("BRET Dashboard is Displayed as expected");
	}
	
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}


}
