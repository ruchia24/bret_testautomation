package com.bret.pages;

import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bret.enums.SummaryPageTabsE;
import com.bret.util.BretTestUtils;


public class SummaryPage {
	
	private static final String XPATH_USER_MANAGEMENT = "/html/body/div[1]/div[1]/div/div/div[3]/div/span[1]/a";
	private static final String XPATH_MY_TASK_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[1]";
	private static final String XPATH_NON_FOCUS_BIDS_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[2]";
	private static final String XPATH_FOCUS_BIDS_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[3]";
	private static final String XPATH_ARCHIVE_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[4]";
	private static final String XPATH_SEARCH_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[5]";
	private static final String XPATH_WAITING_BIDS_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[5]";		
	private static final String XPATH_ERROR_BIDS_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[6]";	
	private static final String XPATH_ADV_SEARCH_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[8]";	
	private static final String XPATH_MANUAL_REVIEW_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[8]";
	private static final String XPATH_GOE_TERMINATION_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[10]";
	
	//Added by Calvin on July 2
	private static final String XPATH_GOE_TERMINATION_TAB_2 = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[9]";
	
	
	private static final String XPATH_REPORT_TAB = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div[11]";
	
	// Added by Calvin on June 27
	private static final String XPATH_REMEDIATION_LOG_TAB = "//*[@id=\"dijit_layout_TabContainer_0_tablist_remediationTab\"]";
	
	//Added by Calvin on June 29
	private static final String XPATH_BELOW_CLIP_BIDS_TAB = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[1]/div[4]/div/div[5]";
	
	private static final String XPATH_MY_TASK_PART_1 = ".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_MY_TASK_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_MY_TASK_PART_3 = "]";

	private static final String XPATH_NON_FOCUS_PART_1 = ".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_NON_FOCUS_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_NON_FOCUS_PART_3 = "]";
	
	private static final String XPATH_NPS = "//*[@id=\"ibmNpsClose\"]";

	private WebDriver driver;

	/**
	 * Method for navigating between Summary page tabs.
	 * 
	 * @param driver
	 *            the current driver.
	 * @param tab
	 *            the tab to navigate TO
	 */
	public void navigateToTab(WebDriver driver, SummaryPageTabsE tab) {
		this.driver = driver;

		switch (tab) {
		case USER_MANAGEMENT:
			clickTab(XPATH_USER_MANAGEMENT, SummaryPageTabsE.USER_MANAGEMENT);
			break;
		case MY_TASK:
			clickTab(XPATH_MY_TASK_TAB, SummaryPageTabsE.MY_TASK);
			break;
		case NON_FOCUS_BIDS:
			clickTab(XPATH_NON_FOCUS_BIDS_TAB, SummaryPageTabsE.NON_FOCUS_BIDS);
			break;
		case FOCUS_BIDS:
			clickTab(XPATH_FOCUS_BIDS_TAB, SummaryPageTabsE.FOCUS_BIDS);
			break;
		case ARCHIVE:
			clickTab(XPATH_ARCHIVE_TAB, SummaryPageTabsE.ARCHIVE);
			break;
		case SEARCH:
			clickTab(XPATH_SEARCH_TAB, SummaryPageTabsE.SEARCH);
			break;
		case WAITING_BIDS:
			clickTab(XPATH_WAITING_BIDS_TAB, SummaryPageTabsE.WAITING_BIDS);
			break;
		case ERROR_BIDS:
			clickTab(XPATH_ERROR_BIDS_TAB, SummaryPageTabsE.ERROR_BIDS);
			break;
		case ADVANCED_SEARCH:
			clickTab(XPATH_ADV_SEARCH_TAB, SummaryPageTabsE.ADVANCED_SEARCH);
			break;
		case MANUAL_REVIEW:
			clickTab(XPATH_MANUAL_REVIEW_TAB, SummaryPageTabsE.MANUAL_REVIEW);
			break;
		case GOE_DETERMINATION:
			clickTab(XPATH_GOE_TERMINATION_TAB, SummaryPageTabsE.GOE_DETERMINATION);
			break;
		case GOE_DETERMINATION_2:
			clickTab(XPATH_GOE_TERMINATION_TAB_2, SummaryPageTabsE.GOE_DETERMINATION_2);
			break;
		case REPORT:
			clickTab(XPATH_REPORT_TAB, SummaryPageTabsE.REPORT);
			break;
		case REMEDIATION_LOG:
			clickTab(XPATH_REMEDIATION_LOG_TAB, SummaryPageTabsE.REMEDIATION_LOG);
			break;
		case BELOW_CLIP_BIDS:
			clickTab(XPATH_BELOW_CLIP_BIDS_TAB, SummaryPageTabsE.BELOW_CLIP_BIDS);
			break;
		default:
			break;

		}
	}

	/**
	 * Gets the url part from the go to detail link icon. To be used on
	 * navigating to Details page.
	 * 
	 * @param driver
	 *            the current driver
	 * @param tabOrigin
	 *            origin tab
	 * @param rowIndex
	 *            row index of the bid the user wish to click.
	 * @return the url part
	 */
	public String getDetailSubUrl(WebDriver driver, SummaryPageTabsE tabOrigin,
			int rowIndex) {
		this.driver = driver;
		String subUrl = "";
		switch (tabOrigin) {
		case MY_TASK:
			subUrl = getDetailLinkPart(constructXpath(XPATH_MY_TASK_PART_1,
					XPATH_MY_TASK_PART_2, XPATH_MY_TASK_PART_3, rowIndex,
					MyTaskPage.DETAIL_LINK_COL));
			break;
		case NON_FOCUS_BIDS:
			subUrl = getDetailLinkPart(constructXpath(XPATH_NON_FOCUS_PART_1,
					XPATH_NON_FOCUS_PART_2, XPATH_NON_FOCUS_PART_3, rowIndex,
					NonFocusPage.DETAIL_LINK_COL));
			break;
		default:
			subUrl = "Invalid";
			break;
		}
		return subUrl;
	}

	/**
	 * Clicks the tab which the user wish to navigate to.
	 * 
	 * @param xpath
	 *            of the tab to be clicked.
	 * @param tab
	 *            that the user wants to click
	 */
	private void clickTab(String xpath, SummaryPageTabsE tab) {
		driver.findElement(By.xpath(xpath)).click();
		
		BretTestUtils.printToConsole("Successful Navigation TO: " + tab.getLabel()
				+ " tab");

	}

	/**
	 * Gets the detail url of the link.
	 * 
	 * @param xpath
	 *            of the link icon.
	 * @return the url part.
	 */
	private String getDetailLinkPart(String xpath) {
		String attribute = driver.findElement(By.xpath(xpath + "/span/a"))
				.getAttribute("onclick");

		return attribute;
	}

	/**
	 * 
	 * Constructs the xpath.
	 * 
	 * @param xpath1
	 *            xpath part 1
	 * @param xpath2
	 *            xpath part 2
	 * @param xpath3
	 *            xpath part
	 * @param rowIndex
	 *            the row index to be appended on the xpath.
	 * @param colIndex
	 *            the column index to be appended on the xpath.
	 * @return constructed xpath.
	 */
	private String constructXpath(String xpath1, String xpath2, String xpath3,
			int rowIndex, int colIndex) {
		return xpath1 + rowIndex + xpath2 + colIndex + xpath3;
	}
	
	/**
	 * Closes the NPS Survey pop up.
	 * 
	 * @param driver
	 */
	public void closeNPSSurvey(WebDriver driver) {
		this.driver = driver;
		
		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		try {
			if(dayOfMonth <= 7) {
				Thread.sleep(60000);
				
				if(checkElementVisibility(driver, XPATH_NPS)) {
					driver.findElement(By.xpath(XPATH_NPS)).click();
				}
				
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Checking if the Element in parameter is visible.
	 * 
	 * @param driver
	 * @return true if the element is visible and false if otherwise.
	 */
	public boolean checkElementVisibility(WebDriver driver, String xpath) {
		this.driver = driver;
		boolean isVisible = false;
		try {
			driver.findElement(By.xpath(xpath));
			isVisible = true;
		} catch (NoSuchElementException e) {
			isVisible = false;
		}
		return isVisible;
	}
	
	public boolean userManagementIsDisplayed(WebDriver driver) {
		WebElement userManagement = driver.findElement(By.xpath(XPATH_USER_MANAGEMENT));
		boolean userManagementIsDisplayed = userManagement.isDisplayed();
		return userManagementIsDisplayed;
	}

}
