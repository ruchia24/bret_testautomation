package com.bret.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import decoder.bret.ibm.com.Decoder;



/** 
* [added-Belle Pondiong]<br>
* This page class contained BRET web login. <br>
*/
public class BRETValidLogin {

	public WebDriver driver;
	
	 //Declaring the Login page web elements
	 @FindBy(id="idx_form_TextBox_0")
	 private WebElement username;

	 @FindBy(id="idx_app_LoginFrame_0Password")
	 private WebElement password;
	 
	 @FindBy (id = "dijit_form_Button_0_label")    //dijit_form_Button_0  dijit_form_Button_0_label
	 private WebElement loginBtn;
	 
	// This is a web page class constructor		 
		 public BRETValidLogin (WebDriver driver) {
			 this.driver= driver;
			 //System.out.println("BRETLoginPage driver = " + this.driver.hashCode());
			 PageFactory.initElements(driver, this);
		 }
	/** 
	* [added-Belle Pondiong]<br>
	* This method performs BRET Login routine and will look for My Task Tab as validation. <br>
	* This will return void. <br>
	* <br>[Updated-Belle-9/10/17]<br>
	* Updated the code to check successfully login.<br>
	* Assertion on page title instead of My Task tab. <br>
	* <br>Test Verification: <br>
	* Special Bid Risk Analysis - Bid Summary page title is checked on successful login.
	*/
	 public void userValidLogin(String Username, String Password) {
//		 StringBuffer verificationErrors = new StringBuffer();
		 String Password2;
		 
//		 System.out.println("trace: passed username from spreadsheet: " + Username);  
//		 System.out.println("trace: passed password from spreadsheet: " + Password); 
 	 
		 Password2 = Decoder.decodeString(Password.trim()); //decode
		 
		 // Login entry
		 username.clear(); 
		 username.sendKeys(Username);
		 password.clear();
    	 password.sendKeys(Password2);	 
	 
		 //Login button clicked
		 loginBtn.click();
		 
		 String pageTitle = driver.getTitle();

			Assert.assertEquals(pageTitle, "Special Bid Risk Analysis - Bid Summary", "Unable to login. Please check credentials or problem loading the page.!"); 
			
		    //Looking for My Task tab after login to designate successful login
//		    try {
//		    	Assert.assertEquals(driver.findElement(By.id("dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_0")).getText(), "My Task");
		    	// manual tracing
		    	System.out.println("Login Validation: BRET Login was successful!");	    	   		
//		    } catch (Error e) {	
//		       System.out.println("Unable to login. Please check credentials or problem loading the page.!");	 	
//		       verificationErrors.append(e.toString());
//		    }
     }
}