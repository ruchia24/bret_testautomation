package com.bret.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BidQPage extends DetailPage {

	private static final String XPATH_PART_1_REL_Q = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[3]/div/div[1]/div[3]/div[2]/div[";
	private static final String XPATH_PART_2_REL_Q = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3_REL_Q = "]";

	private static final String XPATH_PART_1_HIST = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[3]/div/div[2]/div[3]/div[3]/div[2]/div[";
	private static final String XPATH_PART_2_HIST = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3_HIST = "]";

	private static final String XPATH_CHANNEL_OVERRIDE_DESC = ".//*[@id='factor10Div']/div[3]/div[2]/div/table/tbody/tr/td[3]";
	private static final String XPATH_CHANNEL_OVERRIDE_REASON = ".//*[@id='factor10Div']/div[3]/div[2]/div/table/tbody/tr/td[2]";

	public static final int COL_FACTOR_NUM = 1;
	public static final int COL_REVIEW_FACTOR = 2;
	public static final int COL_SCORE = 3;
	public static final int COL_CURRENT_VALUE = 4;
	public static final int COL_OUTLIER_STATUS_VAL = 5;

	public static final int ROW_CONFIDENTIALITY = 1;
	public static final int ROW_ROUTE_TO_MARKET = 2;
	public static final int ROW_NON_COMPETITIVE_BID = 3;
	public static final int ROW_OFFSHORE_PAYMENT_TERMS = 4;
	public static final int ROW_BUNDLED_SOLUTIONS = 5;
	public static final int ROW_CONTINGENCY_FEE_PAYMENTS = 6;

	public static final int COL_HIST_FACTOR = 1;
	public static final int COL_HIST_ROLE = 2;
	public static final int COL_HIST_NAME = 3;
	public static final int COL_HIST_BPC_OPS_CAT = 4;
	public static final int COL_HIST_NOT_AUTH_PART = 5;
	public static final int COL_HIST_SCORE_1 = 6;
	public static final int COL_HIST_OUTLIER_STATUS_1 = 7;
	public static final int COL_HIST_SCORE_2 = 8;
	public static final int COL_HIST_OUTLIER_STATUS_2 = 9;

	public static final int ROW_HIST_DISTRIBUTOR = 1;
	public static final int ROW_HIST_CUST_FACING_BP = 2;
	public static final int ROW_HIST_ADDITIONAL_BP_TIER_3 = 3;
	public static final int ROW_HIST_ADDITIONAL_BP_TIER_4 = 4;

	private WebDriver driver;

	public BidQPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getValueAtIndexRelQuestions(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By
				.xpath(constructXpath(rowIndex, colIndex, XPATH_PART_1_REL_Q, XPATH_PART_2_REL_Q, XPATH_PART_3_REL_Q)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	public String getValueAtIndexHistory(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(
				By.xpath(constructXpath(rowIndex, colIndex, XPATH_PART_1_HIST, XPATH_PART_2_HIST, XPATH_PART_3_HIST)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	public int getTotalForBPCops(WebDriver driver, boolean isCeidSimilar) {
		return computeRelatedQuestionScore(driver) + computeHistoryScore(driver, isCeidSimilar);
	}

	/**
	 * changed from private to public March 3, 2018 Lhoyd Castillo
	 * 
	 */
	public int computeRelatedQuestionScore(WebDriver driver) {
		int score = 0;
		for (int i = ROW_CONFIDENTIALITY; i <= ROW_CONTINGENCY_FEE_PAYMENTS; i++) {
			WebElement col = driver.findElement(
					By.xpath(constructXpath(i, COL_SCORE, XPATH_PART_1_REL_Q, XPATH_PART_2_REL_Q, XPATH_PART_3_REL_Q)));

			if (null != col && !col.getText().equalsIgnoreCase("") && !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	/**
	 * changed from private to public March 3, 2018 Lhoyd Castillo
	 */
	public int computeHistoryScore(WebDriver driver, boolean isCeidSimilar) {
		int score = 0;
		int maxRow = getMaxRow(driver);
		for (int i = ROW_HIST_DISTRIBUTOR; i <= maxRow; i++) {
			/**
			 * (int i = ROW_HIST_DISTRIBUTOR; i <= ROW_HIST_CUST_FACING_BP; i++)code before
			 * 03/07/2018
			 */

			WebElement col1 = driver.findElement(By.xpath(
					constructXpath(i, COL_HIST_SCORE_1, XPATH_PART_1_HIST, XPATH_PART_2_HIST, XPATH_PART_3_HIST)));

			if (null != col1 && !col1.getText().equalsIgnoreCase("") && !col1.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col1.getText());
			}

			WebElement col2 = driver.findElement(By.xpath(
					constructXpath(i, COL_HIST_SCORE_2, XPATH_PART_1_HIST, XPATH_PART_2_HIST, XPATH_PART_3_HIST)));

			if (null != col2 && !col2.getText().equalsIgnoreCase("") && !col2.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col2.getText());
			}

			if (isCeidSimilar) {
				break;
			}

		}

		return score;
	}
	
	public int computeHistoryScore(WebDriver driver, List<Integer> countedCeid) {
		int score = 0;
		int max_row = getMaxRow(driver);
		for (int i = ROW_HIST_DISTRIBUTOR; i <= max_row; i++) {

			if (countedCeid.contains(i)) {
				WebElement col1 = driver.findElement(By.xpath(
						constructXpath(i, COL_HIST_SCORE_1, XPATH_PART_1_HIST, XPATH_PART_2_HIST, XPATH_PART_3_HIST)));

				if (null != col1 && !col1.getText().equalsIgnoreCase("") && !col1.getText().equalsIgnoreCase("N/A")) {
					score += Integer.parseInt(col1.getText());
				}

				WebElement col2 = driver.findElement(By.xpath(
						constructXpath(i, COL_HIST_SCORE_2, XPATH_PART_1_HIST, XPATH_PART_2_HIST, XPATH_PART_3_HIST)));

				if (null != col2 && !col2.getText().equalsIgnoreCase("") && !col2.getText().equalsIgnoreCase("N/A")) {
					score += Integer.parseInt(col2.getText());
				}
			}
		}

		return score;
	}

	private String constructXpath(int rowIndex, int colIndex, String xpathPart1, String xpathPart2, String xpathPart3) {
		return xpathPart1 + rowIndex + xpathPart2 + colIndex + xpathPart3;
	}

	public String getChannelOverrideDesc() {
		String desc = driver.findElement(By.xpath(XPATH_CHANNEL_OVERRIDE_DESC)).getText();
		System.out.println(desc);
		return desc;
	}

	public boolean channelOverrideReasonPresent(WebDriver driver) {
		boolean result = false;
		try {
			driver.findElement(By.xpath(XPATH_CHANNEL_OVERRIDE_REASON));
			System.out.println("\nSQO Channel Override Reason field is Present");
			result = true;
		} catch (Exception e) {
			System.out.println("\nSQO Channel Override Reason Field not Present");
			result = false;
		}
		return result;
	}

	/**
	 * 
	 * @author LhoydCastillo 
	 * March 3, 2018
	 * 
	 */

	public boolean distributorConfidentialityScoreAndOutlierHasCorrectFlagging() {
		String distribScore = getValueAtIndexHistory(driver, ROW_HIST_DISTRIBUTOR, COL_HIST_SCORE_1);
		String distribOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_DISTRIBUTOR, COL_HIST_OUTLIER_STATUS_1);
		if ((!distribScore.equals("0") && distribOutlierStatus.equals("Y"))
				|| (distribScore.equals("0") && distribOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}

	public boolean distributorTierInvestigationScoreAndOutlierHasCorrectFlagging() {
		String distribScore = getValueAtIndexHistory(driver, ROW_HIST_DISTRIBUTOR, COL_HIST_SCORE_2);
		String distribOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_DISTRIBUTOR, COL_HIST_OUTLIER_STATUS_2);
		if ((!distribScore.equals("0") && distribOutlierStatus.equals("Y"))
				|| (distribScore.equals("0") && distribOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}

	public boolean customerFacingConfidentialityScoreAndOutlierHasCorrectFlagging() {
		String custScore = getValueAtIndexHistory(driver, ROW_HIST_CUST_FACING_BP, COL_HIST_SCORE_1);
		String custOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_CUST_FACING_BP, COL_HIST_OUTLIER_STATUS_1);
		if ((!custScore.equals("0") && custOutlierStatus.equals("Y"))
				|| (custScore.equals("0") && custOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}

	public boolean customerFacingTierInvestigationScoreAndOutlierHasCorrectFlagging() {
		String custScore = getValueAtIndexHistory(driver, ROW_HIST_CUST_FACING_BP, COL_HIST_SCORE_2);
		String custOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_CUST_FACING_BP, COL_HIST_OUTLIER_STATUS_2);
		if ((!custScore.equals("0") && custOutlierStatus.equals("Y"))
				|| (custScore.equals("0") && custOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author LhoydCastillo
	 * March 7, 2018
	 * @param driver
	 * @return
	 */
	public boolean additionalBPTier3ConfidentialityScoreAndOutlierHasCorrectFlagging() {
		String additionalBPScore = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_3, COL_HIST_SCORE_1);
		String additionalBPOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_3, COL_HIST_OUTLIER_STATUS_1);
		if ((!additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("Y"))
				|| (additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean additionalBPTier3TierInvestigationScoreAndOutlierHasCorrectFlagging() {
		String additionalBPScore = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_3, COL_HIST_SCORE_2);
		String additionalBPOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_3, COL_HIST_OUTLIER_STATUS_2);
		if ((!additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("Y"))
				|| (additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean additionalBPTier4ConfidentialityScoreAndOutlierHasCorrectFlagging() {
		String additionalBPScore = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_4, COL_HIST_SCORE_1);
		String additionalBPOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_4, COL_HIST_OUTLIER_STATUS_1);
		if ((!additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("Y"))
				|| (additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean additionalBPTier4TierInvestigationScoreAndOutlierHasCorrectFlagging() {
		String additionalBPScore = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_4, COL_HIST_SCORE_2);
		String additionalBPOutlierStatus = getValueAtIndexHistory(driver, ROW_HIST_ADDITIONAL_BP_TIER_4, COL_HIST_OUTLIER_STATUS_2);
		if ((!additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("Y"))
				|| (additionalBPScore.equals("0") && additionalBPOutlierStatus.equals("N")) == true) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * @author LhoydCastillo
	 * March 7, 2018
	 * @param driver
	 * @return
	 */
	public boolean tier3isPresent(WebDriver driver) {
		boolean result = false;
		try {
			driver.findElement(By.xpath(constructXpath(ROW_HIST_ADDITIONAL_BP_TIER_3, COL_HIST_SCORE_1, XPATH_PART_1_HIST,
					XPATH_PART_2_HIST, XPATH_PART_3_HIST)));
			System.out.println("\nTier 3 is Present");
			result = true;
		} catch (Exception e) {
			System.out.println("\nTier 3 not Present");
			result = false;
		}
		return result;
	}

	public boolean tier4isPresent(WebDriver driver) {
		boolean result = false;
		try {
			driver.findElement(By.xpath(constructXpath(ROW_HIST_ADDITIONAL_BP_TIER_4, COL_HIST_SCORE_1, XPATH_PART_1_HIST,
					XPATH_PART_2_HIST, XPATH_PART_3_HIST)));
			System.out.println("\nTier 4 is Present");
			result = true;
		} catch (Exception e) {
			System.out.println("\nTier 4 not Present");
			result = false;
		}
		return result;
	}
	
	private int getMaxRow(WebDriver driver) {
		int max = 2;
		if(tier3isPresent(driver)) {
			max = 3;
		}
		if(tier4isPresent(driver)) {
			max = 4;
		}		
		return max;
	}

}
