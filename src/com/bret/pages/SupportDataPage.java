package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.bret.util.BretTestUtils;

public class SupportDataPage extends DetailPage {
	public static final int TBL_BP_WIN_RATE = 1;
	public static final int TBL_DIFF_BP_WIN_RATE = 2;
	public static final int TBL_REVENUE_SHARE = 3;
	public static final int TBL_BP_GROWTH_COMP_1 = 4;
	public static final int TBL_BP_GROWTH_COMP_2 = 5;
	public static final int TBL_BP_GROWTH_COMP_3 = 6;
	public static final int TBL_BP_GROWTH_COMP_4 = 7;

	private static final int TBL_BP_WIN_RATE_MAX_ROWS = 3;
	private static final int TBL_DIFF_BP_WIN_RATE_MAX_ROWS = 2;
	private static final int TBL_REVENUE_SHARE_MAX_ROWS = 5;
	private static final int TBL_BP_GROWTH_COMP_1_MAX_ROWS = 2;
	private static final int TBL_BP_GROWTH_COMP_2_MAX_ROWS = 2;
	private static final int TBL_BP_GROWTH_COMP_3_MAX_ROWS = 3;
	private static final int TBL_BP_GROWTH_COMP_4_MAX_ROWS = 4;

	public static final int COL_SCORE = 3;

	private static final String XPATH_PART_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[4]/div/div[";
	private static final String XPATH_PART_2 = "]/div[3]/div[2]/div[";
	private static final String XPATH_PART_3 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_4 = "]";

	@SuppressWarnings("unused")
	private WebDriver driver;

	/**
	 * Constructor
	 * 
	 * @param driver
	 */
	public SupportDataPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Computes the total score of the the Support data tab.
	 * 
	 * @param driver
	 * @return the total score of the support data tab.
	 */
	public int computeTotalScore(WebDriver driver) {
		this.driver = driver;
		int total = 0;

		total += computeBPWinRateScore();

		total += computeDiffBPWinRateScore();

		total += computeRevenueShareScore();

		total += computeBPGrowthCompScore1();

		total += computeBPGrowthCompScore2();

		total += computeBPGrowthCompScore3();

		total += computeBPGrowthCompScore4();

		return total;
	}
	
	
	public int computeTotalScore2(WebDriver driver) {
		this.driver = driver;
		int total = 0;

		total += computeBPWinRateScore2();

		total += computeDiffBPWinRateScore();

		total += computeRevenueShareScore();

		total += computeBPGrowthCompScore1();

		total += computeBPGrowthCompScore2();

		total += computeBPGrowthCompScore3();

		total += computeBPGrowthCompScore4();

		return total;
	}
	
	

	private int computeBPWinRateScore() {
		
		int score = 0;

		for (int i = 1; i <= TBL_BP_WIN_RATE_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_BP_WIN_RATE, i, COL_SCORE)));
			
			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score = Integer.parseInt(col.getText());
			}
			
		}
		
		return score;
	}
	
	private int computeBPWinRateScore2() {
		
		int score = 0;

		for (int i = 1; i < TBL_BP_WIN_RATE_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_BP_WIN_RATE, i, COL_SCORE)));
			
			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score = Integer.parseInt(col.getText());
			}

		}
		

		return score;
	}
	

	private int computeDiffBPWinRateScore() {

		int score = 0;
		for (int i = 1; i <= TBL_DIFF_BP_WIN_RATE_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_DIFF_BP_WIN_RATE, i, COL_SCORE)));

			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	private int computeRevenueShareScore() {

		int score = 0;
		for (int i = 1; i <= TBL_REVENUE_SHARE_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_REVENUE_SHARE, i, COL_SCORE)));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", col);

			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	private int computeBPGrowthCompScore1() {
		int score = 0;
		for (int i = 1; i <= TBL_BP_GROWTH_COMP_1_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_BP_GROWTH_COMP_1, i, COL_SCORE)));

			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	private int computeBPGrowthCompScore2() {
		int score = 0;
		for (int i = 1; i <= TBL_BP_GROWTH_COMP_2_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_BP_GROWTH_COMP_2, i, COL_SCORE)));

			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	private int computeBPGrowthCompScore3() {
		int score = 0;
		for (int i = 1; i <= TBL_BP_GROWTH_COMP_3_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_BP_GROWTH_COMP_3, i, COL_SCORE)));

			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	private int computeBPGrowthCompScore4() {
		int score = 0;
		for (int i = 1; i <= TBL_BP_GROWTH_COMP_4_MAX_ROWS; i++) {
			WebElement col = driver.findElement(By.xpath(constructXpath(
					TBL_BP_GROWTH_COMP_4, i, COL_SCORE)));

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", col);
			if (null != col && !col.getText().equalsIgnoreCase("")
					&& !col.getText().equalsIgnoreCase("N/A")) {
				score += Integer.parseInt(col.getText());
			}
		}

		return score;
	}

	private String constructXpath(int tableNum, int rowIndex, int colIndex) {
		return XPATH_PART_1 + tableNum + XPATH_PART_2 + rowIndex + XPATH_PART_3
				+ colIndex + XPATH_PART_4;
	}
	
	public void scrollingScreenshot(WebDriver driver, String directoryPath, String imgFilename) { 
		for (int i = 1; i <= 5; i++) {
			BretTestUtils.screenCapture(driver, directoryPath, imgFilename + "_" + i +  BretTestUtils.getImgFormat());
			WebElement table = driver.findElement(By.xpath(constructXpath(i, 1,
					COL_SCORE)));
			
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", table);
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
	}
	
	////*[@id="focusBidAttributesDiv"]

}
