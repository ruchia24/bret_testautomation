package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.bret.enums.DetailPageTabsE;
import com.bret.util.BretTestUtils;

public class DetailPage {
	private static final String XPATH_FLAGGING_BID_DATA_TAB = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[1]/div[4]/div/div[1]";
	private static final String XPATH_DISCOUNT_MARGIN_TAB = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[1]/div[4]/div/div[2]";
	public static final String XPATH_BID_Q_TAB = "//span[contains(text(),'Bid Qs')]";
	private static final String XPATH_SUPPORT_DATA_TAB = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[1]/div[4]/div/div[4]";
	private static final String XPATH_BELOW_CLIP_TAB = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[1]/div[4]/div/div[5]";
	public static final String XPATH_REMEDIATION_LOG = "//span[text()='Remediation Log']";
	
	
	public static final String NAVIGATE_BACK_MY_TASK = "My Task";
	public static final String NAVIGATE_BACK_NON_FOCUS = "Non-focus Bids";
	public static final String NAVIGATE_BACK_FOCUS = "Focus Bids";
	public static final String NAVIGATE_BACK_ARCHIVE = "Archive";
	
	private WebDriver driver;

	/**
	 * Method for navigating between Summary page tabs.
	 * 
	 * @param driver
	 *            the current driver.
	 * @param tab
	 *            the tab to navigate TO
	 */
	public void navigateToTab(WebDriver driver, DetailPageTabsE tab) {
		this.driver = driver;

		switch (tab) {
		case FLAGGING_BID_DATA:
			clickTab(XPATH_FLAGGING_BID_DATA_TAB,
					DetailPageTabsE.FLAGGING_BID_DATA);
			break;
		case DISCOUNT_MARGIN:
			clickTab(XPATH_DISCOUNT_MARGIN_TAB, DetailPageTabsE.DISCOUNT_MARGIN);
			break;
		case BID_Q:
			clickTab(XPATH_BID_Q_TAB, DetailPageTabsE.BID_Q);
			break;
		case SUPPORT_DATA:
			clickTab(XPATH_SUPPORT_DATA_TAB, DetailPageTabsE.SUPPORT_DATA);
			break;
		case BELOW_CLIP_BIDS:
			clickTab(XPATH_BELOW_CLIP_TAB, DetailPageTabsE.BELOW_CLIP_BIDS);
			break;
		case REMEDIATION_LOG:
			clickTab(XPATH_REMEDIATION_LOG, DetailPageTabsE.REMEDIATION_LOG);
			break;
		default:
			break;

		}
	}

	private void clickTab(String xpath, DetailPageTabsE tab) {
		BretTestUtils.printToConsole("Navigating TO: " + tab.getLabel());
		System.out.println();
		driver.findElement(By.xpath(xpath)).click();
	}
	
	/**
	 * Navigates back to Summary page
	 * 
	 * @param summaryTab summary tab to navigate to.
	 */
	public void navigateBackToSummary(WebDriver driver, String summaryTab) {
		this.driver = driver;
		
		BretTestUtils.printToConsole("Detail Page -- Navigating back to Summary Page -> " + summaryTab + " tab");
		String xpath = "//td[text()='" + summaryTab + "']";
		driver.findElement(By.xpath("//a[text()='Go back to']")).click();
		driver.findElement(By.xpath(xpath)).click();
	}
	
	/**
	 * Checking if the Element in parameter is visible.
	 * 
	 * @param driver
	 * @return true if the element is visible and false if otherwise.
	 */
	public boolean checkElementVisibility(WebDriver driver, String xpath) {
		this.driver = driver;
		boolean isVisible = false;
		try {
			driver.findElement(By.xpath(xpath));
			isVisible = true;
		} catch (NoSuchElementException e) {
			isVisible = false;
		}
		return isVisible;
	}
	
}
