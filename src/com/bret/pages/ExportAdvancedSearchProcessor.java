package com.bret.pages;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bret.obj.export.ExportDetailData;
import com.bret.util.BretTestUtils;

public class ExportAdvancedSearchProcessor {

	private static final int TYPE = 0;
	private static final int BID_ID = 1;
	private static final int ADDITIONAL_ID = 2;
	private static final int BID_DATE = 3;
	private static final int SOURCE_SYSTEM = 4;
	private static final int REGION = 5;
	private static final int COUNTRY = 6;
	private static final int REVIEWER_DECISION = 7;
	private static final int REVIEWER = 8;
	private static final int REVIEWER_RELEASED_ON = 9;
	private static final int DISTRIBUTOR = 10;
	private static final int CUSTOMER_FACING_BP = 11;
	private static final int CUSTOMER_NAME = 12;
	private static final int CONFIDENTIALITY = 13;
	private static final int ROUTE_TO_MARKET = 14;
	private static final int OFFSHORE_PAYMT_TERMS = 15;
	private static final int BUNDLED_SOLUTIONS = 16;
	private static final int CONTINGENCY_FEE = 17;
	private static final int SOLE_SOURCE_PROC = 18;
	private static final int FOCUS_BP = 19;
	private static final int BP_MARG_PERC = 20;
	private static final int DISCOUNT_PERC = 21;
	private static final int BP_MARG_TOTAL = 22;
	private static final int DISCOUNT_TOTAL = 23;
	private static final int OPERATION_OWNER = 24;
	private static final int BID_MGR = 25;
	private static final int DISTRIBUTOR_CEID = 26;
	private static final int CUSTOMER_FACING_BP_CEID = 27;
	private static final int T3_NAME = 28;
	private static final int T4_NAME = 29;
	private static final int T5_NAME = 30;
	private static final int BPM_T1_FIN = 31;
	private static final int BPM_T1_SERV = 32;
	private static final int BPM_T1_INS = 33;
	private static final int BPM_T1_PROFIT = 34;
	private static final int BPM_T2_FIN = 35;
	private static final int BPM_T2_SERV = 36;
	private static final int BPM_T2_INS = 37;
	private static final int BPM_T2_PROFIT = 38;
	private static final int BPM_OTHER = 39;
	private static final int LOG = 40;
	private static final int CHANNEL_PROGRAM = 41;
	private static final int ACTION_OWNER = 42;
	private static final int CLOSED = 43;
	private static final int CLOSE_DATE = 44;
	private static final int OUTCOME = 45;
	private static final int OUTCOME_COMMENT = 46;
	private static final int CONDITION = 47;
	private static final int TRANSPARENCY_LETTER = 48;
	private static final int CYCLE = 49;
	private static final int EXCEPTION_FLAG = 50;
	private static final int FULLFILLED = 51;
	private static final int COMMENT = 52;
	private static final int BID_EXPIRED = 53;
	private static final int EX_PRE_FULFILLMENT = 54;
	private static final int POST_VERIFY_DATE = 55;
	private static final int POST_SHIP_AUDIT_STAT = 56;
	private static final int WAITING_FOR_EXTENSION = 57;
	private static final int PROBLEMS_WITH_CHASING = 58;
	private static final int REQUEST_SENT_TOO_EARLY = 59;
	private static final int REALISTICALLY_DUE = 60;
	private static final int TEST_NOTE_STARTED_YET = 61;
	private static final int POTENTIAL_ISSUES = 62;
	private static final int FOLLOW_UP_LOG = 63;
	private static final int DEADLINE_OF_REQUEST = 64;
	private static final int BID_EVENT = 65;

	public ExportDetailData getExportDetailData(String filePath) {
		List<String> csvContents = BretTestUtils.readFile(filePath);
		csvContents.remove(0);
		csvContents.remove(0);

		// ExportDetailData exportData =
		// mapExportCSVAdvancedSearchToObj(cleanupCSVData(csvContents));
		List<String> arrangedList = arrangeContents(csvContents.get(0));
		List<String> cleanedUpList = cleanupCSVData(arrangedList);

		ExportDetailData exportData = mapExportCSVAdvancedSearchToObj(cleanedUpList);

		return exportData;
	}

	private List<String> arrangeContents(String content) {
		List<String> arrangedContent = Arrays.asList(content.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
		return arrangedContent;
	}

	private ExportDetailData mapExportCSVAdvancedSearchToObj(List<String> csvContents) {
		ExportDetailData exportData = new ExportDetailData();

		exportData.setType(csvContents.get(TYPE));
		exportData.setSourceSystem(csvContents.get(SOURCE_SYSTEM));
		exportData.setRegion(csvContents.get(REGION));
		exportData.setBidId(csvContents.get(BID_ID));
		exportData.setAdditionalId(csvContents.get(ADDITIONAL_ID));
		exportData.setBidDate(csvContents.get(BID_DATE));
		exportData.setReviewerDecision(csvContents.get(REVIEWER_DECISION));
		exportData.setReviewer(csvContents.get(REVIEWER));
		exportData.setReviewerReleasedOn(csvContents.get(REVIEWER_RELEASED_ON));
		exportData.setCountryName(csvContents.get(COUNTRY));
		exportData.setDistributor(csvContents.get(DISTRIBUTOR));
		exportData.setCustomerFacingBP(csvContents.get(CUSTOMER_FACING_BP));
		exportData.setCustomerName(csvContents.get(CUSTOMER_NAME));
		exportData.setConfidentiality(csvContents.get(CONFIDENTIALITY));
		exportData.setRouteToMarket(csvContents.get(ROUTE_TO_MARKET));
		exportData.setOffshorePaymentTerms(csvContents.get(OFFSHORE_PAYMT_TERMS));
		exportData.setBundledSolutions(csvContents.get(BUNDLED_SOLUTIONS));
		exportData.setContingencyFee(csvContents.get(CONTINGENCY_FEE));
		exportData.setSoleSourceProc(csvContents.get(SOLE_SOURCE_PROC));
		exportData.setFocusBP(csvContents.get(FOCUS_BP));
		exportData.setBpMargPerc(csvContents.get(BP_MARG_PERC));
		exportData.setDiscountPerc(csvContents.get(DISCOUNT_PERC));
		exportData.setBpMarginTotal(csvContents.get(BP_MARG_TOTAL));
		exportData.setDiscountTotal(csvContents.get(DISCOUNT_TOTAL));
		exportData.setActionOwner(csvContents.get(ACTION_OWNER));
		exportData.setBidExpired(csvContents.get(BID_EXPIRED));
		exportData.setExPreFulfillment(csvContents.get(EX_PRE_FULFILLMENT));
		exportData.setPostVerifyDate(csvContents.get(POST_VERIFY_DATE));
		exportData.setPostShipAuditFollowUpStatus(csvContents.get(POST_SHIP_AUDIT_STAT));
		exportData.setWaitingForExtension(csvContents.get(WAITING_FOR_EXTENSION));
		exportData.setProblemsWithChasing(csvContents.get(PROBLEMS_WITH_CHASING));
		exportData.setRequestSentTooEarly(csvContents.get(REQUEST_SENT_TOO_EARLY));
		exportData.setRealisticallyDue(csvContents.get(REALISTICALLY_DUE));
		exportData.setTestNoteStartedYet(csvContents.get(TEST_NOTE_STARTED_YET));
		exportData.setPotentialIssues(csvContents.get(POTENTIAL_ISSUES));
		exportData.setFollowUpLog(csvContents.get(FOLLOW_UP_LOG));
		exportData.setDeadlineOfRequest(csvContents.get(DEADLINE_OF_REQUEST));
		exportData.setBidEvent(csvContents.get(BID_EVENT));
		exportData.setOperationOwner(csvContents.get(OPERATION_OWNER));
		exportData.setBidMgr(csvContents.get(BID_MGR));
		exportData.setDistributorCeid(csvContents.get(DISTRIBUTOR_CEID));
		exportData.setCustomerFacingBPCeid(csvContents.get(CUSTOMER_FACING_BP_CEID));
		exportData.setT3Name(csvContents.get(T3_NAME));
		exportData.setT4Name(csvContents.get(T4_NAME));
		exportData.setT5Name(csvContents.get(T5_NAME));
		exportData.setBpmT1Fin(formatToPercent(csvContents.get(BPM_T1_FIN)));
		exportData.setBpmT1Serv(formatToPercent(csvContents.get(BPM_T1_SERV)));
		exportData.setBpmT1Ins(formatToPercent(csvContents.get(BPM_T1_INS)));
		exportData.setBpmT1Profit(formatToPercent(csvContents.get(BPM_T1_PROFIT)));
		exportData.setBpmT2Fin(formatToPercent(csvContents.get(BPM_T2_FIN)));
		exportData.setBpmT2Serv(formatToPercent(csvContents.get(BPM_T2_SERV)));
		exportData.setBpmT2Ins(formatToPercent(csvContents.get(BPM_T2_INS)));
		exportData.setBpmT2Profit(formatToPercent(csvContents.get(BPM_T2_PROFIT)));
		exportData.setBpmOther(formatToPercent(csvContents.get(BPM_OTHER)));
		exportData.setLog(csvContents.get(LOG));
		exportData.setChannelProgram(csvContents.get(CHANNEL_PROGRAM));
		exportData.setActionOwner(csvContents.get(ACTION_OWNER));
		exportData.setClosed(csvContents.get(CLOSED));
		exportData.setClosedDate(csvContents.get(CLOSE_DATE));
		exportData.setOutcome(csvContents.get(OUTCOME));
		exportData.setOutcomeComment(csvContents.get(OUTCOME_COMMENT));
		exportData.setCondition(csvContents.get(CONDITION));
		exportData.setTransparencyLetter(csvContents.get(TRANSPARENCY_LETTER));
		exportData.setCycle(csvContents.get(CYCLE));
		exportData.setExceptionFlag(csvContents.get(EXCEPTION_FLAG));
		exportData.setFulfilled(csvContents.get(FULLFILLED));
		exportData.setComment(csvContents.get(COMMENT));

		return exportData;

	}

	private String formatToPercent(String number) {
		try {  
			if(number == null || number.equalsIgnoreCase("null") || number.trim().isEmpty()) {
				number = "0 ";
			}
			
			double amount = Double.parseDouble(number);
			DecimalFormat formatter = new DecimalFormat("#0.0");


			return formatter.format(amount) + "%";

		} catch (Exception e) {
			return "";
		}
	} 

	private List<String> cleanupCSVData(List<String> origCsvData) {
		List<String> cleanCsvData = new ArrayList<>();

		for (String data : origCsvData) {
			cleanCsvData.add(cleanupDetailData(data));
		}

		return cleanCsvData;
	}

	private String cleanupDetailData(String data) {
		String cleanData = "";
		cleanData = data.replaceAll("\"=\"", "").replaceAll("\"", "").trim();
		
		cleanData.trim();
		
		return cleanData;
	}

	public List<String> getExportedDataDifference(ExportDetailData exportedCsv, ExportDetailData bretWebData) {
		List<String> difference = new ArrayList<>();

		for (Field field : ExportDetailData.class.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if ((null != field.get(exportedCsv) && null != field.get(bretWebData))
						&& !field.get(exportedCsv).equals(field.get(bretWebData))) {

					difference.add(field.getName() + ": " + "[CSV Value: " + field.get(exportedCsv) + " : "
							+ "Bret web Value: " + field.get(bretWebData) + "]");
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return difference;
	}

}
