package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/** 
* [added-Belle Pondiong]<br>
* This page class contained all the methods on what to perform on BRET Non Focus page. <br>
*/
public class BRETNonFocusPage {
	
	private static final String NULL = null;

	//Declaring variables
	public WebDriver driver;
	
	@FindBy (id = "dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_1") // Non Focus Tab
	private WebElement nonFocusTab;
		
	// This is a web page class constructor		 
	public BRETNonFocusPage (WebDriver driver) {
			 this.driver= driver;
			 PageFactory.initElements(driver, this);
	}
	
//------------------ Rolled Back bid Check Routines  --------------------------------------- 
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that check details of the rolled back bid in Non focus tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify correct Bid Date (not NULL), Flag (not NULL). <br>
	 */
	public void rolledbackBidChck (String bidNmbr) throws Exception{

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null;
			
		
		nonFocusTab.click(); 							// loads Non Focus Tab first
		grdCntrfound = bidFindonNonFocusTab (bidNmbr);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get the details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		
		//Printing located bid and details on the logs
		// manual tracing
		System.out.println("\n=====Bid located in Non Focus Tab=====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:  " +bidDate);
				
		// Routine to verify that Flag and Bid Date are not NULL
		if ((bidFlag.equals("N")) & (!(bidDate.equals(NULL)))) {  /// NEED to change validation here once issue on Bid date Rollback is FIX! RTC 1047098
			
			System.out.println("\n Bid Found in Non Focus Tab with correct details.");
		}
		else
			Assert.fail("Failed. Bid found in Non Focus Tab but with wrong/incomplete details."); // Will fail the test case if IF-condition is FALSE
	
	}
	

//------------------ Rolled Back bid Check Routines - ends here ----------------------------- 
	
	
//------------------ Call Routines  --------------------------------------- 
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will look for the bid in Non Focus Tab Grid. <br>
	 * This will return the location of the bid in the grid.<br>
	 */ 
	protected int bidFindonNonFocusTab (String bidNumber) throws Exception {
		
		int bidlocatecntr = 0,
		    grdCntr = 0, 
		    pgLink = 1; 
		String bidcheck = null;
		WebElement element;
					
		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_4']/span[3]")).click();  //Clicks '30' records per page
		
		// Routine to find the bids on Non Focus Tab
       do {
       	
			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_3']/span[2]/span["+pgLink+"]")).click();  // click on the page link
			     //System.out.println("Tracing...page number " +pgLink);
			
			 grdCntr = 0;  // set to zero, this will reset the row # on the grid should the code enters the next page
				 //System.out.println("Tracing...grid number init" +grdCntr);
			
			//Routine to check the bid per page 			
			for (int pgDataCntr = 0; pgDataCntr < 30; pgDataCntr ++) {	
				   
			    grdCntr = grdCntr +1;
			    
			    // scroll until element is in focus since grid is dynamic with scroll bars
				element = driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500); 
				
				bidcheck = driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]")).getText();
					//System.out.println("Tracing...grid number inside FOR LOOP  " +grdCntr);
					//System.out.println("Tracing...bid number " +bidcheck);
													
					if (bidNumber.equals(bidcheck)) { // break if bid is found
						break;
					}					
			}
			
			pgLink ++;  // go to next page
						
		} while (!(bidNumber.equals(bidcheck)));  //checking the specified bid on the grid
		
       bidlocatecntr = grdCntr;
       
		return bidlocatecntr;			
	}

	/**
	 * Added by Lhoyd Castillo - 1/31/2018
	 * this is to get the bid number on the search result
	 * 
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	
	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}
	
	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}
}
