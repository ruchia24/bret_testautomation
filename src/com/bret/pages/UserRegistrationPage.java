package com.bret.pages;

//import java.util.NoSuchElementException;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.PageFactory;

public class UserRegistrationPage extends UserManagementPage {

	
	private static final String XPATH_REGISTER_BTN = "//*[@id=\"btnRegister\"]";
	private static final String XPATH_ADD_USER_BTN = "//*[@id=\"dijit_form_Button_0\"]";
	private static final String XPATH_RETURN_TO_USER_MANAGEMENT = "/html/body/div[1]/div[3]/div/div[3]/div/div[1]/a";
	
	
	public boolean registerButtonIsDisplayed(WebDriver driver) {
		WebElement registerButton = driver.findElement(By.xpath(XPATH_REGISTER_BTN));
		boolean registerButtonIsDisplayed = registerButton.isDisplayed();
		return registerButtonIsDisplayed;
	}
	
	public boolean addUserButtonIsDisplayed(WebDriver driver) {
		WebElement addUserButton = driver.findElement(By.xpath(XPATH_ADD_USER_BTN));
		boolean addUserButtonIsDisplayed = addUserButton.isDisplayed();
		return addUserButtonIsDisplayed;
	}
	
	public void clickUserManagement(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_RETURN_TO_USER_MANAGEMENT)).click();
	}
	
	
}
