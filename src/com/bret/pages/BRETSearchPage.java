package com.bret.pages;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/**
 * [added-Belle Pondiong]<br>
 * This page class contained all the methods on what to perform on BRET Search
 * page. <br>
 */
public class BRETSearchPage {

	public WebDriver driver;

	// Declaring the Search page web elements
	@FindBy(id = "dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_4") // Search Tab
	private WebElement searchTab;

	@FindBy(id = "bidSearchInputBox") // Search box
	private WebElement searchBox;

	@FindBy(id = "bidSearchBtn") // Search Button
	private WebElement searchBtn;

	@FindBy(id = "idx_layout_ContentPane_2")
	private WebElement focusTab;

	@FindBy(id = "idx_layout_ContentPane_1")
	private WebElement nonfocusTab;

	@FindBy(id = "idx_layout_ContentPane_3")
	private WebElement archiveTab;

	private boolean acceptNextAlert = true;

	// This is a web page class constructor
	public BRETSearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * [added-Belle Pondiong]<br>
	 * Routine that will search Focus/Non Focus and bid on Archive bid.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 */
	public void bidSearchAnyRole(String Bidnum, String Flag) {

		String chckFlag = null, checkBid = null, searchType = null, searchRes = null;

		searchTab.click(); // clicks the Search Tab
		searchBox.clear();
		searchBox.click();
		searchBox.sendKeys(Bidnum); // enter bid Num
		searchBtn.click(); // click the search button

		// Click on the link
		driver.findElement(By.cssSelector("span.pageLink")).click();

		// Routine to check which TAB to check
		if (Flag.equals("Y")) {

			// chckFlag =
			// driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]")).getText();
			// checkBid =
			// driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]")).getText();
			// searchType = "Focus Bid Search";

			// if ((chckFlag.equals(Flag)) & (checkBid.equals(Bidnum))) {

			// searchRes = "Focus search passed"; //checking id bid/flag found on web equals
			// to the given data
			// }
			// else
			// Assert.fail("This bid is not a Focus bid. Please check you data."); // Will
			// fail the test case if IF-condition is FALSE
			// searchRes = "This bid is not a Focus bid. Please check you data.";

			// Verifies that if Y, Focus tab should be displayed
			if (focusTab.isDisplayed()) { // (driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_2']")).isDisplayed()){

				chckFlag = driver
						.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]"))
						.getText();
				checkBid = driver
						.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]"))
						.getText();
				searchType = "Focus Bid Search";
				searchRes = "Focus search passed"; // checking id bid/flag found on web equals to the given data
			} else
				Assert.fail("FB search FAILED: Bid not found on Focus Tab. Please check your data."); // Will fail the
																										// test case if
																										// IF-condition
																										// is FALSE

		} else if (Flag.equals("N")) {

			// chckFlag =
			// driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]")).getText();
			// checkBid =
			// driver.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]")).getText();
			// searchType = "Non Focus Bid Search";

			// if ((chckFlag.equals(Flag)) & (checkBid.equals(Bidnum))) {

			// searchRes = "Non Focus search passed"; //checking id bid/flag found on web
			// equals to the given data
			// }
			// else
			// Assert.fail("This bid is not a Non Focus bid. Please check you data."); //
			// Will fail the test case if IF-condition is FALSE
			// searchRes = "This bid is not a Non Focus bid. Please check you data.";

			// Verifies that if N, Focus tab should be displayed
			if (nonfocusTab.isDisplayed()) { // (driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_1']")).isDisplayed()){

				chckFlag = driver
						.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]"))
						.getText();
				checkBid = driver
						.findElement(By.xpath(".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]"))
						.getText();
				searchType = "Non Focus Bid Search";
				searchRes = "Non Focus search passed"; // checking id bid/flag found on web equals to the given data
			} else
				Assert.fail("NFB search FAILED: Bid not found on Non Focus Tab. Please check your data."); // Will fail
																											// the test
																											// case if
																											// IF-condition
																											// is FALSE

		} else if (Flag.equals("A")) {

			// chckFlag =
			// driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]")).getText();
			// checkBid =
			// driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]")).getText();
			// searchType = "Archived Bid Search";

			// if (checkBid.equals(Bidnum)) {

			// searchRes = "Bid found in Archived Tab."; //checking id bid/flag found on web
			// equals to the given data
			// }
			// else
			// Assert.fail("Bid not found in Archived Tab. Please check you data."); // Will
			// fail the test case if IF-condition is FALSE
			// searchRes = "Bid not found in Archived Tab. Please check you data.";

			// Verifies that if A, Focus tab should be displayed
			if (archiveTab.isDisplayed()) { // (driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_3']")).isDisplayed()){

				chckFlag = driver
						.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]"))
						.getText();
				checkBid = driver
						.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]"))
						.getText();
				searchType = "Archived Bid Search";
				searchRes = "Bid found in Archived Tab."; // checking id bid/flag found on web equals to the given data
			} else
				Assert.fail("ArchBid search FAILED: Bid not found on Archive Tab. Please check your data."); // Will
																												// fail
																												// the
																												// test
																												// case
																												// if
																												// IF-condition
																												// is
																												// FALSE

		} else
			searchRes = "Invalid Flag entered.";

		// Manula tracing
		// Printing out details on the found bid
		System.out.println("\n=====" + searchType + "=====");
		System.out.println("The Bid # and Flag found on Web aftrer Search");
		System.out.println("     Bid Num:" + checkBid);
		System.out.println("     Flag:" + chckFlag);
		System.out.println(searchRes);

	}

	/**
	 * [added-Belle Pondiong]<br>
	 * Routine that will search Error and Waiting for Pricing.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 */
	public void bidSearchAdministrator(String Bidnum, String Flag) {

		String chckFlag = null, checkBid = null, searchType = null, searchRes = null, bidStat = null;

		searchTab.click(); // clicks the Search Tab
		searchBox.clear();
		searchBox.click();
		searchBox.sendKeys(Bidnum); // enter bid Num
		searchBtn.click(); // click the search button

		// Click on the link
		driver.findElement(By.cssSelector("span.pageLink")).click();

		// Verifies that My Task tab is displayed
		if (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) {

			Assert.fail("My Task search FAILED: Bid not found on My Task Tab. Please check your data."); // Will fail
																											// the test
																											// case if
																											// IF-condition
																											// is FALSE
		}

		if (Flag.equals("E")) {

			chckFlag = driver
					.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]"))
					.getText();
			checkBid = driver
					.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]"))
					.getText();
			searchType = "Error Bid Search";

			if ((chckFlag.equals(Flag)) & (checkBid.equals(Bidnum))) {

				searchRes = "Error Bid search passed"; // checking id bid/flag found on web equals to the given data
			} else
				Assert.fail("This bid is not an  Error bid. Please check your data."); // Will fail the test case if
																						// IF-condition is FALSE
			// searchRes = "This bid is not an Error bid. Please check you data.";

		}

		else if (Flag.equals("W")) {

			chckFlag = driver
					.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[1]"))
					.getText();
			checkBid = driver
					.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[2]"))
					.getText();
			bidStat = driver
					.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[6]"))
					.getText();
			searchType = "SQO Waiting for Pricing Bid Search";

			if ((bidStat.equals("Waiting for Pricing")) & (checkBid.equals(Bidnum))) {

				searchRes = "SQo Waiting for Pricing Bid search passed"; // checking id bid/status found on web
			} else
				Assert.fail("This bid is not a SQO Waiting for Pricing bid. Please check your data."); // Will fail the
																										// test case if
																										// IF-condition
																										// is FALSE
			// searchRes = "This bid is not a SQO Waiting for Pricing bid. Please check you
			// data.";
		} else
			searchRes = "Invalid Flag entered.";

		// logs verificaiton

		System.out.println("\n=====" + searchType + "=====");
		System.out.println("The Bid # and Flag found on Web aftrer Search");
		System.out.println("     Bid Num:" + checkBid);
		System.out.println("     Flag:" + chckFlag);

		if (Flag.equals("W")) {
			System.out.println("     Bid Status:" + bidStat);
		}

		System.out.println(searchRes);

	}

	/**
	 * [added-Belle Pondiong]<br>
	 * Routine that will perform invalid searchs.<br>
	 * This will return void.<br>
	 */
	public void bidInvalidSearch(String Bidnum) {
		// ------------------------------------------
		// Routine that will search Invalidf bid
		// ------------------------------------------
		String InvSearcTxt = "Cannot find the bid in the system or there may be a system error. Please verify your input or consult your supervisor.",
				chckTxt = null, searchType = "Search not found.";

		searchTab.click(); // clicks the Search Tab
		searchBox.clear();
		searchBox.click();
		searchBox.sendKeys(Bidnum); // enter bid Num
		searchBtn.click(); // click the search button

		chckTxt = driver.findElement(By.cssSelector("#locateBidDiv > div")).getText();

		if (chckTxt.equals(InvSearcTxt)) {
			// logs verificaiton, checking the message display
			System.out.println("\n=====" + searchType + "=====");
			System.out.println("The Bid # " + Bidnum + " is not found on BRET web.");
			System.out.println("Message rendered on BRET web: ");
			System.out.println("   " + chckTxt);
		} else
			Assert.fail("Failed. Bid entered was found on BRET web."); // Will fail the test case if IF-condition is
																		// FALSE

	}

	/**
	 * [added-Belle Pondiong]<br>
	 * Routine that will verify error promot on no bid search.<br>
	 * This will return void.<br>
	 */
	public void noBidID() throws IOException {
		// ----------------------------------------------------------
		// Routine that will verify error promot on no bid search
		// ----------------------------------------------------------
		String searchType = "No Bid Entered";

		searchTab.click(); // clicks the Search Tab
		searchBox.clear();
		searchBox.click();
		searchBtn.click(); // click the search button

		// check for the pop up and close it
		Assert.assertEquals(closeAlertAndGetItsText(), "Plesae input a Bid ID.");

		System.out.println("\n=====" + searchType + "=====");
		System.out.println("Plesae input a Bid ID. pop up window was found and closed.");

	}

	/**
	 * [added-Belle Pondiong]<br>
	 * Routine that will handle pop ups alerts.<br>
	 * This will return void.<br>
	 */
	private String closeAlertAndGetItsText() {

		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	/**
	 * 
	 * Author: Lhoyd 1/31/2018
	 * 
	 * 
	 * @param bidId
	 */

	private static final String XPATH_SEARCH_INPUT = ".//*[@id='bidSearchInputBox']";
	private static final String XPATH_SEARCH_BUTTON = ".//*[@id='bidSearchBtn']";
	// private static final String XPATH_CLEAR_INPUT =
	// ".//*[@id='bidSearchInputBox']";
	private static final String XPATH_CLICK_HERE_BUTTON = ".//*[@id='locateBidDiv']/div/span";
	// ".//*[@id='locateBidDiv']";
	private static final String XPATH_RESULT_FIELD = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[5]/div/div[3]/div";
	
	public void enterBid(String bidId) {
		WebElement enterBid = driver.findElement(By.xpath(XPATH_SEARCH_INPUT));
		enterBid.sendKeys(bidId);
	}

	public void clickSearch() {
		WebElement searchButton = driver.findElement(By.xpath(XPATH_SEARCH_BUTTON));
		searchButton.click();
	}

	public void clearSearch() {
		WebElement searchField = driver.findElement(By.xpath(XPATH_SEARCH_INPUT));
		searchField.clear();
	}

	public void clickHere() {
		WebElement clickHere = driver.findElement(By.xpath(XPATH_CLICK_HERE_BUTTON));
		clickHere.click();
	}

	public String getResultFieldText(WebDriver driver) {
		String fieldText = "";
		WebElement resultField = driver.findElement(By.xpath(XPATH_RESULT_FIELD));
		fieldText = resultField.getText();
		return fieldText;
	}

}
