package com.bret.pages;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.bret.util.BretTestUtils;

public class DiscountMarginPage extends DetailPage {

	private static final String XPATH_PART_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[2]/div/div[";
	private static final String XPATH_PART_2 = "]/div/div[2]/div/div[3]/div[2]/div[";
	private static final String XPATH_PART_3 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_4 = "]";
	
	private static final String XPATH_FACTOR_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[2]/div/div[1]";
	private static final String XPATH_FACTOR_2 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[2]/div/div[2]";

	/* Index of Margin and Discount to be appended in Xpath */
	private static final int TABLE_MARGIN_INDEX = 1;
	private static final int TABLE_DISCOUNT_INDEX = 2;

	/*
	 * Index of Columns from BRET Web -- for Discount and Margin to be appended
	 * in Xpath
	 */
	private static final int COL_LINE_ITEM = 1;
	private static final int COL_LINE_ITEM_NUM = 2;
	private static final int COL_SCORE = 3;
	private static final int COL_CUR_VAL = 4;
	private static final int COL_VAL_AT_THREASHOLD = 5;
	private static final int COL_HIST_MEDIAN = 6;
	private static final int COL_OUTLIER = 7;

	/*
	 * Common Columns between Margin and Discount -- to be compared with BRET
	 * Web actual results
	 */
	private static final int CSV_LINE_ITEM = 0;
	private static final int CSV_LINE_ITEM_NUM = 1;

	/*
	 * Margin Columns CSV test data index -- to be compared with BRET Web actual
	 * results
	 */
	private static final int CSV_MARG_SCORE = 2;
	private static final int CSV_MARG_CUR_VAL = 3;
	private static final int CSV_MARG_VAL_AT_THREASHOLD = 4;
	private static final int CSV_MARG_HIST_MEDIAN = 5;
	private static final int CSV_MARG_OUTLIER = 6;

	/*
	 * Discount Columns CSV test data index -- to be compared with BRET Web
	 * actual results
	 */
	private static final int CSV_DISC_SCORE = 7;
	private static final int CSV_DISC_CUR_VAL = 8;
	private static final int CSV_DISC_VAL_AT_THREASHOLD = 9;
	private static final int CSV_DISC_HIST_MEDIAN = 10;
	private static final int CSV_DISC_OUTLIER = 11;

	private static final String CSV_SEPARATOR = ",";
	private static final int EXPECTED_CSV_COLUMNS = 12;
	private static final int LINEITEM_CUTOFF = 6;
	private String imgFilename;
	private String screenshotPath;
	private int imgCount = 0;

	WebDriver driver;

	public DiscountMarginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public boolean checkIsDiscountMarginValid(List<String> testDataDiscMarg, String screenshotPath, String imgFilename) {
		boolean isValid = true;
		int rowNum = testDataDiscMarg.size();
		boolean factor1MargVis = checkFactor1Visibility();
		boolean factor2DiscVis = checkFactor2Visibility();
		this.screenshotPath = screenshotPath;
		this.imgFilename = imgFilename;

		printTableVisibility(factor1MargVis, factor2DiscVis);
		
		if (null != testDataDiscMarg && !testDataDiscMarg.isEmpty()) {
			for (int i = 0; i < rowNum; i++) {
				String[] csvRowData = (testDataDiscMarg.get(i).replaceAll("\"", "")).split(CSV_SEPARATOR);

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (null != csvRowData && csvRowData.length == EXPECTED_CSV_COLUMNS) {
					
					if(factor1MargVis && factor2DiscVis) {
						isValid = (checkMarginRowMatch(csvRowData, (i + 1)) && checkDiscountRowMatch(
								csvRowData, (i + 1)));						
					} else if (!factor1MargVis && factor2DiscVis) {
						isValid = checkDiscountRowMatch(csvRowData, (i + 1));
					} else if (factor1MargVis && !factor2DiscVis) {
						isValid = checkMarginRowMatch(csvRowData, (i + 1));
					}


					if (!isValid) {
						break;
					}
				} else {
					System.out
							.println("Invalid CSV test data -- CSV Columns are invalid expected is: "
									+ EXPECTED_CSV_COLUMNS
									+ " but Found: "
									+ csvRowData.length);
				}

			}
		} else {
			System.out
					.println("CSV file is either null or empty");
		}

		return isValid;
	}
	
	/**
	 * Checks if Factor #1 table is visible.
	 * 
	 * @return
	 */
	private boolean checkFactor1Visibility() {
		boolean visible = true;

		try {
			String text = driver.findElement(By.xpath(XPATH_FACTOR_1)).getText();
			
			if(null == text || text.isEmpty()) {
				visible = false;
			}
			
		} catch (NoSuchElementException | NullPointerException e) {
			visible = false;
		}

		return visible;
	}

	/**
	 * Checks if Factor #2 table is visible.
	 * 
	 * @return
	 */
	private boolean checkFactor2Visibility() {
		boolean visible = true;

		try {
			String text =  driver.findElement(By.xpath(XPATH_FACTOR_2)).getText();
			
			if(null == text || text.isEmpty()) {
				visible = false;
			}
			
		} catch (NoSuchElementException | NullPointerException e) {
			visible = false;
		}

		return visible;
	}

	private boolean checkMarginRowMatch(String[] csvRows, int rowIndex) {
		boolean lineItem = checkEquality(csvRows[CSV_LINE_ITEM], getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex, COL_LINE_ITEM));
		boolean lineItemNum = checkEquality(csvRows[CSV_LINE_ITEM_NUM], getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex, COL_LINE_ITEM_NUM));
		boolean score = checkEquality(csvRows[CSV_MARG_SCORE], getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex, COL_SCORE));
		boolean currentValue = checkEquality(formatToPercentage(csvRows[CSV_MARG_CUR_VAL]), getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex, COL_CUR_VAL));
		boolean valueAtThreashold = checkEquality(formatToPercentage(csvRows[CSV_MARG_VAL_AT_THREASHOLD]), getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex,COL_VAL_AT_THREASHOLD));
		boolean histMedian = checkEquality(formatToPercentage(csvRows[CSV_MARG_HIST_MEDIAN]), getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex, COL_HIST_MEDIAN));
		boolean outlier = checkEquality(csvRows[CSV_MARG_OUTLIER], getValueAtIndex(TABLE_MARGIN_INDEX, rowIndex, COL_OUTLIER));

		return lineItem && lineItemNum && score && currentValue
				&& valueAtThreashold && histMedian && outlier;
	}
	
	private boolean checkDiscountRowMatch(String[] csvRows, int rowIndex) {
		boolean lineItem = checkEquality(csvRows[CSV_LINE_ITEM], getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex, COL_LINE_ITEM));
		boolean lineItemNum = checkEquality(csvRows[CSV_LINE_ITEM_NUM], getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex, COL_LINE_ITEM_NUM));
		boolean score = checkEquality(csvRows[CSV_DISC_SCORE], getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex, COL_SCORE));
		boolean currentValue = checkEquality(formatToPercentage(csvRows[CSV_DISC_CUR_VAL]), getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex, COL_CUR_VAL));
		boolean valueAtThreashold = checkEquality(formatToPercentage(csvRows[CSV_DISC_VAL_AT_THREASHOLD]), getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex,COL_VAL_AT_THREASHOLD));
		boolean histMedian = checkEquality(formatToPercentage(csvRows[CSV_DISC_HIST_MEDIAN]), getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex, COL_HIST_MEDIAN));
		boolean outlier = checkEquality(csvRows[CSV_DISC_OUTLIER], getValueAtIndex(TABLE_DISCOUNT_INDEX, rowIndex, COL_OUTLIER));

		return lineItem && lineItemNum && score && currentValue
				&& valueAtThreashold && histMedian && outlier;
	}

	private boolean checkEquality(String csvVal, String bretWebVal) {
		
		// Handle NULL to 0 or N/A
		if ((bretWebVal.equalsIgnoreCase("0") || bretWebVal
				.equalsIgnoreCase("N/A"))
				&& (csvVal.equalsIgnoreCase("NULL")
						|| csvVal.equalsIgnoreCase("") || null == csvVal)) {
			return true;
		}
		
		boolean isEqual = bretWebVal.equalsIgnoreCase(csvVal);
		
		if(!isEqual) {
			System.out.println("The values are not equal");
			System.out.println("CSV Value: " + csvVal);
			System.out.println("BRET Web Value: " + bretWebVal);
		}

		return isEqual;

	}

	private String getValueAtIndex(int tableIndex, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructXpath(tableIndex,
				rowIndex, colIndex)));

		if(rowIndex % LINEITEM_CUTOFF == 0 && colIndex == COL_LINE_ITEM_NUM) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", col);
			BretTestUtils
					.screenCapture(
							driver,
							screenshotPath,
							imgFilename + "_" + ++imgCount
									+ BretTestUtils.getImgFormat());
		}
		
		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;
	}
	
	/**
	 * Format to percentage
	 * 
	 * @param num
	 * @return
	 */
	private String formatToPercentage(String num) {
		double n1 = 0;

		try {
			n1 = Double.parseDouble(num);
			NumberFormat formatter = new DecimalFormat("#0.0");
			return (formatter.format(n1)) + "%";

		} catch (NumberFormatException e) {
			return num;
		}
	}

	private String constructXpath(int tableIndex, int rowIndex, int colIndex) {
		// xpath parts
		return XPATH_PART_1 + tableIndex + XPATH_PART_2 + rowIndex
				+ XPATH_PART_3 + colIndex + XPATH_PART_4;
	}
	
	private void printTableVisibility(boolean factor1MargVis, boolean factor2DiscVis) {
		System.out.println("Factor #1 - Margin table is visible: " + factor1MargVis);
		System.out.println("Factor #2 - Discount table is visible: " + factor2DiscVis);		
	}

}
