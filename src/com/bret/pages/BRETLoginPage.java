package com.bret.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import decoder.bret.ibm.com.Decoder;   

/** 
* [added-Belle Pondiong]<br>
* This page class contained all the methods on what to perform on BRET login page. <br>
*/
public class BRETLoginPage {
	 
	public WebDriver driver;
	
	 //Declaring the Login page web elements
	 @FindBy(name = "username")
	 private WebElement username;

	 @FindBy(name = "password")
	 private WebElement password;
	 
	 @FindBy (id = "dijit_form_Button_0")  //dijit_form_Button_0  dijit_form_Button_0_label
	 private WebElement loginBtn;
	 
	 
	// This is a web page class constructor		 
		 public BRETLoginPage (WebDriver driver) {
			 this.driver= driver;			 
			 PageFactory.initElements(driver, this);
		 }

	 /** 
	 * [added-Belle Pondiong]<br>
	 * This method performs BRET Login routine. <br>
	 * This will return void. <br>
	 * 
	 * <br>[Updated-Name-Date] <br>
	 * Details on what was updated in the code
	 */
	 public void userLogin(String Username, String Password) {
		 String Password2;
		 
//		 System.out.println("trace: passed username from spreadsheet: " + Username);  
//		 System.out.println("trace: passed password from spreadsheet: " + Password); 
		 
		 Password2 = Decoder.decodeString(Password.trim()); //decode
//		 System.out.println("Decoded:"+ Password2);
		 // Login entry
		 username.clear();
		 username.sendKeys(Username);
		 password.clear();
		 password.sendKeys(Password2);
		 	
		 //Login button clicked
		 loginBtn.click();
	 }
} 
