package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.bret.util.BretTestUtils;

public class FlaggingBidDataPage extends DetailPage {
	private static String XPATH_PART_1_ROW = ".//html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[1]/div/div/div[5]/div[5]/div[3]/div[2]/div[";
	private static String XPATH_PART_2_ROW = "]";
	private static String XPATH_PART_1_COL = "/table/tbody/tr/td[";
	private static String XPATH_PART_2_COL = "]";

	private static String XPATH_PART_1_BDG = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[1]/div/div/div[3]/div/div/table/tbody/tr[";
	private static String XPATH_PART_2_BDG = "]/td";
	
	private static String XPATH_FACTORS = ".//*[@id='idx_grid_PropertyFormatter_14']";
	private static String XPATH_ROLLBACK_BTN = "//*[@id=\"rollbackReleasedBtn\"]";
	
	private static final String XPATH_TAKE_BID_BTN = "//span[contains(text(), 'Take this bid')]";
	private static final String XPATH_EXPORT_CSV_BTN = "//*[@id=\"exportToExcelBtn\"]";
	private static final String XPATH_PRINT_VERSION_BTN = "/html/body/div[1]/div[3]/div/div[4]/div[1]/div[1]/span[2]";
	private static final String XPATH_PRINT_VERSION_CLOSE_BTN = "/html/body/div[1]/div[1]/button[2]";

	public static int ROW_BID_MARGIN_PERC = 1;
	public static int ROW_BID_MARGIN_VALUE = 2;
	public static int ROW_BID_DISC_PERC = 3;
	public static int ROW_BID_DISC_VALUE = 4;
	public static int ROW_QUEST_BPCOPS = 5;
	public static int ROW_SUPPORTING_FACTORS = 6;
	public static int ROW_TOTAL_BID_LEVEL_SCORE = 7;

	public static int COL_SCORE = 2;
	public static int COL_OUTLIER_STAT = 3;
	public static int COL_CURRENT_VALUE = 4;
	public static int COL_VAL_THRESHOLD = 5;
	public static int COL_VAL_MEDIAN = 6;

	// bidDataGrid
	public static int ROW_SOURCE_SYSTEM = 1;
	public static int ROW_BID_DATE = 2;
	public static int ROW_COUNTRY_NAME = 3;
	public static int ROW_REGION = 4;
	public static int ROW_PRIMARY_BRAND = 5;
	public static int ROW_DISTRIBUTOR = 6;
	public static int ROW_DISTRIBUTOR_CEID = 7;
	public static int ROW_CUSTOMER_FACING_BP = 8;
	public static int ROW_CUSTOMER_FACTING_BP_CEID = 9;
	public static int ROW_CUSTOMER_NAME = 10;
	public static int ROW_CUSTOMER_CMR = 11;
	public static int ROW_ADDITIONAL_ID = 12;
	public static int ROW_ADDITIONAL_BP_CEID = 13;
	public static int ROW_OPPTY_NUMBER = 14;
	public static int ROW_GP = 15;

	//new added
	public static String FOCUS_BP_TIER_1 = "//ul[contains(.,'FOCUS BP:  Tier1 Investigation Findings;')]";
		
	WebDriver driver;

	/**
	 * Constructor
	 * 
	 * @param driver
	 */
	public FlaggingBidDataPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	/**
	 * Gets the value at index given the row index and the column index
	 * 
	 * @param driver
	 * @param rowIndex
	 * @param colIndex
	 * @return the text of the Web element
	 */
	public String getValueAtIndex(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructXpath(rowIndex,
				colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	/**
	 * Gets the value of the Bid Data grid based on the row id that was passed
	 * in the parameter.
	 * 
	 * @param driver
	 * @param rowIndex
	 * @return
	 */
	public String getValueAtIndexBidDataGrid(WebDriver driver, int rowIndex) {
		WebElement col = driver.findElement(By
				.xpath(constructXpathBidDataGrid(rowIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	/**
	 * Constructs the xpath
	 * 
	 * @param rowIndex
	 * @param colIndex
	 * @return
	 */
	private String constructXpath(int rowIndex, int colIndex) {
		return XPATH_PART_1_ROW + rowIndex + XPATH_PART_2_ROW
				+ XPATH_PART_1_COL + colIndex + XPATH_PART_2_COL;
	}

	/**
	 * Constructs the xpath for the Bid Data grid
	 * 
	 * @param rowIndex
	 * @return
	 */
	private String constructXpathBidDataGrid(int rowIndex) {
		return XPATH_PART_1_BDG + rowIndex + XPATH_PART_2_BDG;
	}

	/**
	 * Returns the total bid level Score.
	 * 
	 * @return
	 */
	public int getTotalBidLevelScore() {
		int total = 0;
		for (int row = ROW_BID_MARGIN_PERC; row < ROW_TOTAL_BID_LEVEL_SCORE; row++) {
			String scoreVal = getValueAtIndex(this.driver, row, COL_SCORE);
			total += Integer.parseInt(scoreVal);
		}

		return total;
	}
	
	public String getFactorsValue() {
		String factors = driver.findElement(
				By.xpath(XPATH_FACTORS))
				.getText();
		
		return factors;
		
	}
	
	public boolean checkRollbackButtonVisibility(WebDriver driver) {
		boolean isVisible = false;
		
		try {
			driver.findElement(By.xpath(XPATH_ROLLBACK_BTN));
			isVisible = true;
		} catch (NoSuchElementException e) {
			isVisible = false;
		}
		
		return isVisible;
	}
	
	/**
	 * Method for clicking the 'Rollback Button'. 
	 * @param driver
	 */
	public void clickRollbackButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_ROLLBACK_BTN)).click();
	}
	
	public void clickTakeThisBid() {
		BretTestUtils.printToConsole("Detail Page -- Clicking 'Take this bid' Button");
		driver.findElement(By.xpath(XPATH_TAKE_BID_BTN)).click();
	}
	
	public void clickExportToCSVButton() {
		BretTestUtils.printToConsole("Detail Page -- Clicking 'Export to CSV' Button");
		driver.findElement(By.xpath(XPATH_EXPORT_CSV_BTN)).click();
	}
	
	public void clickPrintVersionButton() {
		BretTestUtils.printToConsole("Detail Page -- Clicking 'Print version' Button");
		driver.findElement(By.xpath(XPATH_PRINT_VERSION_BTN)).click();
	}
	
	public boolean checkPrintVersionVisibility() {
		BretTestUtils.printToConsole("Detail Page -- Checking if the Print Version is visible");
		
		return checkElementVisibility(driver, XPATH_PRINT_VERSION_BTN);

	}
	
	public void closePrintVersion(WebDriver newDriver) {
		this.driver = newDriver;
		BretTestUtils.printToConsole("Print Detail Report -- Clicking 'Close me' Button ");
		driver.findElement(By.xpath(XPATH_PRINT_VERSION_CLOSE_BTN)).click();
	}

}
