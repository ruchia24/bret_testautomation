package com.bret.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import decoder.bret.ibm.com.Decoder;   

// author: Calvin

public class BRETDashboardLoginPage {
	 
	public WebDriver driver;
	
	 //Declaring the Login page web elements
	 @FindBy(id = "login-name")
	 private WebElement username;

	 @FindBy(id = "login-pass")
	 private WebElement password;
	 
	 @FindBy(xpath = "/html/body/div/div/form/div/input")
	 private WebElement loginBtn;
	 
	 
	// This is a web page class constructor		 
		 public BRETDashboardLoginPage (WebDriver driver) {
			 this.driver= driver;
			 //System.out.println("BRETLoginPage driver = " + this.driver.hashCode());
			 PageFactory.initElements(driver, this);
			
		 }


	 public void userLogin(String Username, String Password) {
		 String Password2;
		 
//		 System.out.println("trace: passed username from spreadsheet: " + Username);  
//		 System.out.println("trace: passed password from spreadsheet: " + Password); 
		 
		 Password2 = Decoder.decodeString(Password.trim()); //decode
//		 System.out.println("Decoded:"+ Password2);
		 // Login entry
		 username.clear();
		 username.sendKeys(Username);
		 password.clear();
		 password.sendKeys(Password2);
		 	
		 //Login button clicked
		 loginBtn.click();
	 }
} 
