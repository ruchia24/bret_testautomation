package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ManualReviewPage extends SummaryPage {
	private WebDriver driver;

	private static final String XPATH_DOWNLOAD_LATEST_FILE_BTN = "//*[@id='manualDownloadButton_label']";
	//private static final String XPATH_DOWNLOAD_LATEST_FILE_BTN = "//*[@id=\"manualDownloadButton_label\"]";

	private static final String XPATH_UPLOAD_A_NEW_FILE_BTN = "//*[@id='manualUploadButton_label']";
	//private static final String XPATH_UPLOAD_A_NEW_FILE_BTN = "//*[@id=\"manualUploadButton_label\"]";
	

	public ManualReviewPage() {
		// Do nothing
	}

	/**
	 * Constructor - for initializing the elements again. Used for switching back to
	 * Summary page from Details page.
	 * 
	 * @param driver
	 */
	public ManualReviewPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public boolean downloadTheLatestFileIsDisplayed(WebDriver driver) {
		WebElement downloadTheLatestFile = driver.findElement(By.xpath(XPATH_DOWNLOAD_LATEST_FILE_BTN));
		boolean downloadTheLatestFileIsDisplayed = downloadTheLatestFile.isDisplayed();
		return downloadTheLatestFileIsDisplayed;

	}

	public boolean uploadANewFileIsDisplayed(WebDriver driver) {
		WebElement uploadANewFileButton = driver.findElement(By.xpath(XPATH_UPLOAD_A_NEW_FILE_BTN));
		boolean uploadANewFileIsDisplayed = uploadANewFileButton.isDisplayed();
		return uploadANewFileIsDisplayed;

	}

}
