package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.bret.util.BretTestUtils;

public class RemediationLogPage extends DetailPage {

	private static final String XPATH_REM_PART_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[6]/div/div[2]/div/div[2]/div/div/div[";
	private static final String XPATH_REM_PART_2 = "]/table/tbody/tr[";
	private static final String XPATH_REM_PART_3 = "]/td";

	private static final String XPATH_EDIT_LINK = "//*[@id=\"remediationEditor\"]";

	private static final String XPATH_GEN_BID_INFO_PART_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[6]/div/div[1]/div[1]/div[2]/div/div/div/table/tbody/tr[";
	private static final String XPATH_GEN_BID_INFO_PART_2 = "]/td";

	private static final String XPATH_FOCUS_BID_ATTR_PART_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[6]/div/div[1]/div[3]/div[2]/div/div/div/table/tbody/tr[";
	private static final String XPATH_FOCUS_BID_ATTR_PART_2 = "]/td";

	private static final String XPATH_POST_SHIPMENT_PART_1 = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[6]/div/div[3]/div/div[2]/div/div/div/table/tbody/tr[";
	private static final String XPATH_POST_SHIPMENT_PART_2 = "]/td";

	private static final String XPATH_OUTCOME_DROPDOWN = "//*[@id='outcomeSelectBox']";
	private static final String XPATH_INPUT_OUTCOME_COMMENT = "//*[@id=\"decisionOutcomeCommentText\"]";

	private static final String XPATH_PRESHIP_VERIFICATION_DROPDOWN = "//*[@id=\"downStreamSelect\"]";

	private static final String XPATH_SAVE_BTN = "/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[6]/div/div[2]/div/div[1]/div/div/div/div/div[2]/span[1]";
	private static final String XPATH_CONFIRM_BTN = "//*[@id=\"dijit_form_Button_5\"]";

	public static final int SUB_TBL_CONTACTS = 2;
	public static final int SUB_TBL_RTM = 4;
	public static final int SUB_TBL_BPMARGDET = 6;
	public static final int SUB_TBL_REMEDIATION = 8;
	public static final int SUB_TBL_FOLLOWUP = 10;

	public static final int ROW_GEN_SOURCE_SYSTEM = 1;
	public static final int ROW_GEN_RECORD_ID = 2;
	public static final int ROW_GEN_BID_ID = 3;
	public static final int ROW_GEN_ADDL_ID = 4;
	public static final int ROW_GEN_BID_CPS = 5;
	public static final int ROW_GEN_BID_OPPTY = 6;
	public static final int ROW_GEN_BID_DATE = 7;
	public static final int ROW_GEN_BID_EXPIRY_DATE = 8;
	public static final int ROW_GEN_COUNTRY = 9;
	public static final int ROW_GEN_REGION = 10;
	public static final int ROW_GEN_BRAND = 11;
	public static final int ROW_GEN_CUSTOMER = 12;
	public static final int ROW_GEN_CUSTOMER_ID = 13;
	public static final int ROW_GEN_BRET_SCORE = 14;
	public static final int ROW_GEN_END_USER_PRICE = 15;
	public static final int ROW_GEN_BID_TCV = 16;
	public static final int ROW_GEN_DISCOUNT_PERC = 17;
	public static final int ROW_GEN_DISCOUNT_TOTAL = 18;
	public static final int ROW_GEN_MARGIN_PERC = 19;
	public static final int ROW_GEN_MARGIN_TOTAL = 20;
	public static final int ROW_GEN_SUPPORTING_FACTORS = 21;

	public static final int ROW_FBA_CONFIDENTIALITY = 1;
	public static final int ROW_FBA_ROUTE_TO_MARKET = 2;
	public static final int ROW_FBA_OFFSHORE_PAYMENT_TERMS = 3;
	public static final int ROW_FBA_BUNDLED_SOLUTIONS = 4;
	public static final int ROW_FBA_CONTINGENCY_FEE = 5;
	public static final int ROW_FBA_SOLE_SOURCE_PROC = 6;
	public static final int ROW_FBA_FOCUS_BP = 7;
	public static final int ROW_FBA_BP_MARGIN_PERC = 8;
	public static final int ROW_FBA_DISCOUNT_PERC = 9;
	public static final int ROW_FBA_BP_MARGIN_TOTAL = 10;
	public static final int ROW_FBA_DISCOUNT_TOTAL = 11;
	public static final int ROW_FBA_SUPPORTING_FACTORS = 12;

	public static final int ROW_CONTACTS_OO = 1;
	public static final int ROW_CONTACTS_BID_MGR = 2;
	public static final int ROW_CONTACTS_REMEDIATON_USER = 3;

	public static final int ROW_RTM_T1_NAME = 1;
	public static final int ROW_RTM_T1_ID = 2;
	public static final int ROW_RTM_T2_NAME = 3;
	public static final int ROW_RTM_T2_ID = 4;
	public static final int ROW_RTM_T3_NAME = 5;
	public static final int ROW_RTM_T3_ID = 6;
	public static final int ROW_RTM_T4_NAME = 7;
	public static final int ROW_RTM_T4_ID = 8;
	public static final int ROW_RTM_T5_NAME = 9;

	public static final int ROW_BPMARGDET_BPM_T1_FIN = 1;
	public static final int ROW_BPMARGDET_BPM_T1_SERV = 2;
	public static final int ROW_BPMARGDET_BPM_T1_INS_FREIGHT = 3;
	public static final int ROW_BPMARGDET_BPM_T1_PROFIT = 4;
	public static final int ROW_BPMARGDET_BPM_T2_FIN = 5;
	public static final int ROW_BPMARGDET_BPM_T2_SERV = 6;
	public static final int ROW_BPMARGDET_BPM_T2_INS_FREIGHT = 7;
	public static final int ROW_BPMARGDET_BPM_T2_PROFIT = 8;
	public static final int ROW_BPMARGDET_BPM_OTHER = 9;

	public static final int ROW_REM_LOG = 1;
	public static final int ROW_REM_CHANNEL_PROGRAM = 2;
	public static final int ROW_REM_CLOSED = 3;
	public static final int ROW_REM_ACTION_OWNER = 4;
	public static final int ROW_REM_CLOSE_DATE = 5;
	public static final int ROW_REM_OUTCOME = 6;
	public static final int ROW_REM_CONDITION = 7;
	public static final int ROW_REM_OUTCOME_COMMENT = 8;
	public static final int ROW_REM_TRANSPARENCY_LETTER = 9;
	public static final int ROW_REM_CYCLE = 10;
	public static final int ROW_REM_EXCEPTION_FLAG = 11;
	public static final int ROW_REM_PRE_SHIP_VERIFICATION_HOLD = 12;

	public static final int ROW_FOLLOWUP_FULFILLED = 1;
	public static final int ROW_FOLLOWUP_COMMENT = 2;

	public static final String REM_OUTCOME_ASSIGNED = "Assigned";
	public static final String REM_OUTCOME_RELEASE = "Release";
	public static final String REM_OUTCOME_REJECT = "Reject";
	public static final String REM_OUTCOME_RETURN = "Return";

	public static final int ROW_POSTSHIP_EX_PRE_FULFILMENT = 1;
	public static final int ROW_POSTSHIP_POST_VERIFY_DATE = 2;
	public static final int ROW_POSTSHIP_POST_SHIP_AUDIT_FOLLOW_UP_STATUS = 3;
	public static final int ROW_POSTSHIP_WAITING_FOR_EXTENSION = 4;
	public static final int ROW_POSTSHIP_PROBLEMS_WITH_CHASING = 5;
	public static final int ROW_POSTSHIP_REQUEST_SENT_TOO_EARLY = 6;
	public static final int ROW_POSTSHIP_REALISTICALLY_DUE = 7;
	public static final int ROW_POSTSHIP_TEST_NOT_STARTED_YET = 8;
	public static final int ROW_POSTSHIP_POTENTIAL_ISSUES = 9;
	public static final int ROW_POSTSHIP_FOLLOW_UP_LOG = 10;
	public static final int ROW_POSTSHIP_DEADLINE_OF_REQUEST = 11;
	public static final int ROW_POSTSHIP_DRAFT_MODE = 12;

	
	// bidDataGrid
	/* Added by Calvin 2-22-2018 */
	public static int ROW_PRIMARY_BRAND = 11;
	public static int ROW_BID_DATE = 7;
	public static int ROW_COUNTRY_NAME = 9;
	public static int ROW_REGION = 10;
	@SuppressWarnings("unused")
	private WebDriver driver;

	//Newly added
	public static String OUTCOME_COMMENT_FLAGGED_BID = "//div[contains(text(),'Bid was flagged for BPCOPS risk, but was released as part of a sampling methodology.')]";
	
	
	/**
	 * Constructor
	 * 
	 * @param driver
	 */
	public RemediationLogPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Returns the text of the web element at the specified sub-table and row
	 * index
	 * 
	 * @param driver
	 * @param subTable
	 * @return
	 */
	public String getRemediationValueAtIndex(WebDriver driver, int subTable,
			int rowIndex) {
		WebElement col = driver.findElement(By.xpath(constructRemediationXpath(
				subTable, rowIndex)));
		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}
	

	/**
	 * Returns the text of the web element at the specified row.
	 * 
	 * @param driver
	 *            the current WebDriver * @param rowIndex - row that you wish to get
	 *            the value.
	 * @return String of the row value that you specified.
	 */
	public String getGenBidInforValueAtIndex(WebDriver driver, int rowIndex) {
		String xpath = constructGenBidInfoXpath(rowIndex);
		
		scrollIntoView(driver, xpath);
		WebElement col = driver.findElement(By.xpath(xpath));
		String value = "";
		
		if (null != value) {
			value = col.getText();
		}

		return value;
	}

	/**
	 * Returns the text of the web element at the specified row. (Focus Bid
	 * Attributes)
	 * 
	 * @param driver
	 *            the current WebDriver * @param rowIndex - row that you wish to get
	 *            the value.
	 * @return String of the row value that you specified.
	 */
	public String getFocusBidAttrValueAtIndex(WebDriver driver, int rowIndex) {
		WebElement col = driver.findElement(By.xpath(constructFocusBidAttrXpath(rowIndex)));
		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;
	}

	public String getPostShipmentValueAtIndex(WebDriver driver, int rowIndex) {
		WebElement col = driver.findElement(By.xpath(constructPostShipmentXpath(rowIndex)));
		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;
	}

	/**
	 * Constructs xpath for the Remediation table.
	 * 
	 * @param subTable
	 *            - index of the subtable inside the remediation table.
	 * @param rowIndex
	 *            - row that you wish to get the value.
	 * @return String of constructed xpath
	 */
	private String constructRemediationXpath(int subTable, int rowIndex) {
		return XPATH_REM_PART_1 + subTable + XPATH_REM_PART_2 + rowIndex + XPATH_REM_PART_3;
	}

	/**
	 * Constructs xpath for General Bid Info
	 * 
	 * @param rowIndex
	 *            - row that you wish to get the value.
	 * @return String of constructed xpath
	 */
	private String constructGenBidInfoXpath(int rowIndex) {
		return XPATH_GEN_BID_INFO_PART_1 + rowIndex + XPATH_GEN_BID_INFO_PART_2;
	}

	/**
	 * Constructs xpath for Focus Bid Attributes
	 * 
	 * @param rowIndex
	 *            - row that you wish to get the value.
	 * @return String of constructed xpath
	 */
	private String constructFocusBidAttrXpath(int rowIndex) {
		return XPATH_FOCUS_BID_ATTR_PART_1 + rowIndex + XPATH_FOCUS_BID_ATTR_PART_2;
	}

	/**
	 * Constructs xpath for Post Shipment
	 * 
	 * @param rowIndex
	 *            - row that you wish to get the value.
	 * @return String of constructed xpath
	 */
	private String constructPostShipmentXpath(int rowIndex) {
		return XPATH_POST_SHIPMENT_PART_1 + rowIndex + XPATH_POST_SHIPMENT_PART_2;
	}

	/**
	 * Clicks the 'Edit' link of the Remediation tab
	 */
	public void clickRemediationEdit() {
		BretTestUtils.printToConsole("Remediation Log -- Clicking 'Edit' link");
		driver.findElement(By.xpath(XPATH_EDIT_LINK)).click();
	}

	/**
	 * Selects the outcome from the dropdown based on the parameter.
	 * 
	 * @param outcome
	 *            string parameter for outcome selection.
	 */
	public void selectOutcome(String outcome) {
		BretTestUtils.printToConsole("Remediation Log -- setting Outcome to: " + outcome);
		String xpathOutcome = "//td[text()='" + outcome + "']";

		driver.findElement(By.xpath(XPATH_OUTCOME_DROPDOWN)).click();
		driver.findElement(By.xpath(xpathOutcome)).click();
		;
	}

	public void selectPreshipVerification(String preShipVerification) {
		BretTestUtils
				.printToConsole("Remediation Log -- setting Preshipment Verification Hold to: " + preShipVerification);
		String xpathPreshipVerfication = "//td[text()='" + preShipVerification + "']";

		driver.findElement(By.xpath(XPATH_PRESHIP_VERIFICATION_DROPDOWN)).click();
		driver.findElement(By.xpath(xpathPreshipVerfication)).click();
		;
	}

	/**
	 * Method for outcome comment input
	 * 
	 * @param outcomeComment
	 */
	public void inputOutcomeComment(String outcomeComment) {
		BretTestUtils.printToConsole("Remediation Log -- setting Outcome Comment to: " + outcomeComment);
		driver.findElement(By.xpath(XPATH_INPUT_OUTCOME_COMMENT)).sendKeys(outcomeComment);
	}

	/**
	 * Method for clicking the Remediation Save button
	 */
	public void clickSaveButton() {
		BretTestUtils.printToConsole("Remediation Log -- Clicking 'Save' Button");
		driver.findElement(By.xpath(XPATH_SAVE_BTN)).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		BretTestUtils.printToConsole("Remediation Log -- Clicking 'Confirm' Button");
		driver.findElement(By.xpath(XPATH_CONFIRM_BTN)).click();
	}

	private String constructXpath(int i, int j, int rowRemOutcomeComment) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Method for scrolling to outcome comment (@Harel)
	 */
	public void scrollingScreenshot(WebDriver driver, String directoryPath, String imgFilename) {
		BretTestUtils.screenCapture(driver, directoryPath, imgFilename + "_" + BretTestUtils.getImgFormat());
		WebElement table = driver.findElement(By.xpath(
				"/html/body/div[1]/div[3]/div/div[4]/div[3]/div[3]/div[6]/div/div[2]/div/div[2]/div/div/div[8]/table/tbody/tr[8]/td"));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", table);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void scrollIntoView(WebDriver driver, String elementXpath) {
		WebElement table = driver.findElement(By.xpath(
				elementXpath));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", table);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
