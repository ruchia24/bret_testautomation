package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.bret.util.ScreenCapture;

/** 
* [added-Belle Pondiong]<br>
* This page class contained all the methods on what to perform on My Task page. <br>
*/
public class BRETMyTaskPage {
	
	//Declaring variables
	public WebDriver driver;
	 
	@FindBy (id = "myTaskReleaseBidsBtn")				//Release Button
	private WebElement releaseBtn;
	
	@FindBy (id = "myTaskReadyToReviewBtn")             //Ready for Review Button
	private WebElement rdyReviewBtn;
	
	@FindBy (id = "dijit_form_Button_1")			    //My Task tab filter button
	private WebElement filterBtnMyTask;
	
	public static String bidDateMyTask = null, 
			             bidFlagMyTask = null, 
			             bidRevDecMyTask = null,
			             bidReviewerRelOnDecMyTask = null;
	
	// This is a web page class constructor		 
	public BRETMyTaskPage (WebDriver driver) {
			 this.driver= driver;
			 PageFactory.initElements(driver, this);
	}
	

//--------------- Find HW NF bid on My TaskTab Routines, manual archive  --------------------------- 	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the NF bid and select the bid on My Task tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * This will save the found Bid date, Flag, Status that will be use for checking on other routines.
	 */
	public void bidFindonMyTasktab_NF (String bidNum) throws Exception {

		int grdCntrfound = 0;
		String bidcheck = null, 
			   bidFlag = null, 
			   bidReviewerDec = null,
			   bidDate = null;  
		
		grdCntrfound = bidFindonMyTaskTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		
		//Printing located bid and details on the logs
		// manual tracing
		System.out.println("\n=====Bid located in My Task Tab=====");
		System.out.println("   Bid Num:  " +bidcheck);	
		System.out.println("   Flag:     " +bidFlag);	
		System.out.println("   Bid Date: " +bidDate);
		System.out.println("   Reviwer Decesion::  " +bidReviewerDec);
		
		// save details for validation
		bidDateMyTask = bidDate;
	    bidFlagMyTask = bidFlag; 
	    bidRevDecMyTask = bidReviewerDec;

	}
	
//--------------- Find NFbid on My TaskTab Routines - ends here ----------------- 
	
//--------------- Find SW bid on My TaskTab Routines  --------------------------- 	
	 /** 
	 * [added-Belle Pondiong-9/13/2017]<br>   
	 * Routine that will check a focus bid in My Task Tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * This will return the found bid date, flag and status of the bid. <br>
	 * 	foundBid [0] = bidcheck <br>
	 * 	foundBid [1] = bidFlag <br>
	 * 	foundBid [2] = bidReviewerDecision <br>
	 * 	foundBid [3] = bidDate <br>
	 * 	foundBid [4] = bidReviewer <br>
	 * 	foundBid [5] = bidReviwerReleaseOn <br>
	 */	
	public String[] bidFindonMyTasktabSW (String bidNum) throws Exception {
		String[] foundBid = new String[7] ;
		
		int grdCntrfound = 0;
		String bidcheck = null, bidFlag = null, bidReviewerDecision = null,
			   bidDate = null, bidReviewer  = null, bidReviwerReleaseOn = null;  
		
		grdCntrfound = bidFindonMyTaskTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDecision = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewer = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		bidReviwerReleaseOn = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		
		//Printing located bid and details on the logs
		System.out.println("\n=====Bid located in My Task Tab=====");
		System.out.println("   Bid Num:  " +bidcheck);	
		System.out.println("   Flag:     " +bidFlag);	
		System.out.println("   Bid Date: " +bidDate);
		System.out.println("   Bid Status: " +bidReviewerDecision);
		System.out.println("   Bid Reviewer: " +bidReviewer);
		System.out.println("   Bid Release Date: " +bidReviwerReleaseOn);
		
		foundBid [0] = bidcheck;
		foundBid [1] = bidFlag;
		foundBid [2] = bidReviewerDecision;
		foundBid [3] = bidDate;
		foundBid [4] = bidReviewer;
		foundBid [5] = bidReviwerReleaseOn;
		
		return foundBid;
		
	}
//--------------- Find Focus bid on My TaskTab Routines  --------------------------- 	

//--------------- Release Focus HW bid  ---------------------------
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will manually release the Focus bid to Archive.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * This will save the found Bid date, Flag, Status that will be use for checking after bid is released to Archive.
	 */
	public void releaseFbid2Archive (String bidNum, String mtdNmae) throws Exception {
		
		int grdCntrfound = 0;
		String bidcheck = null, 
			   bidFlag = null, 
			   bidReviewerDec = null,
			   bidDate = null,
			   bidReviewer = null,
			   bidReviewerReleaseOn = null;  
		
		grdCntrfound = bidFindonMyTaskTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewer = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		bidReviewerReleaseOn = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		
		//Printing located bid and details on the logs
		// manual tracing
		System.out.println("\n=====Bid located in My Task Tab=====");
		System.out.println("   Bid Num:  " +bidcheck);	
		System.out.println("   Flag:     " +bidFlag);	
		System.out.println("   Bid Date: " +bidDate);
		System.out.println("   Reviwer::  " +bidReviewer);
		System.out.println("   Reviwer Decesion::  " +bidReviewerDec);
		System.out.println("   Reviwer Relese On::  " +bidReviewerReleaseOn);
		
		// save details for validation
		bidDateMyTask = bidDate;
	    bidFlagMyTask = bidFlag; 
	    bidRevDecMyTask = bidReviewerDec;
        bidReviewerRelOnDecMyTask = bidReviewerReleaseOn;
		
		ScreenCapture screenShots = new ScreenCapture(driver);  
	    screenShots.screenCaptures(mtdNmae);
	    
	    bidReleasetoArchive ();     // release bid to archived
	    //System.out.println("Release button clicked.");
				
	}
	
	
//---------- Release Focus HW bid - ends here  -------------------- 	

//---------- REEADY FOR REVIEW SW bid   -------------------- 		
	 /** 
	 * [added-Belle Pondiong-9/13/2017]<br>
	 * Routine that will set the waiting for pricing bid to ready to Review<br>
	 * This will return void.<br>
	 */
	public void bidReadyforRview (String bidNum) throws Exception{
		
		int grdCntrfound = 0;
		String bidcheck = null;
		
		grdCntrfound = bidFindonMyTaskTab (bidNum);  //call the routine to locate the bid
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();

		System.out.println("\nBid found prior to changing from W to N: "+bidcheck);
		
		// click ready for review on the found bid
		rdyReviewBtn.click();	
		System.out.println("\nREADY FOR REVIEW  button clicked.");
		
	}

//---------- REEADY FOR REVIEW SW bid - ends here  --------------------
	
//------------------ Call Routines  --------------------------------------- 
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will verify that release button exist on My Task tab.<br>
	 * This will return void.<br>
	 */
	public void releaseBtnCheck () {

		// Assert.assertEquals(driver.findElement(By.id("myTaskReleaseBidsBtn_label")).getText(), "Release");
		
		if (releaseBtn.isDisplayed()) {
			// manual tracing
			 System.out.println("\n=====Release button in My Task Tab=====");
			 System.out.println(" Release button found in My Task tab");
		}
		else
			Assert.fail("Failed. Release button is not found on page.");
		 
	

	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will released the selected bid to Archive.<br>
	 * This will return void.<br>
	 */
	public void bidReleasetoArchive (){

		//clicking RELEASE button
		releaseBtn.click();
		//driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']/div[2]/span[2]/span")).click();
		
		System.out.println("\nRELEASE button clicked.");
		
	}
	
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will look for the bid in My Task Tab Grid. <br>
	 * This will return the location of the bid in the grid.<br>
	 */ 
	protected int bidFindonMyTaskTab (String bidNumber) throws Exception {
		
		int bidlocatecntr = 0,
		    grdCntr = 0, 
		    pgLink = 1; 
		String bidcheck = null;
		WebElement element;
					
		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_1']/span[3]")).click(); //Clicks '25' records per page

		//Routine to find the bid on My Task Grid
		do {
			
			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_0']/span[2]/span["+pgLink+"]")).click();  // click on the page link
//			     System.out.println("Tracing...page number " +pgLink);
			
			 grdCntr = 0;  // set to zero, this will reset the row # on the grid should the code enters the next page
//				 System.out.println("Tracing...grid number init" +grdCntr);
			
			//Routine to check the bid per page 			
			for (int pgDataCntr = 0; pgDataCntr < 25; pgDataCntr ++) {	
				   
			    grdCntr = grdCntr +1;
			    
			    // scroll until element is in view since grid is dynamic with scroll bars
				element = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500); 
				
				bidcheck = driver.findElement(By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]")).getText();
//					System.out.println("Tracing...grid number inside FOR LOOP  " +grdCntr);
//					System.out.println("Tracing...bid number " +bidcheck);
													
					if (bidNumber.equals(bidcheck)) { // break if bid is found
						break;
					}	
					
					//focusToElement(By.id("idx_layout_ContentPane_0"));
			}
			
			pgLink ++;  // go to next page
						
		} while (!(bidNumber.equals(bidcheck)));  //checking the specified bid on the grid
		
      bidlocatecntr = grdCntr;
      
		return bidlocatecntr;			
	}

	
}
