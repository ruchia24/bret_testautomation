package com.bret.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.PropertiesFiles.BWPropertiesFiles;

public class ScreenCapture {
	
	public WebDriver driver;
	
	// This is a web page class constructor
	public ScreenCapture (WebDriver driver) {
			 this.driver= driver;
			//System.out.println("ScreenCapture driver = " + this.driver.hashCode());
			 PageFactory.initElements(driver, this);
			 
	  }

		 
    // initially saved the screenshot into Memory
    // indicate path on where screenshot will be saved
    // Include the current ran TC on the screenshot name		 
	public void screenCaptures (String methodName) throws IOException {
	 
		//declaring location on where to save the images
		//using timestamp for unique filename
		BWPropertiesFiles propRead = new BWPropertiesFiles();
		String localDrive = propRead.propBW_screenShotPath();
		String timeStamp = new SimpleDateFormat("MMddyyyy_hhmm").format(Calendar.getInstance().getTime());
	
	    //takes the screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        
        //save screenshot locally
        FileUtils.copyFile(scrFile, new File(localDrive +methodName+ "_" + timeStamp + ".jpg"));
        
		// manual tracing
		 System.out.println("\nScreen captured for " +methodName+ " saved on specified path.\n");		
	}

}
