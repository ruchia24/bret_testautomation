package com.bret.util;

public class ConstUtils {

	public static int INDEX_ZERO = 0;
	public static int INDEX_ONE = 1;
	public static int INDEX_TWO = 2;
	public static int INDEX_THREE = 3;
	public static int INDEX_FOUR = 4;
	public static int INDEX_FIVE = 5;
	public static int INDEX_FIFTEEN = 15; 
	public static int SIXTY = 60;
	
	/**BPCOPS**/
	public static String BRET_FOCUS_FLAG_COL_NAME = "BRET_FOCUS_FLAG";
	public static String NON_FOCUS_FLAG = "N";
	public static String testCEIDSevZero = "Dummy00";
	public static String testCEIDSevOne = "Dummy01";
	public static String testCEIDSevTwo = "Dummy02";
	public static String testCEIDSevThree = "Dummy03";
	public static String testCEIDSevFour = "Dummy04";
	public static String testCEIDSevFive = "Dummy05";
	public static String testCEIDSevDumNull = "DumNULL";
}
