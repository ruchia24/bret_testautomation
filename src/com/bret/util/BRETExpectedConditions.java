package com.bret.util;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;

import com.bret.base.BaseTest;

public class BRETExpectedConditions {

	private BRETExpectedConditions() {
		// Utility class
	}
	final static Logger LOGGER = Logger.getLogger(BRETExpectedConditions.class);
	private static FluentWait<WebDriver> wait = null;
	
	public static Boolean noLoadingElement(WebDriver driver, final int fastTimeOut) {
		  wait = new FluentWait<WebDriver>(driver).withTimeout(fastTimeOut, TimeUnit.SECONDS)
				  .pollingEvery(fastTimeOut, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		  return wait.until(new ExpectedCondition<Boolean>() {
			  @Override
			  public Boolean apply(WebDriver driver) {
				  WebElement element = null;
				  try {
					  driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
					  element = driver.findElement(By.xpath("//img[contains(@class,'center')]"));
				  } catch (NoSuchElementException e) {
					  return true;
				  } finally {
					  driver.manage().timeouts().implicitlyWait(BaseTest.getMaxTimeOut(), TimeUnit.SECONDS);
				  }
				  return false;
			  }
		  });
	
	}
}
