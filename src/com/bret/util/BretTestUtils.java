package com.bret.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bret.base.BaseTest;
import com.bret.pages.BRETAdvSearchPage;
import com.bret.testcases.bpcops.BpcopsSevZeroTestNew;
import com.google.common.base.Function;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.concurrent.TimeUnit;

/**
 * Bret Test - Utilities class
 * 
 * @author CheyenneFreyLazo
 * 
 */
public final class BretTestUtils {

	final static Logger LOGGER = Logger.getLogger(BretTestUtils.class);
	private static final int MAX_ROWS_READ = 100;
	private static final String UNDERSCORE = "_";
	private static final String DATE_FORMAT = "MMddyyyy_hhmm";
	private static final String JPG_FORMAT = ".jpg";
	private WebDriver driver;
	private static final String LOADING_COMPLETE = "complete";
	private static final String DOCUMENT_READY_STATE = "readyState";
	WebDriverWait wait; 
	
	public BretTestUtils(WebDriver driver) {
		this.driver = driver;		
	}
	/**
	 * Retrieves map of values from the excel file
	 * 
	 * @param sheetName
	 *            the sheetname where the test data is located
	 * @param excelPath
	 *            DataSource path
	 * @return Map key value pair of test data
	 */
	public static Map<String, String> getTestDataFromExcel(String sheetName,
			String excelPath) {
		Map<String, String> testDataMap = Excel.getRowValues(excelPath,
				sheetName, MAX_ROWS_READ);

		return testDataMap;
	}

	/**
	 * Performs the screen capture of test evidence
	 * 
	 * @param driver
	 *            WebDriver of the object to be captured.
	 * @param directoryPath
	 *            path where the images should be saved.
	 * @param imgFilename
	 *            String the file name of the screen capture.
	 * @throws IOException
	 * @throws WebDriverException
	 */
	public static void screenCapture(WebDriver driver, String directoryPath,
			String imgFilename) {

		try {
			File screenshot = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);

			String fullFilePath = directoryPath + imgFilename;
			FileUtils.copyFile(screenshot, new File(fullFilePath));

			outputToConsole("Screen capture is saved in: " + fullFilePath);
		} catch (WebDriverException | IOException e) {
			e.printStackTrace();
		}
	}

	public static String getImgFilename(String tcName,
			@SuppressWarnings("rawtypes") Class clazz) {
		String timeStamp = new SimpleDateFormat(DATE_FORMAT).format(Calendar
				.getInstance().getTime());

		String filename = tcName + UNDERSCORE + clazz.getSimpleName()
				+ UNDERSCORE + timeStamp + JPG_FORMAT;

		return filename;
	}
	
	public static String getImgFilenameNoFormat(String tcName,
			@SuppressWarnings("rawtypes") Class clazz) {
		String timeStamp = new SimpleDateFormat(DATE_FORMAT).format(Calendar
				.getInstance().getTime());

		String filename = tcName + UNDERSCORE + clazz.getSimpleName()
				+ UNDERSCORE + timeStamp;

		return filename;
	}
	
	public static String getImgFormat() {
		return JPG_FORMAT;
	}


	/**
	 * Just a helper method for performing console output
	 * 
	 * @param message
	 */
	private static void outputToConsole(String message) {
		System.out.println(message);
	}

	public static void printToConsole(String... texts) {
		for (String text : texts) {
			System.out.println(text);
		}
	}

	/**
	 * Returns an ArrayList containing the file rows (line by line)
	 * 
	 * @param filePath
	 * @return
	 */
	public static List<String> readFile(String filePath) {
		String line = "";
		List<String> contents = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

			while ((line = br.readLine()) != null) {
				contents.add(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return contents;
	}
	public static String getPropertyValue(String propertyKey) {
		String propertyPath = "./properties/brettest.properties";
		Properties prop = new Properties();
		String propertyValue = "";
		
		try (InputStream propInput = new FileInputStream(new File(propertyPath))) {
			prop.load(propInput);
			
			propertyValue = prop.getProperty(propertyKey);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return propertyValue;
	}
	
	
	
	/****NEW METHODS ADDED BELOW - IF ABOVE NOT REQUIRED WILL BE REMOVED LATER*********/
	
	  /**
	   * Search for element with a given search criteria
	   * 
	   * @param by
	   * @return
	   */
	  public WebElement getElement(By by) {
	    try {
	      return driver.findElement(by);
	    } catch (NoSuchElementException e) {
	      return null;
	    }
	  }
	  
	  /**
	   * Clears the text from a text field, and sets it.
	   * 
	   * @param by The element to set the text of.
	   * @param text The text that the element will have.
	 * @param tab 
	   * @return The implementing class for fluency
	   */
	  public void setText(By by, String text) {
	    WebElement element = waitForElement(by);	
	    element.clear();
	    element.sendKeys(text);    
	  }
	  
	  
	  /**
	   * Wait for an element to be drawn.
	   * 
	   * @param by The element to search
	   * @return <i>this method is not meant to be used fluently.</i><br>
	   *         <br>
	   *         Returns <code>WebElement</code> if the element are found.
	   */
	  public WebElement waitForElement(By by) {
		  wait = new WebDriverWait(driver, BaseTest.maxTimeOut);
		  wait.until(ExpectedConditions.presenceOfElementLocated(by));
		  driver.manage().timeouts().implicitlyWait(BaseTest.maxTimeOut, TimeUnit.SECONDS);		  
		  return driver.findElement(by);
	  }
	  
	  /**
	   * click an element without using the fluent wait
	   * @return
	   */
	  public void click(By by) {
		  WebElement element = waitForElement(by);
		  element.click();		 
	  }
  
	  
	  /**
	   * Click an element.
	   * 
	   * @param element The element to click.
	   * @return The implementing class for fluency
	   */
	  public void clickAndWait(By by) {
		WebElement element = waitForElement(by);
	    element.click();
	    // Wait for page to render
	    wait = new WebDriverWait(driver, BaseTest.maxTimeOut);
	  
	  }
	  
	  /**
	   * navigateTo("url") // navigate to a relative URL. will simply append "url" to the current URL.
	   * @param url
	   */
	  public void navigateTo(String url) {
		  driver.navigate().to(driver.getCurrentUrl().concat(url));
		  waitNoContentLoadingFluent();
	  }
	  
	  /**
	   * Wait until no element is loading on page
	   * @param fastTimeOut - max time sec.
	   */
	  public void waitNoContentLoadingFluent() {
	    BRETExpectedConditions.noLoadingElement(driver, BaseTest.maxTimeOut);
	  }
} 



