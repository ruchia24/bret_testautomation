package com.bret.obj.export;

public class ExportNonFocusData {

	private String type;
	private String sourceSystem;
	private String region;
	private String bidId;
	private String additionalId;
	private String bidDate;
	private String country;
	private String ibmSellPrice;
	private String primaryBrand;
	private String distributor;
	private String customerFacingBP;
	private String customerName;
	private String reviewerComments;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getBidId() {
		return bidId;
	}
	public void setBidId(String bidId) {
		this.bidId = bidId;
	}
	public String getAdditionalId() {
		return additionalId;
	}
	public void setAdditionalId(String additionalId) {
		this.additionalId = additionalId;
	}
	public String getBidDate() {
		return bidDate;
	}
	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getIbmSellPrice() {
		return ibmSellPrice;
	}
	public void setIbmSellPrice(String ibmSellPrice) {
		this.ibmSellPrice = ibmSellPrice;
	}
	public String getPrimaryBrand() {
		return primaryBrand;
	}
	public void setPrimaryBrand(String primaryBrand) {
		this.primaryBrand = primaryBrand;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	public String getCustomerFacingBP() {
		return customerFacingBP;
	}
	public void setCustomerFacingBP(String customerFacingBP) {
		this.customerFacingBP = customerFacingBP;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getReviewerComments() {
		return reviewerComments;
	}
	public void setReviewerComments(String reviewerComments) {
		this.reviewerComments = reviewerComments;
	}
}
