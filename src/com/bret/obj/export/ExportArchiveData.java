package com.bret.obj.export;

public class ExportArchiveData {
	
	private String type;
	private String sourceSystem;
	private String region;
	private String bidId;
	private String additionalId;
	private String bidDate;
	private String reviewerDecision;
	private String reviewer;
	private String reviewerReleasedOn;
	private String bidReleasedBy;
	private String bidReleasedOn;
	private String country;
	private String ibmSellPrice;
	private String primaryBrand;
	private String distributor;
	private String customerFacingBP;
	private String customerName;
	private String marginOrDiscountFlag;
	private String bidQuestionsFlag;
	private String pbcHistoryFlag;
	private String compositeScoreFlag;
	private String reviewerComments;
	private String readyToReviewOn;
	private String conf;
	private String rtm;
	private String nstdPmt;
	private String bundle;
	private String fee;
	private String soleSourceProc;
	private String focusBp;
	private String bpmPerc;
	private String discPerc;
	private String bpmTotal;
	private String discTotal;
	private String remediationLog;
	private String actionOwner;
	private String completedOn;
	private String closedDate;
	private String bidExpired;
	private String exPreFulfilment;
	private String postVerifyDate;
	private String postShipAuditFollowUpStatus;
	private String waitingForExtension;
	private String problemsWithChasing;
	private String requestSentTooEarly;
	private String realisticallyDue;
	private String testNoteStartedYet;
	private String potentialIssues;
	private String followUpLog;
	private String deadlineOfRequest;
	private String bidEvent;
	private String remediationComment;
	private String outcome;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getBidId() {
		return bidId;
	}
	public void setBidId(String bidId) {
		this.bidId = bidId;
	}
	public String getAdditionalId() {
		return additionalId;
	}
	public void setAdditionalId(String additionalId) {
		this.additionalId = additionalId;
	}
	public String getBidDate() {
		return bidDate;
	}
	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}
	public String getReviewerDecision() {
		return reviewerDecision;
	}
	public void setReviewerDecision(String reviewerDecision) {
		this.reviewerDecision = reviewerDecision;
	}
	public String getReviewer() {
		return reviewer;
	}
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	public String getReviewerReleasedOn() {
		return reviewerReleasedOn;
	}
	public void setReviewerReleasedOn(String reviewerReleasedOn) {
		this.reviewerReleasedOn = reviewerReleasedOn;
	}
	public String getBidReleasedBy() {
		return bidReleasedBy;
	}
	public void setBidReleasedBy(String bidReleasedBy) {
		this.bidReleasedBy = bidReleasedBy;
	}
	public String getBidReleasedOn() {
		return bidReleasedOn;
	}
	public void setBidReleasedOn(String bidReleasedOn) {
		this.bidReleasedOn = bidReleasedOn;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getIbmSellPrice() {
		return ibmSellPrice;
	}
	public void setIbmSellPrice(String ibmSellPrice) {
		this.ibmSellPrice = ibmSellPrice;
	}
	public String getPrimaryBrand() {
		return primaryBrand;
	}
	public void setPrimaryBrand(String primaryBrand) {
		this.primaryBrand = primaryBrand;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	public String getCustomerFacingBP() {
		return customerFacingBP;
	}
	public void setCustomerFacingBP(String customerFacingBP) {
		this.customerFacingBP = customerFacingBP;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMarginOrDiscountFlag() {
		return marginOrDiscountFlag;
	}
	public void setMarginOrDiscountFlag(String marginOrDiscountFlag) {
		this.marginOrDiscountFlag = marginOrDiscountFlag;
	}
	public String getBidQuestionsFlag() {
		return bidQuestionsFlag;
	}
	public void setBidQuestionsFlag(String bidQuestionsFlag) {
		this.bidQuestionsFlag = bidQuestionsFlag;
	}
	public String getPbcHistoryFlag() {
		return pbcHistoryFlag;
	}
	public void setPbcHistoryFlag(String pbcHistoryFlag) {
		this.pbcHistoryFlag = pbcHistoryFlag;
	}
	public String getCompositeScoreFlag() {
		return compositeScoreFlag;
	}
	public void setCompositeScoreFlag(String compositeScoreFlag) {
		this.compositeScoreFlag = compositeScoreFlag;
	}
	public String getReviewerComments() {
		return reviewerComments;
	}
	public void setReviewerComments(String reviewerComments) {
		this.reviewerComments = reviewerComments;
	}
	public String getReadyToReviewOn() {
		return readyToReviewOn;
	}
	public void setReadyToReviewOn(String readyToReviewOn) {
		this.readyToReviewOn = readyToReviewOn;
	}
	public String getConf() {
		return conf;
	}
	public void setConf(String conf) {
		this.conf = conf;
	}
	public String getRtm() {
		return rtm;
	}
	public void setRtm(String rtm) {
		this.rtm = rtm;
	}
	public String getNstdPmt() {
		return nstdPmt;
	}
	public void setNstdPmt(String nstdPmt) {
		this.nstdPmt = nstdPmt;
	}
	public String getBundle() {
		return bundle;
	}
	public void setBundle(String bundle) {
		this.bundle = bundle;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getSoleSourceProc() {
		return soleSourceProc;
	}
	public void setSoleSourceProc(String soleSourceProc) {
		this.soleSourceProc = soleSourceProc;
	}
	public String getFocusBp() {
		return focusBp;
	}
	public void setFocusBp(String focusBp) {
		this.focusBp = focusBp;
	}
	public String getBpmPerc() {
		return bpmPerc;
	}
	public void setBpmPerc(String bpmPerc) {
		this.bpmPerc = bpmPerc;
	}
	public String getDiscPerc() {
		return discPerc;
	}
	public void setDiscPerc(String discPerc) {
		this.discPerc = discPerc;
	}
	public String getBpmTotal() {
		return bpmTotal;
	}
	public void setBpmTotal(String bpmTotal) {
		this.bpmTotal = bpmTotal;
	}
	public String getDiscTotal() {
		return discTotal;
	}
	public void setDiscTotal(String discTotal) {
		this.discTotal = discTotal;
	}
	public String getRemediationLog() {
		return remediationLog;
	}
	public void setRemediationLog(String remediationLog) {
		this.remediationLog = remediationLog;
	}
	public String getActionOwner() {
		return actionOwner;
	}
	public void setActionOwner(String actionOwner) {
		this.actionOwner = actionOwner;
	}
	public String getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(String completedOn) {
		this.completedOn = completedOn;
	}
	public String getClosedDate() {
		return closedDate;
	}
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	public String getBidExpired() {
		return bidExpired;
	}
	public void setBidExpired(String bidExpired) {
		this.bidExpired = bidExpired;
	}
	public String getExPreFulfilment() {
		return exPreFulfilment;
	}
	public void setExPreFulfilment(String exPreFulfilment) {
		this.exPreFulfilment = exPreFulfilment;
	}
	public String getPostVerifyDate() {
		return postVerifyDate;
	}
	public void setPostVerifyDate(String postVerifyDate) {
		this.postVerifyDate = postVerifyDate;
	}
	public String getPostShipAuditFollowUpStatus() {
		return postShipAuditFollowUpStatus;
	}
	public void setPostShipAuditFollowUpStatus(String postShipAuditFollowUpStatus) {
		this.postShipAuditFollowUpStatus = postShipAuditFollowUpStatus;
	}
	public String getWaitingForExtension() {
		return waitingForExtension;
	}
	public void setWaitingForExtension(String waitingForExtension) {
		this.waitingForExtension = waitingForExtension;
	}
	public String getProblemsWithChasing() {
		return problemsWithChasing;
	}
	public void setProblemsWithChasing(String problemsWithChasing) {
		this.problemsWithChasing = problemsWithChasing;
	}
	public String getRequestSentTooEarly() {
		return requestSentTooEarly;
	}
	public void setRequestSentTooEarly(String requestSentTooEarly) {
		this.requestSentTooEarly = requestSentTooEarly;
	}
	public String getRealisticallyDue() {
		return realisticallyDue;
	}
	public void setRealisticallyDue(String realisticallyDue) {
		this.realisticallyDue = realisticallyDue;
	}
	public String getTestNoteStartedYet() {
		return testNoteStartedYet;
	}
	public void setTestNoteStartedYet(String testNoteStartedYet) {
		this.testNoteStartedYet = testNoteStartedYet;
	}
	public String getPotentialIssues() {
		return potentialIssues;
	}
	public void setPotentialIssues(String potentialIssues) {
		this.potentialIssues = potentialIssues;
	}
	public String getFollowUpLog() {
		return followUpLog;
	}
	public void setFollowUpLog(String followUpLog) {
		this.followUpLog = followUpLog;
	}
	public String getDeadlineOfRequest() {
		return deadlineOfRequest;
	}
	public void setDeadlineOfRequest(String dealineofRequest) {
		this.deadlineOfRequest = dealineofRequest;
	}
	public String getBidEvent() {
		return bidEvent;
	}
	public void setBidEvent(String bidEvent) {
		this.bidEvent = bidEvent;
	}
	public String getRemediationComment() {
		return remediationComment;
	}
	public void setRemediationComment(String remediationComment) {
		this.remediationComment = remediationComment;
	}
	public String getOutcome() {
		return outcome;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	
}
