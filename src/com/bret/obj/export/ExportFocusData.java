package com.bret.obj.export;

public class ExportFocusData {
	
	private String type;
	private String sourceSystem;
	private String region;
	private String bidId;
	private String additionalId;
	private String bidDate;
	private String reviewerDecision;
	private String reviewer;
	private String reviewerReleasedOn;
	private String country;
	private String ibmSellPrice;
	private String primaryBrand;
	private String distributor;
	private String customerFacingBP;
	private String customerName;
	private String marginOrDiscountFlag;
	private String bidQuestionsFlag;
	private String pbcHistoryFlag;
	private String compositeScoreFlag;
	private String reviewerComments;
	private String readyToReviewOn;
	private String confidentiality;
	private String routeToMarket;
	private String offshorePaymentTerms;
	private String bundledSolutions;
	private String contingencyFee;
	private String soleSourceProc;
	private String focusBP;
	private String bpMargPerc;
	private String discountPerc;
	private String bpMarginTotal;
	private String discountTotal;
	private String remediationLog;
	private String actionOwner;
	private String remediationFulfilled;
	private String remediationComment;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getBidId() {
		return bidId;
	}
	public void setBidId(String bidId) {
		this.bidId = bidId;
	}
	public String getAdditionalId() {
		return additionalId;
	}
	public void setAdditionalId(String additionalId) {
		this.additionalId = additionalId;
	}
	public String getBidDate() {
		return bidDate;
	}
	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}
	public String getReviewerDecision() {
		return reviewerDecision;
	}
	public void setReviewerDecision(String reviewerDecision) {
		this.reviewerDecision = reviewerDecision;
	}
	public String getReviewerReleasedOn() {
		return reviewerReleasedOn;
	}
	public void setReviewerReleasedOn(String reviewerReleasedOn) {
		this.reviewerReleasedOn = reviewerReleasedOn;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getIbmSellPrice() {
		return ibmSellPrice;
	}
	public void setIbmSellPrice(String ibmSellPrice) {
		this.ibmSellPrice = ibmSellPrice;
	}
	public String getPrimaryBrand() {
		return primaryBrand;
	}
	public void setPrimaryBrand(String primaryBrand) {
		this.primaryBrand = primaryBrand;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	public String getCustomerFacingBP() {
		return customerFacingBP;
	}
	public void setCustomerFacingBP(String customerFacingBP) {
		this.customerFacingBP = customerFacingBP;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMarginOrDiscountFlag() {
		return marginOrDiscountFlag;
	}
	public void setMarginOrDiscountFlag(String marginOrDiscountFlag) {
		this.marginOrDiscountFlag = marginOrDiscountFlag;
	}
	public String getBidQuestionsFlag() {
		return bidQuestionsFlag;
	}
	public void setBidQuestionsFlag(String bidQuestionsFlag) {
		this.bidQuestionsFlag = bidQuestionsFlag;
	}
	public String getPbcHistoryFlag() {
		return pbcHistoryFlag;
	}
	public void setPbcHistoryFlag(String pbcHistoryFlag) {
		this.pbcHistoryFlag = pbcHistoryFlag;
	}
	public String getCompositeScoreFlag() {
		return compositeScoreFlag;
	}
	public void setCompositeScoreFlag(String compositeScoreFlag) {
		this.compositeScoreFlag = compositeScoreFlag;
	}
	public String getReviewerComments() {
		return reviewerComments;
	}
	public void setReviewerComments(String reviewerComments) {
		this.reviewerComments = reviewerComments;
	}
	public String getReadyToReviewOn() {
		return readyToReviewOn;
	}
	public void setReadyToReviewOn(String readyToReviewOn) {
		this.readyToReviewOn = readyToReviewOn;
	}
	
	public String getSoleSourceProc() {
		return soleSourceProc;
	}
	public void setSoleSourceProc(String soleSourceProc) {
		this.soleSourceProc = soleSourceProc;
	}
	
	public String getRemediationLog() {
		return remediationLog;
	}
	public void setRemediationLog(String remediationLog) {
		this.remediationLog = remediationLog;
	}
	public String getActionOwner() {
		return actionOwner;
	}
	public void setActionOwner(String actionOwner) {
		this.actionOwner = actionOwner;
	}
	public String getRemediationFulfilled() {
		return remediationFulfilled;
	}
	public void setRemediationFulfilled(String remediationFulfilled) {
		this.remediationFulfilled = remediationFulfilled;
	}
	public String getRemediationComment() {
		return remediationComment;
	}
	public void setRemediationComment(String remediationComment) {
		this.remediationComment = remediationComment;
	}
	public String getReviewer() {
		return reviewer;
	}
	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	public String getConfidentiality() {
		return confidentiality;
	}
	public void setConfidentiality(String confidentiality) {
		this.confidentiality = confidentiality;
	}
	public String getRouteToMarket() {
		return routeToMarket;
	}
	public void setRouteToMarket(String routeToMarket) {
		this.routeToMarket = routeToMarket;
	}
	public String getOffshorePaymentTerms() {
		return offshorePaymentTerms;
	}
	public void setOffshorePaymentTerms(String offshorePaymentTerms) {
		this.offshorePaymentTerms = offshorePaymentTerms;
	}
	public String getBundledSolutions() {
		return bundledSolutions;
	}
	public void setBundledSolutions(String bundledSolutions) {
		this.bundledSolutions = bundledSolutions;
	}
	public String getContingencyFee() {
		return contingencyFee;
	}
	public void setContingencyFee(String contingencyFee) {
		this.contingencyFee = contingencyFee;
	}
	public String getFocusBP() {
		return focusBP;
	}
	public void setFocusBP(String focusBP) {
		this.focusBP = focusBP;
	}
	public String getBpMargPerc() {
		return bpMargPerc;
	}
	public void setBpMargPerc(String bpMargPerc) {
		this.bpMargPerc = bpMargPerc;
	}
	public String getDiscountPerc() {
		return discountPerc;
	}
	public void setDiscountPerc(String discountPerc) {
		this.discountPerc = discountPerc;
	}
	public String getBpMarginTotal() {
		return bpMarginTotal;
	}
	public void setBpMarginTotal(String bpMarginTotal) {
		this.bpMarginTotal = bpMarginTotal;
	}
	public String getDiscountTotal() {
		return discountTotal;
	}
	public void setDiscountTotal(String discountTotal) {
		this.discountTotal = discountTotal;
	}
	
	
}
