package com.bret.obj.export;

public class ExportDetailData {
	private String type;
	private String bidId;
	private String additionalId;
	private String sourceSystem;
	private String bidDate;
	private String region;
	private String countryName;
	private String reviewerDecision;
	private String reviewer;
	private String reviewerReleasedOn;
	private String primaryBrand;
	private String distributor;
	private String distributorCeid;
	private String customerFacingBP;
	private String customerFacingBPCeid;
	private String customerName;
	private String customerCMR;
	private String bidLevelMargin;
	private String bidLevelDiscount;
	private String aggregateScoreNdm;
	private String totalBidScore;
	private String revenueSize;
	private String confidentiality;
	private String routeToMarket;
	private String offshorePaymentTerms;
	private String bundledSolutions;
	private String contingencyFee;
	private String soleSourceProc;
	private String focusBP;
	private String bpMargPerc;
	private String discountPerc;
	private String bpMarginTotal;
	private String discountTotal;
	private String supportingFactors;
	private String justificationQuestions;
	private String operationOwner;
	private String bidMgr;
	private String remediationUser;
	private String t3Name;
	private String t4Name;
	private String t5Name;
	private String bpmT1Fin;
	private String bpmT1Serv;
	private String bpmT1Ins;
	private String bpmT1Profit;
	private String bpmT2Fin;
	private String bpmT2Serv;
	private String bpmT2Ins;
	private String bpmT2Profit;
	private String bpmOther;
	private String log;
	private String channelProgram;
	private String actionOwner;
	private String closed;
	private String closedDate;
	private String outcomeComment;
	private String condition;
	private String transparencyLetter;
	private String cycle;
	private String exceptionFlag;
	private String fulfilled;
	private String comment;
	private String bidExpired;
	private String exPreFulfillment;
	private String postVerifyDate;
	private String postShipAuditFollowUpStatus;
	private String waitingForExtension;
	private String problemsWithChasing;
	private String requestSentTooEarly;
	private String realisticallyDue;
	private String testNoteStartedYet;
	private String potentialIssues;
	private String followUpLog;
	private String deadlineOfRequest;
	private String bidEvent;
	private String outcome;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBidId() {
		return bidId;
	}

	public void setBidId(String bidId) {
		this.bidId = bidId;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getBidDate() {
		return bidDate;
	}

	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getReviewerDecision() {
		return reviewerDecision;
	}

	public void setReviewerDecision(String reviewerDecision) {
		this.reviewerDecision = reviewerDecision;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public String getReviewerReleasedOn() {
		return reviewerReleasedOn;
	}

	public void setReviewerReleasedOn(String reviewerReleasedOn) {
		this.reviewerReleasedOn = reviewerReleasedOn;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPrimaryBrand() {
		return primaryBrand;
	}

	public void setPrimaryBrand(String primaryBrand) {
		this.primaryBrand = primaryBrand;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public String getDistributorCeid() {
		return distributorCeid;
	}

	public void setDistributorCeid(String distributorCeid) {
		this.distributorCeid = distributorCeid;
	}

	public String getCustomerFacingBP() {
		return customerFacingBP;
	}

	public void setCustomerFacingBP(String customerFacingBP) {
		this.customerFacingBP = customerFacingBP;
	}

	public String getCustomerFacingBPCeid() {
		return customerFacingBPCeid;
	}

	public void setCustomerFacingBPCeid(String customerFacingBPCeid) {
		this.customerFacingBPCeid = customerFacingBPCeid;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCMR() {
		return customerCMR;
	}

	public void setCustomerCMR(String customerCMR) {
		this.customerCMR = customerCMR;
	}

	public String getAdditionalId() {
		return additionalId;
	}

	public void setAdditionalId(String additionalId) {
		this.additionalId = additionalId;
	}

	public String getBidLevelMargin() {
		return bidLevelMargin;
	}

	public void setBidLevelMargin(String bidLevelMargin) {
		this.bidLevelMargin = bidLevelMargin;
	}

	public String getBidLevelDiscount() {
		return bidLevelDiscount;
	}

	public void setBidLevelDiscount(String bidLevelDiscount) {
		this.bidLevelDiscount = bidLevelDiscount;
	}

	public String getAggregateScoreNdm() {
		return aggregateScoreNdm;
	}

	public void setAggregateScoreNdm(String aggregateScoreNdm) {
		this.aggregateScoreNdm = aggregateScoreNdm;
	}

	public String getTotalBidScore() {
		return totalBidScore;
	}

	public void setTotalBidScore(String totalBidScore) {
		this.totalBidScore = totalBidScore;
	}

	public String getRevenueSize() {
		return revenueSize;
	}

	public void setRevenueSize(String revenueSize) {
		this.revenueSize = revenueSize;
	}

	public String getConfidentiality() {
		return confidentiality;
	}

	public void setConfidentiality(String confidentiality) {
		this.confidentiality = confidentiality;
	}

	public String getRouteToMarket() {
		return routeToMarket;
	}

	public void setRouteToMarket(String routeToMarket) {
		this.routeToMarket = routeToMarket;
	}

	public String getOffshorePaymentTerms() {
		return offshorePaymentTerms;
	}

	public void setOffshorePaymentTerms(String offshorePaymentTerms) {
		this.offshorePaymentTerms = offshorePaymentTerms;
	}

	public String getBundledSolutions() {
		return bundledSolutions;
	}

	public void setBundledSolutions(String bundledSolutions) {
		this.bundledSolutions = bundledSolutions;
	}

	public String getContingencyFee() {
		return contingencyFee;
	}

	public void setContingencyFee(String contingencyFee) {
		this.contingencyFee = contingencyFee;
	}

	public String getSoleSourceProc() {
		return soleSourceProc;
	}

	public void setSoleSourceProc(String soleSourceProc) {
		this.soleSourceProc = soleSourceProc;
	}

	public String getFocusBP() {
		return focusBP;
	}

	public void setFocusBP(String focusBP) {
		this.focusBP = focusBP;
	}

	public String getBpMargPerc() {
		return bpMargPerc;
	}

	public void setBpMargPerc(String bpMargPerc) {
		this.bpMargPerc = bpMargPerc;
	}

	public String getDiscountPerc() {
		return discountPerc;
	}

	public void setDiscountPerc(String discountPerc) {
		this.discountPerc = discountPerc;
	}

	public String getBpMarginTotal() {
		return bpMarginTotal;
	}

	public void setBpMarginTotal(String bpMarginTotal) {
		this.bpMarginTotal = bpMarginTotal;
	}

	public String getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(String discountTotal) {
		this.discountTotal = discountTotal;
	}

	public String getSupportingFactors() {
		return supportingFactors;
	}

	public void setSupportingFactors(String supportingFactors) {
		this.supportingFactors = supportingFactors;
	}

	public String getJustificationQuestions() {
		return justificationQuestions;
	}

	public void setJustificationQuestions(String justificationQuestions) {
		this.justificationQuestions = justificationQuestions;
	}

	public String getOperationOwner() {
		return operationOwner;
	}

	public void setOperationOwner(String operationOwner) {
		this.operationOwner = operationOwner;
	}

	public String getBidMgr() {
		return bidMgr;
	}

	public void setBidMgr(String bidMgr) {
		this.bidMgr = bidMgr;
	}

	public String getRemediationUser() {
		return remediationUser;
	}

	public void setRemediationUser(String remediationUser) {
		this.remediationUser = remediationUser;
	}

	public String getT3Name() {
		return t3Name;
	}

	public void setT3Name(String t3Name) {
		this.t3Name = t3Name;
	}

	public String getT4Name() {
		return t4Name;
	}

	public void setT4Name(String t4Name) {
		this.t4Name = t4Name;
	}

	public String getT5Name() {
		return t5Name;
	}

	public void setT5Name(String t5Name) {
		this.t5Name = t5Name;
	}

	public String getBpmT1Fin() {
		return bpmT1Fin;
	}

	public void setBpmT1Fin(String bpmT1Fin) {
		this.bpmT1Fin = bpmT1Fin;
	}

	public String getBpmT1Serv() {
		return bpmT1Serv;
	}

	public void setBpmT1Serv(String bpmT1Serv) {
		this.bpmT1Serv = bpmT1Serv;
	}

	public String getBpmT1Ins() {
		return bpmT1Ins;
	}

	public void setBpmT1Ins(String bpmT1Ins) {
		this.bpmT1Ins = bpmT1Ins;
	}

	public String getBpmT1Profit() {
		return bpmT1Profit;
	}

	public void setBpmT1Profit(String bpmT1Profit) {
		this.bpmT1Profit = bpmT1Profit;
	}

	public String getBpmT2Fin() {
		return bpmT2Fin;
	}

	public void setBpmT2Fin(String bpmT2Fin) {
		this.bpmT2Fin = bpmT2Fin;
	}

	public String getBpmT2Serv() {
		return bpmT2Serv;
	}

	public void setBpmT2Serv(String bpmT2Serv) {
		this.bpmT2Serv = bpmT2Serv;
	}

	public String getBpmT2Ins() {
		return bpmT2Ins;
	}

	public void setBpmT2Ins(String bpmT2Ins) {
		this.bpmT2Ins = bpmT2Ins;
	}

	public String getBpmT2Profit() {
		return bpmT2Profit;
	}

	public void setBpmT2Profit(String bpmT2Profit) {
		this.bpmT2Profit = bpmT2Profit;
	}

	public String getBpmOther() {
		return bpmOther;
	}

	public void setBpmOther(String bpmOther) {
		this.bpmOther = bpmOther;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getChannelProgram() {
		return channelProgram;
	}

	public void setChannelProgram(String channelProgram) {
		this.channelProgram = channelProgram;
	}

	public String getActionOwner() {
		return actionOwner;
	}

	public void setActionOwner(String actionOwner) {
		this.actionOwner = actionOwner;
	}

	public String getClosed() {
		return closed;
	}

	public void setClosed(String closed) {
		this.closed = closed;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getOutcomeComment() {
		return outcomeComment;
	}

	public void setOutcomeComment(String outcomeComment) {
		this.outcomeComment = outcomeComment;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getTransparencyLetter() {
		return transparencyLetter;
	}

	public void setTransparencyLetter(String transparencyLetter) {
		this.transparencyLetter = transparencyLetter;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getExceptionFlag() {
		return exceptionFlag;
	}

	public void setExceptionFlag(String exceptionFlag) {
		this.exceptionFlag = exceptionFlag;
	}

	public String getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(String fulfilled) {
		this.fulfilled = fulfilled;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBidExpired() {
		return bidExpired;
	}

	public void setBidExpired(String bidExpired) {
		this.bidExpired = bidExpired;
	}

	public String getExPreFulfillment() {
		return exPreFulfillment;
	}

	public void setExPreFulfillment(String exPreFulfillment) {
		this.exPreFulfillment = exPreFulfillment;
	}

	public String getPostVerifyDate() {
		return postVerifyDate;
	}

	public void setPostVerifyDate(String postVerifyDate) {
		this.postVerifyDate = postVerifyDate;
	}

	public String getPostShipAuditFollowUpStatus() {
		return postShipAuditFollowUpStatus;
	}

	public void setPostShipAuditFollowUpStatus(String postShipAuditFollowUpStatus) {
		this.postShipAuditFollowUpStatus = postShipAuditFollowUpStatus;
	}

	public String getWaitingForExtension() {
		return waitingForExtension;
	}

	public void setWaitingForExtension(String waitingForExtension) {
		this.waitingForExtension = waitingForExtension;
	}

	public String getProblemsWithChasing() {
		return problemsWithChasing;
	}

	public void setProblemsWithChasing(String problemsWithChasing) {
		this.problemsWithChasing = problemsWithChasing;
	}

	public String getRequestSentTooEarly() {
		return requestSentTooEarly;
	}

	public void setRequestSentTooEarly(String requestSentTooEarly) {
		this.requestSentTooEarly = requestSentTooEarly;
	}

	public String getRealisticallyDue() {
		return realisticallyDue;
	}

	public void setRealisticallyDue(String realisticallyDue) {
		this.realisticallyDue = realisticallyDue;
	}

	public String getTestNoteStartedYet() {
		return testNoteStartedYet;
	}

	public void setTestNoteStartedYet(String testNoteStartedYet) {
		this.testNoteStartedYet = testNoteStartedYet;
	}

	public String getPotentialIssues() {
		return potentialIssues;
	}

	public void setPotentialIssues(String potentialIssues) {
		this.potentialIssues = potentialIssues;
	}

	public String getFollowUpLog() {
		return followUpLog;
	}

	public void setFollowUpLog(String followUpLog) {
		this.followUpLog = followUpLog;
	}

	public String getDeadlineOfRequest() {
		return deadlineOfRequest;
	}

	public void setDeadlineOfRequest(String dealineOfRequest) {
		this.deadlineOfRequest = dealineOfRequest;
	}

	public String getBidEvent() {
		return bidEvent;
	}

	public void setBidEvent(String bidEvent) {
		this.bidEvent = bidEvent;
	}
	
	public String getOutcome() {
		return outcome;
	}

	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	
}
