package com.PropertiesFiles;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.Assert;

/** 
* [added-Belle Pondiong]<br>
* This page class contained Property File handlings. <br>
*/
public class BWPropertiesFiles {

	public void PropertyFiles() {
		// to initialize the object
		// methods inside the object will be use in other class
	} 
	
	protected static Properties propFile = new Properties();
	protected InputStream cred = null,  // credentials
                          bretURL = null,  //url
                          bidsrch = null, // bid search
                		  bretWeb = null; // general
	
	protected String propfilePath = "/home/jalberto/data/projects/bret/selenium/properties";
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine to read the URL  from the property files.<br>
	 * Returns the found details.<br>
	 */
	public String propBW_URL() {

		 String webURL = null ;
		 
	try {

		bretURL =  BWPropertiesFiles.class.getClassLoader().getResourceAsStream("BRETWEB_url.properties");
		//bretURL = new FileInputStream(propfilePath+"/BRETWEB_url.properties");

		// load a properties file
		propFile.load(bretURL);

		// get the property value and stored in array
		webURL  = propFile.getProperty("bretURL").trim();
				
	} catch (IOException ex) {    // exception if there is failure in reading/loading the file
        Assert.fail("Got an exception in the property file!");
		ex.printStackTrace();
	} finally {				      // this will make sure that Inputsteam will be close
		if (bretURL != null) {
			try {
				bretURL.close();
			} catch (IOException e) {
				e.printStackTrace();
			} }
		}
	return webURL;
	
	}
	
	/** 
	 * [added-Belle Pondiong]<br>cred
	 * Routine to read the credentails from the property files.<br>
	 * Returns the found details.<br>
	 * usrRoles == "Any" , if test case is using credentials not role specific.<br>
	 * usrRoles == "Administrator", if test case is using credentials with Administrator roles.<br>
	 * usrRoles == "LeadReviewer", if test case is using credentials with Lead Reviewer roles.<br>
	 * usrRoles == "Reviewer", if test case is using credentials with Reviewer roles.<br>
	 */
	public String[]  propBW_credentials(String usrRoles) {

		 String[] bretWebCredentials = new String[2] ;
		 
	try {

		 cred =  BWPropertiesFiles.class.getClassLoader().getResourceAsStream("BRETWEB_cred.properties");
		//cred = new FileInputStream(propfilePath+"/BRETWEB_cred.properties");

		// load a properties file
		propFile.load(cred);

		if (usrRoles == "Any") {
		// get the property value and stored in array
		bretWebCredentials [0]  = propFile.getProperty("anyRoleUsername").trim();
		bretWebCredentials [1] = propFile.getProperty("anyRolePassword").trim();
		}
		
		if (usrRoles == "Administrator") { // Administrator Role
		// get the property value and stored in array
		bretWebCredentials [0]  = propFile.getProperty("adminUsername").trim();
		bretWebCredentials [1] = propFile.getProperty("adminPassword").trim();
		}
		
		if (usrRoles == "LeadReviewer") {  // lead reviewer Role
		// get the property value and stored in array
		bretWebCredentials [0]  = propFile.getProperty("leadReviewerUsername").trim();
		bretWebCredentials [1] = propFile.getProperty("leadReviewerPassword").trim();
		}
		
		if (usrRoles == "Reviewer") {  // lead reviewer Role
		// get the property value and stored in array
		bretWebCredentials [0]  = propFile.getProperty("reviewerUsername").trim();
		bretWebCredentials [1] = propFile.getProperty("reviewerPassword").trim();
		}
				
	} catch (IOException ex) {    // exception if there is failure in reading/loading the file
        Assert.fail("Got an exception in the property file!");
		ex.printStackTrace();
	} finally {				      // this will make sure that Inputsteam will be close
		if (cred != null) {
			try {
				cred.close();
			} catch (IOException e) {
				e.printStackTrace();
			} }
		}
	return bretWebCredentials;
	}
	
	public String  propBW_screenShotPath() {
		String path = null; 
		try {
			bretWeb =  BWPropertiesFiles.class.getClassLoader().getResourceAsStream("BRETWEB.properties");
			propFile.load(bretWeb);
			path = propFile.getProperty("screenShotPath");
		} catch (IOException ex) {    // exception if there is failure in reading/loading the file
			Assert.fail("Got an exception in the property file!");
			ex.printStackTrace();
		} finally {				      // this will make sure that Inputsteam will be close
			if (bretWeb != null) {
				try {
					bretWeb.close();
				} catch (IOException e) {
					e.printStackTrace();
				} }
		}
		return path;
	}


}
